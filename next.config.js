/** @type {import('next').NextConfig} */
const nextConfig = {
  output: 'standalone',
  reactStrictMode: true,
  productionBrowserSourceMaps: false,
  webpack: (config, { isServer }) => {
    config.resolve.extensionAlias = {
      '.js': ['.js', '.ts'],
      '.jsx': ['.jsx', '.tsx']
    };

    if (!isServer) {
      config.resolve.fallback = {
        fs: false
      };
    }

    return config;
  },
  images: {
    unoptimized: true,
    domains: []
  },
  typescript: {
    ignoreBuildErrors: true
  }
};

export default nextConfig;
