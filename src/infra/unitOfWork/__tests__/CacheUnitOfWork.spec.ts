import type { GatewayError } from '@/adapters/gateways/GatewayError.js';
import type { InTransactionRepository } from '@/applications/unitOfWork/InTransactionRepository.js';
import type { UnitOfWorkError } from '@/applications/unitOfWork/UnitOfWorkError.js';
import type { GenericError } from '@/lib/Error/Errors.js';
import type { ILogger } from '@/lib/Logger/ILogger.js';
import { MockLogger } from '@/lib/Logger/mocks/MockLogger.js';
import { UnimplementedError } from '@jamashita/anden/error';
import { Kind } from '@jamashita/anden/type';
import { Delay } from '@jamashita/steckdose/delay';
import { type Either, isLeft, isRight } from 'fp-ts/lib/Either.js';
import { type Option, none } from 'fp-ts/lib/Option.js';
import { right } from 'fp-ts/lib/TaskEither.js';
import { CacheUnitOfWork } from '../CacheUnitOfWork.js';

type DummyEntity = Readonly<{
  id: string;
}>;

class DummyRepository implements InTransactionRepository<string, DummyEntity> {
  public create(): Promise<Either<GenericError | GatewayError, unknown>> {
    throw new UnimplementedError();
  }

  public delete(): Promise<Either<GenericError | GatewayError, unknown>> {
    throw new UnimplementedError();
  }

  public find(): Promise<Either<GenericError | GatewayError, Option<DummyEntity>>> {
    throw new UnimplementedError();
  }

  public update(): Promise<Either<GenericError | GatewayError, unknown>> {
    throw new UnimplementedError();
  }
}

describe('CacheUnitOfWork', () => {
  let logger: ILogger;
  let repository: InTransactionRepository<string, DummyEntity>;

  beforeEach(() => {
    logger = new MockLogger();
    repository = new DummyRepository();
  });

  afterEach(() => {
    vi.clearAllMocks();
  });

  describe('commit', () => {
    it('should return UnitOfWorkError when the transaction is not started', async () => {
      const uow = new CacheUnitOfWork(logger);

      const either = await uow.commit();

      expect(isLeft(either)).toBe(true);

      if (isRight(either)) {
        throw new Error('This should not happen');
      }

      expect(Kind.isObject(either.left)).toBe(true);

      if (Kind.isObject<UnitOfWorkError>(either.left)) {
        expect(either.left.error).toBe('UnitOfWorkError');
      }
    });

    it('should return UnitOfWorkError when repository is not registered', async () => {
      const uow = new CacheUnitOfWork(logger);

      await uow.startTransaction();

      await uow.create('dummy', { id: 'dummy' });
      await uow.update('dummy', { id: 'dummy' });
      await uow.delete('dummy', { id: 'dummy' });

      const either = await uow.commit();

      expect(isLeft(either)).toBe(true);

      if (isRight(either)) {
        throw new Error('This should not happen');
      }

      expect(Kind.isObject(either.left)).toBe(true);

      if (Kind.isObject<UnitOfWorkError>(either.left)) {
        expect(either.left.error).toBe('UnitOfWorkError');
      }
    });

    it('should return UnitOfWorkError when repository is not found', async () => {
      const uow = new CacheUnitOfWork(logger);

      await uow.startTransaction();

      uow.registerRepository('dummy', repository);

      await uow.create('dummy', { id: 'dummy' });
      await uow.update('dummy', { id: 'dummy' });
      await uow.delete('dummy1', { id: 'dummy' });

      const either = await uow.commit();

      expect(isLeft(either)).toBe(true);

      if (isRight(either)) {
        throw new Error('This should not happen');
      }

      expect(Kind.isObject(either.left)).toBe(true);

      if (Kind.isObject<UnitOfWorkError>(either.left)) {
        expect(either.left.error).toBe('UnitOfWorkError');
      }
    });

    it('should invoke repository methods when commit is called', async () => {
      const uow = new CacheUnitOfWork(logger);

      await uow.startTransaction();

      uow.registerRepository('dummy', repository);

      const spy1 = vi.spyOn(repository, 'create').mockImplementation(() => right(null)());
      const spy2 = vi.spyOn(repository, 'update').mockImplementation(() => right(null)());
      const spy3 = vi.spyOn(repository, 'delete').mockImplementation(() => right(null)());

      uow.create('dummy', { id: 'dummy' });
      uow.update('dummy', { id: 'dummy' });
      uow.delete('dummy', 'dummy');

      expect(spy1).not.toHaveBeenCalled();
      expect(spy2).not.toHaveBeenCalled();
      expect(spy3).not.toHaveBeenCalled();

      await uow.commit();

      expect(spy1).toHaveBeenCalledOnce();
      expect(spy2).toHaveBeenCalledOnce();
      expect(spy3).toHaveBeenCalledOnce();
    });

    it('should process multiple transactions sequentially', async () => {
      const uow = new CacheUnitOfWork(logger);
      const results: Array<string> = [];

      const tx1 = new Promise<void>((resolve) => {
        uow.startTransaction().then(async () => {
          results.push('tx1 start');
          await Delay.wait(500);
          uow.commit();
          results.push('tx1 end');
          resolve();
        });
      });
      const tx2 = new Promise<void>((resolve) => {
        uow.startTransaction().then(async () => {
          results.push('tx2 start');
          uow.commit();
          results.push('tx2 end');
          resolve();
        });
      });

      await Promise.all([tx1, tx2]);

      logger.info(results);

      expect(results[0]).toBe('tx1 start');
      expect(results[1]).toBe('tx1 end');
      expect(results[2]).toBe('tx2 start');
      expect(results[3]).toBe('tx2 end');
    });
  });

  describe('create', () => {
    it('should return UnitOfWorkError when the transaction is not started', async () => {
      const uow = new CacheUnitOfWork(logger);

      const either = await uow.create('dummy', { id: 'dummy' });

      expect(isLeft(either)).toBe(true);

      if (isRight(either)) {
        throw new Error('This should not happen');
      }

      expect(either.left.error).toBe('UnitOfWorkError');
    });

    it('should not invoke repository.create when commit is not called', async () => {
      const uow = new CacheUnitOfWork(logger);

      await uow.startTransaction();

      uow.registerRepository('dummy', repository);

      const spy = vi.spyOn(repository, 'create').mockImplementation(() => right(null)());

      await uow.create('dummy', { id: 'dummy' });

      expect(spy).not.toHaveBeenCalled();
    });

    it('should invoke repository.create when commit is called', async () => {
      const uow = new CacheUnitOfWork(logger);

      await uow.startTransaction();

      uow.registerRepository('dummy', repository);

      const spy = vi.spyOn(repository, 'create').mockImplementation(() => right(null)());

      await uow.create('dummy', { id: 'dummy' });

      expect(spy).not.toHaveBeenCalled();

      await uow.commit();

      expect(spy).toHaveBeenCalledOnce();
    });
  });

  describe('delete', () => {
    it('should return UnitOfWorkError when the transaction is not started', async () => {
      const uow = new CacheUnitOfWork(logger);

      const either = await uow.delete('dummy', { id: 'dummy' });

      expect(isLeft(either)).toBe(true);

      if (isRight(either)) {
        throw new Error('This should not happen');
      }

      expect(either.left.error).toBe('UnitOfWorkError');
    });

    it('should not invoke repository.delete when commit is not called', async () => {
      const uow = new CacheUnitOfWork(logger);

      await uow.startTransaction();

      uow.registerRepository('dummy', repository);

      const spy = vi.spyOn(repository, 'delete').mockImplementation(() => right(null)());

      await uow.delete('dummy', 'dummy');

      expect(spy).not.toHaveBeenCalled();
    });

    it('should invoke repository.delete when commit is called', async () => {
      const uow = new CacheUnitOfWork(logger);

      await uow.startTransaction();

      uow.registerRepository('dummy', repository);

      const spy = vi.spyOn(repository, 'delete').mockImplementation(() => right(null)());

      await uow.delete('dummy', 'dummy');

      expect(spy).not.toHaveBeenCalled();

      await uow.commit();

      expect(spy).toHaveBeenCalledOnce();
    });
  });

  describe('find', () => {
    it('should return UnitOfWorkError when the transaction is not started', async () => {
      const uow = new CacheUnitOfWork(logger);

      const either = await uow.find('dummy', 'dummy');

      expect(isLeft(either)).toBe(true);

      if (isRight(either)) {
        throw new Error('This should not happen');
      }

      expect(either.left.error).toBe('UnitOfWorkError');
    });

    it('should return UnitOfWorkError when repository is not registered', async () => {
      const uow = new CacheUnitOfWork(logger);

      await uow.startTransaction();

      const either = await uow.find('dummy', 'dummy');

      expect(isLeft(either)).toBe(true);

      if (isRight(either)) {
        throw new Error('This should not happen');
      }

      expect(either.left.error).toBe('UnitOfWorkError');
    });

    it('should return UnitOfWorkError when repository is not found', async () => {
      const uow = new CacheUnitOfWork(logger);

      await uow.startTransaction();

      uow.registerRepository('dummy', repository);

      const either = await uow.find('dummy1', 'dummy');

      expect(isLeft(either)).toBe(true);

      if (isRight(either)) {
        throw new Error('This should not happen');
      }

      expect(either.left.error).toBe('UnitOfWorkError');
    });

    it('should invoke repository.find when commit is called', async () => {
      const uow = new CacheUnitOfWork(logger);

      await uow.startTransaction();

      uow.registerRepository('dummy', repository);

      const spy = vi.spyOn(repository, 'find').mockImplementation(() => right(none)());

      await uow.find('dummy', 'dummy');

      expect(spy).toHaveBeenCalledOnce();

      await uow.commit();

      expect(spy).toHaveBeenCalledOnce();
    });
  });

  describe('rollback', () => {
    it('should return UnitOfWorkError when the transaction is not started', async () => {
      const uow = new CacheUnitOfWork(logger);

      const either = await uow.rollback();

      expect(isLeft(either)).toBe(true);

      if (isRight(either)) {
        throw new Error('This should not happen');
      }

      expect(either.left.error).toBe('UnitOfWorkError');
    });

    it('should not invoke repository methods when rollback is called', async () => {
      const uow = new CacheUnitOfWork(logger);

      await uow.startTransaction();

      uow.registerRepository('dummy', repository);

      const spy1 = vi.spyOn(repository, 'create').mockImplementation(() => right(null)());
      const spy2 = vi.spyOn(repository, 'update').mockImplementation(() => right(null)());
      const spy3 = vi.spyOn(repository, 'delete').mockImplementation(() => right(null)());

      uow.create('dummy', { id: 'dummy' });
      uow.update('dummy', { id: 'dummy' });
      uow.delete('dummy', 'dummy');

      await uow.rollback();

      expect(spy1).not.toHaveBeenCalled();
      expect(spy2).not.toHaveBeenCalled();
      expect(spy3).not.toHaveBeenCalled();
    });

    it('should process multiple transactions sequentially', async () => {
      const uow = new CacheUnitOfWork(logger);
      const results: Array<string> = [];

      const tx1 = new Promise<void>((resolve) => {
        uow.startTransaction().then(async () => {
          results.push('tx1 start');
          await Delay.wait(500);
          uow.rollback();
          results.push('tx1 end');
          resolve();
        });
      });
      const tx2 = new Promise<void>((resolve) => {
        uow.startTransaction().then(async () => {
          results.push('tx2 start');
          uow.rollback();
          results.push('tx2 end');
          resolve();
        });
      });

      await Promise.all([tx1, tx2]);

      logger.info(results);

      expect(results[0]).toBe('tx1 start');
      expect(results[1]).toBe('tx1 end');
      expect(results[2]).toBe('tx2 start');
      expect(results[3]).toBe('tx2 end');
    });
  });

  describe('startTransaction', () => {
    it('should successfully start a transaction', async () => {
      const uow = new CacheUnitOfWork(logger);

      const either = await uow.startTransaction();

      expect(isRight(either)).toBe(true);

      await uow.rollback();
    });
  });

  describe('update', () => {
    it('should return UnitOfWorkError when the transaction is not started', async () => {
      const uow = new CacheUnitOfWork(logger);

      const either = await uow.update('dummy', { id: 'dummy' });

      expect(isLeft(either)).toBe(true);

      if (isRight(either)) {
        throw new Error('This should not happen');
      }

      expect(either.left.error).toBe('UnitOfWorkError');
    });

    it('should not invoke repository.update when commit is not called', async () => {
      const uow = new CacheUnitOfWork(logger);

      await uow.startTransaction();

      uow.registerRepository('dummy', repository);

      const spy = vi.spyOn(repository, 'update').mockImplementation(() => right(null)());

      await uow.update('dummy', { id: 'dummy' });

      expect(spy).not.toHaveBeenCalled();
    });

    it('should invoke repository.update when commit is called', async () => {
      const uow = new CacheUnitOfWork(logger);

      await uow.startTransaction();

      uow.registerRepository('dummy', repository);

      const spy = vi.spyOn(repository, 'update').mockImplementation(() => right(null)());

      await uow.update('dummy', { id: 'dummy' });

      expect(spy).not.toHaveBeenCalled();

      await uow.commit();

      expect(spy).toHaveBeenCalledOnce();
    });
  });
});
