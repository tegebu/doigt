import type { IUnitOfWork, RepositoryTypeMap } from '@/applications/unitOfWork/IUnitOfWork.js';
import type { IUnitOfWorkFactory } from '@/applications/unitOfWork/IUnitOfWorkFactory.js';
import { Types } from '@/containers/Types.js';
import type { ILogger } from '@/lib/Logger/ILogger.js';
import { inject, injectable } from 'inversify';
import { CacheUnitOfWork } from './CacheUnitOfWork.js';

@injectable()
export class CacheUnitOfWorkFactory<T extends RepositoryTypeMap> implements IUnitOfWorkFactory<T> {
  private readonly logger: ILogger;

  constructor(@inject(Types.Logger) logger: ILogger) {
    this.logger = logger;
  }

  public forge(): IUnitOfWork<T> {
    return new CacheUnitOfWork<T>(this.logger);
  }
}
