import type { GatewayError } from '@/adapters/gateways/GatewayError.js';
import type { InTransactionRepository } from '@/applications/unitOfWork/InTransactionRepository.js';
import type { IUnitOfWork, RepositoryTypeMap } from '@/applications/unitOfWork/IUnitOfWork.js';
import { createUnitOfWorkError, type UnitOfWorkError } from '@/applications/unitOfWork/UnitOfWorkError.js';
import { Types } from '@/containers/Types.js';
import type { GenericError } from '@/lib/Error/Errors.js';
import type { ILogger } from '@/lib/Logger/ILogger.js';
import { Kind } from '@jamashita/anden/type';
import { Mutex } from 'async-mutex';
import type { Either } from 'fp-ts/lib/Either.js';
import { pipe } from 'fp-ts/lib/function.js';
import type { Option } from 'fp-ts/lib/Option.js';
import { flatMap, left, mapError, right, sequenceArray, type TaskEither } from 'fp-ts/lib/TaskEither.js';
import { inject, injectable } from 'inversify';

@injectable()
export class CacheUnitOfWork<T extends RepositoryTypeMap> implements IUnitOfWork<T> {
  private readonly mutex: Mutex;
  private readonly repositories: Map<keyof T, InTransactionRepository<T[keyof T]['id'], T[keyof T]['entity']>>;
  private pendingCreates: Map<keyof T, Map<T[keyof T]['id'], T[keyof T]['entity']>>;
  private pendingUpdates: Map<keyof T, Map<T[keyof T]['id'], T[keyof T]['entity']>>;
  private pendingDeletes: Map<keyof T, Set<T[keyof T]['id']>>;
  private readonly logger: ILogger;

  public constructor(@inject(Types.Logger) logger: ILogger) {
    this.mutex = new Mutex();
    this.repositories = new Map();
    this.pendingCreates = new Map();
    this.pendingUpdates = new Map();
    this.pendingDeletes = new Map();
    this.logger = logger;
  }

  private bulkCreate(): TaskEither<GenericError | UnitOfWorkError | GatewayError, unknown> {
    const ret: Array<TaskEither<GenericError | GatewayError, unknown>> = [];

    for (const [repoKey, creates] of this.pendingCreates) {
      const repository = this.repositories.get(repoKey);

      if (Kind.isUndefined(repository)) {
        return left(createUnitOfWorkError('Create', `リポジトリが見つかりません: ${String(repoKey)}`));
      }

      for (const [, entity] of creates) {
        ret.push(() => repository.create(entity));
      }
    }

    return sequenceArray(ret);
  }

  private bulkDelete(): TaskEither<GenericError | UnitOfWorkError | GatewayError, unknown> {
    const ret: Array<TaskEither<GenericError | GatewayError, unknown>> = [];

    for (const [repoKey, deletes] of this.pendingDeletes) {
      const repository = this.repositories.get(repoKey);

      if (Kind.isUndefined(repository)) {
        return left({
          error: 'UnitOfWorkError',
          detail: 'Delete',
          message: `りポジトリが見つかりません: ${String(repoKey)}`
        });
      }

      for (const id of deletes.values()) {
        ret.push(() => repository.delete(id));
      }
    }

    return sequenceArray(ret);
  }

  private bulkUpdate(): TaskEither<GenericError | UnitOfWorkError | GatewayError, unknown> {
    const ret: Array<TaskEither<GenericError | GatewayError, unknown>> = [];

    for (const [repoKey, updates] of this.pendingUpdates) {
      const repository = this.repositories.get(repoKey);

      if (Kind.isUndefined(repository)) {
        return left({
          error: 'UnitOfWorkError',
          detail: 'Update',
          message: `リポジトリが見つかりません: ${String(repoKey)}`
        });
      }

      for (const [, entity] of updates) {
        ret.push(() => repository.update(entity));
      }
    }

    return sequenceArray(ret);
  }

  public commit(): Promise<Either<GenericError | UnitOfWorkError | GatewayError, unknown>> {
    this.logger.info('CacheUnitOfWork.commit()');

    if (!this.mutex.isLocked()) {
      return left(createUnitOfWorkError('Commit', 'トランザクションが開始されていません。'))();
    }

    // すべてのkeyはrepositoryのkeyとして存在するか
    const keys = new Set([...this.pendingCreates.keys(), ...this.pendingUpdates.keys(), ...this.pendingDeletes.keys()]);

    const notFoundKey = [...keys].find((key) => {
      return !this.repositories.has(key);
    });

    if (Kind.isString(notFoundKey)) {
      return left(createUnitOfWorkError('Commit', `リポジトリが見つかりません: ${notFoundKey}`))();
    }

    return pipe(
      sequenceArray([this.bulkCreate(), this.bulkUpdate(), this.bulkDelete()]),
      flatMap(() => {
        this.pendingCreates = new Map();
        this.pendingUpdates = new Map();
        this.pendingDeletes = new Map();

        this.mutex.release();

        return right(null);
      }),
      mapError((e) => {
        this.logger.error(e);

        this.pendingCreates = new Map();
        this.pendingUpdates = new Map();
        this.pendingDeletes = new Map();

        this.mutex.release();

        return e;
      })
    )();
  }

  public create<K extends keyof T>(key: K, entity: T[K]['entity']): Promise<Either<GenericError | UnitOfWorkError, unknown>> {
    this.logger.info('CacheUnitOfWork.create()');

    if (!this.mutex.isLocked()) {
      return left(createUnitOfWorkError('Create', 'トランザクションが開始されていません。'))();
    }

    const dic = this.pendingCreates.get(key);

    if (Kind.isUndefined(dic)) {
      const newDic = new Map<T[K]['id'], T[K]['entity']>();

      newDic.set(entity.id, entity);
      this.pendingCreates.set(key, newDic);

      return right(null)();
    }

    dic.set(entity.id, entity);

    return right(null)();
  }

  public delete<K extends keyof T>(key: K, id: T[K]['id']): Promise<Either<GenericError | UnitOfWorkError, unknown>> {
    this.logger.info('CacheUnitOfWork.delete()');

    if (!this.mutex.isLocked()) {
      return left(createUnitOfWorkError('Delete', 'トランザクションが開始されていません。'))();
    }

    const add = this.pendingDeletes.get(key);

    if (Kind.isUndefined(add)) {
      const newAdd = new Set<T[K]['id']>();

      newAdd.add(id);
      this.pendingDeletes.set(key, newAdd);

      return right(null)();
    }

    add.add(id);

    return right(null)();
  }

  public find<K extends keyof T>(key: K, id: T[K]['id']): Promise<Either<GenericError | UnitOfWorkError | GatewayError, Option<T[K]['entity']>>> {
    this.logger.info('CacheUnitOfWork.find()');

    if (!this.mutex.isLocked()) {
      return left(createUnitOfWorkError('Find', 'トランザクションが開始されていません。'))();
    }

    const repository = this.repositories.get(key);

    if (Kind.isUndefined(repository)) {
      return left(createUnitOfWorkError('Find', `リポジトリが見つかりません: ${String(key)}`))();
    }

    return repository.find(id) as Promise<Either<GenericError | GatewayError, Option<T[K]['entity']>>>;
  }

  public isLocked(): boolean {
    return this.mutex.isLocked();
  }

  public registerRepository<K extends keyof T>(key: K, repository: InTransactionRepository<T[K]['id'], T[K]['entity']>): void {
    this.logger.info('CacheUnitOfWork.registerRepository()');

    this.repositories.set(key, repository);
  }

  public rollback(): Promise<Either<GenericError | UnitOfWorkError | GatewayError, unknown>> {
    this.logger.info('CacheUnitOfWork.rollback()');

    if (!this.mutex.isLocked()) {
      return left(createUnitOfWorkError('Rollback', 'トランザクションが開始されていません。'))();
    }

    this.pendingCreates = new Map();
    this.pendingUpdates = new Map();
    this.pendingDeletes = new Map();

    this.mutex.release();

    return right(null)();
  }

  public async startTransaction(): Promise<Either<GenericError | UnitOfWorkError, unknown>> {
    this.logger.info('CacheUnitOfWork.startTransaction()');

    await this.mutex.acquire();

    this.pendingCreates = new Map();
    this.pendingUpdates = new Map();
    this.pendingDeletes = new Map();

    return right(null)();
  }

  public update<K extends keyof T>(key: K, entity: T[K]['entity']): Promise<Either<GenericError | UnitOfWorkError, unknown>> {
    this.logger.info('CacheUnitOfWork.update()');

    if (!this.mutex.isLocked()) {
      return left(createUnitOfWorkError('Update', 'トランザクションが開始されていません。'))();
    }

    const dic = this.pendingUpdates.get(key);

    if (Kind.isUndefined(dic)) {
      const newDic = new Map<T[K]['id'], T[K]['entity']>();

      newDic.set(entity.id, entity);
      this.pendingUpdates.set(key, newDic);

      return right(null)();
    }

    dic.set(entity.id, entity);

    return right(null)();
  }
}
