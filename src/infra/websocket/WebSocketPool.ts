import type { IConnectionPool } from '@/applications/connection/IConnectionPool.js';
import { Types } from '@/containers/Types.js';
import type { ClientID } from '@/domains/Client/Client.js';
import type { ILogger } from '@/lib/Logger/ILogger.js';
import { Kind } from '@jamashita/anden/type';
import { Zeit } from '@jamashita/anden/zeit';
import { type Option, fromNullable, map } from 'fp-ts/lib/Option.js';
import { pipe } from 'fp-ts/lib/function.js';
import { inject, injectable } from 'inversify';
import { WebSocket } from 'ws';

type ActiveConnection = Readonly<{
  status: 'CONNECTED';
  connection: WebSocket;
}>;
type InactiveConnection = Readonly<{
  status: 'DISCONNECTED';
  connection: WebSocket;
  disconnectedAt: Zeit;
}>;
type ConnectionEntry = ActiveConnection | InactiveConnection;

// 再接続のタイムアウト, 300s
const RECONNECTION_TIMEOUT = 300;

@injectable()
export class WebSocketPool implements IConnectionPool<WebSocket> {
  private readonly connectionsByClient: Map<ClientID, ConnectionEntry>;
  private readonly clientsByConnection: Map<WebSocket, ClientID>;
  private readonly disconnectionTimer: Map<ClientID, NodeJS.Timeout>;
  private readonly logger: ILogger;

  public constructor(@inject(Types.Logger) logger: ILogger) {
    this.connectionsByClient = new Map();
    this.clientsByConnection = new Map();
    this.disconnectionTimer = new Map();
    this.logger = logger;
  }

  // TODO 定期的にできるとよい
  private cleanup(id: ClientID, connection: WebSocket): void {
    this.clientsByConnection.delete(connection);
    this.connectionsByClient.delete(id);

    const timer = this.disconnectionTimer.get(id);

    if (!Kind.isUndefined(timer)) {
      clearTimeout(timer);
      this.disconnectionTimer.delete(id);
    }
    if (connection.readyState === WebSocket.OPEN) {
      connection.close();
    }
  }

  public disconnect(id: ClientID): void {
    const entry = this.connectionsByClient.get(id);

    if (Kind.isUndefined(entry) || entry.status === 'DISCONNECTED') {
      return;
    }

    this.connectionsByClient.set(id, {
      status: 'DISCONNECTED',
      connection: entry.connection,
      disconnectedAt: Zeit.now()
    } satisfies ConnectionEntry);
    this.startReconnectionTimer(id);
  }

  public getAllActiveConnections(): Array<WebSocket> {
    return [...this.connectionsByClient.values()]
      .filter((entry) => {
        return entry.status === 'CONNECTED';
      })
      .map((entry) => {
        return entry.connection;
      });
  }

  public getClientID(connection: WebSocket): Option<ClientID> {
    return fromNullable(this.clientsByConnection.get(connection));
  }

  public getConnection(id: ClientID): Option<WebSocket> {
    this.logger.info('WebsocketPool.getConnection()');

    return pipe(
      this.connectionsByClient.get(id),
      fromNullable,
      map((option) => option.connection)
    );
  }

  public isDisconnected(id: ClientID): boolean {
    const entry = this.connectionsByClient.get(id);

    if (Kind.isUndefined(entry)) {
      return true;
    }

    return entry.status === 'DISCONNECTED';
  }

  public reconnect(id: ClientID, connection: WebSocket): void {
    const entry = this.connectionsByClient.get(id);

    if (Kind.isUndefined(entry)) {
      return;
    }
    if (entry.status === 'DISCONNECTED') {
      if (entry.connection.readyState === WebSocket.OPEN) {
        entry.connection.close();
      }

      this.clientsByConnection.delete(entry.connection);
      this.connectionsByClient.set(id, {
        status: 'CONNECTED',
        connection
      } satisfies ConnectionEntry);
      this.clientsByConnection.set(connection, id);

      const timer = this.disconnectionTimer.get(id);

      if (!Kind.isUndefined(timer)) {
        clearTimeout(timer);
        this.disconnectionTimer.delete(id);
      }
    }
  }

  public register(id: ClientID, connection: WebSocket): void {
    this.logger.info('WebsocketPool.register()');

    const status = {
      status: 'CONNECTED',
      connection
    } satisfies ConnectionEntry;

    this.connectionsByClient.set(id, status);
    this.clientsByConnection.set(connection, id);
  }

  private startReconnectionTimer(id: ClientID): void {
    const timeout = setTimeout(() => {
      const entry = this.connectionsByClient.get(id);

      if (Kind.isUndefined(entry)) {
        return;
      }
      if (entry.status === 'DISCONNECTED') {
        this.cleanup(id, entry.connection);
      }
    }, RECONNECTION_TIMEOUT * 1_000);

    this.disconnectionTimer.set(id, timeout);
  }

  public unregister(id: ClientID): void {
    this.logger.info('WebsocketPool.unregister()');

    const entry = this.connectionsByClient.get(id);

    if (!Kind.isUndefined(entry)) {
      this.cleanup(id, entry.connection);
    } else {
      const timer = this.disconnectionTimer.get(id);

      clearTimeout(timer);
      this.disconnectionTimer.delete(id);
    }
  }
}
