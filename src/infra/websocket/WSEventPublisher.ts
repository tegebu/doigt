import { createEventPublishError, type EventPublishError } from '@/adapters/presenters/EventPublisher/EventPublishError.js';
import type { IEventPublisher } from '@/adapters/presenters/EventPublisher/IEventPublisher.js';
import type { GetPublishEventData, PublishEventType } from '@/applications/websocket/PublishEvents.js';
import { Types } from '@/containers/Types.js';
import type { ClientID } from '@/domains/Client/Client.js';
import { type ClientError, createClientError } from '@/domains/Client/ClientError.js';
import { createUnknownError, type GenericError } from '@/lib/Error/Errors.js';
import type { ILogger } from '@/lib/Logger/ILogger.js';
import { Kind } from '@jamashita/anden/type';
import { JSONA } from '@jamashita/steckdose/json';
import { type Either, left } from 'fp-ts/lib/Either.js';
import { isNone, isSome, type Option, type Some } from 'fp-ts/lib/Option.js';
import { right, sequenceArray, type TaskEither, tryCatch } from 'fp-ts/lib/TaskEither.js';
import { inject, injectable } from 'inversify';
import { WebSocket } from 'ws';
import type { WebSocketPool } from './WebSocketPool.js';

@injectable()
export class WSEventPublisher implements IEventPublisher {
  private readonly pool: WebSocketPool;
  private readonly logger: ILogger;

  public constructor(@inject(Types.WebSocketPool) pool: WebSocketPool, @inject(Types.Logger) logger: ILogger) {
    this.pool = pool;
    this.logger = logger;
  }

  public async broadcast<T extends PublishEventType>(
    event: T,
    data: GetPublishEventData<T>
  ): Promise<Either<GenericError | EventPublishError, unknown>> {
    this.logger.info('WSEventPublisher.broadcast()');

    const str = await JSONA.stringify({
      type: event,
      data
    });

    const te = this.pool.getAllActiveConnections().map((ws) => {
      if (ws.readyState === WebSocket.OPEN) {
        return this.emit(ws, str);
      }

      return right(null);
    });

    return sequenceArray(te)();
  }

  private emit(ws: WebSocket, str: string): TaskEither<GenericError | EventPublishError, unknown> {
    return tryCatch(
      () =>
        new Promise<void>((resolve, reject) => {
          ws.send(str, (e) => {
            if (Kind.isNone(e)) {
              resolve();

              return;
            }

            reject(e);
          });
        }),
      (e: unknown) => {
        this.logger.error(e);

        if (e instanceof Error) {
          return createEventPublishError('EmitFailed', `送信に失敗しました。: ${e.message}`);
        }

        return createUnknownError('Unknown');
      }
    );
  }

  private async send<T extends PublishEventType>(
    ws: WebSocket,
    event: T,
    data: GetPublishEventData<T>
  ): Promise<Either<GenericError | EventPublishError, unknown>> {
    if (ws.readyState !== WebSocket.OPEN) {
      this.logger.error('CLIENT NOT CONNECTED');

      return left(createEventPublishError('ClientNotConnected', 'クライアントが接続されていません。'));
    }

    const str = await JSONA.stringify({
      type: event,
      data
    });

    return this.emit(ws, str)();
  }

  public async toClient<T extends PublishEventType>(
    id: ClientID,
    event: T,
    data: GetPublishEventData<T>
  ): Promise<Either<GenericError | EventPublishError | ClientError, unknown>> {
    this.logger.info('WSEventPublisher.toClient()');

    const option = this.pool.getConnection(id);

    if (isNone(option)) {
      this.logger.error(`NO SUCH CLIENT. CLIENT ID: ${id}`);

      return left(createClientError('NoSuchClient', `該当するクライアントが存在しません。: ${id}`));
    }

    return this.send(option.value, event, data);
  }

  public async toClients<T extends PublishEventType>(
    ids: ReadonlyArray<ClientID>,
    event: T,
    data: GetPublishEventData<T>
  ): Promise<Either<GenericError | EventPublishError | ClientError, unknown>> {
    this.logger.info('WSEventPublisher.toClients()');

    const te = ids
      .map((id) => {
        return this.pool.getConnection(id);
      })
      .filter((o: Option<WebSocket>) => {
        return isSome(o);
      })
      .map((o: Some<WebSocket>) => {
        return () => this.send(o.value, event, data);
      });

    return sequenceArray(te)();
  }
}
