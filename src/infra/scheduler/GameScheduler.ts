import type { IScheduler } from '@/applications/scheduler/IScheduler.js';
import { Types } from '@/containers/Types.js';
import type { RoomID } from '@/domains/Room/Room.js';
import type { ILogger } from '@/lib/Logger/ILogger.js';
import { Kind, type UnaryFunction } from '@jamashita/anden/type';
import type { Either } from 'fp-ts/lib/Either.js';
import { mapError } from 'fp-ts/lib/TaskEither.js';
import { pipe } from 'fp-ts/lib/function.js';
import { inject, injectable, postConstruct } from 'inversify';

@injectable()
export class GameScheduler implements IScheduler {
  private readonly timer: Map<RoomID, NodeJS.Timeout>;
  private readonly logger: ILogger;

  public constructor(@inject(Types.Logger) logger: ILogger) {
    this.timer = new Map();
    this.logger = logger;
  }

  private clearAll(): void {
    this.timer.forEach((_, roomID) => {
      this.stopPhase(roomID);
    });
  }

  public hasTimer(roomID: RoomID): boolean {
    return this.timer.has(roomID);
  }

  @postConstruct()
  public initialize(): void {
    // アプリケーション終了時のクリーンアップ
    process.on('SIGTERM', () => this.clearAll());
    process.on('SIGINT', () => this.clearAll());
  }

  public startPhase(roomID: RoomID, second: number, cb: UnaryFunction<RoomID, Promise<Either<unknown, unknown>>>): void {
    const timeout = setTimeout(async () => {
      await pipe(
        () => cb(roomID),
        mapError((e) => {
          this.logger.error(e);
        })
      )();
      this.timer.delete(roomID);
    }, second * 1_000);

    this.timer.set(roomID, timeout);
  }

  public stopPhase(roomID: RoomID): void {
    const timeout = this.timer.get(roomID);

    if (Kind.isNull(timeout)) {
      return;
    }

    clearTimeout(timeout);
  }
}
