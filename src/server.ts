import { baseController } from '@/adapters/controllers/BaseController.js';
import { SUBMIT } from '@/applications/websocket/SubmitEvents.js';
import { container } from '@/containers/Container.js';
import { Types } from '@/containers/Types.js';
import { Kind } from '@jamashita/anden/type';
import { JSONA } from '@jamashita/steckdose/json';
import Fastify from 'fastify';
import { isLeft } from 'fp-ts/lib/Either.js';
import next from 'next';
import type { Logger } from 'pino';
import { type RawData, type WebSocket, WebSocketServer } from 'ws';

const dev = process.env.NODE_ENV !== 'production';
const hostname = 'localhost';
const logger = container.get<Logger>(Types.Logger);

const fastify = Fastify({
  // loggerInstance: logger,
});

const nextApp = next({ dev });
const handle = nextApp.getRequestHandler();

try {
  await nextApp.prepare();
  const wss = new WebSocketServer({
    host: hostname,
    port: 3001
  });

  wss.on('connection', (ws: WebSocket) => {
    ws.on('open', () => {
      logger.info('opened');
    });

    ws.on('message', async (message: RawData) => {
      logger.info('message: %s', message);

      if (Buffer.isBuffer(message)) {
        try {
          const obj = await JSONA.parse(message.toString('utf-8'));

          baseController
            .emit({
              obj,
              ws
            })
            .then((either) => {
              if (isLeft(either)) {
                logger.fatal(either.left);
              }
            });
        } catch (e: unknown) {
          logger.error(e);
        }
      }
    });

    ws.on('close', () => {
      try {
        baseController
          .emit({
            obj: {
              type: SUBMIT.CONNECTION.DISCONNECT,
              data: {}
            },
            ws
          })
          .then((either) => {
            if (isLeft(either)) {
              logger.fatal(either.left);
            }
          });
      } catch (e: unknown) {
        logger.error(e);
      }
    });
  });

  fastify.all('*', async (req, res) => {
    await handle(req.raw, res.raw);
  });

  fastify.listen(
    {
      port: 3000,
      host: hostname
    },
    (e) => {
      if (!Kind.isNull(e)) {
        logger.error(e);
        process.exit(1);
      }
    }
  );
} catch (e: unknown) {
  logger.error(e);
  process.exit(1);
}
