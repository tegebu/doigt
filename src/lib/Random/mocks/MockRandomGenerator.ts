import { UnimplementedError } from '@jamashita/anden/error';
import type { IRandomGenerator } from '../interfaces/IRandomGenerator.js';

export class MockRandomGenerator implements IRandomGenerator {
  public integer(): number {
    throw new UnimplementedError();
  }
}
