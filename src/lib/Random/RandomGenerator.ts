import { Random } from '@jamashita/steckdose/random';
import type { IRandomGenerator } from './interfaces/IRandomGenerator.js';

export class RandomGenerator implements IRandomGenerator {
  public integer(min: number, max: number): number {
    return Random.integer(min, max);
  }
}
