export interface IRandomGenerator {
  integer(min: number, max: number): number;
}
