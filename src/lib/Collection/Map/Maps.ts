import type { Predicate } from '@jamashita/anden/type';

export namespace Maps {
  export const filter = <K, V>(map: ReadonlyMap<K, V>, predicate: Predicate<V>): Map<K, V> => {
    const newMap = new Map<K, V>();

    for (const [k, v] of map.entries()) {
      if (predicate(v)) {
        newMap.set(k, v);
      }
    }

    return newMap;
  };
}
