import { Maps } from '@/lib/Collection/Map/Maps.js';
import { describe } from 'vitest';

describe('Maps', () => {
  describe('filter', () => {
    it('should return the filtered Map', () => {
      const map = new Map([
        ['a', 1],
        ['b', 2],
        ['c', 3],
        ['d', 4]
      ]);

      const result = Maps.filter(map, (v) => v % 2 === 0);

      expect(result.size).toBe(2);
      expect(result.get('b')).toBe(2);
      expect(result.get('d')).toBe(4);
    });
  });
});
