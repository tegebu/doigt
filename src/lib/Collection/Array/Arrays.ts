import { Random } from '@jamashita/steckdose/random';

export namespace Arrays {
  export const chunk = <T>(arr: ReadonlyArray<T>, size: number): Array<Array<T>> => {
    return arr.reduce((acc: Array<Array<T>>, _, i: number) => {
      if (i % size === 0) {
        acc.push(arr.slice(i, i + size));
      }

      return acc;
    }, []);
  };

  export const remove = <T>(arr: ReadonlyArray<T>, index: number): Array<T> => {
    return arr.filter((_, i: number) => {
      return i !== index;
    });
  };

  export const shuffle = <T>(arr: ReadonlyArray<T>): Array<T> => {
    const arrayCopy = [...arr];

    for (let i = arrayCopy.length - 1; i > 0; i--) {
      const j = Random.integer(0, i);

      // @ts-ignore
      [arrayCopy[i], arrayCopy[j]] = [arrayCopy[j], arrayCopy[i]];
    }

    return arrayCopy;
  };
}
