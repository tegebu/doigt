import { describe } from 'vitest';
import { Arrays } from '../Arrays.js';

describe('Arrays', () => {
  describe('chunk', () => {
    it('returns chunked array', () => {
      const arr = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10];
      const chunked = Arrays.chunk(arr, 3);

      expect(chunked).toStrictEqual([[1, 2, 3], [4, 5, 6], [7, 8, 9], [10]]);
    });
  });

  describe('remove', () => {
    it('returns removed array', () => {
      const arr = [1, 2, 3, 4, 5];
      const removed = Arrays.remove(arr, 2);

      expect(removed).toStrictEqual([1, 2, 4, 5]);
    });
  });

  describe('shuffle', () => {
    it('returns shuffled array', () => {
      const arr = [1, 2, 3, 4, 5];
      const shuffled = Arrays.shuffle(arr);

      expect(arr.length).toBe(shuffled.length);
      expect(shuffled.includes(1)).toBe(true);
      expect(shuffled.includes(2)).toBe(true);
      expect(shuffled.includes(3)).toBe(true);
      expect(shuffled.includes(4)).toBe(true);
      expect(shuffled.includes(5)).toBe(true);
    });
  });
});
