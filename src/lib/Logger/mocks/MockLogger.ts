import type { ILogger } from '../ILogger.js';

export class MockLogger implements ILogger {
  public debug(...args: Array<unknown>): void {
    console.log(...args);
  }

  public error(...args: Array<unknown>): void {
    console.error(...args);
  }

  public fatal(...args: Array<unknown>): void {
    console.error(...args);
  }

  public info(...args: Array<unknown>): void {
    console.log(...args);
  }

  public silly(...args: Array<unknown>): void {
    console.log(...args);
  }

  public trace(...args: Array<unknown>): void {
    console.log(...args);
  }

  public warn(...args: Array<unknown>): void {
    console.warn(...args);
  }
}
