export interface ILogger {
  debug(...args: Array<unknown>): unknown;

  error(...args: Array<unknown>): unknown;

  fatal(...args: Array<unknown>): unknown;

  info(...args: Array<unknown>): unknown;

  trace(...args: Array<unknown>): unknown;

  warn(...args: Array<unknown>): unknown;
}
