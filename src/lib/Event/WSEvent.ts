import { type Either, left, right } from 'fp-ts/lib/Either.js';
import { z } from 'zod';
import type { ParseError } from '../Error/ParseError.js';

const WSEventSchema = z.object({
  type: z.string(),
  data: z.unknown()
});

export type WSEvent = Readonly<z.infer<typeof WSEventSchema>>;

export namespace WSEvent {
  export const SCHEMA = WSEventSchema;

  export const parse = (value: unknown): Either<ParseError, WSEvent> => {
    const result = WSEvent.SCHEMA.safeParse(value);

    if (result.success) {
      return right(result.data);
    }

    return left({
      error: 'ParseError',
      message: `INVALID VALUE. GIVEN: ${String(value)}`
    });
  };
}
