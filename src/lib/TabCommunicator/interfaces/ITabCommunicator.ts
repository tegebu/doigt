import type { UnaryFunction } from '@jamashita/anden/type';
import type { TabMessage } from '../Tab.js';

export interface ITabCommunicator {
  close(): void;

  onMessage(handler: UnaryFunction<TabMessage, void>): void;

  postMessage(message: TabMessage): void;
}
