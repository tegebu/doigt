import { UnimplementedError } from '@jamashita/anden/error';
import type { ITabCommunicator } from '../interfaces/ITabCommunicator.js';

export class MockTabCommunicator implements ITabCommunicator {
  public close(): void {
    throw new UnimplementedError();
  }

  public onMessage(): void {
    throw new UnimplementedError();
  }

  public postMessage(): void {
    throw new UnimplementedError();
  }
}
