import type { Tagged } from 'type-fest';

export type TabID = Tagged<string, 'TabID'>;

export type TabMessage = Readonly<{
  type: 'TAB_REGISTER' | 'TAB_ACKNOWLEDGE' | 'TAB_CLOSED';
  tabID: TabID;
}>;

export namespace Tab {
  export namespace ID {
    export const from = (value: string): TabID => {
      return value as TabID;
    };
  }
}
