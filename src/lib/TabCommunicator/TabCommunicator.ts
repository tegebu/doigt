import type { UnaryFunction } from '@jamashita/anden/type';
import type { ITabCommunicator } from './interfaces/ITabCommunicator.js';
import type { TabMessage } from './Tab.js';

export class TabCommunicator implements ITabCommunicator {
  private readonly channel: BroadcastChannel;

  public constructor(channelName: string) {
    this.channel = new BroadcastChannel(channelName);
  }

  public close(this: TabCommunicator): void {
    this.channel.close();
  }

  public onMessage(this: TabCommunicator, handler: UnaryFunction<TabMessage, void>): void {
    this.channel.onmessage = (event: MessageEvent<TabMessage>) => {
      handler(event.data);
    };
  }

  public postMessage(this: TabCommunicator, message: TabMessage): void {
    this.channel.postMessage(message);
  }
}
