export type ParseError = Readonly<{
  error: 'ParseError';
  message: string;
}>;
