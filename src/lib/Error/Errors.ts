import { z } from 'zod';

export const UnknownErrorSchema = z.object({
  error: z.literal('UnknownError'),
  cause: z.unknown()
});
export const ThrownErrorSchema = z.object({
  error: z.literal('ThrownError'),
  e: z.instanceof(Error)
});
export const RuntimeErrorSchema = z.object({
  error: z.literal('RuntimeError'),
  message: z.string()
});
export const GenericErrorSchema = z.union([UnknownErrorSchema, ThrownErrorSchema, RuntimeErrorSchema]);

export type UnknownError = Readonly<z.infer<typeof UnknownErrorSchema>>;
export type ThrownError = Readonly<z.infer<typeof ThrownErrorSchema>>;
export type RuntimeError = Readonly<z.infer<typeof RuntimeErrorSchema>>;
export type GenericError = UnknownError | ThrownError | RuntimeError;

export const createUnknownError = (cause: unknown): UnknownError => {
  return {
    error: 'UnknownError',
    cause
  };
};

export const createThrownError = (e: Error): ThrownError => {
  return {
    error: 'ThrownError',
    e
  };
};

export const createRuntimeError = (message: string): RuntimeError => {
  return {
    error: 'RuntimeError',
    message
  };
};
