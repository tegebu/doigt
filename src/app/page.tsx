'use client';

import { Hero } from '@/components/molecules/Hero.jsx';
import { Features } from '@/components/organisms/Features.jsx';
import { NameInputModal } from '@/features/Auth/components/organisms/NameInputModal.jsx';
import { useAuth } from '@/features/Auth/hooks/UseAuth.js';
import { useContainer } from '@/store/ContainerContext.jsx';
import { pipe } from 'fp-ts/lib/function.js';
import { bindW, Do, map } from 'fp-ts/lib/TaskEither.js';
import { type FC, type MouseEvent, useCallback, useState } from 'react';

const Page: FC = () => {
  const {
    client,
    connection,
    loading: { end: endLoading, start: startLoading },
    router: { push }
  } = useContainer();
  const { register } = useAuth(client, connection);
  const [isModalOpen, setIsModalOpen] = useState(false);

  const handleOpenModal = useCallback((e: MouseEvent<HTMLButtonElement>): void => {
    e.preventDefault();
    setIsModalOpen(true);
  }, []);
  const handleCloseModal = useCallback((): void => {
    setIsModalOpen(false);
  }, []);
  const handleRegister = useCallback(
    (name: string): void => {
      startLoading();

      pipe(
        Do,
        bindW('client', () => () => register(name)),
        map(() => {
          setIsModalOpen(false);
          endLoading();
          push('/rooms');

          return null;
        })
      )();
    },
    [endLoading, push, register, startLoading]
  );

  return (
    <>
      <main className="flex-grow">
        <Hero onLogin={handleOpenModal} />
        <Features />
      </main>
      <footer className="bg-primary py-4 text-primary-foreground">
        <div className="container mx-auto text-center">
          <p>&copy; 2025 汝は大次狼なりや？. All rights reserved.</p>
        </div>
      </footer>
      <NameInputModal isOpen={isModalOpen} onClose={handleCloseModal} onSubmit={handleRegister} />
    </>
  );
};

export default Page;
