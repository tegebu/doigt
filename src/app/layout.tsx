import { Header } from '@/components/organisms/Header.jsx';
import { AuthGuard } from '@/features/Auth/components/organisms/AuthGuard.jsx';
import { MultipleTabsModal } from '@/features/Browser/components/organisms/MultipleTabsModal.jsx';
import { LoadingIndicator } from '@/features/Loading/components/molecules/LoadingIndicator.jsx';
import { Toaster } from '@/features/Toast/components/organisms/Toaster.jsx';
import { ConnectionDot } from '@/features/WebSocket/components/molecules/ConnectionDot.jsx';
import { ContainerProvider } from '@/store/ContainerContext.jsx';
import type { Metadata } from 'next';
import type { ReactNode } from 'react';
import './global.css';

export const metadata = {
  title: '汝は大次狼なりや？',
  icons: {
    icon: [
      {
        url: '/favicon.ico',
        type: 'image/png'
      }
    ]
  },
  openGraph: {
    title: '汝は大次狼なりや？',
    siteName: '汝は大次狼なりや？',
    description: '大いなる次世代の人狼',
    url: 'https://www.jinrou.xyz/',
    images: [
      {
        url: 'https://www.tegebu.xyz/ogp.png',
        width: 1024,
        height: 1024,
        alt: '汝は大次狼なりや？'
      }
    ]
  }
} satisfies Metadata;

const Layout = ({ children }: { children: ReactNode }) => {
  return (
    <html lang="ja">
      <head>
        <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" />
        <link href="https://fonts.googleapis.com/css2?family=Zen+Maru+Gothic:wght@400;500&display=swap" rel="stylesheet" />
      </head>
      <body className="box-border bg-slate-50 font-zen text-slate-700 leading-relaxed">
        <ContainerProvider>
          <Header />
          <div className="flex min-h-screen flex-col">
            {children}
            <div className="fixed right-2 bottom-2">
              <ConnectionDot />
            </div>
          </div>
          <AuthGuard />
          <LoadingIndicator />
          <MultipleTabsModal />
          <Toaster />
        </ContainerProvider>
      </body>
    </html>
  );
};

export default Layout;
