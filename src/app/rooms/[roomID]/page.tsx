import { Card, CardContent, CardHeader, CardTitle } from '@/components/atoms/Card.jsx';
import { ScrollArea } from '@/components/molecules/ScrollArea.jsx';
import { Timer } from '@/features/Room/components/molecules/Timer.jsx';
import { VoteIcon } from 'lucide-react';
import type { FC } from 'react';

type Props = Readonly<{
  params: Readonly<{
    roomID: Promise<string>;
  }>;
}>;

const players = [
  { id: 1, name: 'Alice', isAlive: true },
  { id: 2, name: 'Bob', isAlive: true },
  { id: 3, name: 'Charlie', isAlive: true },
  { id: 4, name: 'David', isAlive: false },
  { id: 5, name: 'Eve', isAlive: true }
];

const messages = [
  { id: 1, content: 'ゲームを開始します。役職を確認してください。' },
  { id: 2, content: '1日目の夜になりました。' },
  { id: 3, content: '占い師は占う対象を選んでください。' }
];

// Mock user data
const userData = {
  name: 'Player1',
  role: '村人',
  history: [
    { day: 1, action: '投票', target: 'Bob' },
    { day: 2, action: '投票', target: 'Charlie' }
  ]
};

const Page: FC<Props> = ({ params: { roomID } }) => {
  return (
    <div className="container mx-auto p-4">
      <div className="grid grid-cols-1 gap-4 md:grid-cols-3">
        <Card>
          <CardHeader>
            <CardTitle>プレイヤー情報</CardTitle>
          </CardHeader>
          <CardContent>
            <div className="mb-4 flex flex-col items-center">
              <h3 className="font-bold text-xl">{userData.name}</h3>
              <p className="text-lg">役職: {userData.role}</p>
            </div>
            <div>
              <h4 className="mb-2 font-semibold">行動履歴:</h4>
              <ul className="space-y-2">
                {userData.history.map((item, index) => (
                  <li key={index} className="flex items-center">
                    <VoteIcon className="mr-2" size={16} />
                    {item.day}日目: {item.action} - {item.target}
                  </li>
                ))}
              </ul>
            </div>
          </CardContent>
        </Card>

        <Card className="md:col-span-2">
          <CardHeader>
            <CardTitle className="flex items-center justify-between">
              <span>システムメッセージ</span>
              <div className="flex items-center text-xl">
                <Timer seconds={50} />
              </div>
            </CardTitle>
          </CardHeader>
          <CardContent>
            <ScrollArea className="h-[calc(100vh-400px)] w-full pr-4">
              {messages.map((message, _index) => (
                <div key={message.id} className="mb-4 rounded-lg bg-secondary p-3">
                  <p>{message.content}</p>
                </div>
              ))}
            </ScrollArea>
          </CardContent>
        </Card>

        <Card className="md:col-span-3">
          <CardHeader>
            <CardTitle>ゲーム状況</CardTitle>
          </CardHeader>
          <CardContent>
            <div className="mb-4">
              <h3 className="mb-2 font-semibold text-lg">生存者</h3>
              <div className="grid grid-cols-2 gap-4 sm:grid-cols-3 md:grid-cols-4 lg:grid-cols-6">
                {players
                  .filter((player) => player.isAlive)
                  .map((player) => (
                    <Card key={player.id}>
                      <CardContent className="flex flex-col items-center p-4">
                        <p className="font-bold">{player.name}</p>
                      </CardContent>
                    </Card>
                  ))}
              </div>
            </div>
            <div>
              <h3 className="mb-2 font-semibold text-lg">死亡者</h3>
              <div className="grid grid-cols-2 gap-4 sm:grid-cols-3 md:grid-cols-4 lg:grid-cols-6">
                {players
                  .filter((player) => !player.isAlive)
                  .map((player) => (
                    <Card key={player.id} className="opacity-50">
                      <CardContent className="flex flex-col items-center p-4">
                        <p className="font-bold">{player.name}</p>
                      </CardContent>
                    </Card>
                  ))}
              </div>
            </div>
          </CardContent>
        </Card>
      </div>
    </div>
  );
};

export default Page;
