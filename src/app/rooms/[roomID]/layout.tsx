import { AppGuard } from '@/features/Werewolf/components/AppGuard.jsx';
import type { ReactNode } from 'react';

const Layout = ({ children }: { children: ReactNode }) => {
  return (
    <>
      {children}
      <AppGuard />
    </>
  );
};

export default Layout;
