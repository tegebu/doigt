'use client';

import type { RoomSettings } from '@/applications/dtos/Role/RoleSettings.js';
import type { Client } from '@/domains/Client/Client.js';
import type { RoomList, RoomList as RoomListItem } from '@/domains/Room/Room.js';
import { Rooms } from '@/domains/Room/Rooms.js';
import { NewRoomButton } from '@/features/RoomList/components/atoms/NewRoomButton.jsx';
import { RoomSearchInput } from '@/features/RoomList/components/molecules/RoomSearchInput.jsx';
import { CreateRoomModal } from '@/features/RoomList/components/organisms/CreateRoomModal.jsx';
import { PasswordEntryModal } from '@/features/RoomList/components/organisms/PasswordEntryModal.jsx';
import { RoomCard } from '@/features/RoomList/components/organisms/RoomCard.jsx';
import { useRoomList } from '@/features/RoomList/hooks/UseRoomList.js';
import { useContainer } from '@/store/ContainerContext.jsx';
import { Kind, type Nullable } from '@jamashita/anden/type';
import { isNone } from 'fp-ts/lib/Option.js';
import { type FC, type MouseEvent, useCallback, useEffect, useMemo, useState } from 'react';

const Page: FC = () => {
  const {
    client,
    client: { get: getClient },
    connection: { status },
    loading,
    router,
    router: { push },
    werewolf,
    wsEvent
  } = useContainer();
  const {
    joinRoomError,
    rooms,
    clearJoinRoomError,
    create: createRoom,
    fetch: fetchRoomList,
    join: joinRoom
  } = useRoomList(client, wsEvent, loading, router, werewolf);
  const [me, setMe] = useState<Nullable<Client>>(null);
  const [selectedRoom, setSelectedRoom] = useState<Nullable<RoomList>>(null);
  const [searchTerm, setSearchTerm] = useState('');
  const [isCreateModalOpen, setIsCreateModalOpen] = useState(false);
  const [isPasswordEntryModalOpen, setIsPasswordEntryModalOpen] = useState(false);
  const filteredRooms = useMemo(() => {
    return Rooms.List.filter(rooms, (room: RoomListItem) => {
      return room.name.toLowerCase().includes(searchTerm.toLowerCase());
    });
  }, [rooms, searchTerm]);

  const handleInputSearchTerm = useCallback((s: string): void => {
    setSearchTerm(s);
  }, []);
  const handleClickCreateRoom = useCallback((e: MouseEvent<HTMLButtonElement>): void => {
    e.preventDefault();
    setIsCreateModalOpen(true);
  }, []);
  const handleCloseCreateRoom = useCallback((): void => {
    setIsCreateModalOpen(false);
  }, []);
  const handleCreateRoom = useCallback(
    (settings: RoomSettings): void => {
      if (settings.password?.trim() === '') {
        createRoom({
          ...settings,
          password: null
        });

        return;
      }

      createRoom(settings);
    },
    [createRoom]
  );
  const handleClickJoin = useCallback(
    (room: RoomListItem): void => {
      if (room.requirePassword) {
        setIsPasswordEntryModalOpen(true);
        setSelectedRoom(room);

        return;
      }

      joinRoom(null, room);
    },
    [joinRoom]
  );
  const handleClosePasswordEntry = useCallback((): void => {
    setIsPasswordEntryModalOpen(false);
    setSelectedRoom(null);
    clearJoinRoomError();
  }, [clearJoinRoomError]);
  const handleJoin = useCallback(
    (password: string): void => {
      if (Kind.isNull(selectedRoom)) {
        return;
      }

      joinRoom(password, selectedRoom);
    },
    [joinRoom, selectedRoom]
  );

  useEffect(() => {
    const option = getClient();

    if (isNone(option)) {
      push('/');

      return;
    }

    setMe(option.value);
  }, [getClient, push]);
  useEffect(() => {
    // TODO connectedになってあなたがゲーム進行中でないならfetchRoomListを呼ぶ
    if (status === 'CONNECTED') {
      fetchRoomList();
    }
  }, [status, fetchRoomList]);

  if (Kind.isNull(me)) {
    return null;
  }

  return (
    <div className="p-4">
      <main className="min-h-screen bg-slate-200 py-8">
        <div className="container mx-auto p-4">
          <h1 className="mb-6 font-bold text-3xl">部屋の一覧</h1>
          <div className="mb-4 flex items-center justify-between">
            <RoomSearchInput onSearch={handleInputSearchTerm} />
            <NewRoomButton onClick={handleClickCreateRoom} />
          </div>
          <div className="grid gap-4 md:grid-cols-2 lg:grid-cols-3">
            {[...filteredRooms.values()].map((room: RoomListItem) => {
              return <RoomCard key={room.id} room={room} onClick={handleClickJoin} />;
            })}
          </div>
          <CreateRoomModal hostID={me.id} isOpen={isCreateModalOpen} onClose={handleCloseCreateRoom} onSubmit={handleCreateRoom} />
          <PasswordEntryModal
            error={joinRoomError}
            isOpen={isPasswordEntryModalOpen}
            room={selectedRoom}
            onClose={handleClosePasswordEntry}
            onSubmit={handleJoin}
          />
        </div>
      </main>
    </div>
  );
};

export default Page;
