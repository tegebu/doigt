import { Button } from '@/components/atoms/Button.jsx';
import { Dialog, DialogContent, DialogDescription, DialogFooter, DialogHeader, DialogTitle } from '@/components/atoms/Dialog.jsx';
import { Form, FormControl, FormDescription, FormField, FormItem, FormLabel, FormMessage } from '@/components/atoms/Form.jsx';
import { Input } from '@/components/atoms/Input.jsx';
import { zodResolver } from '@hookform/resolvers/zod';
import { type FC, memo, useCallback, useEffect } from 'react';
import { useForm } from 'react-hook-form';
import { z } from 'zod';

type Props = Readonly<{
  isOpen: boolean;
  onSubmit(name: string): void;
  onClose(): void;
}>;

const MAX_NAME_LENGTH = 10;

const FormSchema = z.object({
  name: z
    .string()
    .min(1, {
      message: 'お名前を入力してください'
    })
    .max(MAX_NAME_LENGTH, {
      message: `お名前を${MAX_NAME_LENGTH}文字以内で入力してください`
    })
});

type FormValues = z.infer<typeof FormSchema>;

export const NameInputModal: FC<Props> = memo(({ isOpen, onSubmit, onClose }) => {
  const form = useForm<FormValues>({
    resolver: zodResolver(FormSchema),
    defaultValues: {
      name: ''
    }
  });

  const handleSubmit = useCallback(
    (data: FormValues): void => {
      onSubmit(data.name);
    },
    [onSubmit]
  );

  useEffect(() => {
    if (isOpen) {
      form.reset({
        name: ''
      });
    }
  }, [form, isOpen]);

  return (
    <Dialog modal open={isOpen} onOpenChange={onClose}>
      <DialogContent className="space-y-4" closeButton closeOnOutsideClick closeOnESC>
        <DialogHeader>
          <DialogTitle>お名前を入力してください</DialogTitle>
        </DialogHeader>
        <Form {...form}>
          <form onSubmit={form.handleSubmit(handleSubmit)} className="space-y-4">
            <div className="space-y-2">
              <FormField
                name="name"
                render={({ field }) => {
                  return (
                    <FormItem>
                      <FormLabel>{`お名前を入力してください(${MAX_NAME_LENGTH}文字以内)`}</FormLabel>
                      <FormControl>
                        <Input placeholder="お名前" {...field} />
                      </FormControl>
                      <FormDescription>ほかのかたにも見えるお名前です。ほかのかたの迷惑になるような名前はやめましょう。</FormDescription>
                      <FormMessage />
                    </FormItem>
                  );
                }}
              />
            </div>
            <DialogFooter>
              <Button type="submit" disabled={!form.formState.isValid}>
                登録
              </Button>
            </DialogFooter>
          </form>
        </Form>
        <DialogDescription className="sr-only">お名前入力です</DialogDescription>
      </DialogContent>
    </Dialog>
  );
});
