'use client';

import { useAuth } from '@/features/Auth/hooks/UseAuth.js';
import { useContainer } from '@/store/ContainerContext.jsx';
import { pipe } from 'fp-ts/lib/function.js';
import { bindW, Do, fromEither, map, right } from 'fp-ts/lib/TaskEither.js';
import { type FC, memo, type PropsWithChildren, useEffect } from 'react';

export const AuthGuard: FC<PropsWithChildren> = memo(({ children }) => {
  const {
    client,
    connection,
    connection: { connect },
    loading: { end: endLoading, start: startLoading },
    router: { push }
  } = useContainer();
  const { checkAuth } = useAuth(client, connection);

  useEffect(() => {
    startLoading();

    pipe(
      Do,
      bindW('authResult', () => {
        return pipe(checkAuth(), fromEither);
      }),
      bindW('res1', ({ authResult }) => {
        if (authResult.succeeded) {
          return pipe(
            Do,
            bindW('res1', () => () => connect(authResult.client)),
            map(() => {
              push('/rooms');
              endLoading();

              return null;
            })
          );
        }

        push('/');
        endLoading();

        return right(null);
      })
    )();
  }, [connect, endLoading, startLoading, push, checkAuth]);

  return children;
});
