import { createGatewayError } from '@/adapters/gateways/GatewayError.js';
import { mockClient } from '@/domains/Client/mocks/MockClient.js';
import { mockUseClient } from '@/hooks/mocks/MockUseClient.js';
import { mockUseConnection } from '@/hooks/mocks/MockUseConnection.js';
import type { UseClient } from '@/hooks/UseClient.js';
import type { UseConnection } from '@/hooks/UseConnection.js';
import { act, renderHook } from '@testing-library/react';
import { isLeft, isRight, left as l, right as r } from 'fp-ts/lib/Either.js';
import { right } from 'fp-ts/lib/TaskEither.js';
import { unknown } from 'zod';
import { useAuth } from '../UseAuth.js';

describe('UseAuth', () => {
  let useClient: UseClient;
  let useConnection: UseConnection;

  beforeEach(() => {
    useClient = mockUseClient();
    useConnection = mockUseConnection();
  });

  afterEach(() => {
    vi.clearAllMocks();
  });

  describe('checkAuth', () => {
    it('should return success when client.find returns right', async () => {
      const spy1 = vi.spyOn(useClient, 'find').mockImplementation(() =>
        r(
          mockClient({
            id: '4f0dcff4-0ce7-41b5-b2df-32e091530242',
            name: 'BorVuWr'
          })
        )
      );

      const { result } = renderHook(() => useAuth(useClient, useConnection));

      const either = await act(() => {
        return result.current.checkAuth();
      });

      expect(isRight(either)).toBe(true);

      if (isLeft(either)) {
        throw new Error('This should not happen');
      }

      expect(either.right.succeeded).toBe(true);

      if (!either.right.succeeded) {
        throw new Error('This should not happen');
      }

      expect(either.right.client.id).toBe('4f0dcff4-0ce7-41b5-b2df-32e091530242');
      expect(either.right.client.name).toBe('BorVuWr');
      expect(spy1).toHaveBeenCalledOnce();
    });

    it('should return failure when client.find returns left', async () => {
      const spy1 = vi.spyOn(useClient, 'find').mockImplementation(() => l(createGatewayError('EntityNotFound', '')));

      const { result } = renderHook(() => useAuth(useClient, useConnection));

      const either = await act(() => {
        return result.current.checkAuth();
      });

      expect(isRight(either)).toBe(true);

      if (isLeft(either)) {
        throw new Error('This should not happen');
      }

      expect(either.right.succeeded).toBe(false);
      expect(spy1).toHaveBeenCalledOnce();
    });
  });

  describe('register', () => {
    it('should return a client when invoked', async () => {
      const spy1 = vi.spyOn(useConnection, 'connect').mockImplementation(() => right(null)());
      const spy2 = vi.spyOn(useClient, 'create').mockImplementation(() => r(unknown));

      const { result } = renderHook(() => useAuth(useClient, useConnection));

      const either = await act(() => {
        return result.current.register('BorVuWr');
      });

      expect(isRight(either)).toBe(true);

      if (isLeft(either)) {
        throw new Error('This should not happen');
      }

      expect(either.right.name).toBe('BorVuWr');
      expect(spy1).toHaveBeenCalledOnce();
      expect(spy2).toHaveBeenCalledOnce();
    });
  });
});
