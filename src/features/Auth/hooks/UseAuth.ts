import type { GatewayError } from '@/adapters/gateways/GatewayError.js';
import { Client } from '@/domains/Client/Client.js';
import type { UseClient } from '@/hooks/UseClient.js';
import type { UseConnection } from '@/hooks/UseConnection.js';
import type { GenericError } from '@/lib/Error/Errors.js';
import type { ParseError } from '@/lib/Error/ParseError.js';
import type { Either } from 'fp-ts/lib/Either.js';
import { fold, right } from 'fp-ts/lib/Either.js';
import { pipe } from 'fp-ts/lib/function.js';
import { bindW, Do, fromEither, map } from 'fp-ts/lib/TaskEither.js';
import { useCallback } from 'react';

type SuccessAuthResult = Readonly<{
  succeeded: true;
  client: Client;
}>;
type FailureAuthResult = Readonly<{
  succeeded: false;
}>;
type AuthResult = SuccessAuthResult | FailureAuthResult;

export const useAuth = ({ find: findClient, create: createClient }: UseClient, { connect }: UseConnection) => {
  const checkAuth = useCallback((): Either<GenericError | GatewayError, AuthResult> => {
    return pipe(
      findClient(),
      fold(
        () =>
          right({
            succeeded: false
          } as AuthResult),
        (client: Client) =>
          right({
            succeeded: true,
            client
          } satisfies AuthResult)
      )
    );
  }, [findClient]);

  const register = useCallback(
    (name: string): Promise<Either<GenericError | GatewayError | ParseError, Client>> => {
      const c = {
        id: Client.ID.generate(),
        name
      } satisfies Client;

      return pipe(
        Do,
        bindW('res1', () => async () => connect(c)),
        bindW('res2', () => {
          return pipe(createClient(c), fromEither);
        }),
        map(() => c)
      )();
    },
    [createClient, connect]
  );

  return {
    checkAuth,
    register
  };
};

export type UseAuth = ReturnType<typeof useAuth>;
