import { useContainer } from '@/store/ContainerContext.jsx';
import { pipe } from 'fp-ts/lib/function.js';
import { fold } from 'fp-ts/lib/Option.js';
import { bindW, Do, right } from 'fp-ts/lib/TaskEither.js';
import { type FC, memo, type PropsWithChildren, useEffect } from 'react';

export const AppGuard: FC<PropsWithChildren> = memo(({ children }) => {
  const {
    router: { push },
    werewolf: { find: findWerewolf }
  } = useContainer();

  useEffect(() => {
    pipe(
      Do,
      bindW('option', () => () => findWerewolf()),
      bindW('res', ({ option }) => {
        return pipe(
          option,
          fold(
            () => right(null),
            (werewolf) => {
              push(`/room/${werewolf.roomID}`);

              return right(null);
            }
          )
        );
      })
    )();
  }, [push, findWerewolf]);

  return <>{children}</>;
});
