'use client';

import { FullScreenSpinner } from '@/components/molecules/FullScreenSpinner.jsx';
import { useContainer } from '@/store/ContainerContext.jsx';
import { type FC, memo } from 'react';

export const LoadingIndicator: FC = memo(() => {
  const {
    loading: { loading }
  } = useContainer();

  return <FullScreenSpinner isLoading={loading} />;
});
