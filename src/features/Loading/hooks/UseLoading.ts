import { useCallback, useMemo, useState } from 'react';

export const useLoading = () => {
  const [count, setCount] = useState(0);
  const loading = useMemo(() => count > 0, [count]);

  const end = useCallback((): void => {
    setCount((prev) => prev - 1);
  }, []);
  const start = useCallback((): void => {
    setCount((prev) => prev + 1);
  }, []);

  return {
    loading,
    end,
    start
  };
};

export type UseLoading = ReturnType<typeof useLoading>;
