import type { UseLoading } from '@/features/Loading/hooks/UseLoading.js';

export const mockUseLoading = () => {
  return {
    end: vi.fn(),
    loading: false,
    start: vi.fn()
  } satisfies UseLoading;
};
