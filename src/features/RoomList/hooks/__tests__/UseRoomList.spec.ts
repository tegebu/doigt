import { mockRoomSettings } from '@/applications/dtos/Role/mocks/MockRoomSettings.js';
import { PUBLISH } from '@/applications/websocket/PublishEvents.js';
import { SUBMIT } from '@/applications/websocket/SubmitEvents.js';
import { Client } from '@/domains/Client/Client.js';
import { mockClient } from '@/domains/Client/mocks/MockClient.js';
import { mockRoomList } from '@/domains/Room/mocks/MockRoom.js';
import { Room } from '@/domains/Room/Room.js';
import { Rooms } from '@/domains/Room/Rooms.js';
import { mockUseLoading } from '@/features/Loading/hooks/mocks/MockUseLoading.js';
import type { UseLoading } from '@/features/Loading/hooks/UseLoading.js';
import { useRoomList } from '@/features/RoomList/hooks/UseRoomList.js';
import { mockUseAppRouter } from '@/hooks/mocks/MockUseAppRouter.js';
import { mockUseClient } from '@/hooks/mocks/MockUseClient.js';
import { mockUseWerewolf } from '@/hooks/mocks/MockUseWerewolf.js';
import { mockUseWSEvent } from '@/hooks/mocks/MockUseWSEvent.js';
import type { UseAppRouter } from '@/hooks/UseAppRouter.js';
import type { UseClient } from '@/hooks/UseClient.js';
import type { UseWerewolf } from '@/hooks/UseWerewolf';
import type { UseWSEvent } from '@/hooks/UseWSEvent.js';
import { wait } from '@jamashita/anden/helper';
import { act, renderHook } from '@testing-library/react';
import { isRight, right as r } from 'fp-ts/lib/Either.js';
import { right } from 'fp-ts/lib/TaskEither.js';

describe('UseRoomList', () => {
  let useClient: UseClient;
  let useWSEvent: UseWSEvent;
  let useLoading: UseLoading;
  let useAppRouter: UseAppRouter;
  let useWerewolf: UseWerewolf;

  beforeEach(() => {
    useClient = mockUseClient();
    useWSEvent = mockUseWSEvent();
    useLoading = mockUseLoading();
    useAppRouter = mockUseAppRouter();
    useWerewolf = mockUseWerewolf();
  });

  afterEach(() => {
    vi.clearAllMocks();
  });

  describe('create', () => {
    it('should create a room', async () => {
      const clientID = Client.ID.from('a49f0610-b80a-4093-a20c-063a32ea300f');
      const client = mockClient({
        id: clientID
      });
      const settings = mockRoomSettings();

      const spy1 = vi.spyOn(useClient, 'find').mockImplementation(() => r(client));
      const spy2 = vi.spyOn(useWSEvent, 'send').mockImplementation(() => right(null)());

      const { result } = renderHook(() => useRoomList(useClient, useWSEvent, useLoading, useAppRouter, useWerewolf));

      const either = await act(() => {
        return result.current.create(settings);
      });

      expect(isRight(either)).toBe(true);
      expect(spy1).toHaveBeenCalledOnce();
      expect(spy2).toHaveBeenCalledOnce();
      expect(spy2).toHaveBeenCalledWith(SUBMIT.ROOM.CREATE, {
        settings: {
          ...settings,
          hostID: clientID
        }
      });
    });
  });

  describe('fetch', () => {
    it('should fetch rooms', async () => {
      const clientID = Client.ID.from('a49f0610-b80a-4093-a20c-063a32ea300f');
      const client = mockClient({
        id: clientID
      });

      const spy1 = vi.spyOn(useClient, 'find').mockImplementation(() => r(client));
      const spy2 = vi.spyOn(useWSEvent, 'send').mockImplementation(() => right(null)());

      const { result } = renderHook(() => useRoomList(useClient, useWSEvent, useLoading, useAppRouter, useWerewolf));

      const either = await act(() => {
        return result.current.fetch();
      });

      expect(isRight(either)).toBe(true);
      expect(spy1).toHaveBeenCalledOnce();
      expect(spy2).toHaveBeenCalledOnce();
      expect(spy2).toHaveBeenCalledWith(SUBMIT.ROOM.LIST, {
        clientID
      });
    });
  });

  describe('join', () => {
    it('should join a room', async () => {
      const clientID = Client.ID.from('a49f0610-b80a-4093-a20c-063a32ea300f');
      const client = mockClient({
        id: clientID
      });
      const roomList = mockRoomList({
        id: '9d41d58a-20d1-4653-b701-7ae2fa3067de'
      });

      const spy1 = vi.spyOn(useClient, 'find').mockImplementation(() => r(client));
      const spy2 = vi.spyOn(useWSEvent, 'send').mockImplementation(() => right(null)());

      const { result } = renderHook(() => useRoomList(useClient, useWSEvent, useLoading, useAppRouter, useWerewolf));

      const either = await act(() => {
        return result.current.join(null, roomList);
      });

      expect(isRight(either)).toBe(true);
      expect(spy1).toHaveBeenCalledOnce();
      expect(spy2).toHaveBeenCalledOnce();
      expect(spy2).toHaveBeenCalledWith(SUBMIT.ROOM.JOIN, {
        roomID: roomList.id,
        clientID,
        password: null
      });
    });
  });

  describe('setupSubscriptions', () => {
    it('should receive PUBLISH.ROOM.CREATE.SUCCESS', async () => {
      const roomID = Room.ID.from('9d41d58a-20d1-4653-b701-7ae2fa3067de');

      const spy1 = vi.spyOn(useWSEvent, 'subscribe').mockImplementation(async (event, cb) => {
        if (event === PUBLISH.ROOM.CREATE.SUCCESS) {
          cb({
            roomID
          });
        }
      });
      const spy2 = vi.spyOn(useClient, 'find').mockImplementation(() => r(mockClient()));
      const spy3 = vi.spyOn(useWerewolf, 'create').mockImplementation(() => r(null));
      const spy4 = vi.spyOn(useLoading, 'end').mockImplementation(() => undefined);
      const spy5 = vi.spyOn(useAppRouter, 'push').mockImplementation(() => undefined);

      renderHook(() => useRoomList(useClient, useWSEvent, useLoading, useAppRouter, useWerewolf));

      await wait(0);

      expect(spy1).toHaveBeenCalled();
      expect(spy2).toHaveBeenCalledOnce();
      expect(spy3).toHaveBeenCalledOnce();
      expect(spy4).toHaveBeenCalledOnce();
      expect(spy5).toHaveBeenCalled();
      expect(spy5).toHaveBeenNthCalledWith(1, `/room/${roomID}`);
    });

    it('should receive PUBLISH.ROOM.CREATE.FAILURE', () => {
      const spy1 = vi.spyOn(useWSEvent, 'subscribe').mockImplementation((event, cb) => {
        if (event === PUBLISH.ROOM.CREATE.FAILURE) {
          cb({
            cause: 'NOT_FOUND'
          });
        }
      });

      const { result } = renderHook(() => useRoomList(useClient, useWSEvent, useLoading, useAppRouter, useWerewolf));

      expect(spy1).toHaveBeenCalled();
      expect(result.current.roomListError).toBe('部屋の作成に失敗しました。');
    });

    it('should receive PUBLISH.ROOM.JOIN.SUCCESS', async () => {
      const roomID = Room.ID.from('9d41d58a-20d1-4653-b701-7ae2fa3067de');

      const spy1 = vi.spyOn(useWSEvent, 'subscribe').mockImplementation(async (event, cb) => {
        if (event === PUBLISH.ROOM.JOIN.SUCCESS) {
          await cb({
            roomID
          });
        }
      });
      const spy2 = vi.spyOn(useWerewolf, 'create').mockImplementation(() => r(null));
      const spy3 = vi.spyOn(useAppRouter, 'push').mockImplementation(() => undefined);

      renderHook(() => useRoomList(useClient, useWSEvent, useLoading, useAppRouter, useWerewolf));

      await wait(0);

      expect(spy1).toHaveBeenCalled();
      expect(spy2).toHaveBeenCalledOnce();
      expect(spy3).toHaveBeenCalledWith(`/room/${roomID}`);
    });

    describe('should receive PUBLISH.ROOM.JOIN.FAILURE', () => {
      it('NOT_FOUND', () => {
        const spy1 = vi.spyOn(useWSEvent, 'subscribe').mockImplementation((event, cb) => {
          if (event === PUBLISH.ROOM.JOIN.FAILURE) {
            cb({
              cause: 'NOT_FOUND'
            });
          }
        });

        const { result } = renderHook(() => useRoomList(useClient, useWSEvent, useLoading, useAppRouter, useWerewolf));

        expect(spy1).toHaveBeenCalled();
        expect(result.current.joinRoomError).toBe('部屋が見つかりませんでした。');
      });

      it('FULL', () => {
        const spy1 = vi.spyOn(useWSEvent, 'subscribe').mockImplementation((event, cb) => {
          if (event === PUBLISH.ROOM.JOIN.FAILURE) {
            cb({
              cause: 'FULL'
            });
          }
        });

        const { result } = renderHook(() => useRoomList(useClient, useWSEvent, useLoading, useAppRouter, useWerewolf));

        expect(spy1).toHaveBeenCalled();
        expect(result.current.joinRoomError).toBe('部屋が満員で入室できませんでした。');
      });

      it('PASSWORD_MISMATCH', () => {
        const spy1 = vi.spyOn(useWSEvent, 'subscribe').mockImplementation((event, cb) => {
          if (event === PUBLISH.ROOM.JOIN.FAILURE) {
            cb({
              cause: 'PASSWORD_MISMATCH'
            });
          }
        });

        const { result } = renderHook(() => useRoomList(useClient, useWSEvent, useLoading, useAppRouter, useWerewolf));

        expect(spy1).toHaveBeenCalled();
        expect(result.current.joinRoomError).toBe('パスワードが間違っています。');
      });

      it('ALREADY_JOINED', () => {
        const spy1 = vi.spyOn(useWSEvent, 'subscribe').mockImplementation((event, cb) => {
          if (event === PUBLISH.ROOM.JOIN.FAILURE) {
            cb({
              cause: 'ALREADY_JOINED'
            });
          }
        });

        const { result } = renderHook(() => useRoomList(useClient, useWSEvent, useLoading, useAppRouter, useWerewolf));

        expect(spy1).toHaveBeenCalled();
        expect(result.current.joinRoomError).toBe('すでに部屋に入室しています。');
      });

      it('NOT_OPEN', () => {
        const spy1 = vi.spyOn(useWSEvent, 'subscribe').mockImplementation((event, cb) => {
          if (event === PUBLISH.ROOM.JOIN.FAILURE) {
            cb({
              cause: 'NOT_OPEN'
            });
          }
        });

        const { result } = renderHook(() => useRoomList(useClient, useWSEvent, useLoading, useAppRouter, useWerewolf));

        expect(spy1).toHaveBeenCalled();
        expect(result.current.joinRoomError).toBe('募集中ではありません。');
      });
    });

    it('should receive PUBLISH.ROOM.LIST.SUCCESS', () => {
      const roomID1 = Room.ID.from('9d41d58a-20d1-4653-b701-7ae2fa3067de');
      const roomID2 = Room.ID.from('a49f0610-b80a-4093-a20c-063a32ea300f');
      const rooms = [
        mockRoomList({
          id: roomID1
        }),
        mockRoomList({
          id: roomID2
        })
      ];

      const spy1 = vi.spyOn(useWSEvent, 'subscribe').mockImplementation((event, cb) => {
        if (event === PUBLISH.ROOM.LIST.SUCCESS) {
          cb({
            rooms
          });
        }
      });

      const { result } = renderHook(() => useRoomList(useClient, useWSEvent, useLoading, useAppRouter, useWerewolf));

      expect(spy1).toHaveBeenCalled();
      expect(result.current.rooms).toEqual(Rooms.List.ofArray(rooms));
    });

    it('should receive PUBLISH.ROOM.LIST.FAILURE', () => {
      const spy1 = vi.spyOn(useWSEvent, 'subscribe').mockImplementation((event, cb) => {
        if (event === PUBLISH.ROOM.LIST.FAILURE) {
          cb({});
        }
      });

      const { result } = renderHook(() => useRoomList(useClient, useWSEvent, useLoading, useAppRouter, useWerewolf));

      expect(spy1).toHaveBeenCalled();
      expect(result.current.roomListError).toBe('部屋一覧の取得に失敗しました。');
    });

    it('should receive PUBLISH.ROOM.NOTIFY.CREATED', () => {
      const roomID = Room.ID.from('9d41d58a-20d1-4653-b701-7ae2fa3067de');
      const room = mockRoomList({
        id: roomID
      });

      const spy1 = vi.spyOn(useWSEvent, 'subscribe').mockImplementation((event, cb) => {
        if (event === PUBLISH.ROOM.NOTIFY.CREATED) {
          cb({
            room
          });
        }
      });

      const { result } = renderHook(() => useRoomList(useClient, useWSEvent, useLoading, useAppRouter, useWerewolf));

      expect(spy1).toHaveBeenCalled();
      expect(result.current.rooms).toEqual(Rooms.List.ofArray([room]));
    });

    it('should receive PUBLISH.ROOM.NOTIFY.DESTROYED', () => {
      const roomID = Room.ID.from('9d41d58a-20d1-4653-b701-7ae2fa3067de');

      const spy1 = vi.spyOn(useWSEvent, 'subscribe').mockImplementation((event, cb) => {
        if (event === PUBLISH.ROOM.NOTIFY.DESTROYED) {
          cb({
            roomID
          });
        }
      });

      const { result } = renderHook(() => useRoomList(useClient, useWSEvent, useLoading, useAppRouter, useWerewolf));

      expect(spy1).toHaveBeenCalled();
      expect(result.current.rooms).toEqual(Rooms.List.empty());
    });

    it('should receive PUBLISH.ROOM.NOTIFY.JOINED', () => {
      const roomID = Room.ID.from('9d41d58a-20d1-4653-b701-7ae2fa3067de');
      const room = mockRoomList({
        id: roomID,
        participants: 1
      });

      const spy1 = vi.spyOn(useWSEvent, 'subscribe').mockImplementation((event, cb) => {
        if (event === PUBLISH.ROOM.NOTIFY.CREATED) {
          cb({
            room
          });
        }
        if (event === PUBLISH.ROOM.NOTIFY.JOINED) {
          cb({
            roomID
          });
        }
      });

      const { result } = renderHook(() => useRoomList(useClient, useWSEvent, useLoading, useAppRouter, useWerewolf));

      expect(spy1).toHaveBeenCalled();
      expect(result.current.rooms.get(roomID)?.participants).toBe(2);
    });

    it('should receive PUBLISH.ROOM.NOTIFY.LEFT', () => {
      const roomID = Room.ID.from('9d41d58a-20d1-4653-b701-7ae2fa3067de');
      const room = mockRoomList({
        id: roomID,
        participants: 2
      });

      const spy1 = vi.spyOn(useWSEvent, 'subscribe').mockImplementation((event, cb) => {
        if (event === PUBLISH.ROOM.NOTIFY.CREATED) {
          cb({
            room
          });
        }
        if (event === PUBLISH.ROOM.NOTIFY.LEFT) {
          cb({
            roomID
          });
        }
      });

      const { result } = renderHook(() => useRoomList(useClient, useWSEvent, useLoading, useAppRouter, useWerewolf));

      expect(spy1).toHaveBeenCalled();
      expect(result.current.rooms.get(roomID)?.participants).toBe(1);
    });

    it('should receive PUBLISH.ROOM.NOTIFY.STATUS_CHANGED', () => {
      const roomID = Room.ID.from('9d41d58a-20d1-4653-b701-7ae2fa3067de');
      const room = mockRoomList({
        id: roomID,
        status: 'OPEN'
      });

      const spy1 = vi.spyOn(useWSEvent, 'subscribe').mockImplementation((event, cb) => {
        if (event === PUBLISH.ROOM.NOTIFY.CREATED) {
          cb({
            room
          });
        }
        if (event === PUBLISH.ROOM.NOTIFY.STATUS_CHANGED) {
          cb({
            roomID,
            status: 'IN_PROGRESS'
          });
        }
      });

      const { result } = renderHook(() => useRoomList(useClient, useWSEvent, useLoading, useAppRouter, useWerewolf));

      expect(spy1).toHaveBeenCalled();
      expect(result.current.rooms.get(roomID)?.status).toBe('IN_PROGRESS');
    });
  });
});
