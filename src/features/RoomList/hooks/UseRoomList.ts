import type { GatewayError } from '@/adapters/gateways/GatewayError.js';
import type { RoomSettings } from '@/applications/dtos/Role/RoleSettings.js';
import { PUBLISH } from '@/applications/websocket/PublishEvents.js';
import { SUBMIT } from '@/applications/websocket/SubmitEvents.js';
import type { RoomList } from '@/domains/Room/Room.js';
import { type RoomLists, Rooms } from '@/domains/Room/Rooms.js';
import type { UseLoading } from '@/features/Loading/hooks/UseLoading.js';
import type { UseAppRouter } from '@/hooks/UseAppRouter.js';
import type { UseClient } from '@/hooks/UseClient.js';
import type { UseWerewolf } from '@/hooks/UseWerewolf.js';
import type { UseWSEvent } from '@/hooks/UseWSEvent.js';
import type { GenericError } from '@/lib/Error/Errors.js';
import type { ParseError } from '@/lib/Error/ParseError.js';
import { ExhaustiveError } from '@jamashita/anden/error';
import { Kind, type Nullable } from '@jamashita/anden/type';
import type { Either } from 'fp-ts/lib/Either.js';
import { pipe } from 'fp-ts/lib/function.js';
import { bindW, Do, fromEither, map } from 'fp-ts/lib/TaskEither.js';
import { useCallback, useEffect, useRef, useState } from 'react';

export const useRoomList = (
  { find: findClient }: UseClient,
  { send, subscribe, unsubscribe }: UseWSEvent,
  { end: endLoading, start: startLoading }: UseLoading,
  { push }: UseAppRouter,
  { create: createWerewolf }: UseWerewolf
) => {
  const isSubscribed = useRef(false);
  const [rooms, setRooms] = useState<RoomLists>(Rooms.List.empty());
  const [roomListError, setRoomListError] = useState<Nullable<string>>(null);
  const [joinRoomError, setJoinRoomError] = useState<Nullable<string>>(null);

  const cleanup = useCallback((): void => {
    if (!isSubscribed.current) {
      return;
    }

    unsubscribe(PUBLISH.ROOM.CREATE.SUCCESS);
    unsubscribe(PUBLISH.ROOM.CREATE.FAILURE);
    unsubscribe(PUBLISH.ROOM.JOIN.SUCCESS);
    unsubscribe(PUBLISH.ROOM.JOIN.FAILURE);
    unsubscribe(PUBLISH.ROOM.LIST.SUCCESS);
    unsubscribe(PUBLISH.ROOM.LIST.FAILURE);
    unsubscribe(PUBLISH.ROOM.NOTIFY.CREATED);
    unsubscribe(PUBLISH.ROOM.NOTIFY.DESTROYED);
    unsubscribe(PUBLISH.ROOM.NOTIFY.JOINED);
    unsubscribe(PUBLISH.ROOM.NOTIFY.LEFT);
    unsubscribe(PUBLISH.ROOM.NOTIFY.STATUS_CHANGED);

    isSubscribed.current = false;
  }, [unsubscribe]);

  const clearJoinRoomError = useCallback((): void => {
    setJoinRoomError(null);
  }, []);

  const create = useCallback(
    (settings: RoomSettings): Promise<Either<GenericError | GatewayError | ParseError, unknown>> => {
      startLoading();

      return pipe(
        Do,
        bindW('client', () => {
          return pipe(findClient(), fromEither);
        }),
        bindW(
          'res1',
          ({ client }) =>
            () =>
              send(SUBMIT.ROOM.CREATE, {
                settings: {
                  ...settings,
                  hostID: client.id
                }
              })
        )
      )();
    },
    [findClient, send, startLoading]
  );

  const fetch = useCallback((): Promise<Either<GenericError | GatewayError | ParseError, unknown>> => {
    startLoading();

    return pipe(
      Do,
      bindW('client', () => {
        return pipe(findClient(), fromEither);
      }),
      bindW(
        'rooms',
        ({ client }) =>
          () =>
            send(SUBMIT.ROOM.LIST, {
              clientID: client.id
            })
      )
    )();
  }, [findClient, send, startLoading]);

  const join = useCallback(
    (password: Nullable<string>, room: RoomList): Promise<Either<GenericError | GatewayError | ParseError, unknown>> => {
      startLoading();

      return pipe(
        Do,
        bindW('client', () => {
          return pipe(findClient(), fromEither);
        }),
        bindW(
          'rooms',
          ({ client }) =>
            () =>
              send(SUBMIT.ROOM.JOIN, {
                roomID: room.id,
                clientID: client.id,
                password
              })
        )
      )();
    },
    [findClient, send, startLoading]
  );

  const setupSubscriptions = useCallback(() => {
    if (isSubscribed.current) {
      return;
    }

    subscribe(PUBLISH.ROOM.CREATE.SUCCESS, async ({ roomID }) => {
      return pipe(
        Do,
        bindW('client', () => {
          return pipe(findClient(), fromEither);
        }),
        bindW('res1', ({ client }) => {
          return pipe(createWerewolf(roomID, client.id), fromEither);
        }),
        map(() => {
          endLoading();
          push(`/room/${roomID}`);

          return null;
        })
      )();
    });
    subscribe(PUBLISH.ROOM.CREATE.FAILURE, () => {
      endLoading();
      setRoomListError('部屋の作成に失敗しました。');
    });
    subscribe(PUBLISH.ROOM.JOIN.SUCCESS, ({ roomID, moderatorID }) => {
      return pipe(
        Do,
        bindW('res1', () => {
          return pipe(createWerewolf(roomID, moderatorID), fromEither);
        }),
        map(() => {
          endLoading();
          push(`/room/${roomID}`);

          return null;
        })
      )();
    });
    subscribe(PUBLISH.ROOM.JOIN.FAILURE, ({ cause }) => {
      switch (cause) {
        case 'NOT_FOUND': {
          setJoinRoomError('部屋が見つかりませんでした。');

          return;
        }
        case 'FULL': {
          setJoinRoomError('部屋が満員で入室できませんでした。');

          return;
        }
        case 'PASSWORD_MISMATCH': {
          setJoinRoomError('パスワードが間違っています。');

          return;
        }
        case 'ALREADY_JOINED': {
          setJoinRoomError('すでに部屋に入室しています。');

          return;
        }
        case 'NOT_OPEN': {
          setJoinRoomError('募集中ではありません。');

          return;
        }
        default: {
          throw new ExhaustiveError(cause);
        }
      }
    });
    subscribe(PUBLISH.ROOM.LIST.SUCCESS, ({ rooms }) => {
      endLoading();
      setRooms(Rooms.List.ofArray(rooms));
    });
    subscribe(PUBLISH.ROOM.LIST.FAILURE, () => {
      endLoading();
      setRoomListError('部屋一覧の取得に失敗しました。');
    });
    subscribe(PUBLISH.ROOM.NOTIFY.CREATED, ({ room }) => {
      setRooms((prev) => Rooms.List.add(prev, room));
    });
    subscribe(PUBLISH.ROOM.NOTIFY.DESTROYED, ({ roomID }) => {
      setRooms((prev) => Rooms.List.remove(prev, roomID));
    });
    subscribe(PUBLISH.ROOM.NOTIFY.JOINED, ({ roomID }) => {
      setRooms((prev) => {
        const room = prev.get(roomID);

        if (Kind.isUndefined(room)) {
          return prev;
        }

        const newRoom = {
          ...room,
          participants: room.participants + 1
        } satisfies RoomList;

        return Rooms.List.replace(prev, newRoom);
      });
    });
    subscribe(PUBLISH.ROOM.NOTIFY.LEFT, ({ roomID }) => {
      setRooms((prev) => {
        const room = prev.get(roomID);

        if (Kind.isUndefined(room)) {
          return prev;
        }

        const newRoom = {
          ...room,
          participants: room.participants - 1
        } satisfies RoomList;

        return Rooms.List.replace(prev, newRoom);
      });
    });
    subscribe(PUBLISH.ROOM.NOTIFY.STATUS_CHANGED, ({ roomID, status }) => {
      setRooms((prev) => {
        const room = prev.get(roomID);

        if (Kind.isUndefined(room)) {
          return prev;
        }

        const newRoom = {
          ...room,
          status
        } satisfies RoomList;

        return Rooms.List.replace(prev, newRoom);
      });
    });

    isSubscribed.current = true;
  }, [findClient, subscribe, endLoading, push, createWerewolf]);

  useEffect(() => {
    setupSubscriptions();

    return () => {
      cleanup();
    };
  }, [cleanup, setupSubscriptions]);

  return {
    joinRoomError,
    roomListError,
    rooms,
    clearJoinRoomError,
    create,
    fetch,
    join
  };
};

export type UseRoomList = ReturnType<typeof useRoomList>;
