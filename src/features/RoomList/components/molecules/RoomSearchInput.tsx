import { Input } from '@/components/atoms/Input.jsx';
import { Search } from 'lucide-react';
import { type ChangeEvent, type FC, memo, useCallback } from 'react';

type Props = Readonly<{
  onSearch(searchTerm: string): void;
}>;

export const RoomSearchInput: FC<Props> = memo(({ onSearch }) => {
  const handleInputSearchTerm = useCallback(
    (e: ChangeEvent<HTMLInputElement>): void => {
      e.preventDefault();

      onSearch(e.target.value);
    },
    [onSearch]
  );

  return (
    <div className="relative w-64">
      <Input type="text" placeholder="部屋を検索..." onChange={handleInputSearchTerm} className="pl-10" />
      <Search className="absolute top-2.5 left-3 h-5 w-5 text-muted" />
    </div>
  );
});
