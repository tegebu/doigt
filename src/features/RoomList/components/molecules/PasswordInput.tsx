import { FormControl, FormField, FormItem, FormLabel, FormMessage } from '@/components/atoms/Form.jsx';
import { Input } from '@/components/atoms/Input.jsx';
import { Label } from '@/components/atoms/Label.jsx';
import { Switch } from '@/components/atoms/Switch.jsx';
import { type FC, memo } from 'react';

type Props = Readonly<{
  required: boolean;
  onCheckUsePassword(value: boolean): void;
}>;

export const PasswordInput: FC<Props> = memo(({ required, onCheckUsePassword }) => {
  if (required) {
    return (
      <>
        <div className="flex items-center space-x-2">
          <Switch id="usePassword" checked={required} onCheckedChange={onCheckUsePassword} />
          <Label htmlFor="usePassword">パスワードを設定する</Label>
        </div>
        <FormField
          name="password"
          render={({ field }) => {
            return (
              <FormItem>
                <FormLabel>パスワード</FormLabel>
                <FormControl>
                  <Input id="password" type="password" {...field} />
                </FormControl>
                <FormMessage />
              </FormItem>
            );
          }}
        />
      </>
    );
  }

  return (
    <div className="flex items-center space-x-2">
      <Switch id="usePassword" checked={required} onCheckedChange={onCheckUsePassword} />
      <Label htmlFor="usePassword">パスワードを設定する</Label>
    </div>
  );
});
