import { Alert, AlertDescription } from '@/components/atoms/Alert.jsx';
import { Kind, type Nullable } from '@jamashita/anden/type';
import { AlertCircle } from 'lucide-react';
import { type FC, memo } from 'react';

type Props = Readonly<{
  error: Nullable<string>;
}>;

export const PasswordAlert: FC<Props> = memo(({ error }) => {
  if (Kind.isNull(error)) {
    return null;
  }

  return (
    <Alert variant="destructive" className="mt-4">
      <div className="flex items-center">
        <AlertCircle className="mr-2 h-4 w-4 flex-shrink-0" />
        <AlertDescription>{error}</AlertDescription>
      </div>
    </Alert>
  );
});
