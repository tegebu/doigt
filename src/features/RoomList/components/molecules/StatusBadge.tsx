import type { GameStatus } from '@/domains/Room/GameStatus.js';
import { CompletedStatusBadge } from '@/features/RoomList/components/atoms/CompletedStatusBadge.jsx';
import { InProgressStatusBadge } from '@/features/RoomList/components/atoms/InProgressStatusBadge.jsx';
import { OpenStatusBadge } from '@/features/RoomList/components/atoms/OpenStatusBadge.jsx';
import { ExhaustiveError } from '@jamashita/anden/error';
import { type FC, memo } from 'react';

type Props = Readonly<{
  status: GameStatus;
}>;

export const StatusBadge: FC<Props> = memo(({ status }) => {
  switch (status) {
    case 'OPEN': {
      return <OpenStatusBadge />;
    }
    case 'IN_PROGRESS': {
      return <InProgressStatusBadge />;
    }
    case 'COMPLETED': {
      return <CompletedStatusBadge />;
    }
    default: {
      throw new ExhaustiveError(status);
    }
  }
});
