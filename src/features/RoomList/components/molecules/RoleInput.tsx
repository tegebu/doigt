import { FormControl, FormField, FormItem, FormLabel, FormMessage } from '@/components/atoms/Form.jsx';
import { Input } from '@/components/atoms/Input.jsx';
import { Select, SelectContent, SelectItem, SelectTrigger, SelectValue } from '@/components/atoms/Select.jsx';
import type { Identity } from '@/domains/Identity/Identity.js';
import { type FC, memo } from 'react';

type Props = Readonly<{
  role: string;
  identity: Identity;
}>;

export const RoleInput: FC<Props> = memo(({ role, identity }) => {
  return (
    <div>
      <FormLabel>{identity.name}</FormLabel>
      <FormField
        name={`roleAssignment.${role}.type`}
        render={({ field }) => {
          return (
            <FormItem>
              <FormControl>
                <Select onValueChange={field.onChange} value={field.value}>
                  <SelectTrigger>
                    <SelectValue placeholder="選択してください" />
                  </SelectTrigger>
                  <SelectContent>
                    <SelectItem value="SINGLE">固定値</SelectItem>
                    <SelectItem value="RANGE">範囲指定 (開発中)</SelectItem>
                  </SelectContent>
                </Select>
              </FormControl>
              <FormMessage className="!mt-0" />
            </FormItem>
          );
        }}
      />
      <FormField
        name={`roleAssignment.${role}.value`}
        render={({ field }) => {
          return (
            <FormItem>
              <FormControl>
                <Input
                  id={role}
                  type="number"
                  className="!mt-0 w-full"
                  placeholder="値を入力してください"
                  {...field}
                  onChange={(e) => {
                    const value = Number.parseInt(e.target.value);

                    if (Number.isNaN(value)) {
                      field.onChange(0);

                      return;
                    }

                    field.onChange(value);
                  }}
                />
              </FormControl>
              <FormMessage className="!mt-0" />
            </FormItem>
          );
        }}
      />
    </div>
  );
});
