'use client';

import { Button } from '@/components/atoms/Button.jsx';
import { Dialog, DialogContent, DialogDescription, DialogFooter, DialogHeader, DialogTitle } from '@/components/atoms/Dialog.jsx';
import { Form, FormControl, FormField, FormItem, FormLabel, FormMessage } from '@/components/atoms/Form.jsx';
import { Input } from '@/components/atoms/Input.jsx';
import type { RoomList } from '@/domains/Room/Room.js';
import { PasswordAlert } from '@/features/RoomList/components/molecules/PasswordAlert.jsx';
import { zodResolver } from '@hookform/resolvers/zod';
import { Kind, type Nullable } from '@jamashita/anden/type';
import { type FC, memo, useCallback, useEffect } from 'react';
import { useForm } from 'react-hook-form';
import { z } from 'zod';

type Props = Readonly<{
  error: Nullable<string>;
  isOpen: boolean;
  room: Nullable<RoomList>;
  onClose(): void;
  onSubmit(password: string): void;
}>;

const FormSchema = z.object({
  password: z.string().min(1, 'パスワードを入力してください')
});

type FormValues = z.infer<typeof FormSchema>;

export const PasswordEntryModal: FC<Props> = memo(({ error, isOpen, room, onClose, onSubmit }) => {
  const form = useForm<FormValues>({
    resolver: zodResolver(FormSchema),
    defaultValues: {
      password: ''
    }
  });

  const handleSubmit = useCallback(
    ({ password }: FormValues): void => {
      onSubmit(password);
    },
    [onSubmit]
  );

  useEffect(() => {
    if (!Kind.isNull(room)) {
      form.setValue('password', '');
    }
  }, [room, form]);

  if (Kind.isNull(room)) {
    return null;
  }

  return (
    <Dialog open={isOpen} onOpenChange={onClose}>
      <DialogContent className="flex h-[90vh] flex-col sm:max-w-[600px] md:max-w-[700px] lg:max-w-[900px]" closeButton closeOnESC closeOnOutsideClick>
        <DialogHeader>
          <DialogTitle>パスワードが必要です</DialogTitle>
        </DialogHeader>
        <Form {...form}>
          <form onSubmit={form.handleSubmit(handleSubmit)}>
            <div className="space-y-4 p-4">
              <div className="mb-4">{room.name}に入室するためにパスワードを入力してください</div>
              <div className="items-center gap-4">
                <FormField
                  name="password"
                  render={({ field }) => {
                    return (
                      <FormItem>
                        <FormLabel>パスワード</FormLabel>
                        <FormControl>
                          <Input id="password" type="password" className="col-span-3" {...field} />
                        </FormControl>
                        <FormMessage />
                      </FormItem>
                    );
                  }}
                />
                <PasswordAlert error={error} />
              </div>
            </div>
            <DialogFooter>
              <div className="flex w-full items-center justify-end">
                <Button type="button" variant="secondary" onClick={onClose}>
                  キャンセル
                </Button>
                <Button type="submit" disabled={!form.formState.isValid}>
                  入室
                </Button>
              </div>
            </DialogFooter>
          </form>
        </Form>
        <DialogDescription className="sr-only">この部屋に入るためにはパスワードが必要です</DialogDescription>
      </DialogContent>
    </Dialog>
  );
});
