import { Checkbox } from '@/components/atoms/Checkbox.jsx';
import { FormControl, FormField, FormItem, FormLabel, FormMessage } from '@/components/atoms/Form.jsx';
import { Input } from '@/components/atoms/Input.jsx';
import { TabsContent } from '@/components/atoms/Tabs.jsx';
import { PasswordInput } from '@/features/RoomList/components/molecules/PasswordInput.jsx';
import { type FC, memo } from 'react';

type Props = Readonly<{
  usePassword: boolean;
  onCheckUsePassword(value: boolean): void;
}>;

export const BasicTab: FC<Props> = memo(({ usePassword, onCheckUsePassword }) => {
  return (
    <TabsContent value="basic" className="space-y-4 p-4">
      <FormField
        name="name"
        render={({ field }) => {
          return (
            <FormItem>
              <FormLabel>部屋名</FormLabel>
              <FormControl>
                <Input id="name" type="text" {...field} />
              </FormControl>
              <FormMessage />
            </FormItem>
          );
        }}
      />
      <PasswordInput onCheckUsePassword={onCheckUsePassword} required={usePassword} />
      <FormField
        name="quota"
        render={({ field }) => {
          return (
            <FormItem>
              <FormLabel>定員</FormLabel>
              <FormControl>
                <Input
                  id="quota"
                  type="number"
                  {...field}
                  onChange={(e) => {
                    const value = Number(e.target.value);

                    if (Number.isNaN(value)) {
                      field.onChange(0);

                      return;
                    }

                    field.onChange(value);
                  }}
                />
              </FormControl>
              <FormMessage />
            </FormItem>
          );
        }}
      />
      <FormField
        name="needsModerator"
        render={({ field }) => {
          return (
            <FormItem className="flex items-center space-x-2">
              <FormControl>
                <Checkbox id="needsModerator" checked={field.value} onCheckedChange={field.onChange} />
              </FormControl>
              <FormLabel className="!mt-0">私がモデレーターになります(モデレーターはゲームに参加できません)</FormLabel>
            </FormItem>
          );
        }}
      />
    </TabsContent>
  );
});
