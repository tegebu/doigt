import { TabsContent } from '@/components/atoms/Tabs.jsx';
import { Identity } from '@/domains/Identity/Identity.js';
import { RemainingVillagers } from '@/features/RoomList/components/atoms/RemainingVillagers.jsx';
import { RoleInput } from '@/features/RoomList/components/molecules/RoleInput.jsx';
import { type FC, memo } from 'react';

type Props = Readonly<{
  quota: number;
  totalRoles: number;
}>;

const roles: { [key: string]: Identity } = {
  seer: Identity.SEER,
  medium: Identity.MEDIUM,
  knight: Identity.KNIGHT,
  hunter: Identity.HUNTER,
  werewolf: Identity.WEREWOLF,
  informant: Identity.INFORMANT,
  kitsune: Identity.KITSUNE
};

export const RoleTab: FC<Props> = memo(({ quota, totalRoles }) => {
  return (
    <TabsContent value="roles" className="p-4">
      <div className="grid grid-cols-1 gap-4 sm:grid-cols-2 lg:grid-cols-3">
        {Object.entries(roles).map(([role, identity]) => {
          return <RoleInput key={role} identity={identity} role={role} />;
        })}
        <RemainingVillagers quota={quota} totalRoles={totalRoles} />
      </div>
    </TabsContent>
  );
});
