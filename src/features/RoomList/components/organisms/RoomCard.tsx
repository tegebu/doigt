import { Card, CardContent, CardDescription, CardFooter, CardHeader, CardTitle } from '@/components/atoms/Card.jsx';
import type { RoomList } from '@/domains/Room/Room.js';
import { JoinButton } from '@/features/RoomList/components/atoms/JoinButton.jsx';
import { PasswordRequired } from '@/features/RoomList/components/atoms/PasswordRequired.jsx';
import { StatusBadge } from '@/features/RoomList/components/molecules/StatusBadge.jsx';
import { User, Users } from 'lucide-react';
import { type FC, memo } from 'react';

type Props = Readonly<{
  room: RoomList;
  onClick(room: RoomList): void;
}>;

export const RoomCard: FC<Props> = memo(({ room, onClick }) => {
  return (
    <Card key={room.id}>
      <CardHeader>
        <div className="flex items-start justify-between">
          <CardTitle className="mr-2">{room.name}</CardTitle>
          <StatusBadge status={room.status} />
        </div>
        <CardDescription className="mt-2 flex items-center">
          <Users className="mr-1 h-4 w-4" />
          {room.participants}/{room.quota}
          <PasswordRequired passwordRequired={room.requirePassword} />
        </CardDescription>
      </CardHeader>
      <CardContent>
        <div className="flex items-center text-muted text-sm">
          <User className="mr-1 h-4 w-4" />
          {room.moderator ? room.moderator.name : '自動モデレート'}
        </div>
      </CardContent>
      <CardFooter>
        <JoinButton
          onClick={(e) => {
            e.preventDefault();
            onClick(room);
          }}
          room={room}
        />
      </CardFooter>
    </Card>
  );
});
