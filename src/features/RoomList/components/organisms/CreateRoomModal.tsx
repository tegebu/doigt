'use client';

import { RoomSettings } from '@/applications/dtos/Role/RoleSettings.js';
import { Button } from '@/components/atoms/Button.jsx';
import { Dialog, DialogContent, DialogDescription, DialogFooter, DialogHeader, DialogTitle } from '@/components/atoms/Dialog.jsx';
import { Form } from '@/components/atoms/Form.jsx';
import { Tabs, TabsList, TabsTrigger } from '@/components/atoms/Tabs.jsx';
import { ScrollArea } from '@/components/molecules/ScrollArea.jsx';
import { Client } from '@/domains/Client/Client.js';
import { RoleSummary } from '@/features/RoomList/components/atoms/RoleSummary.jsx';
import { BasicTab } from '@/features/RoomList/components/organisms/BasicTab.jsx';
import { RoleTab } from '@/features/RoomList/components/organisms/RoleTab.jsx';
import { zodResolver } from '@hookform/resolvers/zod';
import { ExhaustiveError } from '@jamashita/anden/error';
import { Kind } from '@jamashita/anden/type';
import { type FC, memo, useEffect, useState } from 'react';
import { useForm } from 'react-hook-form';

type Props = Readonly<{
  isOpen: boolean;
  hostID: string;
  onClose(): void;
  onSubmit(data: RoomSettings): void;
}>;

export const CreateRoomModal: FC<Props> = memo(({ isOpen, onClose, onSubmit }) => {
  const [usePassword, setUsePassword] = useState(false);
  const [isValid, setIsValid] = useState(false);
  const form = useForm<RoomSettings>({
    resolver: zodResolver(RoomSettings.SCHEMA),
    defaultValues: {
      // これはダミーの値です
      hostID: Client.ID.from('ffa48893-4d12-4dd2-97b4-b3eb4e9da9a7'),
      name: '',
      password: '',
      quota: 8,
      needsModerator: false,
      roleAssignment: {
        atonement: {
          type: 'SINGLE',
          value: 0
        },
        catalyst: {
          type: 'SINGLE',
          value: 0
        },
        coward: {
          type: 'SINGLE',
          value: 0
        },
        hunter: {
          type: 'SINGLE',
          value: 0
        },
        informant: {
          type: 'SINGLE',
          value: 0
        },
        jester: {
          type: 'SINGLE',
          value: 0
        },
        kitsune: {
          type: 'SINGLE',
          value: 0
        },
        knight: {
          type: 'SINGLE',
          value: 0
        },
        medium: {
          type: 'SINGLE',
          value: 0
        },
        monastic: {
          type: 'SINGLE',
          value: 0
        },
        orator: {
          type: 'SINGLE',
          value: 0
        },
        possessed: {
          type: 'SINGLE',
          value: 0
        },
        seer: {
          type: 'SINGLE',
          value: 0
        },
        werewolf: {
          type: 'SINGLE',
          value: 1
        }
      }
    }
  });

  const password = form.watch('password');
  const quota = form.watch('quota');
  const roleAssignment = form.watch('roleAssignment');
  const totalRoles = Object.values(roleAssignment).reduce((sum, assignment) => {
    switch (assignment.type) {
      case 'SINGLE': {
        return sum + assignment.value;
      }
      case 'RANGE': {
        return sum + assignment.min;
      }
      case 'COMPLEX': {
        return sum + Math.min(...assignment.values);
      }
      default: {
        throw new ExhaustiveError(assignment);
      }
    }
  }, 0);

  useEffect(() => {
    if (usePassword && !Kind.isNull(password)) {
      setIsValid(form.formState.isValid && password.length > 0);

      return;
    }

    form.setValue('password', '');
    setIsValid(form.formState.isValid);
  }, [usePassword, form.formState.isValid, form.setValue, password]);

  return (
    <Dialog open={isOpen} onOpenChange={onClose}>
      <DialogContent className="flex h-[90vh] flex-col sm:max-w-[600px] md:max-w-[700px] lg:max-w-[900px]" closeButton closeOnESC closeOnOutsideClick>
        <DialogHeader>
          <DialogTitle>新しい部屋を作成</DialogTitle>
        </DialogHeader>
        <div className="space-x-2">
          <Form {...form}>
            <form onSubmit={form.handleSubmit(onSubmit)} className="flex-1 overflow-hidden">
              <Tabs defaultValue="basic" className="flex h-full flex-col">
                <TabsList className="grid w-full grid-cols-2">
                  <TabsTrigger value="basic">基本設定</TabsTrigger>
                  <TabsTrigger value="roles">役職設定</TabsTrigger>
                </TabsList>
                <ScrollArea className="h-[calc(90vh-180px)]">
                  <BasicTab usePassword={usePassword} onCheckUsePassword={setUsePassword} />
                  <RoleTab quota={quota} totalRoles={totalRoles} />
                </ScrollArea>
              </Tabs>
              <DialogFooter className="mt-4">
                <div className="flex w-full items-center justify-between">
                  <RoleSummary quota={quota} totalRoles={totalRoles} />
                  <Button type="submit" disabled={!isValid}>
                    作成
                  </Button>
                </div>
              </DialogFooter>
            </form>
          </Form>
        </div>
        <DialogDescription className="sr-only">新しい部屋を作成します</DialogDescription>
      </DialogContent>
    </Dialog>
  );
});
