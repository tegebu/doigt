import { Button } from '@/components/atoms/Button.jsx';
import type { RoomList } from '@/domains/Room/Room.js';
import { type FC, type MouseEvent, memo, useMemo } from 'react';

type Props = Readonly<{
  room: RoomList;
  onClick(e: MouseEvent<HTMLButtonElement>): void;
}>;

export const JoinButton: FC<Props> = memo(({ room, onClick }) => {
  const disabled = useMemo(() => {
    if (room.status !== 'OPEN') {
      return true;
    }

    return room.participants >= room.quota;
  }, [room]);

  return (
    <Button variant="outline" className="w-full" disabled={disabled} onClick={onClick}>
      参加する
    </Button>
  );
});
