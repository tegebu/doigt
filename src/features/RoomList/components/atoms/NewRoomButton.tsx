import { Button } from '@/components/atoms/Button.jsx';
import { Plus } from 'lucide-react';
import { type FC, memo, type MouseEvent } from 'react';

type Props = Readonly<{
  onClick(e: MouseEvent<HTMLButtonElement>): void;
}>;

export const NewRoomButton: FC<Props> = memo(({ onClick }) => {
  return (
    <Button onClick={onClick}>
      <Plus className="mr-2 h-4 w-4" />
      新しい部屋を作成
    </Button>
  );
});
