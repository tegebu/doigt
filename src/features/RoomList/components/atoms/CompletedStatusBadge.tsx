import { Badge } from '@/components/atoms/Badge.jsx';
import { type FC, memo } from 'react';

export const CompletedStatusBadge: FC = memo(() => {
  return <Badge className="bg-fuchsia-200 text-fuchsia-700">終了</Badge>;
});
