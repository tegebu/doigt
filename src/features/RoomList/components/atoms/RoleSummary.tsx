import { type FC, memo } from 'react';

type Props = Readonly<{
  totalRoles: number;
  quota: number;
}>;

export const RoleSummary: FC<Props> = memo(({ totalRoles, quota }) => {
  if (totalRoles < 0) {
    return <p className="text-destructive">合計役職数: 0 / 定員: {quota}</p>;
  }
  if (quota >= totalRoles) {
    return (
      <p className="text-mute">
        合計役職数: {totalRoles} / 定員: {quota}
      </p>
    );
  }

  return (
    <p className="text-destructive">
      合計役職数: {totalRoles} / 定員: {quota}
    </p>
  );
});
