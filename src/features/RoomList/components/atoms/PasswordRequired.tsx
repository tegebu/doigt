import { Lock } from 'lucide-react';
import { type FC, type Ref, memo } from 'react';

type Props = Readonly<{
  passwordRequired: boolean;
  ref?: Ref<SVGSVGElement>;
}>;

export const PasswordRequired: FC<Props> = memo(({ passwordRequired, ref }) => {
  if (!passwordRequired) {
    return null;
  }

  return <Lock className="ml-2 h-4 w-4" ref={ref} />;
});
