import { type FC, memo } from 'react';

type Props = Readonly<{
  quota: number;
  totalRoles: number;
}>;

export const RemainingVillagers: FC<Props> = memo(({ quota, totalRoles }) => {
  if (quota < 0 || totalRoles < 0) {
    return null;
  }
  if (totalRoles > quota) {
    return (
      <div className="flex items-center justify-between text-destructive">
        <span>村人</span>
        <span>0</span>
      </div>
    );
  }

  return (
    <div className="flex items-center justify-between">
      <span>村人</span>
      <span>{quota - totalRoles}</span>
    </div>
  );
});
