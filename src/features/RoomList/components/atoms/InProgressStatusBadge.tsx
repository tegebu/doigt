import { Badge } from '@/components/atoms/Badge.jsx';
import { type FC, memo } from 'react';

export const InProgressStatusBadge: FC = memo(() => {
  return <Badge className="bg-indigo-200 text-indigo-800">進行中</Badge>;
});
