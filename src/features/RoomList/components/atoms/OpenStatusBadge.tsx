import { Badge } from '@/components/atoms/Badge.jsx';
import { type FC, memo } from 'react';

export const OpenStatusBadge: FC = memo(() => {
  return <Badge className="bg-sky-200 text-sky-700">募集中</Badge>;
});
