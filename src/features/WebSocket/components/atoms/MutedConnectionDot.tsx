import { type FC, memo } from 'react';

export const MutedConnectionDot: FC = memo(() => {
  return <div className="h-2 w-2 animate-pulse rounded-full bg-gray-500 opacity-75 transition-colors duration-1000" aria-hidden="true" />;
});
