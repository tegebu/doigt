'use client';

import { DangerConnectionDot } from '@/features/WebSocket/components/atoms/DangerConnectionDot.jsx';
import { MutedConnectionDot } from '@/features/WebSocket/components/atoms/MutedConnectionDot.jsx';
import { SuccessConnectionDot } from '@/features/WebSocket/components/atoms/SuccessConnectionDot.jsx';
import { WarningConnectionDot } from '@/features/WebSocket/components/atoms/WarningConnectionDot.jsx';
import { useContainer } from '@/store/ContainerContext.jsx';
import { ExhaustiveError } from '@jamashita/anden/error';
import { type FC, memo } from 'react';

export const ConnectionDot: FC = memo(() => {
  const {
    connection: { status }
  } = useContainer();

  switch (status) {
    case 'CONNECTED': {
      return <SuccessConnectionDot />;
    }
    case 'CONNECTING': {
      return <WarningConnectionDot />;
    }
    case 'DISCONNECTED': {
      return <MutedConnectionDot />;
    }
    case 'ERROR': {
      return <DangerConnectionDot />;
    }
    default: {
      throw new ExhaustiveError(status);
    }
  }
});
