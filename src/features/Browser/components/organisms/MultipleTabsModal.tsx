'use client';

import { Dialog, DialogContent, DialogDescription, DialogHeader, DialogTitle } from '@/components/atoms/Dialog.jsx';
import { useContainer } from '@/store/ContainerContext.jsx';
import { type FC, memo } from 'react';

export const MultipleTabsModal: FC = memo(() => {
  const {
    tabDetector: { activeTabCount }
  } = useContainer();

  return (
    <Dialog modal open={activeTabCount > 1}>
      <DialogContent
        className="space-y-4"
        onOpenAutoFocus={(e) => {
          e.preventDefault();
        }}
      >
        <DialogHeader>
          <DialogTitle className="font-semibold text-lg">複数タブで開いています</DialogTitle>
        </DialogHeader>
        <div className="space-y-2">
          このアプリケーションは複数タブでの利用をサポートしていません。 既存のタブをご利用いただくか、他のタブをすべて閉じてください。
        </div>
        <DialogDescription className="sr-only">複数タブを開いているときに表示されます</DialogDescription>
      </DialogContent>
    </Dialog>
  );
});
