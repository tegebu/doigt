'use client';

import { Button } from '@/components/atoms/Button.jsx';
import { Dialog, DialogContent, DialogDescription, DialogFooter, DialogHeader, DialogTitle } from '@/components/atoms/Dialog.jsx';
import { type FC, memo, type MouseEvent, useCallback, useEffect, useState } from 'react';

type Props = Readonly<{
  isOpen: boolean;
  onClose(): void;
  onConfirm(): void;
}>;

export const LogoutConfirmModal: FC<Props> = memo(({ isOpen, onClose, onConfirm }) => {
  const [open, setOpen] = useState(isOpen);

  const handleClose = useCallback(
    (e: MouseEvent<HTMLButtonElement>) => {
      e.preventDefault();
      setOpen(false);
      onClose();
    },
    [onClose]
  );
  const handleConfirm = useCallback(
    (e: MouseEvent<HTMLButtonElement>) => {
      e.preventDefault();
      setOpen(false);
      onConfirm();
    },
    [onConfirm]
  );

  useEffect(() => {
    setOpen(isOpen);
  }, [isOpen]);

  return (
    <Dialog open={open} onOpenChange={setOpen}>
      <DialogContent>
        <DialogHeader>
          <DialogTitle>ログアウトしますか？</DialogTitle>
          <DialogDescription>ゲーム進行中のログアウトは他のかたの迷惑になります。おすすめしません。</DialogDescription>
        </DialogHeader>
        <DialogFooter>
          <Button variant="outline" onClick={handleClose}>
            キャンセル
          </Button>
          <Button variant="destructive" onClick={handleConfirm}>
            ログアウト
          </Button>
        </DialogFooter>
      </DialogContent>
    </Dialog>
  );
});
