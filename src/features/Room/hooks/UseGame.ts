import { PUBLISH } from '@/applications/websocket/PublishEvents.js';
import type { Person, PersonBase, PersonID } from '@/domains/Person/Person.js';
import { DayPhase } from '@/domains/Room/DayPhase.js';
import type { UseWSEvent } from '@/hooks/UseWSEvent.js';
import { Kind, type Nullable } from '@jamashita/anden/type';
import { useCallback, useEffect, useRef, useState } from 'react';

export const useGame = ({ send, subscribe, unsubscribe }: UseWSEvent) => {
  const isSubscribed = useRef(false);
  const [you, setYou] = useState<Nullable<Person>>(null);
  const [residents, setResidents] = useState<ReadonlyMap<PersonID, PersonBase>>([]);
  const [survivors, setSurvivors] = useState<ReadonlyMap<PersonID, PersonBase>>([]);
  const [deceased, setDeceased] = useState<ReadonlyMap<PersonID, PersonBase>>([]);
  const [dayPhase, setDayPhase] = useState<DayPhase>(DayPhase.init());

  const cleanup = useCallback((): void => {
    if (!isSubscribed.current) {
      return;
    }

    unsubscribe(PUBLISH.GAME.START);
    unsubscribe(PUBLISH.GAME.OVER);
  }, [unsubscribe]);

  const setupSubscriptions = useCallback(() => {
    if (isSubscribed.current) {
      return;
    }

    subscribe(PUBLISH.GAME.START, () => {
      // TODO
    });
    subscribe(PUBLISH.GAME.OVER, ({ teams, records }) => {
      // TODO
    });
    subscribe(PUBLISH.ROLE.ASSIGNED, ({ you, others }) => {
      setYou(you);
      setResidents(others);
    });
    subscribe(PUBLISH.ROLE.REVEAL_ALL, ({ residents }) => {
      // TODO
    });
    subscribe(PUBLISH.MORNING.START, () => {
      setDayPhase(DayPhase.next(dayPhase));
    });
    subscribe(PUBLISH.MORNING.END, () => {
      // TODO
    });
    subscribe(PUBLISH.DAY.START, () => {
      setDayPhase(DayPhase.next(dayPhase));
    });
    subscribe(PUBLISH.DAY.END, () => {
      // TODO
    });
    subscribe(PUBLISH.EVENING.START, () => {
      setDayPhase(DayPhase.next(dayPhase));
    });
    subscribe(PUBLISH.EVENING.END, () => {
      // TODO
    });
    subscribe(PUBLISH.NIGHT.START, () => {
      setDayPhase(DayPhase.next(dayPhase));
    });
    subscribe(PUBLISH.NIGHT.END, () => {
      // TODO
    });
    subscribe(PUBLISH.RESIDENT.LIST, ({ survivors, deceased }) => {
      const s = survivors
        .map((personID) => {
          return residents.get(personID);
        })
        .filter((person): person is PersonBase => {
          return !Kind.isUndefined(person);
        });

      const survivorMap = new Map<PersonID, PersonBase>();

      for (const person of s) {
        survivorMap.set(person.id, person);
      }

      setSurvivors(survivorMap);

      const d = deceased
        .map((personID) => {
          return residents.get(personID);
        })
        .filter((person): person is PersonBase => {
          return !Kind.isUndefined(person);
        });

      const deceasedMap = new Map<PersonID, PersonBase>();

      for (const person of d) {
        deceasedMap.set(person.id, person);
      }

      setDeceased(deceasedMap);
    });
    subscribe(PUBLISH.VICTIMIZED, ({ outcome }) => {
      const victims = outcome.id
        .map((personID) => {
          return residents.get(personID);
        })
        .filter((person): person is PersonBase => {
          return !Kind.isUndefined(person);
        });

      const newSurvivors = new Map(survivors);

      for (const victim of victims) {
        newSurvivors.delete(victim.id);
      }

      setSurvivors(newSurvivors);

      const newDeceased = new Map(deceased);

      for (const victim of victims) {
        newDeceased.set(victim.id, victim);
      }

      setDeceased(newDeceased);
    });
    subscribe(PUBLISH.LYNCHED.NONE, () => {
      // TODO
    });
    subscribe(PUBLISH.LYNCHED.SOME, ({ victimID }) => {
      const victim = residents.get(victimID);

      if (Kind.isUndefined(victim)) {
        return;
      }

      const newSurvivors = new Map(survivors);

      newSurvivors.delete(victim.id);

      setSurvivors(newSurvivors);

      const newDeceased = new Map(deceased);

      newDeceased.set(victim.id, victim);

      setDeceased(newDeceased);
    });
    subscribe(PUBLISH.VOTE.SET.SUCCESS, ({ sourceID, destinationID }) => {
      // TODO
    });
    subscribe(PUBLISH.VOTE.SET.FAILURE, ({ cause, sourceID, destinationID }) => {
      // TODO
    });

    isSubscribed.current = true;
  }, [subscribe, dayPhase]);

  useEffect(() => {
    setupSubscriptions();

    return () => {
      cleanup();
    };
  }, [cleanup, setupSubscriptions]);

  return {
    dayPhase,
    deceased,
    residents,
    survivors,
    you
  };
};
