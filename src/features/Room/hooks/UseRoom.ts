import type { GatewayError } from '@/adapters/gateways/GatewayError.js';
import { PUBLISH } from '@/applications/websocket/PublishEvents.js';
import { SUBMIT } from '@/applications/websocket/SubmitEvents.js';
import type { RoomID } from '@/domains/Room/Room.js';
import type { UseClient } from '@/hooks/UseClient.js';
import type { UseWerewolf } from '@/hooks/UseWerewolf.js';
import type { UseWSEvent } from '@/hooks/UseWSEvent.js';
import type { GenericError } from '@/lib/Error/Errors.js';
import type { ParseError } from '@/lib/Error/ParseError.js';
import type { Either } from 'fp-ts/lib/Either.js';
import { pipe } from 'fp-ts/lib/function.js';
import { bindW, Do, fromEither, right } from 'fp-ts/lib/TaskEither.js';
import { useCallback, useEffect, useRef } from 'react';

export const useRoom = ({ find: findClient }: UseClient, { send, subscribe, unsubscribe }: UseWSEvent, { delete: deleteWerewolf }: UseWerewolf) => {
  const isSubscribed = useRef(false);

  const cleanup = useCallback((): void => {
    if (!isSubscribed.current) {
      return;
    }

    unsubscribe(PUBLISH.ROOM.DESTROY.SUCCESS);
    unsubscribe(PUBLISH.ROOM.DESTROY.FAILURE);
    unsubscribe(PUBLISH.ROOM.LEAVE.SUCCESS);
    unsubscribe(PUBLISH.ROOM.LEAVE.FAILURE);
    unsubscribe(PUBLISH.ROOM.PARTICIPANT.JOINED);
    unsubscribe(PUBLISH.ROOM.PARTICIPANT.LEFT);
    unsubscribe(PUBLISH.ROOM.DISBAND);
    unsubscribe(PUBLISH.ROOM.FETCH);

    isSubscribed.current = false;
  }, [unsubscribe]);

  const fetch = useCallback(
    (roomID: RoomID): Promise<Either<GenericError | GatewayError | ParseError, unknown>> => {
      return pipe(
        Do,
        bindW('client', () => {
          return pipe(findClient(), fromEither);
        }),
        bindW(
          'rooms',
          ({ client }) =>
            () =>
              send(SUBMIT.ROOM.FETCH, {
                clientID: client.id,
                roomID
              })
        )
      )();
    },
    [findClient, send]
  );

  const setupSubscriptions = useCallback(() => {
    if (isSubscribed.current) {
      return;
    }

    subscribe(PUBLISH.ROOM.DESTROY.SUCCESS, () => {
      return right(null)();
    });
    subscribe(PUBLISH.ROOM.DESTROY.FAILURE, () => {
      return right(null)();
    });
    subscribe(PUBLISH.ROOM.LEAVE.SUCCESS, () => {
      deleteWerewolf();
    });
    subscribe(PUBLISH.ROOM.LEAVE.FAILURE, ({ cause }) => {
      return right(null)();
    });
    subscribe(PUBLISH.ROOM.PARTICIPANT.JOINED, ({ client }) => {
      return right(null)();
    });
    subscribe(PUBLISH.ROOM.PARTICIPANT.LEFT, ({ clientID }) => {
      return right(null)();
    });
    subscribe(PUBLISH.ROOM.DISBAND, () => {
      return right(null)();
    });
    subscribe(PUBLISH.ROOM.FETCH, () => {
      return right(null)();
    });

    isSubscribed.current = true;
  }, [subscribe, deleteWerewolf]);

  useEffect(() => {
    setupSubscriptions();

    return () => {
      cleanup();
    };
  }, [cleanup, setupSubscriptions]);

  return {
    fetch
  };
};

export type UseRoom = ReturnType<typeof useRoom>;
