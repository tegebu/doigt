'use client';

import { Clock } from 'lucide-react';
import { type FC, memo, useCallback, useEffect, useState } from 'react';

type Props = Readonly<{
  seconds: number;
}>;

export const Timer: FC<Props> = memo(({ seconds }) => {
  const [timeLeft, setTimeLeft] = useState(0);

  const formatTime = useCallback((seconds: number) => {
    const minutes = Math.floor(seconds / 60);
    const remainingSeconds = seconds % 60;

    return `${minutes.toString().padStart(2, '0')}:${remainingSeconds.toString().padStart(2, '0')}`;
  }, []);

  useEffect(() => {
    setTimeLeft(seconds);

    const timer = setInterval(() => {
      setTimeLeft((prevTime) => {
        if (prevTime === 0) {
          clearInterval(timer);

          return 0;
        }

        return prevTime - 1;
      });
    }, 1000);

    return () => {
      clearInterval(timer);
    };
  }, [seconds]);

  if (timeLeft === 0) {
    return (
      <div className="flex items-center text-destructive text-xl">
        <Clock className="mr-2" />
        {formatTime(timeLeft)}
      </div>
    );
  }

  return (
    <div className="flex items-center text-xl">
      <Clock className="mr-2" />
      {formatTime(timeLeft)}
    </div>
  );
});
