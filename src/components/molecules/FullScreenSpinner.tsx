import { Loader2 } from 'lucide-react';
import { type FC, memo } from 'react';

type Props = Readonly<{
  isLoading: boolean;
}>;

export const FullScreenSpinner: FC<Props> = memo(({ isLoading }) => {
  if (!isLoading) {
    return null;
  }

  return (
    <div className="fixed inset-0 z-50 flex items-center justify-center bg-black/80 backdrop-blur-sm" aria-busy="true" aria-label="読み込み中">
      <Loader2 className="h-16 w-16 animate-spin text-slate-50" />
    </div>
  );
});
