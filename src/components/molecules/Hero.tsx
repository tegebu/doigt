import { Button } from '@/components/atoms/Button.jsx';
import type { FC, MouseEvent } from 'react';

type Props = Readonly<{
  onLogin(e: MouseEvent<HTMLButtonElement>): void;
}>;

export const Hero: FC<Props> = ({ onLogin }) => {
  return (
    <div className="bg-background py-20">
      <div className="container mx-auto text-center">
        <h1 className="mb-4 font-bold text-4xl">汝は大次狼なりや？</h1>
        <p className="mb-8 text-xl">友達と一緒に楽しむ、オンライン人狼ゲーム</p>
        <Button onClick={onLogin} size="lg">
          ゲームを始める
        </Button>
      </div>
    </div>
  );
};
