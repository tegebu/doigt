'use client';

import { Button } from '@/components/atoms/Button.jsx';
import { LogoutConfirmModal } from '@/features/Header/organisms/LogoutConfirmModal.jsx';
import { TitleConfirmModal } from '@/features/Header/organisms/TitleConfirmModal.jsx';
import { useContainer } from '@/store/ContainerContext.jsx';
import { pipe } from 'fp-ts/lib/function.js';
import { fold } from 'fp-ts/lib/Option.js';
import { bindW, Do, fromEither, right } from 'fp-ts/lib/TaskEither.js';
import Link from 'next/link.js';
import { type FC, memo, type MouseEvent, useCallback, useState } from 'react';

export const Header: FC = memo(() => {
  const {
    client: { delete: deleteClient, find: findClient },
    connection: { disconnect },
    loading: { end: endLoading, start: startLoading },
    router: { push },
    toast: { toast },
    werewolf: { find: findWerewolf }
  } = useContainer();
  const [isTitleConfirmModalOpen, setIsTitleConfirmModalOpen] = useState(false);
  const [isLogoutConfirmModalOpen, setIsLogoutConfirmModalOpen] = useState(false);

  const handleCloseTitleConfirm = useCallback((): void => {
    setIsLogoutConfirmModalOpen(false);
  }, []);
  const handleConfirmTitleConfirm = useCallback((): void => {
    // ここでゲームを終了し、ホームページなどに遷移する
    push('/');
  }, [push]);
  const handleCloseLogoutConfirm = useCallback((): void => {
    setIsLogoutConfirmModalOpen(false);
  }, []);
  const handleConfirmLogoutConfirm = useCallback((): void => {
    startLoading();

    toast({
      title: '接続を断ちます',
      description: 'ログアウトしまーす',
      type: 'foreground',
      duration: 3_000
    });

    pipe(
      Do,
      bindW('res1', () => () => disconnect()),
      bindW('res2', () => {
        return pipe(deleteClient(), fromEither);
      }),
      bindW('res3', () => {
        endLoading();
        push('/');

        return right(null);
      })
    )();
  }, [disconnect, deleteClient, endLoading, startLoading, push, toast]);
  const handleClickTitle = useCallback(
    (e: MouseEvent<HTMLAnchorElement>): void => {
      e.preventDefault();

      pipe(
        Do,
        bindW('client', () => {
          return pipe(findClient(), fromEither);
        }),
        bindW('option', () => () => findWerewolf()),
        bindW('res1', ({ client, option }) => {
          return pipe(
            option,
            fold(
              () => {
                console.log('普通の退出');
                push('/');

                return right(null);
              },
              (werewolf) => {
                if (client.id === werewolf.moderatorID) {
                  console.log('モデレーターが退出');
                  setIsTitleConfirmModalOpen(true);

                  return right(null);
                }

                console.log('モデレーター以外が退出');
                setIsTitleConfirmModalOpen(true);

                return right(null);
              }
            )
          );
        })
      )();
    },
    [findClient, findWerewolf, push]
  );
  const handleLogout = useCallback(
    (e: MouseEvent<HTMLButtonElement>): void => {
      e.preventDefault();

      pipe(
        Do,
        bindW('option', () => () => findWerewolf()),
        bindW('res1', ({ option }) => {
          return pipe(
            option,
            fold(
              () => {
                handleConfirmLogoutConfirm();

                return right(null);
              },
              // werewolfが存在する = ゲームが進行中である
              () => {
                setIsLogoutConfirmModalOpen(true);

                return right(null);
              }
            )
          );
        })
      )();
    },
    [findWerewolf, handleConfirmLogoutConfirm]
  );

  return (
    <header className="bg-primary text-primary-foreground shadow-md">
      <div className="container mx-auto flex items-center justify-between px-4 py-3">
        <h1 className="font-bold text-2xl">
          <Link href="/" onClick={handleClickTitle}>
            汝は大次狼なりや？
          </Link>
        </h1>
        <Button onClick={handleLogout} variant="secondary" className="hover:bg-primary-foreground hover:text-primary">
          ログアウト
        </Button>
      </div>
      <TitleConfirmModal isOpen={isTitleConfirmModalOpen} onClose={handleCloseTitleConfirm} onConfirm={handleConfirmTitleConfirm} />
      <LogoutConfirmModal isOpen={isLogoutConfirmModalOpen} onClose={handleCloseLogoutConfirm} onConfirm={handleConfirmLogoutConfirm} />
    </header>
  );
});
