import { Card, CardContent, CardDescription, CardHeader, CardTitle } from '@/components/atoms/Card.jsx';
import { type FC, memo } from 'react';

const features = [
  {
    title: 'リアルタイム人狼',
    description: '友達や見知らぬ人とリアルタイムで対戦できます。'
  },
  {
    title: '多彩な役職',
    description: '村人、人狼、予言者など、様々な役職で楽しめます。'
  },
  {
    title: 'カスタマイズ可能',
    description: 'ゲームルールをカスタマイズして、自分たち好みの設定で遊べます。'
  }
];

export const Features: FC = memo(() => {
  return (
    <div className="bg-muted py-20">
      <div className="container mx-auto">
        <h2 className="mb-10 text-center font-bold text-3xl">ゲームの特徴</h2>
        <div className="grid grid-cols-1 gap-8 md:grid-cols-3">
          {features.map((feature) => (
            <Card key={feature.title}>
              <CardHeader>
                <CardTitle>{feature.title}</CardTitle>
              </CardHeader>
              <CardContent>
                <CardDescription>{feature.description}</CardDescription>
              </CardContent>
            </Card>
          ))}
        </div>
      </div>
    </div>
  );
});
