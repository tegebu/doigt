import type { UseWSEvent } from '../UseWSEvent.js';

export const mockUseWSEvent = () => {
  return {
    close: vi.fn(),
    connect: vi.fn(),
    send: vi.fn(),
    subscribe: vi.fn(),
    unsubscribe: vi.fn()
  } satisfies UseWSEvent;
};
