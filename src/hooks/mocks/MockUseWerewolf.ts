import type { UseWerewolf } from '@/hooks/UseWerewolf.js';

export const mockUseWerewolf = () => {
  return {
    create: vi.fn(),
    delete: vi.fn(),
    find: vi.fn(),
    get: vi.fn()
  } satisfies UseWerewolf;
};
