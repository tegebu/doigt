import type { UseClient } from '../UseClient.js';

export const mockUseClient = () => {
  return {
    create: vi.fn(),
    delete: vi.fn(),
    find: vi.fn(),
    get: vi.fn(),
    update: vi.fn()
  } satisfies UseClient;
};
