import type { UseWebSocket } from '../UseWebSocket.js';

export const mockUseWebSocket = () => {
  return {
    close: vi.fn(),
    connect: vi.fn(),
    send: vi.fn(),
    subscribe: vi.fn()
  } satisfies UseWebSocket;
};
