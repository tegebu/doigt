import type { UseConnection } from '../UseConnection.js';

export const mockUseConnection = () => {
  return {
    status: 'DISCONNECTED',
    connect: vi.fn(),
    disconnect: vi.fn(),
    reconnect: vi.fn()
  } satisfies UseConnection;
};
