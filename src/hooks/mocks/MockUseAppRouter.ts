import type { UseAppRouter } from '../UseAppRouter.js';

export const mockUseAppRouter = () => {
  return {
    path: '',
    back: vi.fn(),
    forward: vi.fn(),
    push: vi.fn(),
    reload: vi.fn()
  } satisfies UseAppRouter;
};
