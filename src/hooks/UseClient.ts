import { createGatewayError, type GatewayError } from '@/adapters/gateways/GatewayError.js';
import { Client } from '@/domains/Client/Client.js';
import { createThrownError, createUnknownError, type GenericError } from '@/lib/Error/Errors.js';
import { Kind } from '@jamashita/anden/type';
import { Zeit } from '@jamashita/anden/zeit';
import { type Either, isRight, left, right } from 'fp-ts/lib/Either.js';
import { none, type Option, some } from 'fp-ts/lib/Option.js';
import { useCallback, useState } from 'react';
import { z } from 'zod';

type AuthenticatedClientState = Readonly<{
  status: 'AUTHENTICATED';
  client: Client;
}>;
type UnauthenticatedClientState = Readonly<{
  status: 'UNAUTHENTICATED';
}>;
type LoadingClientState = Readonly<{
  status: 'LOADING';
}>;
type ClientState = AuthenticatedClientState | UnauthenticatedClientState | LoadingClientState;

const CLIENT_KEY = 'b2f19648-8fd8-4bce-862f-e82501e75ee9';
const DATETIME_FORMAT = 'YYYY-MM-DD HH:mm:ss';
// 5日有効とする
const LIFESPAN = 5;

const ValidClientSchema = z.object({
  client: Client.SCHEMA,
  expirationDate: z.string()
});

type ValidClient = Readonly<z.infer<typeof ValidClientSchema>>;

export const useClient = () => {
  const [state, setState] = useState<ClientState>({ status: 'LOADING' });

  const create = useCallback((client: Client): Either<GenericError, unknown> => {
    const obj = {
      client,
      expirationDate: Zeit.now().postpone(LIFESPAN, 'day').toString(DATETIME_FORMAT)
    } satisfies ValidClient;

    try {
      const str = JSON.stringify(obj);

      localStorage.setItem(CLIENT_KEY, str);
      setState({
        status: 'AUTHENTICATED',
        client
      });

      return right(null);
    } catch (e: unknown) {
      if (e instanceof Error) {
        return left(createThrownError(e));
      }

      return left(createUnknownError(e));
    }
  }, []);

  const del = useCallback((): Either<GenericError, unknown> => {
    try {
      localStorage.removeItem(CLIENT_KEY);
      setState({
        status: 'UNAUTHENTICATED'
      });

      return right(null);
    } catch (e: unknown) {
      if (e instanceof Error) {
        return left(createThrownError(e));
      }

      return left(createUnknownError(e));
    }
  }, []);

  const find = useCallback((): Either<GenericError | GatewayError, Client> => {
    try {
      const data = localStorage.getItem(CLIENT_KEY);

      if (Kind.isNull(data)) {
        setState({
          status: 'UNAUTHENTICATED'
        });

        return left(createGatewayError('EntityNotFound', 'クライアントが存在しません'));
      }

      const obj = JSON.parse(data);
      const result = ValidClientSchema.safeParse(obj);

      if (!result.success) {
        del();

        return left(createGatewayError('EntityNotFound', '有効なクライアントではありません'));
      }
      // 有効期限が切れていたら削除
      if (Zeit.ofString(result.data.expirationDate, DATETIME_FORMAT).isBefore(Zeit.now())) {
        del();

        return left(createGatewayError('EntityNotFound', 'クライアントの有効期限が切れています'));
      }

      setState({
        status: 'AUTHENTICATED',
        client: result.data.client
      });

      return right(result.data.client);
    } catch (e: unknown) {
      if (e instanceof Error) {
        return left(createThrownError(e));
      }

      return left(createUnknownError(e));
    }
  }, [del]);

  // biome-ignore lint/correctness/useExhaustiveDependencies: <explanation>
  const get = useCallback((): Option<Client> => {
    if (state.status === 'AUTHENTICATED') {
      return some(state.client);
    }

    const either = find();

    if (isRight(either)) {
      return some(either.right);
    }

    return none;
  }, [state.status, find]);

  const update = useCallback((client: Client): Either<GenericError | GatewayError, unknown> => {
    const obj = {
      client,
      expirationDate: Zeit.now().postpone(LIFESPAN, 'day').toString(DATETIME_FORMAT)
    } satisfies ValidClient;

    try {
      const str = JSON.stringify(obj);

      localStorage.setItem(CLIENT_KEY, str);
      setState({
        status: 'AUTHENTICATED',
        client
      });

      return right(null);
    } catch (e: unknown) {
      if (e instanceof Error) {
        return left(createThrownError(e));
      }

      return left(createUnknownError(e));
    }
  }, []);

  return {
    create,
    delete: del,
    find,
    get,
    update
  };
};

export type UseClient = ReturnType<typeof useClient>;
