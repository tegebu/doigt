import { PUBLISH } from '@/applications/websocket/PublishEvents.js';
import { Kind, type Nullable, type UnaryFunction } from '@jamashita/anden/type';
import { Delay } from '@jamashita/steckdose/delay';
import { JSONA } from '@jamashita/steckdose/json';
import { useCallback, useRef } from 'react';

type WebSocketStatus = 'CONNECTING' | 'CONNECTED' | 'DISCONNECTED' | 'ERROR';

const RECONNECT_INTERVAL = 1000;
const MAX_RECONNECT_ATTEMPTS = 5;
const MAX_RECONNECT_INTERVAL = 30000;

export const useWebSocket = (url: string) => {
  const status = useRef<WebSocketStatus>('DISCONNECTED');
  const ws = useRef<Nullable<WebSocket>>(null);
  const emit = useRef<Nullable<UnaryFunction<Pick<MessageEvent, 'data'>, unknown>>>(null);
  const reconnectAttempts = useRef<number>(0);
  const reconnectTimeout = useRef<Nullable<NodeJS.Timeout>>(null);

  const close = useCallback((): void => {
    if (status.current === 'DISCONNECTED') {
      return;
    }

    emit.current = null;

    if (reconnectTimeout.current) {
      clearTimeout(reconnectTimeout.current);

      reconnectTimeout.current = null;
    }

    if (!Kind.isNull(ws.current)) {
      ws.current.onclose = null;
      ws.current.onerror = null;
      ws.current.onmessage = null;

      ws.current.close();

      ws.current = null;
    }

    status.current = 'DISCONNECTED';
    reconnectAttempts.current = 0;
  }, []);

  const connect = useCallback(
    (cb: UnaryFunction<WebSocket, unknown>): Promise<void> => {
      const websocket = new WebSocket(url);

      status.current = 'CONNECTING';

      return new Promise<void>((resolve) => {
        websocket.onopen = () => {
          status.current = 'CONNECTED';
          ws.current = websocket;
          reconnectAttempts.current = 0;

          websocket.onclose = (event) => {
            status.current = 'DISCONNECTED';
            ws.current = null;

            JSONA.stringify({
              type: PUBLISH.CONNECTION.DISCONNECTED,
              data: {}
            }).then((str) => emit.current?.({ data: str }));

            if (event.code !== 1000 && event.code !== 1001) {
              reconnect(cb);
            }
          };
          websocket.onerror = () => {
            status.current = 'ERROR';
            ws.current = null;

            reconnect(cb);
          };
          websocket.onmessage = (e) => {
            emit.current?.(e);
          };

          cb(websocket);
          resolve();
        };
      });
    },
    [url]
  );

  const reconnect = useCallback(
    (cb: UnaryFunction<WebSocket, unknown>): void => {
      if (reconnectAttempts.current >= MAX_RECONNECT_ATTEMPTS) {
        return;
      }

      const backoffDelay = Math.min(RECONNECT_INTERVAL * 2 ** reconnectAttempts.current, MAX_RECONNECT_INTERVAL);

      reconnectTimeout.current = setTimeout(() => {
        reconnectAttempts.current++;

        connect(cb);
      }, backoffDelay);
    },
    [connect]
  );

  const send = useCallback(async (data: string | ArrayBufferLike | Blob | ArrayBufferView): Promise<void> => {
    if (status.current !== 'CONNECTED') {
      await Delay.wait(400);

      return send(data);
    }

    ws.current?.send(data);
  }, []);

  const subscribe = useCallback((callback: UnaryFunction<Pick<MessageEvent, 'data'>, unknown>): void => {
    emit.current = callback;
  }, []);

  return {
    close,
    connect,
    send,
    subscribe
  };
};

export type UseWebSocket = ReturnType<typeof useWebSocket>;
