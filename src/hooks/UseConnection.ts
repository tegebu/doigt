import { PUBLISH } from '@/applications/websocket/PublishEvents.js';
import { SUBMIT } from '@/applications/websocket/SubmitEvents.js';
import type { Client, ClientID } from '@/domains/Client/Client.js';
import type { ParseError } from '@/lib/Error/ParseError.js';
import type { Either } from 'fp-ts/lib/Either.js';
import { useCallback, useEffect, useRef, useState } from 'react';
import type { UseWSEvent } from './UseWSEvent.js';

type WebSocketStatus = 'CONNECTING' | 'CONNECTED' | 'DISCONNECTED' | 'ERROR';

export const useConnection = ({ connect: con, send, subscribe, unsubscribe }: UseWSEvent) => {
  const isSubscribed = useRef(false);
  const [status, setStatus] = useState<WebSocketStatus>('DISCONNECTED');

  const cleanup = useCallback((): void => {
    if (!isSubscribed.current) {
      return;
    }

    unsubscribe(PUBLISH.CONNECTION.CONNECTED.SUCCESS);
    unsubscribe(PUBLISH.CONNECTION.CONNECTED.FAILURE);
    unsubscribe(PUBLISH.CONNECTION.RECONNECTED.SUCCESS);
    unsubscribe(PUBLISH.CONNECTION.RECONNECTED.FAILURE);
    setStatus('DISCONNECTED');

    isSubscribed.current = false;
  }, [unsubscribe]);

  const connect = useCallback(
    async (client: Client): Promise<Either<ParseError, unknown>> => {
      setStatus('CONNECTING');

      await con(() => {
        setupSubscriptions();
      });

      return send(SUBMIT.CONNECTION.CONNECT, {
        client
      });
    },
    [con, send]
  );

  const disconnect = useCallback((): Promise<Either<ParseError, unknown>> => {
    return send(SUBMIT.CONNECTION.DISCONNECT, {});
  }, [send]);

  const reconnect = useCallback(
    (id: ClientID): Promise<Either<ParseError, unknown>> => {
      setStatus('CONNECTING');

      return send(SUBMIT.CONNECTION.RECONNECT, {
        clientID: id
      });
    },
    [send]
  );

  const setupSubscriptions = useCallback((): void => {
    if (isSubscribed.current) {
      return;
    }

    subscribe(PUBLISH.CONNECTION.CONNECTED.SUCCESS, () => {
      setStatus('CONNECTED');
    });
    subscribe(PUBLISH.CONNECTION.CONNECTED.FAILURE, () => {
      setStatus('ERROR');
    });
    subscribe(PUBLISH.CONNECTION.DISCONNECTED, () => {
      setStatus('DISCONNECTED');
    });
    subscribe(PUBLISH.CONNECTION.RECONNECTED.SUCCESS, () => {
      setStatus('CONNECTED');
    });
    subscribe(PUBLISH.CONNECTION.RECONNECTED.FAILURE, () => {
      setStatus('ERROR');
    });

    isSubscribed.current = true;
  }, [subscribe]);

  useEffect(() => {
    setupSubscriptions();

    return () => {
      cleanup();
    };
  }, [cleanup, setupSubscriptions]);

  return {
    status,
    connect,
    disconnect,
    reconnect
  };
};

export type UseConnection = ReturnType<typeof useConnection>;
