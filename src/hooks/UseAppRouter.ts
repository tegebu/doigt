import { usePathname, useRouter } from 'next/navigation.js';

export const useAppRouter = () => {
  const { back, forward, push, refresh } = useRouter();
  const path = usePathname();

  return {
    path,
    back,
    forward,
    push,
    reload: refresh
  };
};

export type UseAppRouter = ReturnType<typeof useAppRouter>;
