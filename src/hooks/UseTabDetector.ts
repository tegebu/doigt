import { TabCommunicator } from '@/lib/TabCommunicator/TabCommunicator.js';
import { useTabCommunicator } from './UseTabCommunicator.js';

const CHANNEL = '11c4d39b-d073-4843-bb14-5f3cdf59cc5b';

export const useTabDetector = () => {
  const { activeTabCount } = useTabCommunicator(new TabCommunicator(CHANNEL));

  return {
    activeTabCount
  };
};

export type UseTabDetector = ReturnType<typeof useTabDetector>;
