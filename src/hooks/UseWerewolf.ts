import type { GatewayError } from '@/adapters/gateways/GatewayError.js';
import { Client, type ClientID } from '@/domains/Client/Client.js';
import { Room, type RoomID } from '@/domains/Room/Room.js';
import { createThrownError, createUnknownError, type GenericError } from '@/lib/Error/Errors.js';
import { Kind, type Nullable } from '@jamashita/anden/type';
import { JSONAError } from '@jamashita/steckdose/json';
import { type Either, left, right } from 'fp-ts/lib/Either.js';
import { none, type Option, some } from 'fp-ts/lib/Option.js';
import { useCallback, useState } from 'react';
import { z } from 'zod';

const WEREWOLF_KEY = 'b2a8e104-9a9f-49b6-b282-2b30a8e190ff';

const WerewolfAppSchema = z.object({
  roomID: Room.ID.SCHEMA,
  moderatorID: Client.ID.SCHEMA.nullable()
});

type WerewolfApp = Readonly<z.infer<typeof WerewolfAppSchema>>;

type JoinedState = Readonly<{
  status: 'JOINED';
  app: WerewolfApp;
}>;
type NotJoinedState = Readonly<{
  status: 'NOT_JOINED';
}>;
type AppState = JoinedState | NotJoinedState;

/**
 * アプリケーションデータを管理します
 */
export const useWerewolf = () => {
  const [state, setState] = useState<AppState>({ status: 'NOT_JOINED' });
  const create = useCallback((roomID: RoomID, moderatorID: Nullable<ClientID>): Either<GenericError, unknown> => {
    const obj = {
      roomID,
      moderatorID
    } satisfies WerewolfApp;

    try {
      const str = JSON.stringify(obj);

      localStorage.setItem(WEREWOLF_KEY, str);
      setState({
        status: 'JOINED',
        app: obj
      });

      return right(null);
    } catch (e: unknown) {
      if (e instanceof Error) {
        return left(createThrownError(e));
      }

      return left(createUnknownError(e));
    }
  }, []);

  const del = useCallback((): Either<GenericError | GatewayError, unknown> => {
    try {
      localStorage.removeItem(WEREWOLF_KEY);
      setState({
        status: 'NOT_JOINED'
      });

      return right(null);
    } catch (e: unknown) {
      if (e instanceof Error) {
        return left(createThrownError(e));
      }

      return left(createUnknownError(e));
    }
  }, []);

  const find = useCallback((): Either<GenericError, Option<WerewolfApp>> => {
    try {
      const data = localStorage.getItem(WEREWOLF_KEY);

      if (Kind.isNull(data)) {
        return right(none);
      }

      const obj = JSON.parse(data);
      const result = WerewolfAppSchema.safeParse(obj);

      if (!result.success) {
        del();

        return right(none);
      }

      return right(some(result.data));
    } catch (e: unknown) {
      if (e instanceof JSONAError) {
        del();

        return left(createUnknownError(e));
      }
      if (e instanceof Error) {
        return left(createThrownError(e));
      }

      return left(createUnknownError(e));
    }
  }, [del]);

  // biome-ignore lint/correctness/useExhaustiveDependencies: <explanation>
  const get = useCallback((): Option<WerewolfApp> => {
    if (state.status !== 'JOINED') {
      return none;
    }

    return some(state.app);
  }, [state.status]);

  return {
    create,
    delete: del,
    find,
    get
  };
};

export type UseWerewolf = ReturnType<typeof useWerewolf>;
