import { type GetPublishEventData, PublishEvent, type PublishEventType } from '@/applications/websocket/PublishEvents.js';
import type { GetSubmitEventData, SubmitEventType } from '@/applications/websocket/SubmitEvents.js';
import type { RuntimeError as RunError } from '@/lib/Error/Errors.js';
import type { ParseError } from '@/lib/Error/ParseError.js';
import { WSEvent } from '@/lib/Event/WSEvent.js';
import { RuntimeError } from '@jamashita/anden/error';
import type { UnaryFunction } from '@jamashita/anden/type';
import { Kind } from '@jamashita/anden/type';
import { JSONA } from '@jamashita/steckdose/json';
import { type Either, tryCatch as eTryCatch } from 'fp-ts/lib/Either.js';
import { Do, bindW, fromEither, map, tryCatch } from 'fp-ts/lib/TaskEither.js';
import { pipe } from 'fp-ts/lib/function.js';
import { useCallback, useEffect, useRef } from 'react';
import type { UseWebSocket } from './UseWebSocket.js';

export const useWSEvent = ({ close, connect, send: emit, subscribe: subsc }: UseWebSocket) => {
  const events = useRef<Map<PublishEventType, UnaryFunction<GetPublishEventData<PublishEventType>, unknown>>>(new Map());

  const send = useCallback(
    async <T extends SubmitEventType>(type: T, data: GetSubmitEventData<T>): Promise<Either<ParseError, unknown>> => {
      return pipe(
        Do,
        bindW('str', () =>
          tryCatch(
            () =>
              JSONA.stringify({
                type,
                data
              }),
            () => {
              return {
                error: 'ParseError',
                message: 'JSONの文字列化に失敗しました'
              } satisfies ParseError;
            }
          )
        ),
        map(({ str }) => emit(str))
      )();
    },
    [emit]
  );

  const subscribe = useCallback(<T extends PublishEventType>(type: T, cb: UnaryFunction<GetPublishEventData<T>, unknown>): void => {
    events.current.set(type, cb as unknown as UnaryFunction<GetPublishEventData<PublishEventType>, unknown>);
  }, []);

  const unsubscribe = useCallback(<T extends PublishEventType>(type: T): void => {
    events.current.delete(type);
  }, []);

  useEffect(() => {
    subsc((e) => {
      pipe(
        Do,
        bindW('obj', () =>
          tryCatch(
            () => JSONA.parse(e.data),
            () => {
              return {
                error: 'ParseError',
                message: `JSONのパースに失敗しました: ${e.data}`
              } satisfies ParseError;
            }
          )
        ),
        bindW('event', ({ obj }) => fromEither(WSEvent.parse(obj))),
        bindW('publishEvent', ({ event }) => {
          return pipe(
            eTryCatch(
              () => {
                if (PublishEvent.is(event.type as PublishEventType, event.data)) {
                  return event as PublishEvent;
                }

                throw new RuntimeError('');
              },
              () => {
                return {
                  error: 'RuntimeError',
                  message: `event.dataがPublishEventに適合しません: type: ${event.type}, data: ${event.data}`
                } satisfies RunError;
              }
            ),
            fromEither
          );
        }),
        map(({ publishEvent }) => {
          const cb = events.current.get(publishEvent.type);

          if (Kind.isUndefined(cb)) {
            return {
              error: 'RuntimeError',
              message: `購読されていません: ${publishEvent.type}`
            } satisfies RunError;
          }

          return cb(publishEvent.data);
        })
      )();
    });
  }, [subsc]);

  return {
    close,
    connect,
    send,
    subscribe,
    unsubscribe
  };
};

export type UseWSEvent = ReturnType<typeof useWSEvent>;
