import type { ITabCommunicator } from '@/lib/TabCommunicator/interfaces/ITabCommunicator.js';
import { Tab, type TabID, type TabMessage } from '@/lib/TabCommunicator/Tab.js';
import { ExhaustiveError } from '@jamashita/anden/error';
import { UUID } from '@jamashita/anden/uuid';
import { useCallback, useEffect, useRef, useState } from 'react';
import type { useTabDetector } from './UseTabDetector.js';

// TODO main tab detector
export const useTabCommunicator = (communicator: ITabCommunicator) => {
  const [activeTabCount, setActiveTabCount] = useState(1);
  const tabID = useRef<TabID>(Tab.ID.from(UUID.v7().get()));
  const knownTabs = useRef<Set<TabID>>(new Set([tabID.current]));

  const post = useCallback(
    (event: TabMessage): void => {
      communicator.postMessage(event);
    },
    [communicator]
  );

  useEffect(() => {
    communicator.postMessage({
      type: 'TAB_REGISTER',
      tabID: tabID.current
    });

    communicator.onMessage((event: TabMessage) => {
      const { type, tabID: sourceTabId } = event;

      if (tabID.current === sourceTabId) {
        return;
      }
      switch (type) {
        case 'TAB_REGISTER': {
          if (!knownTabs.current.has(sourceTabId)) {
            knownTabs.current.add(sourceTabId);
            setActiveTabCount((prev) => prev + 1);
          }

          communicator.postMessage({
            type: 'TAB_ACKNOWLEDGE',
            tabID: tabID.current
          });

          return;
        }
        case 'TAB_ACKNOWLEDGE': {
          if (!knownTabs.current.has(sourceTabId)) {
            knownTabs.current.add(sourceTabId);
            setActiveTabCount((prev) => prev + 1);
          }

          return;
        }
        case 'TAB_CLOSED': {
          if (knownTabs.current.has(sourceTabId)) {
            knownTabs.current.delete(sourceTabId);
            setActiveTabCount((prev) => prev - 1);
          }

          return;
        }
        default: {
          throw new ExhaustiveError(type);
        }
      }
    });

    const cleanup = () => {
      communicator.postMessage({
        type: 'TAB_CLOSED',
        tabID: tabID.current
      });
      communicator.close();
    };

    // タブが閉じられる時の処理
    window.addEventListener('beforeunload', cleanup);

    return () => {
      window.removeEventListener('beforeunload', cleanup);
      cleanup();
    };
  }, [communicator]);

  return {
    activeTabCount,
    post
  };
};

export type UseTabCommunicator = ReturnType<typeof useTabDetector>;
