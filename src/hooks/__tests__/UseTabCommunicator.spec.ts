import { Tab, type TabID } from '@/lib/TabCommunicator/Tab.js';
import type { ITabCommunicator } from '@/lib/TabCommunicator/interfaces/ITabCommunicator.js';
import { MockTabCommunicator } from '@/lib/TabCommunicator/mocks/MockTabCommunicator.js';
import type { Nullable } from '@jamashita/anden/type';
import { act, renderHook } from '@testing-library/react';
import { useTabCommunicator } from '../UseTabCommunicator.js';

describe('UseTabCommunicator', () => {
  let communicator: ITabCommunicator;

  beforeEach(() => {
    communicator = new MockTabCommunicator();
  });

  afterEach(() => {
    vi.clearAllMocks();
  });

  describe('connection', () => {
    it('should invoke communicator.postMessage()', () => {
      let tabID: Nullable<TabID> = null;

      const spy1 = vi.spyOn(communicator, 'postMessage').mockImplementation((event) => {
        if (event.type === 'TAB_REGISTER') {
          tabID = event.tabID;
        }
      });
      const spy2 = vi.spyOn(communicator, 'onMessage').mockImplementation((func) => {
        func({
          type: 'TAB_REGISTER',
          tabID: Tab.ID.from('48e29703-6cfa-4183-bb1c-3ec0621aa9bc')
        });
      });
      const spy3 = vi.spyOn(communicator, 'close').mockImplementation(() => undefined);

      const { result } = renderHook(() => useTabCommunicator(communicator));

      expect(result.current.activeTabCount).toBe(2);
      expect(spy1).toHaveBeenCalledTimes(2);
      expect(spy1).toHaveBeenNthCalledWith(1, {
        type: 'TAB_REGISTER',
        tabID
      });
      expect(spy1).toHaveBeenNthCalledWith(2, {
        type: 'TAB_ACKNOWLEDGE',
        tabID
      });
      expect(spy2).toHaveBeenCalledOnce();
      expect(spy3).not.toHaveBeenCalled();
    });
  });

  describe('post', () => {
    it('should invoke communicator.postMessage()', () => {
      let tabID: Nullable<TabID> = null;

      const spy1 = vi.spyOn(communicator, 'postMessage').mockImplementation((event) => {
        if (event.type === 'TAB_REGISTER') {
          tabID = event.tabID;
        }
      });
      const spy2 = vi.spyOn(communicator, 'onMessage').mockImplementation((func) => {
        func({
          type: 'TAB_REGISTER',
          tabID: Tab.ID.from('48e29703-6cfa-4183-bb1c-3ec0621aa9bc')
        });
      });
      const spy3 = vi.spyOn(communicator, 'close').mockImplementation(() => undefined);

      const { result } = renderHook(() => useTabCommunicator(communicator));

      act(() => {
        result.current.post({
          type: 'TAB_CLOSED',
          tabID: Tab.ID.from('48e29703-6cfa-4183-bb1c-3ec0621aa9bc')
        });
      });

      expect(result.current.activeTabCount).toBe(2);
      expect(spy1).toHaveBeenCalledTimes(3);
      expect(spy1).toHaveBeenNthCalledWith(1, {
        type: 'TAB_REGISTER',
        tabID
      });
      expect(spy1).toHaveBeenNthCalledWith(2, {
        type: 'TAB_ACKNOWLEDGE',
        tabID
      });
      expect(spy1).toHaveBeenNthCalledWith(3, {
        type: 'TAB_CLOSED',
        tabID: '48e29703-6cfa-4183-bb1c-3ec0621aa9bc'
      });
      expect(spy2).toHaveBeenCalledOnce();
      expect(spy3).not.toHaveBeenCalled();
    });
  });
});
