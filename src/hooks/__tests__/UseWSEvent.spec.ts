import { SUBMIT } from '@/applications/websocket/SubmitEvents.js';
import { mockClient } from '@/domains/Client/mocks/MockClient.js';
import { act, renderHook } from '@testing-library/react';
import { useWSEvent } from '../UseWSEvent.js';
import type { UseWebSocket } from '../UseWebSocket.js';
import { mockUseWebSocket } from '../mocks/MockUseWebSocket.js';

describe('UseWSEvent', () => {
  let useWebsocket: UseWebSocket;

  beforeEach(() => {
    useWebsocket = mockUseWebSocket();
  });

  afterEach(() => {
    vi.clearAllMocks();
  });

  describe('send', () => {
    it('should invoke websocket.send()', async () => {
      const spy1 = vi.spyOn(useWebsocket, 'send');

      const { result } = renderHook(() => useWSEvent(useWebsocket));

      await act(() => {
        return result.current.send(SUBMIT.CONNECTION.CONNECT, {
          client: mockClient()
        });
      });

      expect(spy1).toHaveBeenCalledOnce();
    });
  });
});
