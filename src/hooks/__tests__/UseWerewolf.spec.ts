import { Client } from '@/domains/Client/Client.js';
import { Room } from '@/domains/Room/Room.js';
import { useWerewolf } from '@/hooks/UseWerewolf.js';
import { act, renderHook } from '@testing-library/react';
import { isLeft, isRight } from 'fp-ts/lib/Either.js';
import { isNone, isSome } from 'fp-ts/lib/Option.js';

describe('UseWerewolf', () => {
  afterEach(() => {
    localStorage.clear();
    vi.clearAllMocks();
  });

  describe('create', () => {
    it('should create application data', async () => {
      const roomID = Room.ID.from('da328956-15ae-45d2-bc7d-4d9ef22bc155');
      const moderatorID = Client.ID.from('9cb04f3e-b8ad-4940-9258-bc31b22b8861');

      const { result } = renderHook(() => useWerewolf());

      const either = await act(() => {
        return result.current.create(roomID, moderatorID);
      });

      expect(isRight(either)).toBe(true);

      const str = localStorage.getItem('b2a8e104-9a9f-49b6-b282-2b30a8e190ff');

      expect(str).not.toBe(null);

      const obj = JSON.parse(str as string);

      expect(obj.roomID).toBe(roomID);
      expect(obj.moderatorID).toBe(moderatorID);
    });
  });

  describe('delete', () => {
    it('should delete application data', async () => {
      localStorage.setItem(
        'b2a8e104-9a9f-49b6-b282-2b30a8e190ff',
        JSON.stringify({
          roomID: 'da328956-15ae-45d2-bc7d-4d9ef22bc155',
          moderatorID: '9cb04f3e-b8ad-4940-9258-bc31b22b8861'
        })
      );

      const { result } = renderHook(() => useWerewolf());

      const either = await act(() => {
        return result.current.delete();
      });

      expect(isRight(either)).toBe(true);

      const str = localStorage.getItem('b2a8e104-9a9f-49b6-b282-2b30a8e190ff');

      expect(str).toBe(null);
    });
  });

  describe('find', () => {
    it('should find application data', async () => {
      const roomID = Room.ID.from('da328956-15ae-45d2-bc7d-4d9ef22bc155');
      const moderatorID = Client.ID.from('9cb04f3e-b8ad-4940-9258-bc31b22b8861');

      localStorage.setItem(
        'b2a8e104-9a9f-49b6-b282-2b30a8e190ff',
        JSON.stringify({
          roomID,
          moderatorID
        })
      );

      const { result } = renderHook(() => useWerewolf());

      const either = await act(() => {
        return result.current.find();
      });

      expect(isRight(either)).toBe(true);

      if (isLeft(either)) {
        throw new Error('Unexpected error');
      }

      expect(isSome(either.right)).toBe(true);

      if (isNone(either.right)) {
        throw new Error('Unexpected error');
      }

      expect(either.right.value.roomID).toBe(roomID);
      expect(either.right.value.moderatorID).toBe(moderatorID);
    });

    it('should return none when application data is not found', async () => {
      const { result } = renderHook(() => useWerewolf());

      const either = await act(() => {
        return result.current.find();
      });

      expect(isRight(either)).toBe(true);

      if (isLeft(either)) {
        throw new Error('Unexpected error');
      }

      expect(isNone(either.right)).toBe(true);
    });
  });

  describe('get', () => {
    it('should return application data', async () => {
      const roomID = Room.ID.from('da328956-15ae-45d2-bc7d-4d9ef22bc155');
      const moderatorID = Client.ID.from('9cb04f3e-b8ad-4940-9258-bc31b22b8861');

      const { result } = renderHook(() => useWerewolf());

      await act(() => {
        return result.current.create(roomID, moderatorID);
      });

      const option = await act(() => {
        return result.current.get();
      });

      expect(isSome(option)).toBe(true);

      if (isNone(option)) {
        throw new Error('Unexpected error');
      }

      expect(option.value.roomID).toBe('da328956-15ae-45d2-bc7d-4d9ef22bc155');
      expect(option.value.moderatorID).toBe('9cb04f3e-b8ad-4940-9258-bc31b22b8861');
    });

    it('should return none when application data is not found', async () => {
      const { result } = renderHook(() => useWerewolf());

      const either = await act(() => {
        return result.current.get();
      });

      expect(isNone(either)).toBe(true);
    });
  });
});
