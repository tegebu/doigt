import { mockClient } from '@/domains/Client/mocks/MockClient.js';
import { Zeit } from '@jamashita/anden/zeit';
import { act, renderHook } from '@testing-library/react';
import { isLeft, isRight } from 'fp-ts/lib/Either.js';
import { isNone, isSome } from 'fp-ts/lib/Option.js';
import { useClient } from '../UseClient.js';

describe('UseClient', () => {
  afterEach(() => {
    localStorage.clear();
    vi.clearAllMocks();
  });

  describe('create', () => {
    it('should create a client', async () => {
      const client = mockClient();

      const { result } = renderHook(() => useClient());

      const either = await act(() => {
        return result.current.create(client);
      });

      expect(isRight(either)).toBe(true);

      const str = localStorage.getItem('b2f19648-8fd8-4bce-862f-e82501e75ee9');

      expect(str).not.toBeNull();

      const obj = JSON.parse(str as string);

      expect(obj.client.id).toBe(client.id);
    });
  });

  describe('delete', () => {
    it('should delete a client', async () => {
      const client = mockClient();

      const { result } = renderHook(() => useClient());

      const either1 = await act(() => {
        return result.current.create(client);
      });

      expect(isRight(either1)).toBe(true);

      const either2 = await act(() => {
        return result.current.find();
      });

      expect(isRight(either2)).toBe(true);

      if (isLeft(either2)) {
        throw new Error('This should not happen');
      }

      expect(either2.right.id).toBe(client.id);
      expect(either2.right.name).toBe(client.name);

      const either3 = await act(() => {
        return result.current.delete();
      });

      expect(isRight(either3)).toBe(true);

      const either4 = await act(() => {
        return result.current.find();
      });

      expect(isLeft(either4)).toBe(true);
    });
  });

  describe('find', () => {
    it('should find a valid client', async () => {
      const client = mockClient();

      const { result } = renderHook(() => useClient());

      const either1 = await act(() => {
        return result.current.create(client);
      });

      expect(isRight(either1)).toBe(true);

      const either2 = await act(() => {
        return result.current.find();
      });

      expect(isRight(either2)).toBe(true);

      if (isLeft(either2)) {
        throw new Error('This should not happen');
      }

      expect(either2.right.id).toBe(client.id);
      expect(either2.right.name).toBe(client.name);
    });

    it('should not find an invalid client', async () => {
      vi.useFakeTimers();

      const now = Zeit.now();

      // 5日間が有効期限なのでそれ以降にする
      vi.spyOn(Zeit, 'now').mockImplementation(() => {
        return now.postpone(5, 'day').postpone(1, 'second');
      });

      const { result } = renderHook(() => useClient());

      const either = await act(() => {
        return result.current.find();
      });

      expect(isLeft(either)).toBe(true);

      vi.useRealTimers();
    });
  });

  describe('get', () => {
    it('should throw an error when the client is null', async () => {
      const { result } = renderHook(() => useClient());

      expect(isNone(result.current.get())).toBe(true);
    });

    it('should return a client', async () => {
      const client = mockClient();

      const { result } = renderHook(() => useClient());

      const either1 = await act(() => {
        return result.current.create(client);
      });

      expect(isRight(either1)).toBe(true);

      const option = await act(() => {
        return result.current.get();
      });

      expect(isSome(option)).toBe(true);

      if (isNone(option)) {
        throw new Error('This should not happen');
      }

      expect(option.value.id).toBe(client.id);
    });
  });

  describe('update', () => {
    it('should update a client', async () => {
      const client = mockClient({
        id: '7e9730c2-05e4-4252-86dd-0a5b3690a3cc',
        name: 'client'
      });

      const { result } = renderHook(() => useClient());

      const either1 = await act(() => {
        return result.current.create(client);
      });

      expect(isRight(either1)).toBe(true);

      const either2 = await act(() => {
        return result.current.find();
      });

      expect(isRight(either2)).toBe(true);

      if (isLeft(either2)) {
        throw new Error('This should not happen');
      }

      expect(either2.right.id).toBe(client.id);
      expect(either2.right.name).toBe(client.name);

      const updated = mockClient({
        id: '7e9730c2-05e4-4252-86dd-0a5b3690a3cc',
        name: 'updated'
      });

      const either3 = await act(() => {
        return result.current.update(updated);
      });

      expect(isRight(either3)).toBe(true);

      const either4 = await act(() => {
        return result.current.find();
      });

      expect(isRight(either4)).toBe(true);

      if (isLeft(either4)) {
        throw new Error('This should not happen');
      }

      expect(either4.right.id).toBe(client.id);
      expect(either4.right.name).toBe('updated');
    });
  });
});
