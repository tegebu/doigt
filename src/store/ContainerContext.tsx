'use client';

import { type UseLoading, useLoading } from '@/features/Loading/hooks/UseLoading.js';
import { type UseToast, useToast } from '@/features/Toast/hooks/UseToast.js';
import { type UseAppRouter, useAppRouter } from '@/hooks/UseAppRouter.js';
import { type UseClient, useClient } from '@/hooks/UseClient.js';
import { type UseConnection, useConnection } from '@/hooks/UseConnection.js';
import { type UseTabDetector, useTabDetector } from '@/hooks/UseTabDetector.js';
import { useWebSocket } from '@/hooks/UseWebSocket.js';
import { type UseWerewolf, useWerewolf } from '@/hooks/UseWerewolf.js';
import { type UseWSEvent, useWSEvent } from '@/hooks/UseWSEvent.js';
import { Kind, type Nullable } from '@jamashita/anden/type';
import { createContext, type FC, type PropsWithChildren, useContext, useMemo } from 'react';

type ContainerContextType = {
  client: UseClient;
  connection: UseConnection;
  loading: UseLoading;
  router: UseAppRouter;
  tabDetector: UseTabDetector;
  toast: UseToast;
  werewolf: UseWerewolf;
  wsEvent: UseWSEvent;
};

const ContainerContext = createContext<Nullable<ContainerContextType>>(null);

export const ContainerProvider: FC<PropsWithChildren> = ({ children }) => {
  const client = useClient();
  const websocket = useWebSocket('ws://localhost:3001/ws');
  const wsEvent = useWSEvent(websocket);
  const connection = useConnection(wsEvent);
  const loading = useLoading();
  const router = useAppRouter();
  const tabDetector = useTabDetector();
  const toast = useToast();
  const werewolf = useWerewolf();

  const value = useMemo(
    () =>
      ({
        client,
        connection,
        loading,
        router,
        tabDetector,
        toast,
        werewolf,
        wsEvent
      }) satisfies ContainerContextType,
    [client, connection, loading, router, tabDetector, toast, werewolf, wsEvent]
  );

  return <ContainerContext.Provider value={value}>{children}</ContainerContext.Provider>;
};

export const useContainer = () => {
  const context = useContext(ContainerContext);

  if (Kind.isNull(context)) {
    throw new Error('useContainer must be used within ContainerProvider');
  }

  return context;
};

export type UseContainer = ReturnType<typeof useContainer>;
