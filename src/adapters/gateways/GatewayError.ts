import { z } from 'zod';

export const DetailSchema = z.union([
  z.literal('EntityNotFound'),
  z.literal('UniqueConstraintViolation'),
  z.literal('ForeignKeyViolation'),
  z.literal('ConnectionError'),
  z.literal('UnexpectedError')
]);

export const GatewayErrorSchema = z.object({
  error: z.literal('GatewayError'),
  detail: DetailSchema,
  message: z.string()
});

type Detail = z.infer<typeof DetailSchema>;
export type GatewayError = Readonly<z.infer<typeof GatewayErrorSchema>>;

export const createGatewayError = (detail: Detail, message: string): GatewayError => {
  return {
    error: 'GatewayError',
    detail,
    message
  };
};
