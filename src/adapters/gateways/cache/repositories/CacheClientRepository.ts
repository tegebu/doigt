import { type GatewayError, createGatewayError } from '@/adapters/gateways/GatewayError.js';
import { Types } from '@/containers/Types.js';
import type { Client, ClientID } from '@/domains/Client/Client.js';
import type { IClientRepository } from '@/domains/Client/IClientRepository.js';
import type { GenericError } from '@/lib/Error/Errors.js';
import type { ILogger } from '@/lib/Logger/ILogger.js';
import { Kind } from '@jamashita/anden/type';
import { type Either, left, right } from 'fp-ts/lib/Either.js';
import { type Option, none, some } from 'fp-ts/lib/Option.js';
import { inject, injectable } from 'inversify';
import Keyv from 'keyv';

// 5日有効とする
const LIFESPAN = 5 * 24 * 60 * 60 * 1000;

@injectable()
export class CacheClientRepository implements IClientRepository {
  private readonly storage: Keyv<Client>;
  private readonly logger: ILogger;

  public constructor(@inject(Types.Logger) logger: ILogger) {
    this.storage = new Keyv<Client>(undefined, {
      ttl: LIFESPAN
    });
    this.logger = logger;
  }

  public async create(client: Client): Promise<Either<GenericError | GatewayError, unknown>> {
    this.logger.info('CacheClientRepository.create()');

    const result = await this.storage.set(client.id, client);

    if (result) {
      return right(null);
    }

    return left(createGatewayError('ConnectionError', 'Clientの作成に失敗しました'));
  }

  public async delete(id: ClientID): Promise<Either<GenericError | GatewayError, unknown>> {
    this.logger.info('CacheClientRepository.delete()');

    const result = await this.storage.delete(id);

    if (result) {
      return right(null);
    }

    return left(createGatewayError('EntityNotFound', 'Clientが存在しません'));
  }

  public async find(id: ClientID): Promise<Either<GenericError | GatewayError, Option<Client>>> {
    this.logger.info('CacheClientRepository.find()');

    const client = await this.storage.get(id);

    if (Kind.isUndefined(client)) {
      return right(none);
    }

    return right(some(client));
  }

  public async update(client: Client): Promise<Either<GenericError | GatewayError, unknown>> {
    this.logger.info('CacheClientRepository.update()');

    const result = await this.storage.set(client.id, client);

    if (result) {
      return right(null);
    }

    return left(createGatewayError('ConnectionError', 'Clientの更新に失敗しました'));
  }
}
