import { createGatewayError, type GatewayError } from '@/adapters/gateways/GatewayError.js';
import { Types } from '@/containers/Types.js';
import type { IRoomRepository } from '@/domains/Room/IRoomRepository.js';
import type { Room, RoomID } from '@/domains/Room/Room.js';
import { Rooms } from '@/domains/Room/Rooms.js';
import type { GenericError } from '@/lib/Error/Errors.js';
import type { ILogger } from '@/lib/Logger/ILogger.js';
import { Kind } from '@jamashita/anden/type';
import { type Either, left, right } from 'fp-ts/lib/Either.js';
import { fromNullable, type Option } from 'fp-ts/lib/Option.js';
import { inject, injectable } from 'inversify';

@injectable()
export class CacheRoomRepository implements IRoomRepository {
  private readonly storage: Map<RoomID, Room>;
  private readonly logger: ILogger;

  public constructor(@inject(Types.Logger) logger: ILogger) {
    this.storage = new Map<RoomID, Room>();
    this.logger = logger;
  }

  public async all(): Promise<Either<GenericError | GatewayError, Rooms>> {
    this.logger.info('CacheRoomRepository.all()');

    return right(Rooms.from(this.storage));
  }

  public async create(room: Room): Promise<Either<GenericError | GatewayError, unknown>> {
    this.logger.info('CacheRoomRepository.create()');

    const r = this.storage.get(room.id);

    if (Kind.isUndefined(r)) {
      this.storage.set(room.id, room);

      return right(null);
    }

    return left(createGatewayError('UniqueConstraintViolation', `既に部屋が存在しています。: ${room.id}`));
  }

  public async delete(id: RoomID): Promise<Either<GenericError | GatewayError, unknown>> {
    this.logger.info('CacheRoomRepository.delete()');

    const result = this.storage.delete(id);

    if (!result) {
      return left(createGatewayError('EntityNotFound', `部屋が存在しません。: ${id}`));
    }

    return right(null);
  }

  public async find(id: RoomID): Promise<Either<GenericError | GatewayError, Option<Room>>> {
    this.logger.info('CacheRoomRepository.find()');

    const room = this.storage.get(id);

    return right(fromNullable(room));
  }

  public async update(room: Room): Promise<Either<GenericError | GatewayError, unknown>> {
    this.logger.info('CacheRoomRepository.update()');

    const r = this.storage.get(room.id);

    if (Kind.isUndefined(r)) {
      return left(createGatewayError('UnexpectedError', '部屋の更新に失敗しました。'));
    }

    this.storage.set(room.id, room);

    return right(null);
  }
}
