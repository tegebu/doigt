import type { ClientError } from '@/domains/Client/ClientError.js';
import type { GenericError } from '@/lib/Error/Errors.js';
import { UnimplementedError } from '@jamashita/anden/error';
import type { Either } from 'fp-ts/lib/Either.js';
import type { EventPublishError } from '../EventPublishError.js';
import type { IEventPublisher } from '../IEventPublisher.js';

export class MockEventPublisher implements IEventPublisher {
  public broadcast(): Promise<Either<GenericError | EventPublishError, unknown>> {
    throw new UnimplementedError();
  }
  public toClient(): Promise<Either<GenericError | EventPublishError | ClientError, unknown>> {
    throw new UnimplementedError();
  }
  public toClients(): Promise<Either<GenericError | EventPublishError | ClientError, unknown>> {
    throw new UnimplementedError();
  }
}
