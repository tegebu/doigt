type Detail = 'ClientNotConnected' | 'EmitFailed';

export type EventPublishError = Readonly<{
  error: 'EventPublishError';
  detail: Detail;
  message: string;
}>;

export const createEventPublishError = (detail: Detail, message: string): EventPublishError => {
  return {
    error: 'EventPublishError',
    detail,
    message
  };
};
