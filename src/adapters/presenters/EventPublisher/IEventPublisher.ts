import type { GetPublishEventData, PublishEventType } from '@/applications/websocket/PublishEvents.js';
import type { ClientID } from '@/domains/Client/Client.js';
import type { ClientError } from '@/domains/Client/ClientError.js';
import type { GenericError } from '@/lib/Error/Errors.js';
import type { Either } from 'fp-ts/lib/Either.js';
import type { EventPublishError } from './EventPublishError.js';

export interface IEventPublisher {
  broadcast<T extends PublishEventType>(event: T, data: GetPublishEventData<T>): Promise<Either<GenericError | EventPublishError, unknown>>;

  toClient<T extends PublishEventType>(
    id: ClientID,
    event: T,
    data: GetPublishEventData<T>
  ): Promise<Either<GenericError | EventPublishError | ClientError, unknown>>;

  toClients<T extends PublishEventType>(
    ids: ReadonlyArray<ClientID>,
    event: T,
    data: GetPublishEventData<T>
  ): Promise<Either<GenericError | EventPublishError | ClientError, unknown>>;
}
