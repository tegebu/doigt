import { SUBMIT } from '@/applications/websocket/SubmitEvents.js';
import { WebsocketController } from '@/applications/websocket/WebsocketController.js';
import { container } from '@/containers/Container.js';
import { Types } from '@/containers/Types.js';
import type { ILogger } from '@/lib/Logger/ILogger.js';
import type { SetKnightTarget } from '@/usecases/SetKnightTarget.js';

const setKnightTarget = container.get<SetKnightTarget>(Types.SetKnightTarget);
const logger = container.get<ILogger>(Types.Logger);

export const knightController = WebsocketController.empty(logger);

knightController.subscribe(SUBMIT.PROTECTION, ({ data }) => {
  logger.info('SUBMIT.PROTECTION');

  return setKnightTarget.execute(data.roomID, data.sourceID, data.destinationID);
});
