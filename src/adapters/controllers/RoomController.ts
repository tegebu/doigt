import { SUBMIT } from '@/applications/websocket/SubmitEvents.js';
import { WebsocketController } from '@/applications/websocket/WebsocketController.js';
import { container } from '@/containers/Container.js';
import { Types } from '@/containers/Types.js';
import type { ILogger } from '@/lib/Logger/ILogger.js';
import type { CreateRoom } from '@/usecases/CreateRoom.js';
import type { DestroyRoom } from '@/usecases/DestroyRoom.js';
import type { FetchRoom } from '@/usecases/FetchRoom.js';
import type { FetchRoomList } from '@/usecases/FetchRoomList.js';
import type { JoinRoom } from '@/usecases/JoinRoom.js';
import type { LeaveRoom } from '@/usecases/LeaveRoom.js';

const createRoom = container.get<CreateRoom>(Types.CreateRoom);
const joinRoom = container.get<JoinRoom>(Types.JoinRoom);
const leaveRoom = container.get<LeaveRoom>(Types.LeaveRoom);
const destroyRoom = container.get<DestroyRoom>(Types.DestroyRoom);
const fetchRoomList = container.get<FetchRoomList>(Types.FetchRoomList);
const fetchRoom = container.get<FetchRoom>(Types.FetchRoom);
const logger = container.get<ILogger>(Types.Logger);

export const roomController = WebsocketController.empty(logger);

roomController.subscribe(SUBMIT.ROOM.CREATE, ({ data }) => {
  logger.info('SUBMIT.ROOM.CREATE');

  return createRoom.execute(data.settings.name, data.settings.password, data.settings.quota, data.settings.hostID, data.settings.needsModerator);
});

roomController.subscribe(SUBMIT.ROOM.JOIN, ({ data }) => {
  logger.info('SUBMIT.ROOM.JOIN');

  return joinRoom.execute(data.roomID, data.clientID, data.password);
});

roomController.subscribe(SUBMIT.ROOM.LEAVE, ({ data }) => {
  logger.info('SUBMIT.ROOM.LEAVE');

  return leaveRoom.execute(data.roomID, data.clientID);
});

roomController.subscribe(SUBMIT.ROOM.DESTROY, ({ data }) => {
  logger.info('SUBMIT.ROOM.DESTROY');

  return destroyRoom.execute(data.roomID, data.clientID);
});

roomController.subscribe(SUBMIT.ROOM.LIST, ({ data }) => {
  logger.info('SUBMIT.ROOM.LIST');

  return fetchRoomList.execute(data.clientID);
});

roomController.subscribe(SUBMIT.ROOM.FETCH, ({ data }) => {
  logger.info('SUBMIT.ROOM.FETCH');

  return fetchRoom.execute(data.clientID, data.roomID);
});
