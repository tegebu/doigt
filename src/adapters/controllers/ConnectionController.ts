import { SUBMIT } from '@/applications/websocket/SubmitEvents.js';
import { WebsocketController } from '@/applications/websocket/WebsocketController.js';
import { container } from '@/containers/Container.js';
import { Types } from '@/containers/Types.js';
import type { ILogger } from '@/lib/Logger/ILogger.js';
import type { Connect } from '@/usecases/Connect.js';
import type { Disconnect } from '@/usecases/Disconnect.js';
import type { Reconnect } from '@/usecases/Reconnect.js';

const connect = container.get<Connect>(Types.Connect);
const disconnect = container.get<Disconnect>(Types.Disconnect);
const reconnect = container.get<Reconnect>(Types.Reconnect);
const logger = container.get<ILogger>(Types.Logger);

export const connectionController = WebsocketController.empty(logger);

connectionController.subscribe(SUBMIT.CONNECTION.CONNECT, ({ data, ws }) => {
  logger.info('SUBMIT.CONNECTION.CONNECT');

  return connect.execute(data.client, ws);
});

connectionController.subscribe(SUBMIT.CONNECTION.DISCONNECT, ({ ws }) => {
  logger.info('SUBMIT.CONNECTION.DISCONNECT');

  return disconnect.execute(ws);
});

connectionController.subscribe(SUBMIT.CONNECTION.RECONNECT, ({ data, ws }) => {
  logger.info('SUBMIT.CONNECTION.RECONNECT');

  return reconnect.execute(data.clientID, ws);
});
