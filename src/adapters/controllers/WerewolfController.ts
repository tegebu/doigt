import { SUBMIT } from '@/applications/websocket/SubmitEvents.js';
import { WebsocketController } from '@/applications/websocket/WebsocketController.js';
import { container } from '@/containers/Container.js';
import { Types } from '@/containers/Types.js';
import type { ILogger } from '@/lib/Logger/ILogger.js';
import type { SetWerewolfTarget } from '@/usecases/SetWerewolfTarget.js';

const setWerewolfTarget = container.get<SetWerewolfTarget>(Types.SetWerewolfTarget);
const logger = container.get<ILogger>(Types.Logger);

export const werewolfController = WebsocketController.empty(logger);

werewolfController.subscribe(SUBMIT.ATTACK, ({ data }) => {
  logger.info('SUBMIT.ATTACK');

  return setWerewolfTarget.execute(data.roomID, data.sourceID, data.destinationID);
});
