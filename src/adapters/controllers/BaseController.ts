import { roomController } from '@/adapters/controllers/RoomController.js';
import { SUBMIT } from '@/applications/websocket/SubmitEvents.js';
import { WebsocketController } from '@/applications/websocket/WebsocketController.js';
import { container } from '@/containers/Container.js';
import { Types } from '@/containers/Types.js';
import type { ILogger } from '@/lib/Logger/ILogger.js';
import type { PingPong } from '@/usecases/PingPong.js';
import { connectionController } from './ConnectionController.js';
import { knightController } from './KnightController.js';
import { seerController } from './SeerController.js';
import { voteController } from './VoteController.js';
import { werewolfController } from './WerewolfController.js';

const pingPong = container.get<PingPong>(Types.PingPong);
const logger = container.get<ILogger>(Types.Logger);

export const baseController = WebsocketController.empty(logger);

baseController.merge(connectionController);
baseController.merge(roomController);
baseController.merge(voteController);
baseController.merge(seerController);
baseController.merge(knightController);
baseController.merge(werewolfController);

baseController.subscribe(SUBMIT.PING, ({ data, ws }) => {
  logger.info('SUBMIT.PING');

  return pingPong.execute(ws, data.message);
});
