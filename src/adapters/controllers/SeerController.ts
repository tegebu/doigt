import { SUBMIT } from '@/applications/websocket/SubmitEvents.js';
import { WebsocketController } from '@/applications/websocket/WebsocketController.js';
import { container } from '@/containers/Container.js';
import { Types } from '@/containers/Types.js';
import type { ILogger } from '@/lib/Logger/ILogger.js';
import type { SetSeerTarget } from '@/usecases/SetSeerTarget.js';

const setSeerTarget = container.get<SetSeerTarget>(Types.SetSeerTarget);
const logger = container.get<ILogger>(Types.Logger);

export const seerController = WebsocketController.empty(logger);

seerController.subscribe(SUBMIT.DIVINATION, ({ data }) => {
  logger.info('SUBMIT.DIVINATION');

  return setSeerTarget.execute(data.roomID, data.sourceID, data.destinationID);
});
