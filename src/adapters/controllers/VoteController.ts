import { SUBMIT } from '@/applications/websocket/SubmitEvents.js';
import { WebsocketController } from '@/applications/websocket/WebsocketController.js';
import { container } from '@/containers/Container.js';
import { Types } from '@/containers/Types.js';
import type { ILogger } from '@/lib/Logger/ILogger.js';
import type { SetVoteTarget } from '@/usecases/SetVoteTarget.js';

const setVoteTarget = container.get<SetVoteTarget>(Types.SetVoteTarget);
const logger = container.get<ILogger>(Types.Logger);

export const voteController = WebsocketController.empty(logger);

voteController.subscribe(SUBMIT.VOTE, ({ data }) => {
  logger.info('SUBMIT.VOTE');

  return setVoteTarget.execute(data.roomID, data.sourceID, data.destinationID);
});
