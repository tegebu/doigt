import type { IPublishService } from '@/applications/services/interfaces/IPublishService.js';
import type { IRoomService } from '@/applications/services/interfaces/IRoomService.js';
import { PUBLISH } from '@/applications/websocket/PublishEvents.js';
import { Types } from '@/containers/Types.js';
import type { Team } from '@/domains/Identity/Team.js';
import type { RoomID } from '@/domains/Room/Room.js';
import type { ILogger } from '@/lib/Logger/ILogger.js';
import type { Either } from 'fp-ts/lib/Either.js';
import { Do, bindW, map } from 'fp-ts/lib/TaskEither.js';
import { pipe } from 'fp-ts/lib/function.js';
import { inject, injectable } from 'inversify';
import type { IEndPhase } from './interfaces/IEndPhase.js';

/**
 * 夜の時間を終了します
 */
@injectable()
export class EndNightPhase implements IEndPhase {
  private readonly roomService: IRoomService;
  private readonly publishService: IPublishService;
  private readonly logger: ILogger;

  public constructor(
    @inject(Types.RoomService) roomService: IRoomService,
    @inject(Types.PublishService) publishService: IPublishService,
    @inject(Types.Logger) logger: ILogger
  ) {
    this.roomService = roomService;
    this.publishService = publishService;
    this.logger = logger;
  }

  public async execute(id: RoomID): Promise<Either<unknown, Array<Team>>> {
    this.logger.info('EndNightPhase.execute()');

    return this.roomService.withTransaction(id, ({ room }) => {
      return pipe(
        Do,
        bindW('res1', () => () => this.publishService.toEveryone(room, PUBLISH.NIGHT.END, {})),
        map(() => [])
      )();
    });
  }
}
