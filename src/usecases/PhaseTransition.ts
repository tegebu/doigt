import type { IScheduler } from '@/applications/scheduler/IScheduler.js';
import type { IPublishService } from '@/applications/services/interfaces/IPublishService.js';
import type { IRoomService } from '@/applications/services/interfaces/IRoomService.js';
import { PUBLISH } from '@/applications/websocket/PublishEvents.js';
import { Types } from '@/containers/Types.js';
import type { Team } from '@/domains/Identity/Team.js';
import type { Phase } from '@/domains/Room/Phase.js';
import { Room, type RoomID } from '@/domains/Room/Room.js';
import type { ILogger } from '@/lib/Logger/ILogger.js';
import { ExhaustiveError } from '@jamashita/anden/error';
import type { Either } from 'fp-ts/lib/Either.js';
import { pipe } from 'fp-ts/lib/function.js';
import { bindW, Do, right, type TaskEither } from 'fp-ts/lib/TaskEither.js';
import { inject, injectable } from 'inversify';
import type { IEndPhase } from './interfaces/IEndPhase.js';
import type { IPhaseTransition } from './interfaces/IPhaseTransition.js';
import type { IStartPhase } from './interfaces/IStartPhase.js';

type PhaseStrategy = Readonly<{
  startPhase: IStartPhase;
  endPhase: IEndPhase;
}>;

/**
 * 時間の遷移を行います
 */
@injectable()
export class PhaseTransition implements IPhaseTransition {
  private readonly roomService: IRoomService;
  private readonly publishService: IPublishService;
  private readonly scheduler: IScheduler;
  private readonly startMorningPhase: IStartPhase;
  private readonly startDayPhase: IStartPhase;
  private readonly startEveningPhase: IStartPhase;
  private readonly startNightPhase: IStartPhase;
  private readonly endMorningPhase: IEndPhase;
  private readonly endDayPhase: IEndPhase;
  private readonly endEveningPhase: IEndPhase;
  private readonly endNightPhase: IEndPhase;
  private readonly logger: ILogger;

  constructor(
    @inject(Types.RoomService) roomService: IRoomService,
    @inject(Types.PublishService) publishService: IPublishService,
    @inject(Types.GameScheduler) scheduler: IScheduler,
    @inject(Types.StartMorningPhase) startMorningPhase: IStartPhase,
    @inject(Types.StartDayPhase) startDayPhase: IStartPhase,
    @inject(Types.StartEveningPhase) startEveningPhase: IStartPhase,
    @inject(Types.StartNightPhase) startNightPhase: IStartPhase,
    @inject(Types.EndMorningPhase) endMorningPhase: IEndPhase,
    @inject(Types.EndDayPhase) endDayPhase: IEndPhase,
    @inject(Types.EndEveningPhase) endEveningPhase: IEndPhase,
    @inject(Types.EndNightPhase) endNightPhase: IEndPhase,
    @inject(Types.Logger) logger: ILogger
  ) {
    this.roomService = roomService;
    this.publishService = publishService;
    this.scheduler = scheduler;
    this.startMorningPhase = startMorningPhase;
    this.startDayPhase = startDayPhase;
    this.startEveningPhase = startEveningPhase;
    this.startNightPhase = startNightPhase;
    this.endMorningPhase = endMorningPhase;
    this.endDayPhase = endDayPhase;
    this.endEveningPhase = endEveningPhase;
    this.endNightPhase = endNightPhase;
    this.logger = logger;
  }

  public execute(id: RoomID): Promise<Either<unknown, unknown>> {
    this.logger.info('PhaseTransition.execute()');

    return pipe(
      Do,
      bindW(
        'room',
        () => () =>
          this.roomService.withTransaction(id, ({ room }) => {
            return right(room)();
          })
      ),
      bindW('strategy', ({ room }) => right(this.getStrategy(room.game.dayPhase.phase))),
      bindW(
        'winners',
        ({ strategy }) =>
          () =>
            strategy.startPhase.execute(id)
      ),
      bindW('res1', ({ room, strategy, winners }) => {
        if (this.shouldTerminateGame(winners)) {
          return this.terminateGame(id, winners);
        }

        this.scheduler.startPhase(id, Room.getDuration(room), () => {
          return pipe(
            Do,
            bindW('winners', () => () => strategy.endPhase.execute(room.id)),
            bindW('res1', ({ winners }) => {
              if (this.shouldTerminateGame(winners)) {
                return this.terminateGame(id, winners);
              }

              return pipe(
                Do,
                bindW(
                  'res1',
                  () => () =>
                    this.roomService.withTransaction(id, ({ room, uow }) => {
                      return uow.update('room', Room.nextPhase(room));
                    })
                ),
                bindW('res2', () => () => this.execute(id))
              );
            })
          )();
        });

        return right(null);
      })
    )();
  }

  private getStrategy(phase: Phase): PhaseStrategy {
    switch (phase) {
      case 'MORNING': {
        return {
          startPhase: this.startMorningPhase,
          endPhase: this.endMorningPhase
        } satisfies PhaseStrategy;
      }
      case 'DAY': {
        return {
          startPhase: this.startDayPhase,
          endPhase: this.endDayPhase
        } satisfies PhaseStrategy;
      }
      case 'EVENING': {
        return {
          startPhase: this.startEveningPhase,
          endPhase: this.endEveningPhase
        } satisfies PhaseStrategy;
      }
      case 'NIGHT': {
        return {
          startPhase: this.startNightPhase,
          endPhase: this.endNightPhase
        } satisfies PhaseStrategy;
      }
      default: {
        throw new ExhaustiveError(phase);
      }
    }
  }

  private shouldTerminateGame(winners: Array<Team>): boolean {
    return winners.some((winner) => {
      return winner.terminateGame;
    });
  }

  private terminateGame(id: RoomID, winners: Array<Team>): TaskEither<unknown, unknown> {
    this.scheduler.stopPhase(id);

    return () =>
      this.roomService.withTransaction(id, ({ room, uow }) => {
        return pipe(
          Do,
          bindW('newRoom', () => {
            const newRoom = Room.terminateGame(room);

            return right(newRoom);
          }),
          bindW(
            'res1',
            ({ newRoom }) =>
              () =>
                this.publishService.toEveryone(newRoom, PUBLISH.GAME.OVER, {
                  teams: winners,
                  records: [...room.game.records.values()]
                })
          ),
          bindW(
            'res2',
            ({ newRoom }) =>
              () =>
                uow.update('room', newRoom)
          ),
          bindW(
            'res3',
            ({ newRoom }) =>
              () =>
                this.publishService.broadcast(PUBLISH.ROOM.NOTIFY.STATUS_CHANGED, {
                  roomID: newRoom.id,
                  status: 'COMPLETED'
                })
          )
        )();
      });
  }
}
