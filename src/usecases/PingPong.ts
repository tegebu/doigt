import { PUBLISH } from '@/applications/websocket/PublishEvents.js';
import { Types } from '@/containers/Types.js';
import type { ILogger } from '@/lib/Logger/ILogger.js';
import { Kind } from '@jamashita/anden/type';
import { JSONA } from '@jamashita/steckdose/json';
import type { Either } from 'fp-ts/lib/Either.js';
import { right } from 'fp-ts/lib/TaskEither.js';
import { inject, injectable } from 'inversify';
import type { WebSocket } from 'ws';

@injectable()
export class PingPong {
  private readonly logger: ILogger;

  public constructor(@inject(Types.Logger) logger: ILogger) {
    this.logger = logger;
  }

  public async execute(ws: WebSocket, message: string): Promise<Either<unknown, unknown>> {
    this.logger.info('PingPong.execute()');

    const str = await JSONA.stringify({
      type: PUBLISH.PONG,
      data: message
    });

    await new Promise((resolve, reject) => {
      ws.send(str, (e) => {
        if (Kind.isNone(e)) {
          resolve(null);

          return;
        }

        reject(e);
      });
    });

    return right(null)();
  }
}
