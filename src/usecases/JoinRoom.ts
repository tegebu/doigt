import type { GatewayError } from '@/adapters/gateways/GatewayError.js';
import type { EventPublishError } from '@/adapters/presenters/EventPublisher/EventPublishError.js';
import type { IPublishService } from '@/applications/services/interfaces/IPublishService.js';
import type { IRoomService } from '@/applications/services/interfaces/IRoomService.js';
import type { UnitOfWorkError } from '@/applications/unitOfWork/UnitOfWorkError.js';
import { PUBLISH } from '@/applications/websocket/PublishEvents.js';
import { Types } from '@/containers/Types.js';
import type { Client, ClientID } from '@/domains/Client/Client.js';
import type { ClientError } from '@/domains/Client/ClientError.js';
import type { IClientRepository } from '@/domains/Client/IClientRepository.js';
import { Room, type RoomID } from '@/domains/Room/Room.js';
import { createRoomError, type RoomError } from '@/domains/Room/RoomError.js';
import type { GenericError } from '@/lib/Error/Errors.js';
import type { ILogger } from '@/lib/Logger/ILogger.js';
import type { Nullable } from '@jamashita/anden/type';
import type { Either } from 'fp-ts/lib/Either.js';
import { pipe } from 'fp-ts/lib/function.js';
import { bindW, Do, fromOption, left, map, orElse, right, type TaskEither } from 'fp-ts/lib/TaskEither.js';
import { inject, injectable } from 'inversify';

/**
 * 部屋に入ります
 */
@injectable()
export class JoinRoom {
  private readonly clientRepository: IClientRepository;
  private readonly roomService: IRoomService;
  private readonly publishService: IPublishService;
  private readonly logger: ILogger;

  public constructor(
    @inject(Types.CacheClientRepository) clientRepository: IClientRepository,
    @inject(Types.RoomService) roomService: IRoomService,
    @inject(Types.PublishService) publishService: IPublishService,
    @inject(Types.Logger) logger: ILogger
  ) {
    this.clientRepository = clientRepository;
    this.roomService = roomService;
    this.publishService = publishService;
    this.logger = logger;
  }

  public execute(roomID: RoomID, clientID: ClientID, password: Nullable<string>): Promise<Either<unknown, unknown>> {
    this.logger.info('JoinRoom.execute()');

    return pipe(
      Do,
      bindW('client', () => this.findClient(clientID)),
      bindW('res1', ({ client }) => {
        return pipe(
          Do,
          bindW(
            'res1',
            () => () =>
              this.roomService.withTransaction(roomID, ({ room, uow }) => {
                return pipe(
                  Do,
                  bindW('res1', () => this.validateRoom(room, client, password)),
                  bindW('newRoom', () => right(Room.joinParticipant(room, client))),
                  bindW(
                    'res2',
                    ({ newRoom }) =>
                      () =>
                        uow.update('room', newRoom)
                  ),
                  bindW(
                    'res3',
                    ({ newRoom }) =>
                      () =>
                        this.publishService.toEveryone(newRoom, PUBLISH.ROOM.PARTICIPANT.JOINED, {
                          client
                        })
                  ),
                  bindW(
                    'res4',
                    ({ newRoom }) =>
                      () =>
                        this.publishService.toClient(client, PUBLISH.ROOM.JOIN.SUCCESS, {
                          roomID: newRoom.id,
                          moderatorID: newRoom.moderator?.id ?? null
                        })
                  ),
                  bindW(
                    'res5',
                    ({ newRoom }) =>
                      () =>
                        this.publishService.broadcast(PUBLISH.ROOM.NOTIFY.JOINED, {
                          roomID: newRoom.id
                        })
                  )
                )();
              })
          ),
          orElse((e) => this.publishError(client, e))
        );
      })
    )();
  }

  private findClient(id: ClientID): TaskEither<GenericError | GatewayError | ClientError, Client> {
    return pipe(
      Do,
      bindW('option', () => () => this.clientRepository.find(id)),
      bindW('client', ({ option }) => {
        return pipe(
          option,
          fromOption(() => {
            return {
              error: 'ClientError',
              detail: 'NoSuchClient',
              message: `クライアントが見つかりません: ${id}`
            } as ClientError;
          })
        );
      }),
      map(({ client }) => client)
    );
  }

  private publishError(
    client: Client,
    e: RoomError | EventPublishError | UnitOfWorkError | ClientError | GenericError | GatewayError
  ): TaskEither<RoomError | EventPublishError | UnitOfWorkError | ClientError | GenericError | GatewayError, unknown> {
    this.logger.error(e);

    if (e.error === 'RoomError') {
      if (e.detail === 'NoSuchRoom') {
        return pipe(
          Do,
          bindW(
            'res1',
            () => () =>
              this.publishService.toClient(client, PUBLISH.ROOM.JOIN.FAILURE, {
                cause: 'NOT_FOUND'
              })
          ),
          bindW('res2', () => left(createRoomError('NoSuchRoom', '部屋が見つかりません')))
        );
      }
    }

    return left(e);
  }

  private validateRoom(
    room: Room,
    client: Client,
    password: Nullable<string>
  ): TaskEither<EventPublishError | ClientError | GenericError | RoomError, unknown> {
    // 部屋に入室できる余裕があるか
    if (Room.isFull(room)) {
      return pipe(
        Do,
        bindW(
          'res1',
          () => () =>
            this.publishService.toClient(client, PUBLISH.ROOM.JOIN.FAILURE, {
              cause: 'FULL'
            })
        ),
        bindW('res2', () => left(createRoomError('RoomFull', '部屋が満員です')))
      );
    }
    // 入室できる状態であるか
    if (!Room.isOpen(room)) {
      return pipe(
        Do,
        bindW(
          'res1',
          () => () =>
            this.publishService.toClient(client, PUBLISH.ROOM.JOIN.FAILURE, {
              cause: 'NOT_OPEN'
            })
        ),
        bindW('res2', () => left(createRoomError('RoomNotOpen', '部屋が開いていません')))
      );
    }
    // パスワードはあるか
    if (Room.hasPassword(room)) {
      if (room.password !== password) {
        return pipe(
          Do,
          bindW(
            'res1',
            () => () =>
              this.publishService.toClient(client, PUBLISH.ROOM.JOIN.FAILURE, {
                cause: 'PASSWORD_MISMATCH'
              })
          ),
          bindW('res2', () => left(createRoomError('PasswordMismatch', 'パスワードが一致しません')))
        );
      }
    }

    return right(null);
  }
}
