import type { GatewayError } from '@/adapters/gateways/GatewayError.js';
import type { EventPublishError } from '@/adapters/presenters/EventPublisher/EventPublishError.js';
import type { IPublishService } from '@/applications/services/interfaces/IPublishService.js';
import type { IRoomService } from '@/applications/services/interfaces/IRoomService.js';
import type { UnitOfWorkError } from '@/applications/unitOfWork/UnitOfWorkError.js';
import { PUBLISH } from '@/applications/websocket/PublishEvents.js';
import { Types } from '@/containers/Types.js';
import type { Client, ClientID } from '@/domains/Client/Client.js';
import type { ClientError } from '@/domains/Client/ClientError.js';
import type { IClientRepository } from '@/domains/Client/IClientRepository.js';
import { Room, type RoomID } from '@/domains/Room/Room.js';
import { createRoomError, type RoomError } from '@/domains/Room/RoomError.js';
import type { GenericError } from '@/lib/Error/Errors.js';
import type { ILogger } from '@/lib/Logger/ILogger.js';
import type { Either } from 'fp-ts/lib/Either.js';
import { pipe } from 'fp-ts/lib/function.js';
import { bindW, Do, flatMap, fromOption, left, map, orElse, right, type TaskEither } from 'fp-ts/lib/TaskEither.js';
import { inject, injectable } from 'inversify';

/**
 * 部屋から出ます
 *
 * moderatorは部屋から出ることができません
 * 代わりに部屋を破壊する必要があります
 */
@injectable()
export class LeaveRoom {
  private readonly clientRepository: IClientRepository;
  private readonly roomService: IRoomService;
  private readonly publishService: IPublishService;
  private readonly logger: ILogger;

  public constructor(
    @inject(Types.CacheClientRepository) clientRepository: IClientRepository,
    @inject(Types.RoomService) roomService: IRoomService,
    @inject(Types.PublishService) publishService: IPublishService,
    @inject(Types.Logger) logger: ILogger
  ) {
    this.clientRepository = clientRepository;
    this.roomService = roomService;
    this.publishService = publishService;
    this.logger = logger;
  }

  public execute(roomID: RoomID, clientID: ClientID): Promise<Either<unknown, unknown>> {
    this.logger.info('LeaveRoom.execute()');

    return pipe(
      Do,
      bindW('client', () => this.findClient(clientID)),
      bindW('res1', ({ client }) => {
        return pipe(
          Do,
          bindW(
            'res1',
            () => () =>
              this.roomService.withTransaction(roomID, ({ room, uow }) => {
                return pipe(
                  Do,
                  bindW('newRoom', () => {
                    // moderatorは部屋から出ることができない
                    if (Room.isModerator(room, client)) {
                      return left(createRoomError('ModeratorCannotLeave', 'モデレーターは部屋から出ることができません'));
                    }
                    // 部屋に参加していない
                    if (!Room.hasBeenJoined(room, client)) {
                      return left(createRoomError('NotJoined', '部屋に参加していません'));
                    }

                    return right(Room.leaveParticipant(room, client));
                  }),
                  bindW(
                    'res1',
                    ({ newRoom }) =>
                      () =>
                        uow.update('room', newRoom)
                  ),
                  bindW(
                    'res2',
                    ({ newRoom }) =>
                      () =>
                        this.publishService.toEveryone(newRoom, PUBLISH.ROOM.PARTICIPANT.LEFT, {
                          clientID: client.id
                        })
                  ),
                  bindW('res3', () => () => this.publishService.toClient(client, PUBLISH.ROOM.LEAVE.SUCCESS, {})),
                  bindW('res4', ({ newRoom }) => {
                    if (Room.isModeratorAbsent(room) && newRoom.participants.size === 0) {
                      return () => this.roomService.destroy(roomID);
                    }

                    return right(null);
                  }),
                  bindW('res5', () => () => this.publishService.broadcast(PUBLISH.ROOM.NOTIFY.LEFT, { roomID }))
                )();
              })
          ),
          orElse((e) => this.publishFailure(client, e))
        );
      })
    )();
  }

  private findClient(id: ClientID): TaskEither<GenericError | GatewayError | ClientError, Client> {
    return pipe(
      Do,
      bindW('option', () => () => this.clientRepository.find(id)),
      bindW('client', ({ option }) => {
        return pipe(
          option,
          fromOption(() => {
            return {
              error: 'ClientError',
              detail: 'NoSuchClient',
              message: `クライアントが見つかりません: ${id}`
            } as ClientError;
          })
        );
      }),
      map(({ client }) => client)
    );
  }

  private publishFailure(
    client: Client,
    e: RoomError | UnitOfWorkError | EventPublishError | ClientError | GenericError | GatewayError
  ): TaskEither<RoomError | UnitOfWorkError | EventPublishError | ClientError | GenericError | GatewayError, unknown> {
    switch (e.error) {
      case 'RoomError': {
        this.logger.error(e.detail);
        this.logger.error(e.message);

        return pipe(
          Do,
          bindW('res1', () => {
            switch (e.detail) {
              case 'NoSuchRoom': {
                return () =>
                  this.publishService.toClient(client, PUBLISH.ROOM.LEAVE.FAILURE, {
                    cause: 'NOT_FOUND'
                  });
              }
              case 'NotJoined': {
                return () =>
                  this.publishService.toClient(client, PUBLISH.ROOM.LEAVE.FAILURE, {
                    cause: 'NOT_JOINED'
                  });
              }
              case 'ModeratorCannotLeave': {
                return () =>
                  this.publishService.toClient(client, PUBLISH.ROOM.LEAVE.FAILURE, {
                    cause: 'MODERATOR_CANNOT_LEAVE'
                  });
              }
              default: {
                return right(null);
              }
            }
          }),
          flatMap(() => left(e))
        );
      }
      default: {
        return left(e);
      }
    }
  }
}
