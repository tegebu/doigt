import type { EventPublishError } from '@/adapters/presenters/EventPublisher/EventPublishError.js';
import type { IPublishService } from '@/applications/services/interfaces/IPublishService.js';
import type { IRoomService } from '@/applications/services/interfaces/IRoomService.js';
import { PUBLISH } from '@/applications/websocket/PublishEvents.js';
import { Types } from '@/containers/Types.js';
import type { ClientError } from '@/domains/Client/ClientError.js';
import type { Team } from '@/domains/Identity/Team.js';
import type { PersonID } from '@/domains/Person/Person.js';
import { Persons } from '@/domains/Person/Persons.js';
import { Game } from '@/domains/Room/Game.js';
import type { Room, RoomID } from '@/domains/Room/Room.js';
import type { GenericError } from '@/lib/Error/Errors.js';
import type { ILogger } from '@/lib/Logger/ILogger.js';
import type { Either } from 'fp-ts/lib/Either.js';
import { pipe } from 'fp-ts/lib/function.js';
import { apSW, bindW, Do, map, right, type TaskEither } from 'fp-ts/lib/TaskEither.js';
import { inject, injectable } from 'inversify';
import type { IStartPhase } from './interfaces/IStartPhase.js';

/**
 * 夜の時間を開始します
 * TODO 臆病者にも対応する
 */
@injectable()
export class StartNightPhase implements IStartPhase {
  private readonly roomService: IRoomService;
  private readonly publishService: IPublishService;
  private readonly logger: ILogger;

  public constructor(
    @inject(Types.RoomService) roomService: IRoomService,
    @inject(Types.PublishService) publishService: IPublishService,
    @inject(Types.Logger) logger: ILogger
  ) {
    this.roomService = roomService;
    this.publishService = publishService;
    this.logger = logger;
  }

  public execute(id: RoomID): Promise<Either<unknown, Array<Team>>> {
    this.logger.info('StartNightPhase.execute()');

    return pipe(
      Do,
      bindW(
        'room',
        () => () =>
          this.roomService.withTransaction(id, ({ room }) => {
            return right(room)();
          })
      ),
      bindW(
        'res1',
        ({ room }) =>
          () =>
            this.publishService.toEveryone(room, PUBLISH.NIGHT.START, {
              seconds: room.game.phaseDuration.night
            })
      ),
      bindW('res2', ({ room }) => {
        const survivors = Game.getSurvivors(room.game);
        const candidates = [...survivors.keys()];

        return pipe(
          Do,
          apSW('res1', this.promptDivinationToSeer(room, candidates)),
          apSW('res2', this.promptProtectionToKnight(room, candidates)),
          apSW('res3', this.promptAttackToWerewolf(room, candidates))
        );
      }),
      map(() => [])
    )();
  }

  /**
   * 生存している騎士を取得します
   *
   * @param game
   */
  private getKnight(game: Game): Persons {
    return Persons.getByRole(Game.getSurvivors(game), 'KNIGHT');
  }

  /**
   * 生存している予言者を取得します
   *
   * @param game
   */
  private getSeer(game: Game): Persons {
    return Persons.getByRole(Game.getSurvivors(game), 'SEER');
  }

  /**
   * 生存している人狼を取得します
   *
   * @param game
   */
  private getWerewolf(game: Game): Persons {
    return Persons.getByRole(Game.getSurvivors(game), 'WEREWOLF');
  }

  /**
   * 人狼に襲撃を促します
   *
   * @param room
   * @param candidates
   */
  private promptAttackToWerewolf(room: Room, candidates: Array<PersonID>): TaskEither<GenericError | EventPublishError | ClientError, unknown> {
    const seers = this.getWerewolf(room.game);

    return () =>
      this.publishService.toPersons(seers, PUBLISH.ATTACK.PROMPT, {
        candidates
      });
  }

  /**
   * 予言者に予言を促します
   *
   * @param room
   * @param candidates
   */
  private promptDivinationToSeer(room: Room, candidates: Array<PersonID>): TaskEither<GenericError | EventPublishError | ClientError, unknown> {
    const seers = this.getSeer(room.game);

    return () =>
      this.publishService.toPersons(seers, PUBLISH.DIVINATION.PROMPT, {
        candidates
      });
  }

  /**
   * 騎士に護衛を促します
   *
   * @param room
   * @param candidates
   */
  private promptProtectionToKnight(room: Room, candidates: Array<PersonID>): TaskEither<GenericError | EventPublishError | ClientError, unknown> {
    const knights = this.getKnight(room.game);

    return () =>
      this.publishService.toPersons(knights, PUBLISH.PROTECTION.PROMPT, {
        candidates
      });
  }
}
