import type { EventPublishError } from '@/adapters/presenters/EventPublisher/EventPublishError.js';
import type { IPublishService } from '@/applications/services/interfaces/IPublishService.js';
import type { IRoomService } from '@/applications/services/interfaces/IRoomService.js';
import { PUBLISH } from '@/applications/websocket/PublishEvents.js';
import { Types } from '@/containers/Types.js';
import type { ClientError } from '@/domains/Client/ClientError.js';
import type { MediumOutcome } from '@/domains/Outcome/MediumOutcome.js';
import { Person, type PersonID } from '@/domains/Person/Person.js';
import { createPersonError, type PersonError } from '@/domains/Person/PersonError.js';
import { Persons } from '@/domains/Person/Persons.js';
import { DailyOutcomeRecord } from '@/domains/Room/DailyOutcomeRecord.js';
import { Game } from '@/domains/Room/Game.js';
import { Room, type RoomID } from '@/domains/Room/Room.js';
import type { GenericError } from '@/lib/Error/Errors.js';
import type { ILogger } from '@/lib/Logger/ILogger.js';
import type { Either } from 'fp-ts/lib/Either.js';
import { pipe } from 'fp-ts/lib/function.js';
import {
  bind as oBind,
  Do as oDo,
  flatMap as oFlatMap,
  fold as oFold,
  fromNullable,
  map as oMap,
  none,
  type Option,
  some
} from 'fp-ts/lib/Option.js';
import { apSW, bindW as teBindW, Do as teDo, flatMap as teFlatMap, left, map as teMap, right, type TaskEither } from 'fp-ts/lib/TaskEither.js';
import { inject, injectable } from 'inversify';
import type { IPublishOutcome } from './interfaces/IPublishOutcome.js';

/**
 * 霊媒師の結果を返します
 */
@injectable()
export class PublishMediumOutcome implements IPublishOutcome {
  private readonly roomService: IRoomService;
  private readonly publishService: IPublishService;
  private readonly logger: ILogger;

  public constructor(
    @inject(Types.RoomService) roomService: IRoomService,
    @inject(Types.PublishService) publishService: IPublishService,
    @inject(Types.Logger) logger: ILogger
  ) {
    this.roomService = roomService;
    this.publishService = publishService;
    this.logger = logger;
  }

  public execute(id: RoomID): Promise<Either<unknown, unknown>> {
    this.logger.info('PublishMediumOutcome.execute()');

    return pipe(
      teDo,
      teBindW(
        'room',
        () => () =>
          this.roomService.withTransaction(id, ({ room }) => {
            return right(room)();
          })
      ),
      teFlatMap(({ room }) => {
        const mediums = this.getMedium(room.game);

        return this.publishOutcome(room, mediums);
      })
    )();
  }

  /**
   * 夕の時間に処刑された人物を取得します。
   * ルールにより処刑しない場合があるのでOption型で返します。
   *
   * @param room
   */
  private getLastExecutedPerson(room: Room): Option<PersonID> {
    return pipe(
      oDo,
      oBind('record', () => Room.getLatestRecord(room)),
      oFlatMap(({ record }) => fromNullable(DailyOutcomeRecord.getExecution(record)))
    );
  }

  /**
   * 生存している霊媒師を取得します
   *
   * @param game
   */
  private getMedium(game: Game): Persons {
    return Persons.getByRole(Game.getSurvivors(game), 'MEDIUM');
  }

  /**
   * 霊媒師の結果を取得します
   *
   * @param room
   */
  private getOutcome(room: Room): TaskEither<PersonError, Option<MediumOutcome>> {
    return pipe(
      teDo,
      teBindW('option1', () => {
        return pipe(
          oDo,
          oBind('executedPersonID', () => this.getLastExecutedPerson(room)),
          oBind('option', ({ executedPersonID }) => Room.getResident(room, executedPersonID)),
          oMap(({ option }) => option),
          right
        );
      }),
      teBindW('option2', ({ option1 }) => {
        return pipe(
          option1,
          oFold(
            () => right(none),
            (person) => {
              // 処刑者が生存している場合は例外
              if (Person.isAlive(person)) {
                return left(createPersonError('StillAlive', `処刑された住人が生存しています: ${person.client.id}, ${person.client.name}`));
              }

              return right(
                some({
                  id: person.id,
                  identity: Game.channel(person)
                } satisfies MediumOutcome)
              );
            }
          )
        );
      }),
      teMap(({ option2 }) => option2)
    );
  }

  /**
   * 霊媒師に通知します
   *
   * @param room
   * @param mediums
   * @param outcome
   */
  private publish(room: Room, mediums: Persons, outcome: MediumOutcome): TaskEither<GenericError | EventPublishError | ClientError, unknown> {
    return pipe(
      teDo,
      apSW('res1', () =>
        this.publishService.toPersons(mediums, PUBLISH.CHANNEL.OUTCOME, {
          outcome
        })
      ),
      apSW('res2', () =>
        this.publishService.toModerator(room, PUBLISH.CHANNEL.OUTCOME, {
          outcome
        })
      )
    );
  }

  /**
   * 霊媒師に結果を通知します
   *
   * @param room
   * @param mediums
   */
  private publishOutcome(room: Room, mediums: Persons): TaskEither<GenericError | PersonError | EventPublishError | ClientError, unknown> {
    // 霊媒師がいない場合はなにもしない
    if (mediums.size === 0) {
      return right(null);
    }

    return pipe(
      teDo,
      teBindW('option', () => this.getOutcome(room)),
      teBindW('outcome', ({ option }) => {
        return pipe(
          option,
          oFold(
            () => right(null),
            (outcome) => this.publish(room, mediums, outcome)
          )
        );
      })
    );
  }
}
