import type { IEventPublisher } from '@/adapters/presenters/EventPublisher/IEventPublisher.js';
import type { IConnectionPool } from '@/applications/connection/IConnectionPool.js';
import { PUBLISH } from '@/applications/websocket/PublishEvents.js';
import { Types } from '@/containers/Types.js';
import type { ClientID } from '@/domains/Client/Client.js';
import type { ILogger } from '@/lib/Logger/ILogger.js';
import type { Either } from 'fp-ts/lib/Either.js';
import { Do, bindW, flatMap, left, map, orElseW } from 'fp-ts/lib/TaskEither.js';
import { pipe } from 'fp-ts/lib/function.js';
import { inject, injectable } from 'inversify';
import type { WebSocket } from 'ws';

/**
 * クライアントとの再接続を確立します
 */
@injectable()
export class Reconnect {
  private readonly pool: IConnectionPool;
  private readonly eventPublisher: IEventPublisher;
  private readonly logger: ILogger;

  public constructor(
    @inject(Types.WebSocketPool) pool: IConnectionPool,
    @inject(Types.WSEventPublisher) eventPublisher: IEventPublisher,
    @inject(Types.Logger) logger: ILogger
  ) {
    this.pool = pool;
    this.eventPublisher = eventPublisher;
    this.logger = logger;
  }

  public async execute(id: ClientID, ws: WebSocket): Promise<Either<unknown, unknown>> {
    this.logger.info('Disconnect.execute()');

    // TODO キューイングされているメッセージを流したい
    return pipe(
      Do,
      map(() => this.pool.reconnect(id, ws)),
      bindW('res1', () => () => this.eventPublisher.toClient(id, PUBLISH.CONNECTION.RECONNECTED.SUCCESS, {})),
      orElseW((e) => {
        return pipe(
          Do,
          bindW('res1', () => () => this.eventPublisher.toClient(id, PUBLISH.CONNECTION.RECONNECTED.FAILURE, {})),
          flatMap(() => left(e))
        );
      })
    )();
  }
}
