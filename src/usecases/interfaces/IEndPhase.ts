import type { Team } from '@/domains/Identity/Team.js';
import type { RoomID } from '@/domains/Room/Room.js';
import type { Either } from 'fp-ts/lib/Either.js';

export interface IEndPhase {
  /**
   * rightは勝者のことです。空の配列を返すと勝者がいないことを示します。
   *
   * @param id
   */
  execute(id: RoomID): Promise<Either<unknown, Array<Team>>>;
}
