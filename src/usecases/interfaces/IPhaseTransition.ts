import type { RoomID } from '@/domains/Room/Room.js';
import type { Either } from 'fp-ts/lib/Either.js';

export interface IPhaseTransition {
  execute(roomID: RoomID): Promise<Either<unknown, unknown>>;
}
