import type { IPublishService } from '@/applications/services/interfaces/IPublishService.js';
import type { IRoomService } from '@/applications/services/interfaces/IRoomService.js';
import { PUBLISH } from '@/applications/websocket/PublishEvents.js';
import { Types } from '@/containers/Types.js';
import type { Team } from '@/domains/Identity/Team.js';
import type { RoomID } from '@/domains/Room/Room.js';
import type { ILogger } from '@/lib/Logger/ILogger.js';
import type { Either } from 'fp-ts/lib/Either.js';
import { pipe } from 'fp-ts/lib/function.js';
import { bindW, Do, map, right } from 'fp-ts/lib/TaskEither.js';
import { inject, injectable } from 'inversify';
import type { IStartPhase } from './interfaces/IStartPhase.js';

/**
 * 昼の時間を開始します
 */
@injectable()
export class StartDayPhase implements IStartPhase {
  private readonly roomService: IRoomService;
  private readonly publishService: IPublishService;
  private readonly logger: ILogger;

  public constructor(
    @inject(Types.RoomService) roomService: IRoomService,
    @inject(Types.PublishService) publishService: IPublishService,
    @inject(Types.Logger) logger: ILogger
  ) {
    this.roomService = roomService;
    this.publishService = publishService;
    this.logger = logger;
  }

  public execute(id: RoomID): Promise<Either<unknown, Array<Team>>> {
    this.logger.info('StartDayPhase.execute()');

    return pipe(
      Do,
      bindW(
        'room',
        () => () =>
          this.roomService.withTransaction(id, ({ room }) => {
            return right(room)();
          })
      ),
      bindW(
        'res1',
        ({ room }) =>
          () =>
            this.publishService.toEveryone(room, PUBLISH.DAY.START, {
              seconds: room.game.phaseDuration.day
            })
      ),
      map(() => [])
    )();
  }
}
