import { createEventPublishError } from '@/adapters/presenters/EventPublisher/EventPublishError.js';
import { mockRoleAssignmentRequest } from '@/applications/dtos/Role/mocks/MockRoleAssignmentRequest.js';
import type { IScheduler } from '@/applications/scheduler/IScheduler.js';
import { MockScheduler } from '@/applications/scheduler/mocks/MockScheduler.js';
import type { IPublishService } from '@/applications/services/interfaces/IPublishService.js';
import type { IRoleAssignmentService } from '@/applications/services/interfaces/IRoleAssignmentService.js';
import type { IRoomService } from '@/applications/services/interfaces/IRoomService.js';
import { MockPublishService } from '@/applications/services/mocks/MockPublishService.js';
import { MockRoleAssignmentService } from '@/applications/services/mocks/MockRoleAssignmentService.js';
import { MockRoomService } from '@/applications/services/mocks/MockRoomService.js';
import type { AppTypeMap } from '@/applications/unitOfWork/AppTypeMap.js';
import type { IUnitOfWork } from '@/applications/unitOfWork/IUnitOfWork.js';
import { MockUnitOfWork } from '@/applications/unitOfWork/mocks/MockUnitOfWork.js';
import { createUnitOfWorkError } from '@/applications/unitOfWork/UnitOfWorkError.js';
import { PUBLISH } from '@/applications/websocket/PublishEvents.js';
import { Client, type ClientID } from '@/domains/Client/Client.js';
import { mockClient } from '@/domains/Client/mocks/MockClient.js';
import { Identity } from '@/domains/Identity/Identity.js';
import { VoteOutcomes } from '@/domains/Outcome/VoteOutcomes.js';
import { mockPerson } from '@/domains/Person/mocks/MockPerson.js';
import type { Person } from '@/domains/Person/Person.js';
import { Persons } from '@/domains/Person/Persons.js';
import { mockRoleAssignment } from '@/domains/Role/mocks/MockRoleAssignment.js';
import { createRoleError } from '@/domains/Role/RoleError.js';
import { mockDailyOutcomeRecord } from '@/domains/Room/mocks/MockDailyOutcomeRecord.js';
import { mockDayPhase } from '@/domains/Room/mocks/MockDayPhase.js';
import { mockGame } from '@/domains/Room/mocks/MockGame.js';
import { mockPhaseDuration } from '@/domains/Room/mocks/MockPhaseDuration.js';
import { mockRoom } from '@/domains/Room/mocks/MockRoom.js';
import { Room } from '@/domains/Room/Room.js';
import { createRoomError } from '@/domains/Room/RoomError.js';
import type { ILogger } from '@/lib/Logger/ILogger.js';
import { MockLogger } from '@/lib/Logger/mocks/MockLogger.js';
import { Kind } from '@jamashita/anden/type';
import { isLeft, isRight, left as l, right as r } from 'fp-ts/lib/Either.js';
import { left, right } from 'fp-ts/lib/TaskEither.js';
import type { IPhaseTransition } from '../interfaces/IPhaseTransition.js';
import { MockPhaseTransition } from '../mocks/MockPhaseTransition.js';
import { StartGame } from '../StartGame.js';

describe('StartGame', () => {
  let roomService: IRoomService;
  let publishService: IPublishService;
  let roleAssignmentService: IRoleAssignmentService;
  let scheduler: IScheduler;
  let phaseTransition: IPhaseTransition;
  let logger: ILogger;
  let startGame: StartGame;

  beforeEach(() => {
    roomService = new MockRoomService();
    publishService = new MockPublishService();
    roleAssignmentService = new MockRoleAssignmentService();
    scheduler = new MockScheduler();
    phaseTransition = new MockPhaseTransition();
    logger = new MockLogger();
    startGame = new StartGame(roomService, publishService, roleAssignmentService, scheduler, phaseTransition, logger);
  });

  afterEach(() => {
    vi.clearAllMocks();
  });

  describe('execute', () => {
    it.each(Array.from({ length: 100 }, (_, i) => i + 1))('should return right: #%i attempt', async () => {
      const roomID = Room.ID.from('ba86bc02-2d94-4451-af93-2fd579f5e9e0');
      const clientID1 = Client.ID.from('a70f4b01-00f8-4bcd-a3a2-59fca6d0c066');
      const clientID2 = Client.ID.from('936a5853-f48d-48e3-b702-e5bc4a2238bf');
      const clientID3 = Client.ID.from('e4683f77-f0b8-48b8-b057-57470f6d03a3');
      const clientID4 = Client.ID.from('1a7682a4-fa94-4662-911a-eaa0c33ee39c');
      const clientID5 = Client.ID.from('0c6594c6-9950-4ab7-a5e2-4c78d2241bbf');
      const clientID6 = Client.ID.from('d92ea6d7-99c4-4d2c-98b8-8794600008a0');
      const moderatorClientID = Client.ID.from('c177a3e6-2469-4646-9f6b-22411800d823');
      const client1 = mockClient({
        id: clientID1
      });
      const client2 = mockClient({
        id: clientID2
      });
      const client3 = mockClient({
        id: clientID3
      });
      const client4 = mockClient({
        id: clientID4
      });
      const client5 = mockClient({
        id: clientID5
      });
      const client6 = mockClient({
        id: clientID6
      });
      const moderator = mockClient({
        id: moderatorClientID
      });
      const room = mockRoom({
        id: roomID,
        moderator,
        participants: [client1, client2, client3, client4, client5, client6]
      });
      const request = mockRoleAssignmentRequest();
      const assignment = mockRoleAssignment({
        informant: 1,
        seer: 2,
        villager: 1,
        werewolf: 2
      });
      const uow: IUnitOfWork<AppTypeMap> = new MockUnitOfWork<AppTypeMap>();
      let persons = Persons.empty();
      let informant = mockPerson();
      const werewolves: Array<Person> = [];
      const map = new Map<ClientID, Person>();
      let divination = VoteOutcomes.empty();

      const spy1 = vi.spyOn(roomService, 'withTransaction').mockImplementation((_, proc) => {
        return proc({ room, uow });
      });
      const spy2 = vi.spyOn(roleAssignmentService, 'assign').mockImplementation(() => r(assignment));
      const spy3 = vi.spyOn(uow, 'update').mockImplementation((_, room) => {
        persons = room.game.residents;

        for (const [, resident] of room.game.residents) {
          if (resident.identity === Identity.WEREWOLF) {
            werewolves.push(resident);
          }
          if (resident.identity === Identity.INFORMANT) {
            informant = resident;
          }

          map.set(resident.client.id, resident);
        }

        const div = room.game.records.get(0)?.divination;

        if (!Kind.isUndefined(div)) {
          divination = div;
        }

        return right(null)();
      });
      const spy4 = vi.spyOn(publishService, 'toEveryone').mockImplementation(() => right(null)());
      const spy5 = vi.spyOn(publishService, 'broadcast').mockImplementation(() => right(null)());
      const spy6 = vi.spyOn(publishService, 'toPerson').mockImplementation(() => right(null)());
      const spy7 = vi.spyOn(publishService, 'toModerator').mockImplementation(() => right(null)());
      const spy8 = vi.spyOn(scheduler, 'startPhase').mockImplementation((_id, _duration, proc) => {
        proc(roomID);
      });
      const spy9 = vi.spyOn(phaseTransition, 'execute').mockImplementation(() => right(null)());

      const either = await startGame.execute(roomID, request);

      expect(isRight(either)).toBe(true);

      // biome-ignore lint/style/noNonNullAssertion: <explanation>
      const resident1 = map.get(clientID1)!;
      // biome-ignore lint/style/noNonNullAssertion: <explanation>
      const resident2 = map.get(clientID2)!;
      // biome-ignore lint/style/noNonNullAssertion: <explanation>
      const resident3 = map.get(clientID3)!;
      // biome-ignore lint/style/noNonNullAssertion: <explanation>
      const resident4 = map.get(clientID4)!;
      // biome-ignore lint/style/noNonNullAssertion: <explanation>
      const resident5 = map.get(clientID5)!;
      // biome-ignore lint/style/noNonNullAssertion: <explanation>
      const resident6 = map.get(clientID6)!;

      expect(werewolves.length).toBe(2);
      expect(spy1).toHaveBeenCalledOnce();
      expect(spy2).toHaveBeenCalledOnce();
      expect(spy3).toHaveBeenCalledOnce();
      expect(spy4).toHaveBeenCalledOnce();
      expect(spy4).toHaveBeenCalledWith(
        {
          ...room,
          game: mockGame({
            dayPhase: mockDayPhase({
              days: 0,
              phase: 'NIGHT'
            }),
            phaseDuration: mockPhaseDuration({
              morning: 30,
              day: 240,
              evening: 30,
              night: 30
            }),
            residents: [resident1, resident2, resident3, resident4, resident5, resident6],
            records: [
              mockDailyOutcomeRecord({
                divination
              })
            ]
          })
        },
        PUBLISH.GAME.START,
        {}
      );
      expect(spy5).toHaveBeenCalledOnce();
      expect(spy6).toHaveBeenCalledTimes(7);
      expect(spy6).toHaveBeenNthCalledWith(7, informant, PUBLISH.ACKNOWLEDGE.OUTCOME, {
        outcome: {
          ids: werewolves.map((w) => w.id)
        }
      });

      const residents = [resident1, resident2, resident3, resident4, resident5];
      const personsArray = [...persons.values()].map((p) => {
        return {
          id: p.id,
          client: p.client,
          life: p.life
        };
      });

      for (let i = 0; i < 5; i++) {
        const others = personsArray.filter((p) => p.id !== residents[i]?.id);

        expect(spy6).toHaveBeenNthCalledWith(i + 1, residents[i], PUBLISH.ROLE.ASSIGNED, {
          you: residents[i],
          others
        });
      }
      expect(spy7).toHaveBeenCalledOnce();
      expect(spy7).toHaveBeenCalledWith(
        {
          ...room,
          game: mockGame({
            dayPhase: mockDayPhase({
              days: 0,
              phase: 'NIGHT'
            }),
            phaseDuration: mockPhaseDuration({
              morning: 30,
              day: 240,
              evening: 30,
              night: 30
            }),
            residents: [resident1, resident2, resident3, resident4, resident5, resident6],
            records: [
              mockDailyOutcomeRecord({
                divination
              })
            ]
          })
        },
        PUBLISH.ROLE.REVEAL_ALL,
        {
          residents: [resident1, resident2, resident3, resident4, resident5, resident6]
        }
      );
      expect(spy8).toHaveBeenCalledOnce();
      expect(spy9).toHaveBeenCalledOnce();

      expect(divination.length).toBe(2);

      const getPerson = (index: number, a: 'source' | 'destination'): Person => {
        const div = divination[index];

        if (Kind.isUndefined(div)) {
          throw new Error('This should not happen');
        }

        switch (a) {
          case 'source': {
            const p = persons.get(div.source);

            if (Kind.isUndefined(p)) {
              throw new Error('This should not happen');
            }

            return p;
          }
          case 'destination': {
            const p = persons.get(div.destination);

            if (Kind.isUndefined(p)) {
              throw new Error('This should not happen');
            }

            return p;
          }
          default: {
            throw new Error('This should not happen');
          }
        }
      };

      expect(getPerson(0, 'source').identity).toBe(Identity.SEER);
      expect(getPerson(0, 'destination').identity).not.toBe(Identity.WEREWOLF);
      expect(getPerson(1, 'source').identity).toBe(Identity.SEER);
      expect(getPerson(1, 'destination').identity).not.toBe(Identity.WEREWOLF);
    });

    it('should return left when roomService.withTransaction() returns left', async () => {
      const roomID = Room.ID.from('ba86bc02-2d94-4451-af93-2fd579f5e9e0');
      const request = mockRoleAssignmentRequest();
      const uow: IUnitOfWork<AppTypeMap> = new MockUnitOfWork<AppTypeMap>();

      const spy1 = vi.spyOn(roomService, 'withTransaction').mockImplementation(() => left(createRoomError('NoSuchRoom', ''))());
      const spy2 = vi.spyOn(roleAssignmentService, 'assign');
      const spy3 = vi.spyOn(uow, 'update');
      const spy4 = vi.spyOn(publishService, 'toEveryone');
      const spy5 = vi.spyOn(publishService, 'broadcast');
      const spy6 = vi.spyOn(publishService, 'toPerson');
      const spy7 = vi.spyOn(publishService, 'toModerator');
      const spy8 = vi.spyOn(scheduler, 'startPhase');
      const spy9 = vi.spyOn(phaseTransition, 'execute');

      const either = await startGame.execute(roomID, request);

      expect(isLeft(either)).toBe(true);
      expect(spy1).toHaveBeenCalledOnce();
      expect(spy2).not.toHaveBeenCalled();
      expect(spy3).not.toHaveBeenCalled();
      expect(spy4).not.toHaveBeenCalled();
      expect(spy5).not.toHaveBeenCalled();
      expect(spy6).not.toHaveBeenCalled();
      expect(spy7).not.toHaveBeenCalled();
      expect(spy8).not.toHaveBeenCalled();
      expect(spy9).not.toHaveBeenCalled();
    });

    it('returns left when roleAssignmentService.assign() returns left', async () => {
      const roomID = Room.ID.from('ba86bc02-2d94-4451-af93-2fd579f5e9e0');
      const clientID1 = Client.ID.from('a70f4b01-00f8-4bcd-a3a2-59fca6d0c066');
      const clientID2 = Client.ID.from('936a5853-f48d-48e3-b702-e5bc4a2238bf');
      const clientID3 = Client.ID.from('e4683f77-f0b8-48b8-b057-57470f6d03a3');
      const clientID4 = Client.ID.from('1a7682a4-fa94-4662-911a-eaa0c33ee39c');
      const clientID5 = Client.ID.from('0c6594c6-9950-4ab7-a5e2-4c78d2241bbf');
      const moderatorClientID = Client.ID.from('c177a3e6-2469-4646-9f6b-22411800d823');
      const client1 = mockClient({
        id: clientID1
      });
      const client2 = mockClient({
        id: clientID2
      });
      const client3 = mockClient({
        id: clientID3
      });
      const client4 = mockClient({
        id: clientID4
      });
      const client5 = mockClient({
        id: clientID5
      });
      const moderator = mockClient({
        id: moderatorClientID
      });
      const room = mockRoom({
        id: roomID,
        moderator,
        participants: [client1, client2, client3, client4, client5]
      });
      const request = mockRoleAssignmentRequest();
      const uow: IUnitOfWork<AppTypeMap> = new MockUnitOfWork<AppTypeMap>();

      const spy1 = vi.spyOn(roomService, 'withTransaction').mockImplementation((_, proc) => {
        return proc({ room, uow });
      });
      const spy2 = vi.spyOn(roleAssignmentService, 'assign').mockImplementation(() => l(createRoleError('QuotaExceeded', '')));
      const spy3 = vi.spyOn(uow, 'update');
      const spy4 = vi.spyOn(publishService, 'toEveryone');
      const spy5 = vi.spyOn(publishService, 'broadcast');
      const spy6 = vi.spyOn(publishService, 'toPerson');
      const spy7 = vi.spyOn(publishService, 'toModerator');
      const spy8 = vi.spyOn(scheduler, 'startPhase');
      const spy9 = vi.spyOn(phaseTransition, 'execute');
      const either = await startGame.execute(roomID, request);

      expect(isLeft(either)).toBe(true);
      expect(spy1).toHaveBeenCalledOnce();
      expect(spy2).toHaveBeenCalledOnce();
      expect(spy3).not.toHaveBeenCalled();
      expect(spy4).not.toHaveBeenCalled();
      expect(spy5).not.toHaveBeenCalled();
      expect(spy6).not.toHaveBeenCalled();
      expect(spy7).not.toHaveBeenCalled();
      expect(spy8).not.toHaveBeenCalled();
      expect(spy9).not.toHaveBeenCalled();
    });

    it('should return left when Room.startGame() returns left', async () => {
      const roomID = Room.ID.from('ba86bc02-2d94-4451-af93-2fd579f5e9e0');
      const clientID1 = Client.ID.from('a70f4b01-00f8-4bcd-a3a2-59fca6d0c066');
      const clientID2 = Client.ID.from('936a5853-f48d-48e3-b702-e5bc4a2238bf');
      const clientID3 = Client.ID.from('e4683f77-f0b8-48b8-b057-57470f6d03a3');
      const clientID4 = Client.ID.from('1a7682a4-fa94-4662-911a-eaa0c33ee39c');
      const clientID5 = Client.ID.from('0c6594c6-9950-4ab7-a5e2-4c78d2241bbf');
      const moderatorClientID = Client.ID.from('c177a3e6-2469-4646-9f6b-22411800d823');
      const client1 = mockClient({
        id: clientID1
      });
      const client2 = mockClient({
        id: clientID2
      });
      const client3 = mockClient({
        id: clientID3
      });
      const client4 = mockClient({
        id: clientID4
      });
      const client5 = mockClient({
        id: clientID5
      });
      const moderator = mockClient({
        id: moderatorClientID
      });
      const room = mockRoom({
        id: roomID,
        moderator,
        participants: [client1, client2, client3, client4, client5]
      });
      const request = mockRoleAssignmentRequest();
      const assignment = mockRoleAssignment({
        seer: 2,
        villager: 1,
        werewolf: 1
      });
      const uow: IUnitOfWork<AppTypeMap> = new MockUnitOfWork<AppTypeMap>();

      const spy1 = vi.spyOn(roomService, 'withTransaction').mockImplementation((_, proc) => {
        return proc({ room, uow });
      });
      const spy2 = vi.spyOn(roleAssignmentService, 'assign').mockImplementation(() => r(assignment));
      const spy3 = vi.spyOn(uow, 'update');
      const spy4 = vi.spyOn(publishService, 'toEveryone');
      const spy5 = vi.spyOn(publishService, 'broadcast');
      const spy6 = vi.spyOn(publishService, 'toPerson');
      const spy7 = vi.spyOn(publishService, 'toModerator');
      const spy8 = vi.spyOn(scheduler, 'startPhase');
      const spy9 = vi.spyOn(phaseTransition, 'execute');

      const either = await startGame.execute(roomID, request);

      expect(isLeft(either)).toBe(true);
      expect(spy1).toHaveBeenCalledOnce();
      expect(spy2).toHaveBeenCalledOnce();
      expect(spy3).not.toHaveBeenCalled();
      expect(spy4).not.toHaveBeenCalled();
      expect(spy5).not.toHaveBeenCalled();
      expect(spy6).not.toHaveBeenCalled();
      expect(spy7).not.toHaveBeenCalled();
      expect(spy8).not.toHaveBeenCalled();
      expect(spy9).not.toHaveBeenCalled();
    });

    it('should return left when there is no villagers in the room', async () => {
      const roomID = Room.ID.from('ba86bc02-2d94-4451-af93-2fd579f5e9e0');
      const clientID1 = Client.ID.from('a70f4b01-00f8-4bcd-a3a2-59fca6d0c066');
      const clientID2 = Client.ID.from('936a5853-f48d-48e3-b702-e5bc4a2238bf');
      const clientID3 = Client.ID.from('e4683f77-f0b8-48b8-b057-57470f6d03a3');
      const clientID4 = Client.ID.from('1a7682a4-fa94-4662-911a-eaa0c33ee39c');
      const clientID5 = Client.ID.from('0c6594c6-9950-4ab7-a5e2-4c78d2241bbf');
      const moderatorClientID = Client.ID.from('c177a3e6-2469-4646-9f6b-22411800d823');
      const client1 = mockClient({
        id: clientID1
      });
      const client2 = mockClient({
        id: clientID2
      });
      const client3 = mockClient({
        id: clientID3
      });
      const client4 = mockClient({
        id: clientID4
      });
      const client5 = mockClient({
        id: clientID5
      });
      const moderator = mockClient({
        id: moderatorClientID
      });
      const room = mockRoom({
        id: roomID,
        moderator,
        participants: [client1, client2, client3, client4, client5]
      });
      const request = mockRoleAssignmentRequest();
      const assignment = mockRoleAssignment({
        werewolf: 5
      });
      const uow: IUnitOfWork<AppTypeMap> = new MockUnitOfWork<AppTypeMap>();

      const spy1 = vi.spyOn(roomService, 'withTransaction').mockImplementation((_, proc) => {
        return proc({ room, uow });
      });
      const spy2 = vi.spyOn(roleAssignmentService, 'assign').mockImplementation(() => r(assignment));
      const spy3 = vi.spyOn(uow, 'update');
      const spy4 = vi.spyOn(publishService, 'toEveryone');
      const spy5 = vi.spyOn(publishService, 'broadcast');
      const spy6 = vi.spyOn(publishService, 'toPerson');
      const spy7 = vi.spyOn(publishService, 'toModerator');
      const spy8 = vi.spyOn(scheduler, 'startPhase');
      const spy9 = vi.spyOn(phaseTransition, 'execute');

      const either = await startGame.execute(roomID, request);

      expect(isLeft(either)).toBe(true);
      expect(spy1).toHaveBeenCalledOnce();
      expect(spy2).toHaveBeenCalledOnce();
      expect(spy3).not.toHaveBeenCalled();
      expect(spy4).not.toHaveBeenCalled();
      expect(spy5).not.toHaveBeenCalled();
      expect(spy6).not.toHaveBeenCalled();
      expect(spy7).not.toHaveBeenCalled();
      expect(spy8).not.toHaveBeenCalled();
      expect(spy9).not.toHaveBeenCalled();
    });

    it('should return left when there is no werewolves in the room', async () => {
      const roomID = Room.ID.from('ba86bc02-2d94-4451-af93-2fd579f5e9e0');
      const clientID1 = Client.ID.from('a70f4b01-00f8-4bcd-a3a2-59fca6d0c066');
      const clientID2 = Client.ID.from('936a5853-f48d-48e3-b702-e5bc4a2238bf');
      const clientID3 = Client.ID.from('e4683f77-f0b8-48b8-b057-57470f6d03a3');
      const clientID4 = Client.ID.from('1a7682a4-fa94-4662-911a-eaa0c33ee39c');
      const clientID5 = Client.ID.from('0c6594c6-9950-4ab7-a5e2-4c78d2241bbf');
      const moderatorClientID = Client.ID.from('c177a3e6-2469-4646-9f6b-22411800d823');
      const client1 = mockClient({
        id: clientID1
      });
      const client2 = mockClient({
        id: clientID2
      });
      const client3 = mockClient({
        id: clientID3
      });
      const client4 = mockClient({
        id: clientID4
      });
      const client5 = mockClient({
        id: clientID5
      });
      const moderator = mockClient({
        id: moderatorClientID
      });
      const room = mockRoom({
        id: roomID,
        moderator,
        participants: [client1, client2, client3, client4, client5]
      });
      const request = mockRoleAssignmentRequest();
      const assignment = mockRoleAssignment({
        seer: 2,
        villager: 1,
        catalyst: 2
      });
      const uow: IUnitOfWork<AppTypeMap> = new MockUnitOfWork<AppTypeMap>();

      const spy1 = vi.spyOn(roomService, 'withTransaction').mockImplementation((_, proc) => {
        return proc({ room, uow });
      });
      const spy2 = vi.spyOn(roleAssignmentService, 'assign').mockImplementation(() => r(assignment));
      const spy3 = vi.spyOn(uow, 'update');
      const spy4 = vi.spyOn(publishService, 'toEveryone');
      const spy5 = vi.spyOn(publishService, 'broadcast');
      const spy6 = vi.spyOn(publishService, 'toPerson');
      const spy7 = vi.spyOn(publishService, 'toModerator');
      const spy8 = vi.spyOn(scheduler, 'startPhase');
      const spy9 = vi.spyOn(phaseTransition, 'execute');

      const either = await startGame.execute(roomID, request);

      expect(isLeft(either)).toBe(true);
      expect(spy1).toHaveBeenCalledOnce();
      expect(spy2).toHaveBeenCalledOnce();
      expect(spy3).not.toHaveBeenCalled();
      expect(spy4).not.toHaveBeenCalled();
      expect(spy5).not.toHaveBeenCalled();
      expect(spy6).not.toHaveBeenCalled();
      expect(spy7).not.toHaveBeenCalled();
      expect(spy8).not.toHaveBeenCalled();
      expect(spy9).not.toHaveBeenCalled();
    });

    it('should return left when there is more werewolves than villagers in the room', async () => {
      const roomID = Room.ID.from('ba86bc02-2d94-4451-af93-2fd579f5e9e0');
      const clientID1 = Client.ID.from('a70f4b01-00f8-4bcd-a3a2-59fca6d0c066');
      const clientID2 = Client.ID.from('936a5853-f48d-48e3-b702-e5bc4a2238bf');
      const clientID3 = Client.ID.from('e4683f77-f0b8-48b8-b057-57470f6d03a3');
      const clientID4 = Client.ID.from('1a7682a4-fa94-4662-911a-eaa0c33ee39c');
      const clientID5 = Client.ID.from('0c6594c6-9950-4ab7-a5e2-4c78d2241bbf');
      const moderatorClientID = Client.ID.from('c177a3e6-2469-4646-9f6b-22411800d823');
      const client1 = mockClient({
        id: clientID1
      });
      const client2 = mockClient({
        id: clientID2
      });
      const client3 = mockClient({
        id: clientID3
      });
      const client4 = mockClient({
        id: clientID4
      });
      const client5 = mockClient({
        id: clientID5
      });
      const moderator = mockClient({
        id: moderatorClientID
      });
      const room = mockRoom({
        id: roomID,
        moderator,
        participants: [client1, client2, client3, client4, client5]
      });
      const request = mockRoleAssignmentRequest();
      const assignment = mockRoleAssignment({
        seer: 1,
        villager: 1,
        werewolf: 3
      });
      const uow: IUnitOfWork<AppTypeMap> = new MockUnitOfWork<AppTypeMap>();

      const spy1 = vi.spyOn(roomService, 'withTransaction').mockImplementation((_, proc) => {
        return proc({ room, uow });
      });
      const spy2 = vi.spyOn(roleAssignmentService, 'assign').mockImplementation(() => r(assignment));
      const spy3 = vi.spyOn(uow, 'update');
      const spy4 = vi.spyOn(publishService, 'toEveryone');
      const spy5 = vi.spyOn(publishService, 'broadcast');
      const spy6 = vi.spyOn(publishService, 'toPerson');
      const spy7 = vi.spyOn(publishService, 'toModerator');
      const spy8 = vi.spyOn(scheduler, 'startPhase');
      const spy9 = vi.spyOn(phaseTransition, 'execute');

      const either = await startGame.execute(roomID, request);

      expect(isLeft(either)).toBe(true);
      expect(spy1).toHaveBeenCalledOnce();
      expect(spy2).toHaveBeenCalledOnce();
      expect(spy3).not.toHaveBeenCalled();
      expect(spy4).not.toHaveBeenCalled();
      expect(spy5).not.toHaveBeenCalled();
      expect(spy6).not.toHaveBeenCalled();
      expect(spy7).not.toHaveBeenCalled();
      expect(spy8).not.toHaveBeenCalled();
      expect(spy9).not.toHaveBeenCalled();
    });

    it('should return left when uow.update() returns left', async () => {
      const roomID = Room.ID.from('ba86bc02-2d94-4451-af93-2fd579f5e9e0');
      const clientID1 = Client.ID.from('a70f4b01-00f8-4bcd-a3a2-59fca6d0c066');
      const clientID2 = Client.ID.from('936a5853-f48d-48e3-b702-e5bc4a2238bf');
      const clientID3 = Client.ID.from('e4683f77-f0b8-48b8-b057-57470f6d03a3');
      const clientID4 = Client.ID.from('1a7682a4-fa94-4662-911a-eaa0c33ee39c');
      const clientID5 = Client.ID.from('0c6594c6-9950-4ab7-a5e2-4c78d2241bbf');
      const moderatorClientID = Client.ID.from('c177a3e6-2469-4646-9f6b-22411800d823');
      const client1 = mockClient({
        id: clientID1
      });
      const client2 = mockClient({
        id: clientID2
      });
      const client3 = mockClient({
        id: clientID3
      });
      const client4 = mockClient({
        id: clientID4
      });
      const client5 = mockClient({
        id: clientID5
      });
      const moderator = mockClient({
        id: moderatorClientID
      });
      const room = mockRoom({
        id: roomID,
        moderator,
        participants: [client1, client2, client3, client4, client5]
      });
      const request = mockRoleAssignmentRequest();
      const assignment = mockRoleAssignment({
        seer: 2,
        villager: 1,
        werewolf: 2
      });
      const uow: IUnitOfWork<AppTypeMap> = new MockUnitOfWork<AppTypeMap>();

      const spy1 = vi.spyOn(roomService, 'withTransaction').mockImplementation((_, proc) => {
        return proc({ room, uow });
      });
      const spy2 = vi.spyOn(roleAssignmentService, 'assign').mockImplementation(() => r(assignment));
      const spy3 = vi.spyOn(uow, 'update').mockImplementation(() => left(createUnitOfWorkError('Update', ''))());
      const spy4 = vi.spyOn(publishService, 'toEveryone');
      const spy5 = vi.spyOn(publishService, 'broadcast');
      const spy6 = vi.spyOn(publishService, 'toPerson');
      const spy7 = vi.spyOn(publishService, 'toModerator');
      const spy8 = vi.spyOn(scheduler, 'startPhase');
      const spy9 = vi.spyOn(phaseTransition, 'execute');

      const either = await startGame.execute(roomID, request);

      expect(isLeft(either)).toBe(true);
      expect(spy1).toHaveBeenCalledOnce();
      expect(spy2).toHaveBeenCalledOnce();
      expect(spy3).toHaveBeenCalledOnce();
      expect(spy4).not.toHaveBeenCalled();
      expect(spy5).not.toHaveBeenCalled();
      expect(spy6).not.toHaveBeenCalled();
      expect(spy7).not.toHaveBeenCalled();
      expect(spy8).not.toHaveBeenCalled();
      expect(spy9).not.toHaveBeenCalled();
    });

    it('should return left when publishService.toEveryone() returns left', async () => {
      const roomID = Room.ID.from('ba86bc02-2d94-4451-af93-2fd579f5e9e0');
      const clientID1 = Client.ID.from('a70f4b01-00f8-4bcd-a3a2-59fca6d0c066');
      const clientID2 = Client.ID.from('936a5853-f48d-48e3-b702-e5bc4a2238bf');
      const clientID3 = Client.ID.from('e4683f77-f0b8-48b8-b057-57470f6d03a3');
      const clientID4 = Client.ID.from('1a7682a4-fa94-4662-911a-eaa0c33ee39c');
      const clientID5 = Client.ID.from('0c6594c6-9950-4ab7-a5e2-4c78d2241bbf');
      const moderatorClientID = Client.ID.from('c177a3e6-2469-4646-9f6b-22411800d823');
      const client1 = mockClient({
        id: clientID1
      });
      const client2 = mockClient({
        id: clientID2
      });
      const client3 = mockClient({
        id: clientID3
      });
      const client4 = mockClient({
        id: clientID4
      });
      const client5 = mockClient({
        id: clientID5
      });
      const moderator = mockClient({
        id: moderatorClientID
      });
      const room = mockRoom({
        id: roomID,
        moderator,
        participants: [client1, client2, client3, client4, client5]
      });
      const request = mockRoleAssignmentRequest();
      const assignment = mockRoleAssignment({
        seer: 2,
        villager: 1,
        werewolf: 2
      });
      const uow: IUnitOfWork<AppTypeMap> = new MockUnitOfWork<AppTypeMap>();
      let persons = Persons.empty();
      const map = new Map<ClientID, Person>();
      let divination = VoteOutcomes.empty();

      const spy1 = vi.spyOn(roomService, 'withTransaction').mockImplementation((_, proc) => {
        return proc({ room, uow });
      });
      const spy2 = vi.spyOn(roleAssignmentService, 'assign').mockImplementation(() => r(assignment));
      const spy3 = vi.spyOn(uow, 'update').mockImplementation((_, room) => {
        for (const [, resident] of room.game.residents) {
          persons = Persons.join(persons, resident);

          map.set(resident.client.id, resident);
        }

        const div = room.game.records.get(0)?.divination;

        if (!Kind.isUndefined(div)) {
          divination = div;
        }

        return right(null)();
      });
      const spy4 = vi.spyOn(publishService, 'toEveryone').mockImplementation(() => left(createEventPublishError('EmitFailed', ''))());
      const spy5 = vi.spyOn(publishService, 'broadcast');
      const spy6 = vi.spyOn(publishService, 'toPerson');
      const spy7 = vi.spyOn(publishService, 'toModerator');
      const spy8 = vi.spyOn(scheduler, 'startPhase');
      const spy9 = vi.spyOn(phaseTransition, 'execute');

      const either = await startGame.execute(roomID, request);

      expect(isLeft(either)).toBe(true);

      // biome-ignore lint/style/noNonNullAssertion: <explanation>
      const resident1 = map.get(clientID1)!;
      // biome-ignore lint/style/noNonNullAssertion: <explanation>
      const resident2 = map.get(clientID2)!;
      // biome-ignore lint/style/noNonNullAssertion: <explanation>
      const resident3 = map.get(clientID3)!;
      // biome-ignore lint/style/noNonNullAssertion: <explanation>
      const resident4 = map.get(clientID4)!;
      // biome-ignore lint/style/noNonNullAssertion: <explanation>
      const resident5 = map.get(clientID5)!;

      expect(spy1).toHaveBeenCalledOnce();
      expect(spy2).toHaveBeenCalledOnce();
      expect(spy3).toHaveBeenCalledOnce();
      expect(spy4).toHaveBeenCalledOnce();
      expect(spy4).toHaveBeenCalledWith(
        {
          ...room,
          game: mockGame({
            dayPhase: mockDayPhase({
              days: 0,
              phase: 'NIGHT'
            }),
            phaseDuration: mockPhaseDuration({
              morning: 30,
              day: 240,
              evening: 30,
              night: 30
            }),
            residents: [resident1, resident2, resident3, resident4, resident5],
            records: [
              mockDailyOutcomeRecord({
                divination
              })
            ]
          })
        },
        PUBLISH.GAME.START,
        {}
      );
      expect(spy5).not.toHaveBeenCalled();
      expect(spy6).not.toHaveBeenCalled();
      expect(spy7).not.toHaveBeenCalled();
      expect(spy8).not.toHaveBeenCalled();
      expect(spy9).not.toHaveBeenCalled();
    });

    it('should return left when publishService.broadcast() returns left', async () => {
      const roomID = Room.ID.from('ba86bc02-2d94-4451-af93-2fd579f5e9e0');
      const clientID1 = Client.ID.from('a70f4b01-00f8-4bcd-a3a2-59fca6d0c066');
      const clientID2 = Client.ID.from('936a5853-f48d-48e3-b702-e5bc4a2238bf');
      const clientID3 = Client.ID.from('e4683f77-f0b8-48b8-b057-57470f6d03a3');
      const clientID4 = Client.ID.from('1a7682a4-fa94-4662-911a-eaa0c33ee39c');
      const clientID5 = Client.ID.from('0c6594c6-9950-4ab7-a5e2-4c78d2241bbf');
      const moderatorClientID = Client.ID.from('c177a3e6-2469-4646-9f6b-22411800d823');
      const client1 = mockClient({
        id: clientID1
      });
      const client2 = mockClient({
        id: clientID2
      });
      const client3 = mockClient({
        id: clientID3
      });
      const client4 = mockClient({
        id: clientID4
      });
      const client5 = mockClient({
        id: clientID5
      });
      const moderator = mockClient({
        id: moderatorClientID
      });
      const room = mockRoom({
        id: roomID,
        moderator,
        participants: [client1, client2, client3, client4, client5]
      });
      const request = mockRoleAssignmentRequest();
      const assignment = mockRoleAssignment({
        seer: 2,
        villager: 1,
        werewolf: 2
      });
      const uow: IUnitOfWork<AppTypeMap> = new MockUnitOfWork<AppTypeMap>();
      let persons = Persons.empty();
      const map = new Map<ClientID, Person>();
      let divination = VoteOutcomes.empty();

      const spy1 = vi.spyOn(roomService, 'withTransaction').mockImplementation((_, proc) => {
        return proc({ room, uow });
      });
      const spy2 = vi.spyOn(roleAssignmentService, 'assign').mockImplementation(() => r(assignment));
      const spy3 = vi.spyOn(uow, 'update').mockImplementation((_, room) => {
        for (const [, resident] of room.game.residents) {
          persons = Persons.join(persons, resident);

          map.set(resident.client.id, resident);
        }

        const div = room.game.records.get(0)?.divination;

        if (!Kind.isUndefined(div)) {
          divination = div;
        }

        return right(null)();
      });
      const spy4 = vi.spyOn(publishService, 'toEveryone').mockImplementation(() => right(null)());
      const spy5 = vi.spyOn(publishService, 'broadcast').mockImplementation(() => left(createEventPublishError('EmitFailed', ''))());
      const spy6 = vi.spyOn(publishService, 'toPerson');
      const spy7 = vi.spyOn(publishService, 'toModerator');
      const spy8 = vi.spyOn(scheduler, 'startPhase');
      const spy9 = vi.spyOn(phaseTransition, 'execute');

      const either = await startGame.execute(roomID, request);

      expect(isLeft(either)).toBe(true);

      // biome-ignore lint/style/noNonNullAssertion: <explanation>
      const resident1 = map.get(clientID1)!;
      // biome-ignore lint/style/noNonNullAssertion: <explanation>
      const resident2 = map.get(clientID2)!;
      // biome-ignore lint/style/noNonNullAssertion: <explanation>
      const resident3 = map.get(clientID3)!;
      // biome-ignore lint/style/noNonNullAssertion: <explanation>
      const resident4 = map.get(clientID4)!;
      // biome-ignore lint/style/noNonNullAssertion: <explanation>
      const resident5 = map.get(clientID5)!;

      expect(spy1).toHaveBeenCalledOnce();
      expect(spy2).toHaveBeenCalledOnce();
      expect(spy3).toHaveBeenCalledOnce();
      expect(spy4).toHaveBeenCalledOnce();
      expect(spy4).toHaveBeenCalledWith(
        {
          ...room,
          game: mockGame({
            dayPhase: mockDayPhase({
              days: 0,
              phase: 'NIGHT'
            }),
            phaseDuration: mockPhaseDuration({
              morning: 30,
              day: 240,
              evening: 30,
              night: 30
            }),
            residents: [resident1, resident2, resident3, resident4, resident5],
            records: [
              mockDailyOutcomeRecord({
                divination
              })
            ]
          })
        },
        PUBLISH.GAME.START,
        {}
      );
      expect(spy5).toHaveBeenCalledOnce();
      expect(spy6).not.toHaveBeenCalled();
      expect(spy7).not.toHaveBeenCalled();
      expect(spy8).not.toHaveBeenCalled();
      expect(spy9).not.toHaveBeenCalled();
    });

    it('should return left when publishService.toPerson() returns left', async () => {
      const roomID = Room.ID.from('ba86bc02-2d94-4451-af93-2fd579f5e9e0');
      const clientID1 = Client.ID.from('a70f4b01-00f8-4bcd-a3a2-59fca6d0c066');
      const clientID2 = Client.ID.from('936a5853-f48d-48e3-b702-e5bc4a2238bf');
      const clientID3 = Client.ID.from('e4683f77-f0b8-48b8-b057-57470f6d03a3');
      const clientID4 = Client.ID.from('1a7682a4-fa94-4662-911a-eaa0c33ee39c');
      const clientID5 = Client.ID.from('0c6594c6-9950-4ab7-a5e2-4c78d2241bbf');
      const moderatorClientID = Client.ID.from('c177a3e6-2469-4646-9f6b-22411800d823');
      const client1 = mockClient({
        id: clientID1
      });
      const client2 = mockClient({
        id: clientID2
      });
      const client3 = mockClient({
        id: clientID3
      });
      const client4 = mockClient({
        id: clientID4
      });
      const client5 = mockClient({
        id: clientID5
      });
      const moderator = mockClient({
        id: moderatorClientID
      });
      const room = mockRoom({
        id: roomID,
        moderator,
        participants: [client1, client2, client3, client4, client5]
      });
      const request = mockRoleAssignmentRequest();
      const assignment = mockRoleAssignment({
        seer: 2,
        villager: 1,
        werewolf: 2
      });
      const uow: IUnitOfWork<AppTypeMap> = new MockUnitOfWork<AppTypeMap>();
      let persons = Persons.empty();
      const map = new Map<ClientID, Person>();
      let divination = VoteOutcomes.empty();

      const spy1 = vi.spyOn(roomService, 'withTransaction').mockImplementation((_, proc) => {
        return proc({ room, uow });
      });
      const spy2 = vi.spyOn(roleAssignmentService, 'assign').mockImplementation(() => r(assignment));
      const spy3 = vi.spyOn(uow, 'update').mockImplementation((_, room) => {
        for (const [, resident] of room.game.residents) {
          persons = Persons.join(persons, resident);

          map.set(resident.client.id, resident);
        }

        const div = room.game.records.get(0)?.divination;

        if (!Kind.isUndefined(div)) {
          divination = div;
        }

        return right(null)();
      });
      const spy4 = vi.spyOn(publishService, 'toEveryone').mockImplementation(() => right(null)());
      const spy5 = vi.spyOn(publishService, 'broadcast').mockImplementation(() => right(null)());
      const spy6 = vi.spyOn(publishService, 'toPerson').mockImplementation(() => left(createEventPublishError('EmitFailed', ''))());
      const spy7 = vi.spyOn(publishService, 'toModerator').mockImplementation(() => right(null)());
      const spy8 = vi.spyOn(scheduler, 'startPhase');
      const spy9 = vi.spyOn(phaseTransition, 'execute');

      const either = await startGame.execute(roomID, request);

      expect(isLeft(either)).toBe(true);

      // biome-ignore lint/style/noNonNullAssertion: <explanation>
      const resident1 = map.get(clientID1)!;
      // biome-ignore lint/style/noNonNullAssertion: <explanation>
      const resident2 = map.get(clientID2)!;
      // biome-ignore lint/style/noNonNullAssertion: <explanation>
      const resident3 = map.get(clientID3)!;
      // biome-ignore lint/style/noNonNullAssertion: <explanation>
      const resident4 = map.get(clientID4)!;
      // biome-ignore lint/style/noNonNullAssertion: <explanation>
      const resident5 = map.get(clientID5)!;

      expect(spy1).toHaveBeenCalledOnce();
      expect(spy2).toHaveBeenCalledOnce();
      expect(spy3).toHaveBeenCalledOnce();
      expect(spy4).toHaveBeenCalledOnce();
      expect(spy4).toHaveBeenCalledWith(
        {
          ...room,
          game: mockGame({
            dayPhase: mockDayPhase({
              days: 0,
              phase: 'NIGHT'
            }),
            phaseDuration: mockPhaseDuration({
              morning: 30,
              day: 240,
              evening: 30,
              night: 30
            }),
            residents: [resident1, resident2, resident3, resident4, resident5],
            records: [
              mockDailyOutcomeRecord({
                divination
              })
            ]
          })
        },
        PUBLISH.GAME.START,
        {}
      );
      expect(spy5).toHaveBeenCalledOnce();
      expect(spy6).toHaveBeenCalledTimes(5);

      const residents = [resident1, resident2, resident3, resident4, resident5];
      const personsArray = [...persons.values()].map((p) => {
        return {
          id: p.id,
          client: p.client,
          life: p.life
        };
      });

      for (let i = 0; i < 5; i++) {
        const others = personsArray.filter((p) => p.id !== residents[i]?.id);

        expect(spy6).toHaveBeenNthCalledWith(i + 1, residents[i], PUBLISH.ROLE.ASSIGNED, {
          you: residents[i],
          others
        });
      }
      expect(spy7).toHaveBeenCalledOnce();
      expect(spy8).not.toHaveBeenCalled();
      expect(spy9).not.toHaveBeenCalled();
    });

    it('should return left when publishService.toModerator() returns left', async () => {
      const roomID = Room.ID.from('ba86bc02-2d94-4451-af93-2fd579f5e9e0');
      const clientID1 = Client.ID.from('a70f4b01-00f8-4bcd-a3a2-59fca6d0c066');
      const clientID2 = Client.ID.from('936a5853-f48d-48e3-b702-e5bc4a2238bf');
      const clientID3 = Client.ID.from('e4683f77-f0b8-48b8-b057-57470f6d03a3');
      const clientID4 = Client.ID.from('1a7682a4-fa94-4662-911a-eaa0c33ee39c');
      const clientID5 = Client.ID.from('0c6594c6-9950-4ab7-a5e2-4c78d2241bbf');
      const moderatorClientID = Client.ID.from('c177a3e6-2469-4646-9f6b-22411800d823');
      const client1 = mockClient({
        id: clientID1
      });
      const client2 = mockClient({
        id: clientID2
      });
      const client3 = mockClient({
        id: clientID3
      });
      const client4 = mockClient({
        id: clientID4
      });
      const client5 = mockClient({
        id: clientID5
      });
      const moderator = mockClient({
        id: moderatorClientID
      });
      const room = mockRoom({
        id: roomID,
        moderator,
        participants: [client1, client2, client3, client4, client5]
      });
      const request = mockRoleAssignmentRequest();
      const assignment = mockRoleAssignment({
        seer: 2,
        villager: 1,
        werewolf: 2
      });
      const uow: IUnitOfWork<AppTypeMap> = new MockUnitOfWork<AppTypeMap>();
      let persons = Persons.empty();
      const map = new Map<ClientID, Person>();
      let divination = VoteOutcomes.empty();

      const spy1 = vi.spyOn(roomService, 'withTransaction').mockImplementation((_, proc) => {
        return proc({ room, uow });
      });
      const spy2 = vi.spyOn(roleAssignmentService, 'assign').mockImplementation(() => r(assignment));
      const spy3 = vi.spyOn(uow, 'update').mockImplementation((_, room) => {
        for (const [, resident] of room.game.residents) {
          persons = Persons.join(persons, resident);

          map.set(resident.client.id, resident);
        }

        const div = room.game.records.get(0)?.divination;

        if (!Kind.isUndefined(div)) {
          divination = div;
        }

        return right(null)();
      });
      const spy4 = vi.spyOn(publishService, 'toEveryone').mockImplementation(() => right(null)());
      const spy5 = vi.spyOn(publishService, 'broadcast').mockImplementation(() => right(null)());
      const spy6 = vi.spyOn(publishService, 'toPerson').mockImplementation(() => right(null)());
      const spy7 = vi.spyOn(publishService, 'toModerator').mockImplementation(() => left(createEventPublishError('EmitFailed', ''))());
      const spy8 = vi.spyOn(scheduler, 'startPhase');
      const spy9 = vi.spyOn(phaseTransition, 'execute');

      const either = await startGame.execute(roomID, request);

      expect(isLeft(either)).toBe(true);

      // biome-ignore lint/style/noNonNullAssertion: <explanation>
      const resident1 = map.get(clientID1)!;
      // biome-ignore lint/style/noNonNullAssertion: <explanation>
      const resident2 = map.get(clientID2)!;
      // biome-ignore lint/style/noNonNullAssertion: <explanation>
      const resident3 = map.get(clientID3)!;
      // biome-ignore lint/style/noNonNullAssertion: <explanation>
      const resident4 = map.get(clientID4)!;
      // biome-ignore lint/style/noNonNullAssertion: <explanation>
      const resident5 = map.get(clientID5)!;

      expect(spy1).toHaveBeenCalledOnce();
      expect(spy2).toHaveBeenCalledOnce();
      expect(spy3).toHaveBeenCalledOnce();
      expect(spy4).toHaveBeenCalledOnce();
      expect(spy4).toHaveBeenCalledWith(
        {
          ...room,
          game: mockGame({
            dayPhase: mockDayPhase({
              days: 0,
              phase: 'NIGHT'
            }),
            phaseDuration: mockPhaseDuration({
              morning: 30,
              day: 240,
              evening: 30,
              night: 30
            }),
            residents: [resident1, resident2, resident3, resident4, resident5],
            records: [
              mockDailyOutcomeRecord({
                divination
              })
            ]
          })
        },
        PUBLISH.GAME.START,
        {}
      );
      expect(spy5).toHaveBeenCalledOnce();
      expect(spy6).toHaveBeenCalledTimes(5);

      const residents = [resident1, resident2, resident3, resident4, resident5];
      const personsArray = [...persons.values()].map((p) => {
        return {
          id: p.id,
          client: p.client,
          life: p.life
        };
      });

      for (let i = 0; i < 5; i++) {
        const others = personsArray.filter((p) => p.id !== residents[i]?.id);

        expect(spy6).toHaveBeenNthCalledWith(i + 1, residents[i], PUBLISH.ROLE.ASSIGNED, {
          you: residents[i],
          others
        });
      }
      expect(spy7).toHaveBeenCalledOnce();
      expect(spy7).toHaveBeenCalledWith(
        {
          ...room,
          game: mockGame({
            dayPhase: mockDayPhase({
              days: 0,
              phase: 'NIGHT'
            }),
            phaseDuration: mockPhaseDuration({
              morning: 30,
              day: 240,
              evening: 30,
              night: 30
            }),
            residents: [resident1, resident2, resident3, resident4, resident5],
            records: [
              mockDailyOutcomeRecord({
                divination
              })
            ]
          })
        },
        PUBLISH.ROLE.REVEAL_ALL,
        {
          residents: [resident1, resident2, resident3, resident4, resident5]
        }
      );
      expect(spy8).not.toHaveBeenCalled();
      expect(spy9).not.toHaveBeenCalled();
    });
  });
});
