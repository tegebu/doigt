import { createGatewayError } from '@/adapters/gateways/GatewayError.js';
import type { IEventPublisher } from '@/adapters/presenters/EventPublisher/IEventPublisher.js';
import { MockEventPublisher } from '@/adapters/presenters/EventPublisher/mocks/MockEventPublisher.js';
import { PUBLISH } from '@/applications/websocket/PublishEvents.js';
import { Client } from '@/domains/Client/Client.js';
import { mockClient } from '@/domains/Client/mocks/MockClient.js';
import type { IRoomRepository } from '@/domains/Room/IRoomRepository.js';
import { Rooms } from '@/domains/Room/Rooms.js';
import { mockGame } from '@/domains/Room/mocks/MockGame.js';
import { mockRoom } from '@/domains/Room/mocks/MockRoom.js';
import { MockRoomRepository } from '@/domains/Room/mocks/MockRoomRepository.js';
import type { ILogger } from '@/lib/Logger/ILogger.js';
import { MockLogger } from '@/lib/Logger/mocks/MockLogger.js';
import { FetchRoomList } from '@/usecases/FetchRoomList.js';
import { isRight } from 'fp-ts/lib/Either.js';
import { left, right } from 'fp-ts/lib/TaskEither.js';

describe('FetchRoomList', () => {
  let roomRepository: IRoomRepository;
  let eventPublisher: IEventPublisher;
  let logger: ILogger;
  let fetchRoomList: FetchRoomList;

  beforeEach(() => {
    roomRepository = new MockRoomRepository();
    eventPublisher = new MockEventPublisher();
    logger = new MockLogger();
    fetchRoomList = new FetchRoomList(roomRepository, eventPublisher, logger);
  });

  afterEach(() => {
    vi.clearAllMocks();
  });

  describe('execute', () => {
    it('should publish a list of rooms', async () => {
      const clientID = Client.ID.from('d83ab534-3519-40c7-af04-9078ecf13bf8');
      const rooms = Rooms.ofArray([
        mockRoom({
          id: '9455a448-8ec5-4a93-8f95-d08599222bd6',
          name: 'Nl2fadGed',
          password: 'e6ghLZ1Y3xe',
          moderator: null,
          quota: 7,
          participants: [
            mockClient({
              id: '176f7ef7-af4f-4127-8a3a-a6281963cc3f',
              name: 'bdcnES3SEb4'
            }),
            mockClient({
              id: '74ccb26a-f63b-49d8-90c1-9ea36e3fa177',
              name: 'G2pJ2kUGvr'
            }),
            mockClient({
              id: 'adbd4605-5bd5-4a86-a9ec-0b15d6b59eaa',
              name: 'RfrnWfN96fk'
            })
          ],
          game: mockGame({
            status: 'OPEN'
          })
        }),
        mockRoom({
          id: '5d06a0a1-20be-42be-b43f-5e149d5c2d84',
          name: 'hHs0icbogMV',
          moderator: null,
          quota: 4,
          participants: [],
          game: mockGame({
            status: 'IN_PROGRESS'
          })
        }),
        mockRoom({
          id: '2d355468-9e4d-4bc9-acb5-11610b953487',
          name: 'WQjC2foV',
          moderator: null,
          quota: 4,
          participants: [],
          game: mockGame({
            status: 'COMPLETED'
          })
        }),
        mockRoom({
          id: 'da3a841d-11b9-4042-9649-ba1b2ea07c9c',
          name: 'sV890sh',
          password: null,
          moderator: null,
          quota: 10,
          participants: [
            mockClient({
              id: '176f7ef7-af4f-4127-8a3a-a6281963cc3f',
              name: 'bdcnES3SEb4'
            }),
            mockClient({
              id: '74ccb26a-f63b-49d8-90c1-9ea36e3fa177',
              name: 'G2pJ2kUGvr'
            }),
            mockClient({
              id: 'adbd4605-5bd5-4a86-a9ec-0b15d6b59eaa',
              name: 'RfrnWfN96fk'
            }),
            mockClient({
              id: '6fd7b750-9ace-473e-b5f4-6e561c55bcbb',
              name: 'nPdV1dW3Vv'
            }),
            mockClient({
              id: '85706e44-d9d6-4f53-a9d5-52a1dbd049dd',
              name: 'gepGsoqm'
            })
          ],
          game: mockGame({
            status: 'OPEN'
          })
        })
      ]);

      const spy1 = vi.spyOn(roomRepository, 'all').mockImplementation(() => right(rooms)());
      const spy2 = vi.spyOn(eventPublisher, 'toClient').mockImplementation(() => right(null)());

      const either = await fetchRoomList.execute(clientID);

      expect(isRight(either)).toBe(true);
      expect(spy1).toHaveBeenCalledOnce();
      expect(spy2).toHaveBeenCalledOnce();
      expect(spy2).toHaveBeenCalledWith(clientID, PUBLISH.ROOM.LIST.SUCCESS, {
        rooms: [
          {
            id: '9455a448-8ec5-4a93-8f95-d08599222bd6',
            name: 'Nl2fadGed',
            requirePassword: true,
            moderator: null,
            quota: 7,
            participants: 3,
            status: 'OPEN'
          },
          {
            id: 'da3a841d-11b9-4042-9649-ba1b2ea07c9c',
            name: 'sV890sh',
            requirePassword: false,
            moderator: null,
            quota: 10,
            participants: 5,
            status: 'OPEN'
          }
        ]
      });
    });

    it('should publish empty list of rooms', async () => {
      const clientID = Client.ID.from('d83ab534-3519-40c7-af04-9078ecf13bf8');
      const rooms = Rooms.ofArray([
        mockRoom({
          id: '5d06a0a1-20be-42be-b43f-5e149d5c2d84',
          name: 'hHs0icbogMV',
          moderator: null,
          quota: 4,
          participants: [],
          game: mockGame({
            status: 'IN_PROGRESS'
          })
        }),
        mockRoom({
          id: '2d355468-9e4d-4bc9-acb5-11610b953487',
          name: 'WQjC2foV',
          moderator: null,
          quota: 4,
          participants: [],
          game: mockGame({
            status: 'COMPLETED'
          })
        })
      ]);

      const spy1 = vi.spyOn(roomRepository, 'all').mockImplementation(() => right(rooms)());
      const spy2 = vi.spyOn(eventPublisher, 'toClient').mockImplementation(() => right(null)());

      const either = await fetchRoomList.execute(clientID);

      expect(isRight(either)).toBe(true);
      expect(spy1).toHaveBeenCalledOnce();
      expect(spy2).toHaveBeenCalledOnce();
      expect(spy2).toHaveBeenCalledWith(clientID, PUBLISH.ROOM.LIST.SUCCESS, {
        rooms: []
      });
    });

    it('should publish error when roomRepository.all() return left', async () => {
      const clientID = Client.ID.from('d83ab534-3519-40c7-af04-9078ecf13bf8');

      const spy1 = vi.spyOn(roomRepository, 'all').mockImplementation(() => left(createGatewayError('EntityNotFound', ''))());
      const spy2 = vi.spyOn(eventPublisher, 'toClient').mockImplementation(() => right(null)());

      const either = await fetchRoomList.execute(clientID);

      expect(isRight(either)).toBe(false);
      expect(spy1).toHaveBeenCalledOnce();
      expect(spy2).toHaveBeenCalledOnce();
      expect(spy2).toHaveBeenCalledWith(clientID, PUBLISH.ROOM.LIST.FAILURE, {});
    });
  });
});
