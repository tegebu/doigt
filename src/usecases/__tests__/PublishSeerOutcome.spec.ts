import type { IPublishService } from '@/applications/services/interfaces/IPublishService.js';
import type { IRoomService } from '@/applications/services/interfaces/IRoomService.js';
import { MockPublishService } from '@/applications/services/mocks/MockPublishService.js';
import { MockRoomService } from '@/applications/services/mocks/MockRoomService.js';
import type { AppTypeMap } from '@/applications/unitOfWork/AppTypeMap.js';
import { MockUnitOfWork } from '@/applications/unitOfWork/mocks/MockUnitOfWork.js';
import { PUBLISH, type PublishEventType } from '@/applications/websocket/PublishEvents.js';
import { Identity } from '@/domains/Identity/Identity.js';
import { mockPerson } from '@/domains/Person/mocks/MockPerson.js';
import { Person } from '@/domains/Person/Person.js';
import { mockDailyOutcomeRecord } from '@/domains/Room/mocks/MockDailyOutcomeRecord.js';
import { mockGame } from '@/domains/Room/mocks/MockGame.js';
import { mockRoom } from '@/domains/Room/mocks/MockRoom.js';
import { Room } from '@/domains/Room/Room.js';
import type { ILogger } from '@/lib/Logger/ILogger.js';
import { MockLogger } from '@/lib/Logger/mocks/MockLogger.js';
import type { ObjectLiteral } from '@jamashita/anden/type';
import { isRight } from 'fp-ts/lib/Either.js';
import { right } from 'fp-ts/lib/TaskEither.js';
import { PublishSeerOutcome } from '../PublishSeerOutcome.js';

describe('PublishSeerOutcome', () => {
  let roomService: IRoomService;
  let publishService: IPublishService;
  let logger: ILogger;
  let publishSeerOutcome: PublishSeerOutcome;

  beforeEach(() => {
    roomService = new MockRoomService();
    publishService = new MockPublishService();
    logger = new MockLogger();
    publishSeerOutcome = new PublishSeerOutcome(roomService, publishService, logger);
  });

  afterEach(() => {
    vi.clearAllMocks();
  });

  describe('execute', () => {
    it.each`
      role           | expected
      ${'ATONEMENT'} | ${'VILLAGER'}
      ${'CATALYST'}  | ${'VILLAGER'}
      ${'COWARD'}    | ${'VILLAGER'}
      ${'HUNTER'}    | ${'VILLAGER'}
      ${'INFORMANT'} | ${'VILLAGER'}
      ${'JESTER'}    | ${'VILLAGER'}
      ${'KITSUNE'}   | ${'VILLAGER'}
      ${'KNIGHT'}    | ${'VILLAGER'}
      ${'MEDIUM'}    | ${'VILLAGER'}
      ${'MONASTIC'}  | ${'VILLAGER'}
      ${'ORATOR'}    | ${'VILLAGER'}
      ${'POSSESSED'} | ${'WEREWOLF'}
      ${'SEER'}      | ${'VILLAGER'}
      ${'VILLAGER'}  | ${'VILLAGER'}
      ${'WEREWOLF'}  | ${'WEREWOLF'}
    `('should publish $expected when the executed person role is $role', async ({ role, expected }) => {
      const roomID = Room.ID.from('f0efb80d-81cc-4538-9870-793c0419627b');
      const seerID = Person.ID.from('a8ec13bc-93a7-45c3-bdfd-e5f8e53702e6');
      const targetPersonID = Person.ID.from('07307b24-44ff-4a8c-8176-2a2e67567674');
      const seer = mockPerson({
        id: seerID,
        life: 'ALIVE',
        identity: Identity.SEER
      });
      const room = mockRoom({
        id: roomID,
        game: mockGame({
          residents: [
            seer,
            mockPerson({
              id: targetPersonID,
              life: 'ALIVE',
              identity: Identity.getByRole(role)
            })
          ],
          records: [
            mockDailyOutcomeRecord({
              days: 1,
              divination: [
                {
                  source: seerID,
                  destination: targetPersonID
                }
              ]
            })
          ]
        })
      });
      const uow = new MockUnitOfWork<AppTypeMap>();

      const spy1 = vi.spyOn(roomService, 'withTransaction').mockImplementation((_, proc) => {
        return proc({ uow, room });
      });
      const spy2 = vi.spyOn(publishService, 'toPerson').mockImplementation(() => right(null)());
      const spy3 = vi.spyOn(publishService, 'toModerator').mockImplementation(() => right(null)());

      const either = await publishSeerOutcome.execute(roomID);

      expect(isRight(either)).toBe(true);
      expect(spy1).toHaveBeenCalledOnce();
      expect(spy2).toHaveBeenCalledOnce();
      expect(spy2).toHaveBeenCalledWith(seer, PUBLISH.DIVINATION.OUTCOME, {
        outcome: {
          id: targetPersonID,
          identity: expected
        }
      });
      expect(spy3).toHaveBeenCalledOnce();
      expect(spy3).toHaveBeenCalledWith(room, PUBLISH.DIVINATION.OUTCOME, {
        outcome: {
          id: targetPersonID,
          identity: expected
        }
      });
    });

    it('should publish event to each seer when there are multiple seers', async () => {
      const roomID = Room.ID.from('f0efb80d-81cc-4538-9870-793c0419627b');
      const seerID1 = Person.ID.from('a8ec13bc-93a7-45c3-bdfd-e5f8e53702e6');
      const seerID2 = Person.ID.from('fec8d5c2-2375-491c-bbf5-d59ff1a98735');
      const targetPersonID1 = Person.ID.from('07307b24-44ff-4a8c-8176-2a2e67567674');
      const targetPersonID2 = Person.ID.from('e47db109-496f-4e24-9d37-788381180d91');
      const seer1 = mockPerson({
        id: seerID1,
        life: 'ALIVE',
        identity: Identity.SEER
      });
      const seer2 = mockPerson({
        id: seerID2,
        life: 'ALIVE',
        identity: Identity.SEER
      });
      const room = mockRoom({
        id: roomID,
        game: mockGame({
          residents: [
            seer1,
            seer2,
            mockPerson({
              id: targetPersonID1,
              life: 'ALIVE',
              identity: Identity.VILLAGER
            }),
            mockPerson({
              id: targetPersonID2,
              life: 'ALIVE',
              identity: Identity.WEREWOLF
            })
          ],
          records: [
            mockDailyOutcomeRecord({
              days: 1,
              divination: [
                {
                  source: seerID1,
                  destination: targetPersonID1
                },
                {
                  source: seerID2,
                  destination: targetPersonID2
                }
              ]
            })
          ]
        })
      });
      const uow = new MockUnitOfWork<AppTypeMap>();

      const publishCallsToPersons: Array<{ person: Person; event: PublishEventType; data: ObjectLiteral }> = [];
      const publishCallsToModerator: Array<{ room: Room; event: PublishEventType; data: ObjectLiteral }> = [];

      const spy1 = vi.spyOn(roomService, 'withTransaction').mockImplementation((_, proc) => {
        return proc({ uow, room });
      });
      const spy2 = vi.spyOn(publishService, 'toPerson').mockImplementation((person, event, data) => {
        publishCallsToPersons.push({ person, event, data });

        return right(null)();
      });
      const spy3 = vi.spyOn(publishService, 'toModerator').mockImplementation((room, event, data) => {
        publishCallsToModerator.push({ room, event, data });

        return right(null)();
      });

      const either = await publishSeerOutcome.execute(roomID);

      expect(isRight(either)).toBe(true);
      expect(spy1).toHaveBeenCalledOnce();
      expect(spy2).toHaveBeenCalledTimes(2);
      expect(spy3).toHaveBeenCalledTimes(2);
      expect(publishCallsToPersons).toContainEqual({
        person: seer1,
        event: PUBLISH.DIVINATION.OUTCOME,
        data: {
          outcome: {
            id: targetPersonID1,
            identity: 'VILLAGER'
          }
        }
      });
      expect(publishCallsToPersons).toContainEqual({
        person: seer2,
        event: PUBLISH.DIVINATION.OUTCOME,
        data: {
          outcome: {
            id: targetPersonID2,
            identity: 'WEREWOLF'
          }
        }
      });
      expect(publishCallsToModerator).toContainEqual({
        room,
        event: PUBLISH.DIVINATION.OUTCOME,
        data: {
          outcome: {
            id: targetPersonID1,
            identity: 'VILLAGER'
          }
        }
      });
      expect(publishCallsToModerator).toContainEqual({
        room,
        event: PUBLISH.DIVINATION.OUTCOME,
        data: {
          outcome: {
            id: targetPersonID2,
            identity: 'WEREWOLF'
          }
        }
      });
    });

    it('should not publish event to the seer who does not divinate', async () => {
      const roomID = Room.ID.from('f0efb80d-81cc-4538-9870-793c0419627b');
      const seerID = Person.ID.from('a8ec13bc-93a7-45c3-bdfd-e5f8e53702e6');
      const targetPersonID = Person.ID.from('07307b24-44ff-4a8c-8176-2a2e67567674');
      const seer = mockPerson({
        id: seerID,
        life: 'ALIVE',
        identity: Identity.SEER
      });
      const room = mockRoom({
        id: roomID,
        game: mockGame({
          residents: [
            seer,
            mockPerson({
              id: targetPersonID,
              life: 'ALIVE',
              identity: Identity.VILLAGER
            })
          ],
          records: [
            mockDailyOutcomeRecord({
              days: 1,
              divination: []
            })
          ]
        })
      });
      const uow = new MockUnitOfWork<AppTypeMap>();

      const spy1 = vi.spyOn(roomService, 'withTransaction').mockImplementation((_, proc) => {
        return proc({ uow, room });
      });
      const spy2 = vi.spyOn(publishService, 'toPerson');
      const spy3 = vi.spyOn(publishService, 'toModerator');

      const either = await publishSeerOutcome.execute(roomID);

      expect(isRight(either)).toBe(true);
      expect(spy1).toHaveBeenCalledOnce();
      expect(spy2).not.toHaveBeenCalled();
      expect(spy3).not.toHaveBeenCalled();
    });

    it('should not publish event to dead seer', async () => {
      const roomID = Room.ID.from('f0efb80d-81cc-4538-9870-793c0419627b');
      const seerID1 = Person.ID.from('a8ec13bc-93a7-45c3-bdfd-e5f8e53702e6');
      const seerID2 = Person.ID.from('fec8d5c2-2375-491c-bbf5-d59ff1a98735');
      const targetPersonID = Person.ID.from('07307b24-44ff-4a8c-8176-2a2e67567674');
      const seer1 = mockPerson({
        id: seerID1,
        life: 'DEAD',
        identity: Identity.SEER
      });
      const seer2 = mockPerson({
        id: seerID2,
        life: 'ALIVE',
        identity: Identity.SEER
      });
      const room = mockRoom({
        id: roomID,
        game: mockGame({
          residents: [
            seer1,
            seer2,
            mockPerson({
              id: targetPersonID,
              life: 'ALIVE',
              identity: Identity.VILLAGER
            })
          ],
          records: [
            mockDailyOutcomeRecord({
              days: 1,
              divination: [
                {
                  source: seerID1,
                  destination: targetPersonID
                },
                {
                  source: seerID2,
                  destination: targetPersonID
                }
              ]
            })
          ]
        })
      });
      const uow = new MockUnitOfWork<AppTypeMap>();

      const spy1 = vi.spyOn(roomService, 'withTransaction').mockImplementation((_, proc) => {
        return proc({ uow, room });
      });
      const spy2 = vi.spyOn(publishService, 'toPerson').mockImplementation(() => right(null)());
      const spy3 = vi.spyOn(publishService, 'toModerator').mockImplementation(() => right(null)());

      const either = await publishSeerOutcome.execute(roomID);

      expect(isRight(either)).toBe(true);
      expect(spy1).toHaveBeenCalledOnce();
      expect(spy2).toHaveBeenCalledOnce();
      expect(spy2).toHaveBeenCalledWith(seer2, PUBLISH.DIVINATION.OUTCOME, {
        outcome: {
          id: targetPersonID,
          identity: 'VILLAGER'
        }
      });
      expect(spy3).toHaveBeenCalledOnce();
      expect(spy3).toHaveBeenCalledWith(room, PUBLISH.DIVINATION.OUTCOME, {
        outcome: {
          id: targetPersonID,
          identity: 'VILLAGER'
        }
      });
    });

    it('should not publish event when there is no seer', async () => {
      const roomID = Room.ID.from('f0efb80d-81cc-4538-9870-793c0419627b');
      const targetPersonID = Person.ID.from('07307b24-44ff-4a8c-8176-2a2e67567674');
      const room = mockRoom({
        id: roomID,
        game: mockGame({
          residents: [
            mockPerson({
              id: targetPersonID,
              life: 'ALIVE',
              identity: Identity.VILLAGER
            })
          ],
          records: [
            mockDailyOutcomeRecord({
              days: 1,
              divination: []
            })
          ]
        })
      });
      const uow = new MockUnitOfWork<AppTypeMap>();

      const spy1 = vi.spyOn(roomService, 'withTransaction').mockImplementation((_, proc) => {
        return proc({ uow, room });
      });
      const spy2 = vi.spyOn(publishService, 'toPerson');
      const spy3 = vi.spyOn(publishService, 'toModerator');

      const either = await publishSeerOutcome.execute(roomID);

      expect(isRight(either)).toBe(true);
      expect(spy1).toHaveBeenCalledOnce();
      expect(spy2).not.toHaveBeenCalled();
      expect(spy3).not.toHaveBeenCalled();
    });

    it('should not publish event when all seers are dead', async () => {
      const roomID = Room.ID.from('f0efb80d-81cc-4538-9870-793c0419627b');
      const seerID1 = Person.ID.from('a8ec13bc-93a7-45c3-bdfd-e5f8e53702e6');
      const seerID2 = Person.ID.from('a8ec13bc-93a7-45c3-bdfd-e5f8e53702e6');
      const targetPersonID = Person.ID.from('07307b24-44ff-4a8c-8176-2a2e67567674');
      const seer1 = mockPerson({
        id: seerID1,
        life: 'DEAD',
        identity: Identity.SEER
      });
      const seer2 = mockPerson({
        id: seerID1,
        life: 'DEAD',
        identity: Identity.SEER
      });
      const room = mockRoom({
        id: roomID,
        game: mockGame({
          residents: [
            seer1,
            seer2,
            mockPerson({
              id: targetPersonID,
              life: 'ALIVE',
              identity: Identity.VILLAGER
            })
          ],
          records: [
            mockDailyOutcomeRecord({
              days: 1,
              divination: [
                {
                  source: seerID1,
                  destination: targetPersonID
                },
                {
                  source: seerID2,
                  destination: targetPersonID
                }
              ]
            })
          ]
        })
      });
      const uow = new MockUnitOfWork<AppTypeMap>();

      const spy1 = vi.spyOn(roomService, 'withTransaction').mockImplementation((_, proc) => {
        return proc({ uow, room });
      });
      const spy2 = vi.spyOn(publishService, 'toPerson');
      const spy3 = vi.spyOn(publishService, 'toModerator');

      const either = await publishSeerOutcome.execute(roomID);

      expect(isRight(either)).toBe(true);
      expect(spy1).toHaveBeenCalledOnce();
      expect(spy2).not.toHaveBeenCalled();
      expect(spy3).not.toHaveBeenCalled();
    });
  });
});
