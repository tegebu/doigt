import { createEventPublishError } from '@/adapters/presenters/EventPublisher/EventPublishError.js';
import type { IPublishService } from '@/applications/services/interfaces/IPublishService.js';
import type { IRoomService } from '@/applications/services/interfaces/IRoomService.js';
import { MockPublishService } from '@/applications/services/mocks/MockPublishService.js';
import { MockRoomService } from '@/applications/services/mocks/MockRoomService.js';
import type { AppTypeMap } from '@/applications/unitOfWork/AppTypeMap.js';
import { MockUnitOfWork } from '@/applications/unitOfWork/mocks/MockUnitOfWork.js';
import { PUBLISH } from '@/applications/websocket/PublishEvents.js';
import { mockRoom } from '@/domains/Room/mocks/MockRoom.js';
import { Room } from '@/domains/Room/Room.js';
import type { ILogger } from '@/lib/Logger/ILogger.js';
import { MockLogger } from '@/lib/Logger/mocks/MockLogger.js';
import { isLeft, isRight } from 'fp-ts/lib/Either.js';
import { left, right } from 'fp-ts/lib/TaskEither.js';
import { StartDayPhase } from '../StartDayPhase.js';

describe('StartDayPhase', () => {
  let roomService: IRoomService;
  let publishService: IPublishService;
  let logger: ILogger;
  let startDayPhase: StartDayPhase;

  beforeEach(() => {
    roomService = new MockRoomService();
    publishService = new MockPublishService();
    logger = new MockLogger();
    startDayPhase = new StartDayPhase(roomService, publishService, logger);
  });

  afterEach(() => {
    vi.clearAllMocks();
  });

  describe('execute', () => {
    it('should return right', async () => {
      const roomID = Room.ID.from('8ae5f35e-0962-4a6d-bba5-790afed97905');
      const room = mockRoom({
        id: roomID
      });
      const uow = new MockUnitOfWork<AppTypeMap>();

      const spy1 = vi.spyOn(roomService, 'withTransaction').mockImplementation((_, proc) => {
        return proc({ uow, room });
      });
      const spy2 = vi.spyOn(publishService, 'toEveryone').mockImplementation(() => right(null)());

      const either = await startDayPhase.execute(roomID);

      expect(isRight(either)).toBe(true);
      expect(spy1).toHaveBeenCalledOnce();
      expect(spy2).toHaveBeenCalledOnce();
      expect(spy2).toHaveBeenCalledWith(room, PUBLISH.DAY.START, {
        seconds: room.game.phaseDuration.day
      });
    });

    it('should return left when publishService.toEveryone() returns left', async () => {
      const roomID = Room.ID.from('8ae5f35e-0962-4a6d-bba5-790afed97905');
      const room = mockRoom({
        id: roomID
      });
      const uow = new MockUnitOfWork<AppTypeMap>();

      const spy1 = vi.spyOn(roomService, 'withTransaction').mockImplementation((_, proc) => {
        return proc({ uow, room });
      });
      const spy2 = vi.spyOn(publishService, 'toEveryone').mockImplementation(() => left(createEventPublishError('EmitFailed', ''))());

      const either = await startDayPhase.execute(roomID);

      expect(isLeft(either)).toBe(true);
      expect(spy1).toHaveBeenCalledOnce();
      expect(spy2).toHaveBeenCalledOnce();
      expect(spy2).toHaveBeenCalledWith(room, PUBLISH.DAY.START, {
        seconds: room.game.phaseDuration.day
      });
    });
  });
});
