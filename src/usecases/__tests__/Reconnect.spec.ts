import { createEventPublishError } from '@/adapters/presenters/EventPublisher/EventPublishError.js';
import type { IEventPublisher } from '@/adapters/presenters/EventPublisher/IEventPublisher.js';
import { MockEventPublisher } from '@/adapters/presenters/EventPublisher/mocks/MockEventPublisher.js';
import type { IConnectionPool } from '@/applications/connection/IConnectionPool.js';
import { MockConnectionPool } from '@/applications/connection/mocks/MockConnectionPool.js';
import { PUBLISH } from '@/applications/websocket/PublishEvents.js';
import { Client } from '@/domains/Client/Client.js';
import { mockWebSocket } from '@/infra/websocket/mocks/MockWebSocket.js';
import type { ILogger } from '@/lib/Logger/ILogger.js';
import { MockLogger } from '@/lib/Logger/mocks/MockLogger.js';
import { isRight } from 'fp-ts/lib/Either.js';
import { left, right } from 'fp-ts/lib/TaskEither.js';
import { Reconnect } from '../Reconnect.js';

describe('Reconnect', () => {
  let pool: IConnectionPool;
  let eventPublisher: IEventPublisher;
  let logger: ILogger;
  let reconnect: Reconnect;

  beforeEach(() => {
    pool = new MockConnectionPool();
    eventPublisher = new MockEventPublisher();
    logger = new MockLogger();
    reconnect = new Reconnect(pool, eventPublisher, logger);
  });

  afterEach(() => {
    vi.clearAllMocks();
  });

  describe('execute', () => {
    it('should reconnect the client', async () => {
      const id = Client.ID.from('a81b62d1-8cdf-4620-ad41-98fc332adfef');
      const ws = mockWebSocket();

      const spy1 = vi.spyOn(pool, 'reconnect').mockImplementation(() => null);
      const spy2 = vi.spyOn(eventPublisher, 'toClient').mockImplementation(() => right(null)());

      const either = await reconnect.execute(id, ws);

      expect(isRight(either)).toBe(true);
      expect(spy1).toHaveBeenCalledOnce();
      expect(spy2).toHaveBeenCalledOnce();
      expect(spy2).toHaveBeenCalledWith(id, PUBLISH.CONNECTION.RECONNECTED.SUCCESS, {});
    });

    it('should return publish error when reconnecting the client', async () => {
      const id = Client.ID.from('a81b62d1-8cdf-4620-ad41-98fc332adfef');
      const ws = mockWebSocket();

      const spy1 = vi.spyOn(pool, 'reconnect').mockImplementation(() => null);
      const spy2 = vi
        .spyOn(eventPublisher, 'toClient')
        .mockImplementationOnce(() => left(createEventPublishError('EmitFailed', ''))())
        .mockImplementationOnce(() => right(null)());

      const either = await reconnect.execute(id, ws);

      expect(isRight(either)).toBe(false);
      expect(spy1).toHaveBeenCalledOnce();
      expect(spy2).toHaveBeenCalledTimes(2);
      expect(spy2).toHaveBeenNthCalledWith(1, id, PUBLISH.CONNECTION.RECONNECTED.SUCCESS, {});
      expect(spy2).toHaveBeenNthCalledWith(2, id, PUBLISH.CONNECTION.RECONNECTED.FAILURE, {});
    });
  });
});
