import { createEventPublishError } from '@/adapters/presenters/EventPublisher/EventPublishError.js';
import type { IPublishService } from '@/applications/services/interfaces/IPublishService.js';
import type { IRoomService } from '@/applications/services/interfaces/IRoomService.js';
import { MockPublishService } from '@/applications/services/mocks/MockPublishService.js';
import { MockRoomService } from '@/applications/services/mocks/MockRoomService.js';
import type { AppTypeMap } from '@/applications/unitOfWork/AppTypeMap.js';
import { MockUnitOfWork } from '@/applications/unitOfWork/mocks/MockUnitOfWork.js';
import { PUBLISH } from '@/applications/websocket/PublishEvents.js';
import { Room } from '@/domains/Room/Room.js';
import { mockDailyOutcomeRecord } from '@/domains/Room/mocks/MockDailyOutcomeRecord.js';
import { mockDayPhase } from '@/domains/Room/mocks/MockDayPhase.js';
import { mockGame } from '@/domains/Room/mocks/MockGame.js';
import { mockRoom } from '@/domains/Room/mocks/MockRoom.js';
import type { ILogger } from '@/lib/Logger/ILogger.js';
import { MockLogger } from '@/lib/Logger/mocks/MockLogger.js';
import { isLeft, isRight } from 'fp-ts/lib/Either.js';
import { left, right } from 'fp-ts/lib/TaskEither.js';
import { EndNightPhase } from '../EndNightPhase.js';

describe('EndNightPhase', () => {
  let roomService: IRoomService;
  let publishService: IPublishService;
  let logger: ILogger;
  let endNightPhase: EndNightPhase;

  beforeEach(() => {
    roomService = new MockRoomService();
    publishService = new MockPublishService();
    logger = new MockLogger();
    endNightPhase = new EndNightPhase(roomService, publishService, logger);
  });

  afterEach(() => {
    vi.clearAllMocks();
  });

  describe('execute', () => {
    it('should end night phase', async () => {
      const roomID = Room.ID.from('d986e1f4-af74-453f-8c98-39e8e06b730a');
      const room = mockRoom({
        id: roomID,
        game: mockGame({
          dayPhase: mockDayPhase({
            days: 1,
            phase: 'NIGHT'
          }),
          records: [
            mockDailyOutcomeRecord({
              days: 0
            }),
            mockDailyOutcomeRecord({
              days: 1
            })
          ]
        })
      });
      const uow = new MockUnitOfWork<AppTypeMap>();

      const spy1 = vi.spyOn(roomService, 'withTransaction').mockImplementation((_, proc) => {
        return proc({ uow, room });
      });
      const spy2 = vi.spyOn(publishService, 'toEveryone').mockImplementation(() => right(null)());

      const either = await endNightPhase.execute(roomID);

      expect(isRight(either)).toBe(true);
      expect(spy1).toHaveBeenCalledOnce();
      expect(spy2).toHaveBeenCalledOnce();
      expect(spy2).toHaveBeenCalledWith(room, PUBLISH.NIGHT.END, {});
    });

    it('should return left if publishService.toEveryone() fails', async () => {
      const roomID = Room.ID.from('d986e1f4-af74-453f-8c98-39e8e06b730a');
      const room = mockRoom({
        id: roomID,
        game: mockGame({
          dayPhase: mockDayPhase({
            days: 1,
            phase: 'NIGHT'
          }),
          records: [
            mockDailyOutcomeRecord({
              days: 0
            }),
            mockDailyOutcomeRecord({
              days: 1
            })
          ]
        })
      });
      const uow = new MockUnitOfWork<AppTypeMap>();

      const spy1 = vi.spyOn(roomService, 'withTransaction').mockImplementation((_, proc) => {
        return proc({ uow, room });
      });
      const spy2 = vi.spyOn(publishService, 'toEveryone').mockImplementation(() => left(createEventPublishError('EmitFailed', ''))());

      const either = await endNightPhase.execute(roomID);

      expect(isLeft(either)).toBe(true);
      expect(spy1).toHaveBeenCalledOnce();
      expect(spy2).toHaveBeenCalledOnce();
      expect(spy2).toHaveBeenCalledWith(room, PUBLISH.NIGHT.END, {});
    });
  });
});
