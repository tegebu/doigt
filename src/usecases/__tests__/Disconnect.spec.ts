import type { IConnectionPool } from '@/applications/connection/IConnectionPool.js';
import { MockConnectionPool } from '@/applications/connection/mocks/MockConnectionPool.js';
import { Client } from '@/domains/Client/Client.js';
import { mockWebSocket } from '@/infra/websocket/mocks/MockWebSocket.js';
import type { ILogger } from '@/lib/Logger/ILogger.js';
import { MockLogger } from '@/lib/Logger/mocks/MockLogger.js';
import { isLeft, isRight } from 'fp-ts/lib/Either.js';
import { none, some } from 'fp-ts/lib/Option.js';
import { Disconnect } from '../Disconnect.js';

describe('Disconnect', () => {
  let pool: IConnectionPool;
  let logger: ILogger;
  let disconnect: Disconnect;

  beforeEach(() => {
    pool = new MockConnectionPool();
    logger = new MockLogger();
    disconnect = new Disconnect(pool, logger);
  });

  afterEach(() => {
    vi.clearAllMocks();
  });

  describe('execute', async () => {
    it('should disconnect the client', async () => {
      const ws = mockWebSocket();
      const clientID = Client.ID.from('2da4deef-8ace-4b9c-b64e-b093bdd29c0f');

      const spy1 = vi.spyOn(pool, 'getClientID').mockImplementation(() => some(clientID));
      const spy2 = vi.spyOn(pool, 'disconnect').mockImplementation(() => null);

      const either = await disconnect.execute(ws);

      expect(isRight(either)).toBe(true);
      expect(spy1).toHaveBeenCalledOnce();
      expect(spy2).toHaveBeenCalledOnce();
    });

    it('should return left when the client is not found', async () => {
      const ws = mockWebSocket();

      const spy1 = vi.spyOn(pool, 'getClientID').mockImplementation(() => none);
      const spy2 = vi.spyOn(pool, 'disconnect');

      const either = await disconnect.execute(ws);

      expect(isLeft(either)).toBe(true);
      expect(spy1).toHaveBeenCalledOnce();
      expect(spy2).not.toHaveBeenCalled();
    });
  });
});
