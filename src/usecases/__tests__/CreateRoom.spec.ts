import { createGatewayError } from '@/adapters/gateways/GatewayError.js';
import { createEventPublishError } from '@/adapters/presenters/EventPublisher/EventPublishError.js';
import type { IEventPublisher } from '@/adapters/presenters/EventPublisher/IEventPublisher.js';
import { MockEventPublisher } from '@/adapters/presenters/EventPublisher/mocks/MockEventPublisher.js';
import { PUBLISH } from '@/applications/websocket/PublishEvents.js';
import { Client } from '@/domains/Client/Client.js';
import type { IClientRepository } from '@/domains/Client/IClientRepository.js';
import { mockClient } from '@/domains/Client/mocks/MockClient.js';
import { MockClientRepository } from '@/domains/Client/mocks/MockClientRepository.js';
import type { IRoomRepository } from '@/domains/Room/IRoomRepository.js';
import type { RoomID } from '@/domains/Room/Room.js';
import { MockRoomRepository } from '@/domains/Room/mocks/MockRoomRepository.js';
import type { ILogger } from '@/lib/Logger/ILogger.js';
import { MockLogger } from '@/lib/Logger/mocks/MockLogger.js';
import type { Nullable } from '@jamashita/anden/type';
import { isLeft, isRight } from 'fp-ts/lib/Either.js';
import { none, some } from 'fp-ts/lib/Option.js';
import { left, right } from 'fp-ts/lib/TaskEither.js';
import { CreateRoom } from '../CreateRoom.js';

describe('CreateRoom', () => {
  let clientRepository: IClientRepository;
  let roomRepository: IRoomRepository;
  let eventPublisher: IEventPublisher;
  let logger: ILogger;
  let createRoom: CreateRoom;

  beforeEach(() => {
    clientRepository = new MockClientRepository();
    roomRepository = new MockRoomRepository();
    eventPublisher = new MockEventPublisher();
    logger = new MockLogger();
    createRoom = new CreateRoom(clientRepository, roomRepository, eventPublisher, logger);
  });

  afterEach(() => {
    vi.clearAllMocks();
  });

  describe('execute', () => {
    it('should publish success event when goes well', async () => {
      const name = 'QIMqNw7';
      const password = 'IbV5NBusEl2';
      const quota = 12;
      const clientID = Client.ID.from('d94588e9-22a5-4923-b632-940084395847');
      const client = mockClient({
        id: clientID
      });
      let roomID: Nullable<RoomID> = null;

      const spy1 = vi.spyOn(clientRepository, 'find').mockImplementation(() => right(some(client))());
      const spy2 = vi.spyOn(roomRepository, 'create').mockImplementation((room) => {
        roomID = room.id;

        return right(null)();
      });
      const spy3 = vi.spyOn(eventPublisher, 'toClient').mockImplementation(() => right(null)());
      const spy4 = vi.spyOn(eventPublisher, 'broadcast').mockImplementation(() => right(null)());

      const either = await createRoom.execute(name, password, quota, clientID, true);

      expect(isRight(either)).toBe(true);
      expect(spy1).toHaveBeenCalledOnce();
      expect(spy2).toHaveBeenCalledOnce();
      expect(spy3).toHaveBeenCalledOnce();
      expect(spy3).toHaveBeenCalledWith(clientID, PUBLISH.ROOM.CREATE.SUCCESS, {
        roomID
      });
      expect(spy4).toHaveBeenCalledOnce();
      expect(spy4).toHaveBeenCalledWith(PUBLISH.ROOM.NOTIFY.CREATED, {
        room: {
          id: roomID,
          name,
          moderator: client,
          quota,
          participants: 0,
          requirePassword: true,
          status: 'OPEN'
        }
      });
    });

    it('should publish success event when goes well: no password', async () => {
      const name = 'QIMqNw7';
      const password = null;
      const quota = 12;
      const clientID = Client.ID.from('d94588e9-22a5-4923-b632-940084395847');
      const client = mockClient({
        id: clientID
      });
      let roomID: Nullable<RoomID> = null;

      const spy1 = vi.spyOn(clientRepository, 'find').mockImplementation(() => right(some(client))());
      const spy2 = vi.spyOn(roomRepository, 'create').mockImplementation((room) => {
        roomID = room.id;

        return right(null)();
      });
      const spy3 = vi.spyOn(eventPublisher, 'toClient').mockImplementation(() => right(null)());
      const spy4 = vi.spyOn(eventPublisher, 'broadcast').mockImplementation(() => right(null)());

      const either = await createRoom.execute(name, password, quota, clientID, true);

      expect(isRight(either)).toBe(true);
      expect(spy1).toHaveBeenCalledOnce();
      expect(spy2).toHaveBeenCalledOnce();
      expect(spy3).toHaveBeenCalledOnce();
      expect(spy3).toHaveBeenCalledWith(clientID, PUBLISH.ROOM.CREATE.SUCCESS, {
        roomID
      });
      expect(spy4).toHaveBeenCalledOnce();
      expect(spy4).toHaveBeenCalledWith(PUBLISH.ROOM.NOTIFY.CREATED, {
        room: {
          id: roomID,
          name,
          moderator: client,
          quota,
          participants: 0,
          requirePassword: false,
          status: 'OPEN'
        }
      });
    });

    it('should publish success event when goes well: no moderator', async () => {
      const name = 'QIMqNw7';
      const password = 'IbV5NBusEl2';
      const quota = 12;
      const clientID = Client.ID.from('d94588e9-22a5-4923-b632-940084395847');
      const client = mockClient({
        id: clientID
      });
      let roomID: Nullable<RoomID> = null;

      const spy1 = vi.spyOn(clientRepository, 'find').mockImplementation(() => right(some(client))());
      const spy2 = vi.spyOn(roomRepository, 'create').mockImplementation((room) => {
        roomID = room.id;

        return right(null)();
      });
      const spy3 = vi.spyOn(eventPublisher, 'toClient').mockImplementation(() => right(null)());
      const spy4 = vi.spyOn(eventPublisher, 'broadcast').mockImplementation(() => right(null)());

      const either = await createRoom.execute(name, password, quota, clientID, false);

      expect(isRight(either)).toBe(true);
      expect(spy1).toHaveBeenCalledOnce();
      expect(spy2).toHaveBeenCalledOnce();
      expect(spy3).toHaveBeenCalledOnce();
      expect(spy3).toHaveBeenCalledWith(clientID, PUBLISH.ROOM.CREATE.SUCCESS, {
        roomID
      });
      expect(spy4).toHaveBeenCalledOnce();
      expect(spy4).toHaveBeenCalledWith(PUBLISH.ROOM.NOTIFY.CREATED, {
        room: {
          id: roomID,
          name,
          moderator: null,
          quota,
          participants: 1,
          requirePassword: true,
          status: 'OPEN'
        }
      });
    });

    it('should publish failure event when clientRepository.find() return none', async () => {
      const name = 'QIMqNw7';
      const password = 'IbV5NBusEl2';
      const quota = 12;
      const clientID = Client.ID.from('d94588e9-22a5-4923-b632-940084395847');

      const spy1 = vi.spyOn(clientRepository, 'find').mockImplementation(() => right(none)());
      const spy2 = vi.spyOn(roomRepository, 'create');
      const spy3 = vi.spyOn(eventPublisher, 'toClient').mockImplementation(() => right(null)());
      const spy4 = vi.spyOn(eventPublisher, 'broadcast');

      const either = await createRoom.execute(name, password, quota, clientID, true);

      expect(isLeft(either)).toBe(true);
      expect(spy1).toHaveBeenCalledOnce();
      expect(spy2).not.toHaveBeenCalled();
      expect(spy3).toHaveBeenCalledOnce();
      expect(spy3).toHaveBeenCalledWith(clientID, PUBLISH.ROOM.CREATE.FAILURE, {});
      expect(spy4).not.toHaveBeenCalled();
    });

    it('should publish failure event when roomRepository.create() return left', async () => {
      const name = 'QIMqNw7';
      const password = 'IbV5NBusEl2';
      const quota = 12;
      const clientID = Client.ID.from('d94588e9-22a5-4923-b632-940084395847');
      const client = mockClient({
        id: clientID
      });

      const spy1 = vi.spyOn(clientRepository, 'find').mockImplementation(() => right(some(client))());
      const spy2 = vi.spyOn(roomRepository, 'create').mockImplementation(() => left(createGatewayError('UnexpectedError', ''))());
      const spy3 = vi.spyOn(eventPublisher, 'toClient').mockImplementation(() => right(null)());
      const spy4 = vi.spyOn(eventPublisher, 'broadcast');

      const either = await createRoom.execute(name, password, quota, clientID, true);

      expect(isLeft(either)).toBe(true);
      expect(spy1).toHaveBeenCalledOnce();
      expect(spy2).toHaveBeenCalledOnce();
      expect(spy3).toHaveBeenCalledOnce();
      expect(spy3).toHaveBeenCalledWith(client.id, PUBLISH.ROOM.CREATE.FAILURE, {});
      expect(spy4).not.toHaveBeenCalled();
    });

    it('should publish failure event when eventPublisher.toClient() return left', async () => {
      const name = 'QIMqNw7';
      const password = 'IbV5NBusEl2';
      const quota = 12;
      const clientID = Client.ID.from('d94588e9-22a5-4923-b632-940084395847');
      const client = mockClient({
        id: clientID
      });

      const spy1 = vi.spyOn(clientRepository, 'find').mockImplementation(() => right(some(client))());
      const spy2 = vi.spyOn(roomRepository, 'create').mockImplementation(() => right(null)());
      const spy3 = vi.spyOn(eventPublisher, 'toClient').mockImplementation(() => left(createEventPublishError('EmitFailed', ''))());
      const spy4 = vi.spyOn(eventPublisher, 'broadcast');

      const either = await createRoom.execute(name, password, quota, clientID, true);

      expect(isLeft(either)).toBe(true);
      expect(spy1).toHaveBeenCalledOnce();
      expect(spy2).toHaveBeenCalledOnce();
      expect(spy3).toHaveBeenCalledTimes(2);
      expect(spy3).toHaveBeenCalledWith(client.id, PUBLISH.ROOM.CREATE.FAILURE, {});
      expect(spy4).not.toHaveBeenCalled();
    });
  });
});
