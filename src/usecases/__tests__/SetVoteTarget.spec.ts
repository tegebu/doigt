import type { IPublishService } from '@/applications/services/interfaces/IPublishService.js';
import type { IRoomService } from '@/applications/services/interfaces/IRoomService.js';
import { MockPublishService } from '@/applications/services/mocks/MockPublishService.js';
import { MockRoomService } from '@/applications/services/mocks/MockRoomService.js';
import type { AppTypeMap } from '@/applications/unitOfWork/AppTypeMap.js';
import { MockUnitOfWork } from '@/applications/unitOfWork/mocks/MockUnitOfWork.js';
import { createUnitOfWorkError } from '@/applications/unitOfWork/UnitOfWorkError.js';
import { PUBLISH } from '@/applications/websocket/PublishEvents.js';
import { Client } from '@/domains/Client/Client.js';
import { mockClient } from '@/domains/Client/mocks/MockClient.js';
import { Identity } from '@/domains/Identity/Identity.js';
import { mockPerson } from '@/domains/Person/mocks/MockPerson.js';
import { Person } from '@/domains/Person/Person.js';
import { mockDailyOutcomeRecord } from '@/domains/Room/mocks/MockDailyOutcomeRecord.js';
import { mockGame } from '@/domains/Room/mocks/MockGame.js';
import { mockRoom } from '@/domains/Room/mocks/MockRoom.js';
import type { Phase } from '@/domains/Room/Phase.js';
import { Room } from '@/domains/Room/Room.js';
import type { ILogger } from '@/lib/Logger/ILogger.js';
import { MockLogger } from '@/lib/Logger/mocks/MockLogger.js';
import { isLeft, isRight } from 'fp-ts/lib/Either.js';
import { left, right } from 'fp-ts/lib/TaskEither.js';
import { SetVoteTarget } from '../SetVoteTarget.js';

describe('SetVoteTarget', () => {
  let roomService: IRoomService;
  let publishService: IPublishService;
  let logger: ILogger;
  let setVoteTarget: SetVoteTarget;

  beforeEach(() => {
    roomService = new MockRoomService();
    publishService = new MockPublishService();
    logger = new MockLogger();
    setVoteTarget = new SetVoteTarget(roomService, publishService, logger);
  });

  afterEach(() => {
    vi.clearAllMocks();
  });

  describe('execute', () => {
    it('should publish success when everything is successful', async () => {
      const roomID = Room.ID.from('6d717e20-b470-47ee-8035-feef07be98a4');
      const sourceID = Person.ID.from('9a908d08-e4ad-4a87-b747-fe8acf56083e');
      const sourceClientID = Client.ID.from('699ee9cb-517b-4912-a930-fbfb5179a914');
      const source = mockPerson({
        id: sourceID,
        identity: Identity.VILLAGER,
        life: 'ALIVE',
        client: mockClient({
          id: sourceClientID
        })
      });
      const destinationID = Person.ID.from('64d11e44-8013-4643-9d9c-6510c86942f8');
      const destination = mockPerson({
        id: destinationID,
        identity: Identity.VILLAGER,
        life: 'ALIVE'
      });
      const room = mockRoom({
        id: roomID,
        game: mockGame({
          residents: [source, destination],
          dayPhase: {
            days: 1,
            phase: 'EVENING'
          },
          records: [
            mockDailyOutcomeRecord({
              days: 1
            })
          ]
        })
      });
      const uow = new MockUnitOfWork<AppTypeMap>();

      const spy1 = vi.spyOn(roomService, 'withTransaction').mockImplementation((_, proc) => {
        return proc({ uow, room });
      });
      const spy2 = vi.spyOn(uow, 'update').mockImplementation(() => right(null)());
      const spy3 = vi.spyOn(publishService, 'toPerson').mockImplementation(() => right(null)());
      const spy4 = vi.spyOn(publishService, 'toModerator').mockImplementation(() => right(null)());

      const either = await setVoteTarget.execute(roomID, sourceID, destinationID);

      expect(isRight(either)).toBe(true);
      expect(spy1).toHaveBeenCalledOnce();
      expect(spy2).toHaveBeenCalledOnce();
      expect(spy2).toHaveBeenCalledWith(
        'room',
        mockRoom({
          id: roomID,
          game: mockGame({
            residents: [source, destination],
            dayPhase: {
              days: 1,
              phase: 'EVENING'
            },
            records: [
              mockDailyOutcomeRecord({
                days: 1,
                vote: [
                  {
                    source: sourceID,
                    destination: destinationID
                  }
                ]
              })
            ]
          })
        })
      );
      expect(spy3).toHaveBeenCalledOnce();
      expect(spy3).toHaveBeenCalledWith(source, PUBLISH.VOTE.SET.SUCCESS, {
        sourceID,
        destinationID
      });
      expect(spy4).toHaveBeenCalledOnce();
      expect(spy4).toHaveBeenCalledWith(
        mockRoom({
          id: roomID,
          game: mockGame({
            residents: [source, destination],
            dayPhase: {
              days: 1,
              phase: 'EVENING'
            },
            records: [
              mockDailyOutcomeRecord({
                days: 1,
                vote: []
              })
            ]
          })
        }),
        PUBLISH.VOTE.SET.SUCCESS,
        {
          sourceID,
          destinationID
        }
      );
    });

    it('should publish failure when voter is dead', async () => {
      const roomID = Room.ID.from('6d717e20-b470-47ee-8035-feef07be98a4');
      const sourceID = Person.ID.from('9a908d08-e4ad-4a87-b747-fe8acf56083e');
      const source = mockPerson({
        id: sourceID,
        identity: Identity.VILLAGER,
        life: 'DEAD'
      });
      const destinationID = Person.ID.from('64d11e44-8013-4643-9d9c-6510c86942f8');
      const destination = mockPerson({
        id: destinationID,
        identity: Identity.VILLAGER,
        life: 'ALIVE'
      });
      const room = mockRoom({
        id: roomID,
        game: mockGame({
          residents: [source, destination],
          dayPhase: {
            days: 1,
            phase: 'EVENING'
          },
          records: [
            mockDailyOutcomeRecord({
              days: 1
            })
          ]
        })
      });
      const uow = new MockUnitOfWork<AppTypeMap>();

      const spy1 = vi.spyOn(roomService, 'withTransaction').mockImplementation((_, proc) => {
        return proc({ uow, room });
      });
      const spy2 = vi.spyOn(uow, 'update');
      const spy3 = vi.spyOn(publishService, 'toPerson').mockImplementation(() => right(null)());
      const spy4 = vi.spyOn(publishService, 'toModerator').mockImplementation(() => right(null)());

      const either = await setVoteTarget.execute(roomID, sourceID, destinationID);

      expect(isLeft(either)).toBe(true);
      expect(spy1).toHaveBeenCalledOnce();
      expect(spy2).not.toHaveBeenCalled();
      expect(spy3).toHaveBeenCalledOnce();
      expect(spy3).toHaveBeenCalledWith(source, PUBLISH.VOTE.SET.FAILURE, {
        cause: 'SOURCE_DEAD',
        sourceID,
        destinationID
      });
      expect(spy4).toHaveBeenCalledOnce();
      expect(spy4).toHaveBeenCalledWith(room, PUBLISH.VOTE.SET.FAILURE, {
        cause: 'SOURCE_DEAD',
        sourceID,
        destinationID
      });
    });

    it('should publish failure when destination is dead', async () => {
      const roomID = Room.ID.from('6d717e20-b470-47ee-8035-feef07be98a4');
      const sourceID = Person.ID.from('9a908d08-e4ad-4a87-b747-fe8acf56083e');
      const sourceClientID = Client.ID.from('699ee9cb-517b-4912-a930-fbfb5179a914');
      const source = mockPerson({
        id: sourceID,
        identity: Identity.VILLAGER,
        life: 'ALIVE',
        client: mockClient({
          id: sourceClientID
        })
      });
      const destinationID = Person.ID.from('64d11e44-8013-4643-9d9c-6510c86942f8');
      const destination = mockPerson({
        id: destinationID,
        identity: Identity.VILLAGER,
        life: 'DEAD'
      });
      const room = mockRoom({
        id: roomID,
        game: mockGame({
          residents: [source, destination],
          dayPhase: {
            days: 1,
            phase: 'EVENING'
          },
          records: [
            mockDailyOutcomeRecord({
              days: 1
            })
          ]
        })
      });
      const uow = new MockUnitOfWork<AppTypeMap>();

      const spy1 = vi.spyOn(roomService, 'withTransaction').mockImplementation((_, proc) => {
        return proc({ uow, room });
      });
      const spy2 = vi.spyOn(uow, 'update');
      const spy3 = vi.spyOn(publishService, 'toPerson').mockImplementation(() => right(null)());
      const spy4 = vi.spyOn(publishService, 'toModerator').mockImplementation(() => right(null)());

      const either = await setVoteTarget.execute(roomID, sourceID, destinationID);

      expect(isLeft(either)).toBe(true);
      expect(spy1).toHaveBeenCalledOnce();
      expect(spy2).not.toHaveBeenCalled();
      expect(spy3).toHaveBeenCalledOnce();
      expect(spy3).toHaveBeenCalledWith(source, PUBLISH.VOTE.SET.FAILURE, {
        cause: 'DESTINATION_DEAD',
        sourceID,
        destinationID
      });
      expect(spy4).toHaveBeenCalledOnce();
      expect(spy4).toHaveBeenCalledWith(room, PUBLISH.VOTE.SET.FAILURE, {
        cause: 'DESTINATION_DEAD',
        sourceID,
        destinationID
      });
    });

    it.each`
      phase
      ${'MORNING'}
      ${'DAY'}
      ${'NIGHT'}
    `('should publish failure when the phase is $phase', async ({ phase }: { phase: Phase }) => {
      const roomID = Room.ID.from('6d717e20-b470-47ee-8035-feef07be98a4');
      const sourceID = Person.ID.from('9a908d08-e4ad-4a87-b747-fe8acf56083e');
      const sourceClientID = Client.ID.from('699ee9cb-517b-4912-a930-fbfb5179a914');
      const source = mockPerson({
        id: sourceID,
        identity: Identity.VILLAGER,
        life: 'ALIVE',
        client: mockClient({
          id: sourceClientID
        })
      });
      const destinationID = Person.ID.from('64d11e44-8013-4643-9d9c-6510c86942f8');
      const destination = mockPerson({
        id: destinationID,
        identity: Identity.VILLAGER,
        life: 'ALIVE'
      });
      const room = mockRoom({
        id: roomID,
        game: mockGame({
          residents: [source, destination],
          dayPhase: {
            days: 1,
            phase
          },
          records: [
            mockDailyOutcomeRecord({
              days: 1
            })
          ]
        })
      });
      const uow = new MockUnitOfWork<AppTypeMap>();

      const spy1 = vi.spyOn(roomService, 'withTransaction').mockImplementation((_, proc) => {
        return proc({ uow, room });
      });
      const spy2 = vi.spyOn(uow, 'update');
      const spy3 = vi.spyOn(publishService, 'toPerson').mockImplementation(() => right(null)());
      const spy4 = vi.spyOn(publishService, 'toModerator').mockImplementation(() => right(null)());

      const either = await setVoteTarget.execute(roomID, sourceID, destinationID);

      expect(isLeft(either)).toBe(true);
      expect(spy1).toHaveBeenCalledOnce();
      expect(spy2).not.toHaveBeenCalled();
      expect(spy3).toHaveBeenCalledOnce();
      expect(spy3).toHaveBeenCalledWith(source, PUBLISH.VOTE.SET.FAILURE, {
        cause: 'UNACCEPTABLE_PHASE',
        sourceID,
        destinationID
      });
      expect(spy4).toHaveBeenCalledOnce();
      expect(spy4).toHaveBeenCalledWith(room, PUBLISH.VOTE.SET.FAILURE, {
        cause: 'UNACCEPTABLE_PHASE',
        sourceID,
        destinationID
      });
    });

    it('should publish failure when uow.update() returns left', async () => {
      const roomID = Room.ID.from('6d717e20-b470-47ee-8035-feef07be98a4');
      const sourceID = Person.ID.from('9a908d08-e4ad-4a87-b747-fe8acf56083e');
      const sourceClientID = Client.ID.from('699ee9cb-517b-4912-a930-fbfb5179a914');
      const source = mockPerson({
        id: sourceID,
        identity: Identity.VILLAGER,
        life: 'ALIVE',
        client: mockClient({
          id: sourceClientID
        })
      });
      const destinationID = Person.ID.from('64d11e44-8013-4643-9d9c-6510c86942f8');
      const destination = mockPerson({
        id: destinationID,
        identity: Identity.VILLAGER,
        life: 'ALIVE'
      });
      const room = mockRoom({
        id: roomID,
        game: mockGame({
          residents: [source, destination],
          dayPhase: {
            days: 1,
            phase: 'EVENING'
          },
          records: [
            mockDailyOutcomeRecord({
              days: 1
            })
          ]
        })
      });
      const uow = new MockUnitOfWork<AppTypeMap>();

      const spy1 = vi.spyOn(roomService, 'withTransaction').mockImplementation((_, proc) => {
        return proc({ uow, room });
      });
      const spy2 = vi.spyOn(uow, 'update').mockImplementation(() => left(createUnitOfWorkError('Update', ''))());
      const spy3 = vi.spyOn(publishService, 'toPerson').mockImplementation(() => right(null)());
      const spy4 = vi.spyOn(publishService, 'toModerator').mockImplementation(() => right(null)());

      const either = await setVoteTarget.execute(roomID, sourceID, destinationID);

      console.log(either);

      expect(isLeft(either)).toBe(true);
      expect(spy1).toHaveBeenCalledOnce();
      expect(spy2).toHaveBeenCalledOnce();
      expect(spy3).toHaveBeenCalledOnce();
      expect(spy3).toHaveBeenCalledWith(source, PUBLISH.VOTE.SET.FAILURE, {
        cause: null,
        sourceID,
        destinationID
      });
      expect(spy4).toHaveBeenCalledOnce();
      expect(spy4).toHaveBeenCalledWith(room, PUBLISH.VOTE.SET.FAILURE, {
        cause: null,
        sourceID,
        destinationID
      });
    });

    it('should publish success and corruption when voter is possessed', async () => {
      const roomID = Room.ID.from('6d717e20-b470-47ee-8035-feef07be98a4');
      const sourceID = Person.ID.from('9a908d08-e4ad-4a87-b747-fe8acf56083e');
      const sourceClientID = Client.ID.from('699ee9cb-517b-4912-a930-fbfb5179a914');
      const source = mockPerson({
        id: sourceID,
        life: 'ALIVE',
        client: mockClient({
          id: sourceClientID
        }),
        identity: Identity.POSSESSED
      });
      const destinationID = Person.ID.from('64d11e44-8013-4643-9d9c-6510c86942f8');
      const destination = mockPerson({
        id: destinationID,
        identity: Identity.VILLAGER,
        life: 'ALIVE'
      });
      const room = mockRoom({
        id: roomID,
        game: mockGame({
          residents: [source, destination],
          dayPhase: {
            days: 1,
            phase: 'EVENING'
          },
          records: [
            mockDailyOutcomeRecord({
              days: 1
            })
          ]
        })
      });
      const uow = new MockUnitOfWork<AppTypeMap>();

      const spy1 = vi.spyOn(roomService, 'withTransaction').mockImplementation((_, proc) => {
        return proc({ uow, room });
      });
      const spy2 = vi.spyOn(uow, 'update').mockImplementation(() => right(null)());
      const spy3 = vi.spyOn(publishService, 'toPerson').mockImplementation(() => right(null)());
      const spy4 = vi.spyOn(publishService, 'toModerator').mockImplementation(() => right(null)());

      const either = await setVoteTarget.execute(roomID, sourceID, destinationID);

      expect(isRight(either)).toBe(true);
      expect(spy1).toHaveBeenCalledOnce();
      expect(spy2).toHaveBeenCalledOnce();
      expect(spy2).toHaveBeenCalledWith(
        'room',
        mockRoom({
          id: roomID,
          game: mockGame({
            residents: [source, destination],
            dayPhase: {
              days: 1,
              phase: 'EVENING'
            },
            records: [
              mockDailyOutcomeRecord({
                days: 1,
                corruption: [
                  {
                    source: sourceID,
                    destination: destinationID
                  }
                ],
                vote: [
                  {
                    source: sourceID,
                    destination: destinationID
                  }
                ]
              })
            ]
          })
        })
      );
      expect(spy3).toHaveBeenCalledOnce();
      expect(spy3).toHaveBeenCalledWith(source, PUBLISH.VOTE.SET.SUCCESS, {
        sourceID,
        destinationID
      });
      expect(spy4).toHaveBeenCalledOnce();
      expect(spy4).toHaveBeenCalledWith(
        mockRoom({
          id: roomID,
          game: mockGame({
            residents: [source, destination],
            dayPhase: {
              days: 1,
              phase: 'EVENING'
            },
            records: [
              mockDailyOutcomeRecord({
                days: 1,
                vote: []
              })
            ]
          })
        }),
        PUBLISH.VOTE.SET.SUCCESS,
        {
          sourceID,
          destinationID
        }
      );
    });

    it('should publish success and retaliation when voter is hunter', async () => {
      const roomID = Room.ID.from('6d717e20-b470-47ee-8035-feef07be98a4');
      const sourceID = Person.ID.from('9a908d08-e4ad-4a87-b747-fe8acf56083e');
      const sourceClientID = Client.ID.from('699ee9cb-517b-4912-a930-fbfb5179a914');
      const source = mockPerson({
        id: sourceID,
        life: 'ALIVE',
        client: mockClient({
          id: sourceClientID
        }),
        identity: Identity.HUNTER
      });
      const destinationID = Person.ID.from('64d11e44-8013-4643-9d9c-6510c86942f8');
      const destination = mockPerson({
        id: destinationID,
        identity: Identity.VILLAGER,
        life: 'ALIVE'
      });
      const room = mockRoom({
        id: roomID,
        game: mockGame({
          residents: [source, destination],
          dayPhase: {
            days: 1,
            phase: 'EVENING'
          },
          records: [
            mockDailyOutcomeRecord({
              days: 1
            })
          ]
        })
      });
      const uow = new MockUnitOfWork<AppTypeMap>();

      const spy1 = vi.spyOn(roomService, 'withTransaction').mockImplementation((_, proc) => {
        return proc({ uow, room });
      });
      const spy2 = vi.spyOn(uow, 'update').mockImplementation(() => right(null)());
      const spy3 = vi.spyOn(publishService, 'toPerson').mockImplementation(() => right(null)());
      const spy4 = vi.spyOn(publishService, 'toModerator').mockImplementation(() => right(null)());

      const either = await setVoteTarget.execute(roomID, sourceID, destinationID);

      expect(isRight(either)).toBe(true);
      expect(spy1).toHaveBeenCalledOnce();
      expect(spy2).toHaveBeenCalledOnce();
      expect(spy2).toHaveBeenCalledWith(
        'room',
        mockRoom({
          id: roomID,
          game: mockGame({
            residents: [source, destination],
            dayPhase: {
              days: 1,
              phase: 'EVENING'
            },
            records: [
              mockDailyOutcomeRecord({
                days: 1,
                retaliation: [
                  {
                    source: sourceID,
                    destination: destinationID
                  }
                ],
                vote: [
                  {
                    source: sourceID,
                    destination: destinationID
                  }
                ]
              })
            ]
          })
        })
      );
      expect(spy3).toHaveBeenCalledOnce();
      expect(spy3).toHaveBeenCalledWith(source, PUBLISH.VOTE.SET.SUCCESS, {
        sourceID,
        destinationID
      });
      expect(spy4).toHaveBeenCalledOnce();
      expect(spy4).toHaveBeenCalledWith(
        mockRoom({
          id: roomID,
          game: mockGame({
            residents: [source, destination],
            dayPhase: {
              days: 1,
              phase: 'EVENING'
            },
            records: [
              mockDailyOutcomeRecord({
                days: 1,
                vote: []
              })
            ]
          })
        }),
        PUBLISH.VOTE.SET.SUCCESS,
        {
          sourceID,
          destinationID
        }
      );
    });
  });
});
