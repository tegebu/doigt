import type { IPublishService } from '@/applications/services/interfaces/IPublishService.js';
import type { IRoomService } from '@/applications/services/interfaces/IRoomService.js';
import { MockPublishService } from '@/applications/services/mocks/MockPublishService.js';
import { MockRoomService } from '@/applications/services/mocks/MockRoomService.js';
import type { AppTypeMap } from '@/applications/unitOfWork/AppTypeMap.js';
import type { IUnitOfWork } from '@/applications/unitOfWork/IUnitOfWork.js';
import { MockUnitOfWork } from '@/applications/unitOfWork/mocks/MockUnitOfWork.js';
import { PUBLISH } from '@/applications/websocket/PublishEvents.js';
import { Identity } from '@/domains/Identity/Identity.js';
import { mockPerson } from '@/domains/Person/mocks/MockPerson.js';
import { Person } from '@/domains/Person/Person.js';
import { Persons } from '@/domains/Person/Persons.js';
import { mockDailyOutcomeRecord } from '@/domains/Room/mocks/MockDailyOutcomeRecord.js';
import { mockGame } from '@/domains/Room/mocks/MockGame.js';
import { mockRoom } from '@/domains/Room/mocks/MockRoom.js';
import { Room } from '@/domains/Room/Room.js';
import type { ILogger } from '@/lib/Logger/ILogger.js';
import { MockLogger } from '@/lib/Logger/mocks/MockLogger.js';
import type { Nullable } from '@jamashita/anden/type';
import { isLeft, isRight } from 'fp-ts/lib/Either.js';
import { right } from 'fp-ts/lib/TaskEither.js';
import { EndEveningPhase } from '../EndEveningPhase.js';

describe('EndEveningPhase', () => {
  let roomService: IRoomService;
  let publishService: IPublishService;
  let logger: ILogger;
  let endEveningPhase: EndEveningPhase;

  beforeEach(() => {
    roomService = new MockRoomService();
    publishService = new MockPublishService();
    logger = new MockLogger();
    endEveningPhase = new EndEveningPhase(roomService, publishService, logger);
  });

  describe('execute', () => {
    it('should return the result of the evening phase: not game end', async () => {
      const roomID = Room.ID.from('a837da12-c1ae-4f37-aaaa-ad2d45ba90be');
      const personID1 = Person.ID.from('8ba69365-2147-47d0-994c-ec84687f5e00');
      const personID2 = Person.ID.from('49531b1c-f43c-4093-b80b-7abdb1854779');
      const personID3 = Person.ID.from('8e79c286-300d-46a7-8c16-28b15dbc9cb7');
      const personID4 = Person.ID.from('7047de1b-2a46-4191-89cf-4fd9b4a2d4db');
      const room = mockRoom({
        id: roomID,
        game: mockGame({
          residents: [
            mockPerson({
              id: personID1,
              identity: Identity.VILLAGER,
              life: 'ALIVE'
            }),
            mockPerson({
              id: personID2,
              identity: Identity.VILLAGER,
              life: 'ALIVE'
            }),
            mockPerson({
              id: personID3,
              identity: Identity.WEREWOLF,
              life: 'ALIVE'
            }),
            mockPerson({
              id: personID4,
              identity: Identity.VILLAGER,
              life: 'ALIVE'
            })
          ],
          records: [
            mockDailyOutcomeRecord({
              days: 0,
              vote: [
                {
                  source: personID1,
                  destination: personID4
                },
                {
                  source: personID2,
                  destination: personID4
                },
                {
                  source: personID3,
                  destination: personID1
                },
                {
                  source: personID4,
                  destination: personID3
                }
              ]
            })
          ]
        })
      });
      const uow = new MockUnitOfWork<AppTypeMap>();

      const spy1 = vi.spyOn(roomService, 'withTransaction').mockImplementation((_, proc) => {
        return proc({ room, uow });
      });
      const spy2 = vi.spyOn(publishService, 'toEveryone').mockImplementation(() => right(null)());
      const spy3 = vi.spyOn(uow, 'update').mockImplementation(() => right(null)());

      const either = await endEveningPhase.execute(roomID);

      expect(isRight(either)).toBe(true);
      expect(spy1).toHaveBeenCalledOnce();
      expect(spy2).toHaveBeenCalledTimes(2);
      expect(spy2).toHaveBeenNthCalledWith(1, room, PUBLISH.LYNCHED.SOME, {
        victimID: personID4
      });
      expect(spy2).toHaveBeenNthCalledWith(2, room, PUBLISH.EVENING.END, {});
      expect(spy3).toHaveBeenCalledOnce();

      if (isLeft(either)) {
        throw new Error('This should not happen');
      }

      expect(either.right).toEqual([]);
    });

    it('should return the result of the evening phase: game end', async () => {
      const roomID = Room.ID.from('a837da12-c1ae-4f37-aaaa-ad2d45ba90be');
      const personID1 = Person.ID.from('8ba69365-2147-47d0-994c-ec84687f5e00');
      const personID2 = Person.ID.from('49531b1c-f43c-4093-b80b-7abdb1854779');
      const personID3 = Person.ID.from('8e79c286-300d-46a7-8c16-28b15dbc9cb7');
      const personID4 = Person.ID.from('7047de1b-2a46-4191-89cf-4fd9b4a2d4db');
      const room = mockRoom({
        id: roomID,
        game: mockGame({
          residents: [
            mockPerson({
              id: personID1,
              identity: Identity.VILLAGER,
              life: 'ALIVE'
            }),
            mockPerson({
              id: personID2,
              identity: Identity.VILLAGER,
              life: 'DEAD'
            }),
            mockPerson({
              id: personID3,
              identity: Identity.WEREWOLF,
              life: 'ALIVE'
            }),
            mockPerson({
              id: personID4,
              identity: Identity.VILLAGER,
              life: 'ALIVE'
            })
          ],
          records: [
            mockDailyOutcomeRecord({
              days: 0,
              vote: [
                {
                  source: personID1,
                  destination: personID4
                },
                {
                  source: personID3,
                  destination: personID4
                },
                {
                  source: personID4,
                  destination: personID3
                }
              ]
            })
          ]
        })
      });
      const uow = new MockUnitOfWork<AppTypeMap>();

      const spy1 = vi.spyOn(roomService, 'withTransaction').mockImplementation((_, proc) => {
        return proc({ room, uow });
      });
      const spy2 = vi.spyOn(publishService, 'toEveryone').mockImplementation(() => right(null)());
      const spy3 = vi.spyOn(uow, 'update').mockImplementation(() => right(null)());

      const either = await endEveningPhase.execute(roomID);

      expect(isRight(either)).toBe(true);
      expect(spy1).toHaveBeenCalledOnce();
      expect(spy2).toHaveBeenCalledTimes(2);
      expect(spy2).toHaveBeenNthCalledWith(1, room, PUBLISH.LYNCHED.SOME, {
        victimID: personID4
      });
      expect(spy2).toHaveBeenNthCalledWith(2, room, PUBLISH.EVENING.END, {});
      expect(spy3).toHaveBeenCalledOnce();

      if (isLeft(either)) {
        throw new Error('This should not happen');
      }

      expect(either.right).toEqual([
        {
          team: 'WEREWOLVES',
          terminateGame: true
        }
      ]);
    });

    it.each(Array.from({ length: 100 }, (_, i) => i + 1))('should return the result of the evening phase: choose randomly: #%i attempt', async () => {
      const roomID = Room.ID.from('a837da12-c1ae-4f37-aaaa-ad2d45ba90be');
      const personID1 = Person.ID.from('8ba69365-2147-47d0-994c-ec84687f5e00');
      const personID2 = Person.ID.from('49531b1c-f43c-4093-b80b-7abdb1854779');
      const personID3 = Person.ID.from('8e79c286-300d-46a7-8c16-28b15dbc9cb7');
      const personID4 = Person.ID.from('7047de1b-2a46-4191-89cf-4fd9b4a2d4db');
      const room = mockRoom({
        id: roomID,
        game: mockGame({
          residents: [
            mockPerson({
              id: personID1,
              identity: Identity.VILLAGER,
              life: 'ALIVE'
            }),
            mockPerson({
              id: personID2,
              identity: Identity.VILLAGER,
              life: 'ALIVE'
            }),
            mockPerson({
              id: personID3,
              identity: Identity.WEREWOLF,
              life: 'ALIVE'
            }),
            mockPerson({
              id: personID4,
              identity: Identity.VILLAGER,
              life: 'ALIVE'
            })
          ],
          records: [
            mockDailyOutcomeRecord({
              days: 0,
              vote: [
                {
                  source: personID1,
                  destination: personID4
                },
                {
                  source: personID2,
                  destination: personID4
                },
                {
                  source: personID3,
                  destination: personID1
                },
                {
                  source: personID4,
                  destination: personID1
                }
              ]
            })
          ]
        })
      });
      const uow: IUnitOfWork<AppTypeMap> = new MockUnitOfWork<AppTypeMap>();

      let victim: Nullable<Person> = mockPerson();

      const spy1 = vi.spyOn(roomService, 'withTransaction').mockImplementation((_, proc) => {
        return proc({ room, uow });
      });
      const spy2 = vi.spyOn(publishService, 'toEveryone').mockImplementation(() => right(null)());
      const spy3 = vi.spyOn(uow, 'update').mockImplementation((_, room) => {
        victim =
          [
            ...Persons.filter(room.game.residents, (person) => {
              return Person.isDead(person);
            }).values()
          ][0] ?? null;

        return right(null)();
      });

      const either = await endEveningPhase.execute(roomID);

      expect(isRight(either)).toBe(true);
      expect(spy1).toHaveBeenCalledOnce();
      expect(spy2).toHaveBeenCalledTimes(2);
      expect(spy2).toHaveBeenNthCalledWith(1, room, PUBLISH.LYNCHED.SOME, {
        victimID: victim.id
      });
      expect(spy2).toHaveBeenNthCalledWith(2, room, PUBLISH.EVENING.END, {});
      expect(spy3).toHaveBeenCalledOnce();

      if (isLeft(either)) {
        throw new Error('This should not happen');
      }

      expect(either.right).toEqual([]);
    });
  });
});
