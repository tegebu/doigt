import { createGatewayError } from '@/adapters/gateways/GatewayError.js';
import type { IPublishService } from '@/applications/services/interfaces/IPublishService.js';
import type { IRoomService } from '@/applications/services/interfaces/IRoomService.js';
import { MockPublishService } from '@/applications/services/mocks/MockPublishService.js';
import { MockRoomService } from '@/applications/services/mocks/MockRoomService.js';
import type { AppTypeMap } from '@/applications/unitOfWork/AppTypeMap.js';
import { MockUnitOfWork } from '@/applications/unitOfWork/mocks/MockUnitOfWork.js';
import { PUBLISH } from '@/applications/websocket/PublishEvents.js';
import { Client } from '@/domains/Client/Client.js';
import type { IClientRepository } from '@/domains/Client/IClientRepository.js';
import { mockClient } from '@/domains/Client/mocks/MockClient.js';
import { MockClientRepository } from '@/domains/Client/mocks/MockClientRepository.js';
import { Room } from '@/domains/Room/Room.js';
import { createRoomError } from '@/domains/Room/RoomError.js';
import { mockRoom } from '@/domains/Room/mocks/MockRoom.js';
import type { ILogger } from '@/lib/Logger/ILogger.js';
import { MockLogger } from '@/lib/Logger/mocks/MockLogger.js';
import { isLeft, isRight } from 'fp-ts/lib/Either.js';
import { none, some } from 'fp-ts/lib/Option.js';
import { left, right } from 'fp-ts/lib/TaskEither.js';
import { LeaveRoom } from '../LeaveRoom.js';

describe('LeaveRoom', () => {
  let clientRepository: IClientRepository;
  let roomService: IRoomService;
  let publishService: IPublishService;
  let logger: ILogger;
  let leaveRoom: LeaveRoom;

  beforeEach(() => {
    clientRepository = new MockClientRepository();
    roomService = new MockRoomService();
    publishService = new MockPublishService();
    logger = new MockLogger();
    leaveRoom = new LeaveRoom(clientRepository, roomService, publishService, logger);
  });

  afterEach(() => {
    vi.clearAllMocks();
  });

  describe('execute', () => {
    it('should return right: moderator is present', async () => {
      const roomID = Room.ID.from('4378572d-c541-4e14-8972-31a6f4a5e592');
      const clientID = Client.ID.from('8f060c9d-e346-4c26-a313-d1c6a7482e77');
      const moderatorClientID = Client.ID.from('030cd6a4-2beb-420f-8ff4-f07051aa25a3');
      const client = mockClient({
        id: clientID
      });
      const moderator = mockClient({
        id: moderatorClientID
      });
      const room = mockRoom({
        id: roomID,
        moderator,
        participants: [client]
      });
      const uow = new MockUnitOfWork<AppTypeMap>();
      const spy1 = vi.spyOn(clientRepository, 'find').mockImplementation(() => right(some(client))());
      const spy2 = vi.spyOn(roomService, 'withTransaction').mockImplementation((_, proc) => {
        return proc({ uow, room });
      });
      const spy3 = vi.spyOn(uow, 'update').mockImplementation(() => right(null)());
      const spy4 = vi.spyOn(publishService, 'toEveryone').mockImplementation(() => right(null)());
      const spy5 = vi.spyOn(publishService, 'toClient').mockImplementation(() => right(null)());
      const spy6 = vi.spyOn(roomService, 'destroy');
      const spy7 = vi.spyOn(publishService, 'broadcast').mockImplementation(() => right(null)());

      const either = await leaveRoom.execute(roomID, clientID);

      expect(isRight(either)).toBe(true);
      expect(spy1).toHaveBeenCalledOnce();
      expect(spy2).toHaveBeenCalledOnce();
      expect(spy3).toHaveBeenCalledOnce();
      expect(spy4).toHaveBeenCalledOnce();
      expect(spy5).toHaveBeenCalledOnce();
      expect(spy6).not.toHaveBeenCalled();
      expect(spy7).toHaveBeenCalledOnce();
    });

    it('should return right and does not destroy the room when there is a moderator', async () => {
      const roomID = Room.ID.from('4378572d-c541-4e14-8972-31a6f4a5e592');
      const clientID = Client.ID.from('8f060c9d-e346-4c26-a313-d1c6a7482e77');
      const moderatorClientID = Client.ID.from('030cd6a4-2beb-420f-8ff4-f07051aa25a3');
      const client = mockClient({
        id: clientID
      });
      const moderator = mockClient({
        id: moderatorClientID
      });
      const room = mockRoom({
        id: roomID,
        moderator,
        participants: [client]
      });
      const uow = new MockUnitOfWork<AppTypeMap>();
      const spy1 = vi.spyOn(clientRepository, 'find').mockImplementation(() => right(some(client))());
      const spy2 = vi.spyOn(roomService, 'withTransaction').mockImplementation((_, proc) => {
        return proc({ uow, room });
      });
      const spy3 = vi.spyOn(uow, 'update').mockImplementation(() => right(null)());
      const spy4 = vi.spyOn(publishService, 'toEveryone').mockImplementation(() => right(null)());
      const spy5 = vi.spyOn(publishService, 'toClient').mockImplementation(() => right(null)());
      const spy6 = vi.spyOn(roomService, 'destroy');
      const spy7 = vi.spyOn(publishService, 'broadcast').mockImplementation(() => right(null)());

      const either = await leaveRoom.execute(roomID, clientID);

      expect(isRight(either)).toBe(true);
      expect(spy1).toHaveBeenCalledOnce();
      expect(spy2).toHaveBeenCalledOnce();
      expect(spy3).toHaveBeenCalledOnce();
      expect(spy4).toHaveBeenCalledOnce();
      expect(spy5).toHaveBeenCalledOnce();
      expect(spy6).not.toHaveBeenCalled();
      expect(spy7).toHaveBeenCalledOnce();
    });

    it('should return right and destroy the room when there is no moderator', async () => {
      const roomID = Room.ID.from('4378572d-c541-4e14-8972-31a6f4a5e592');
      const clientID = Client.ID.from('8f060c9d-e346-4c26-a313-d1c6a7482e77');
      const client = mockClient({
        id: clientID
      });
      const room = mockRoom({
        id: roomID,
        moderator: null,
        participants: [client]
      });
      const uow = new MockUnitOfWork<AppTypeMap>();
      const spy1 = vi.spyOn(clientRepository, 'find').mockImplementation(() => right(some(client))());
      const spy2 = vi.spyOn(roomService, 'withTransaction').mockImplementation((_, proc) => {
        return proc({ uow, room });
      });
      const spy3 = vi.spyOn(uow, 'update').mockImplementation(() => right(null)());
      const spy4 = vi.spyOn(publishService, 'toEveryone').mockImplementation(() => right(null)());
      const spy5 = vi.spyOn(publishService, 'toClient').mockImplementation(() => right(null)());
      const spy6 = vi.spyOn(roomService, 'destroy').mockImplementation(() => right(null)());
      const spy7 = vi.spyOn(publishService, 'broadcast').mockImplementation(() => right(null)());

      const either = await leaveRoom.execute(roomID, clientID);

      expect(isRight(either)).toBe(true);
      expect(spy1).toHaveBeenCalledOnce();
      expect(spy2).toHaveBeenCalledOnce();
      expect(spy3).toHaveBeenCalledOnce();
      expect(spy4).toHaveBeenCalledOnce();
      expect(spy5).toHaveBeenCalledOnce();
      expect(spy6).toHaveBeenCalledOnce();
      expect(spy7).toHaveBeenCalledOnce();
    });

    it('should return right: moderator is absent', async () => {
      const roomID = Room.ID.from('4378572d-c541-4e14-8972-31a6f4a5e592');
      const clientID1 = Client.ID.from('8f060c9d-e346-4c26-a313-d1c6a7482e77');
      const client1 = mockClient({
        id: clientID1
      });
      const clientID2 = Client.ID.from('13337e49-bcd5-470a-ae8f-ea8a423fba06');
      const client2 = mockClient({
        id: clientID2
      });
      const room = mockRoom({
        id: roomID,
        moderator: null,
        participants: [client1, client2]
      });
      const uow = new MockUnitOfWork<AppTypeMap>();

      const spy1 = vi.spyOn(clientRepository, 'find').mockImplementation(() => right(some(client1))());
      const spy2 = vi.spyOn(roomService, 'withTransaction').mockImplementation((_, proc) => {
        return proc({ uow, room });
      });
      const spy3 = vi.spyOn(uow, 'update').mockImplementation(() => right(null)());
      const spy4 = vi.spyOn(publishService, 'toEveryone').mockImplementation(() => right(null)());
      const spy5 = vi.spyOn(publishService, 'toClient').mockImplementation(() => right(null)());
      const spy6 = vi.spyOn(roomService, 'destroy');
      const spy7 = vi.spyOn(publishService, 'broadcast').mockImplementation(() => right(null)());

      const either = await leaveRoom.execute(roomID, clientID1);

      expect(isRight(either)).toBe(true);
      expect(spy1).toHaveBeenCalledOnce();
      expect(spy2).toHaveBeenCalledOnce();
      expect(spy3).toHaveBeenCalledOnce();
      expect(spy4).toHaveBeenCalledOnce();
      expect(spy4).toHaveBeenCalledWith(
        mockRoom({
          id: roomID,
          moderator: null,
          participants: [client2]
        }),
        PUBLISH.ROOM.PARTICIPANT.LEFT,
        {
          clientID: clientID1
        }
      );
      expect(spy5).toHaveBeenCalledOnce();
      expect(spy5).toHaveBeenCalledWith(client1, PUBLISH.ROOM.LEAVE.SUCCESS, {});
      expect(spy6).not.toHaveBeenCalled();
      expect(spy7).toHaveBeenCalledOnce();
    });

    it('should return left when clientRepository.find() returns left', async () => {
      const roomID = Room.ID.from('4378572d-c541-4e14-8972-31a6f4a5e592');
      const clientID = Client.ID.from('8f060c9d-e346-4c26-a313-d1c6a7482e77');
      const uow = new MockUnitOfWork<AppTypeMap>();
      const spy1 = vi.spyOn(clientRepository, 'find').mockImplementation(() => left(createGatewayError('EntityNotFound', ''))());
      const spy2 = vi.spyOn(roomService, 'withTransaction');
      const spy3 = vi.spyOn(uow, 'update');
      const spy4 = vi.spyOn(publishService, 'toEveryone');
      const spy5 = vi.spyOn(publishService, 'toClient');
      const spy6 = vi.spyOn(roomService, 'destroy');
      const spy7 = vi.spyOn(publishService, 'broadcast');

      const either = await leaveRoom.execute(roomID, clientID);

      expect(isLeft(either)).toBe(true);
      expect(spy1).toHaveBeenCalledOnce();
      expect(spy2).not.toHaveBeenCalled();
      expect(spy3).not.toHaveBeenCalled();
      expect(spy4).not.toHaveBeenCalled();
      expect(spy5).not.toHaveBeenCalled();
      expect(spy6).not.toHaveBeenCalled;
      expect(spy7).not.toHaveBeenCalled;
    });

    it('should return left when clientRepository.find() returns none', async () => {
      const roomID = Room.ID.from('4378572d-c541-4e14-8972-31a6f4a5e592');
      const clientID = Client.ID.from('8f060c9d-e346-4c26-a313-d1c6a7482e77');
      const uow = new MockUnitOfWork<AppTypeMap>();
      const spy1 = vi.spyOn(clientRepository, 'find').mockImplementation(() => right(none)());
      const spy2 = vi.spyOn(roomService, 'withTransaction');
      const spy3 = vi.spyOn(uow, 'update');
      const spy4 = vi.spyOn(publishService, 'toEveryone');
      const spy5 = vi.spyOn(publishService, 'toClient');
      const spy6 = vi.spyOn(roomService, 'destroy');
      const spy7 = vi.spyOn(publishService, 'broadcast');

      const either = await leaveRoom.execute(roomID, clientID);

      expect(isLeft(either)).toBe(true);
      expect(spy1).toHaveBeenCalledOnce();
      expect(spy2).not.toHaveBeenCalled();
      expect(spy3).not.toHaveBeenCalled();
      expect(spy4).not.toHaveBeenCalled();
      expect(spy5).not.toHaveBeenCalled();
      expect(spy6).not.toHaveBeenCalled();
      expect(spy7).not.toHaveBeenCalled();
    });

    it('should publish failure when room is not found', async () => {
      const roomID = Room.ID.from('4378572d-c541-4e14-8972-31a6f4a5e592');
      const clientID = Client.ID.from('8f060c9d-e346-4c26-a313-d1c6a7482e77');
      const client = mockClient({
        id: clientID
      });
      const uow = new MockUnitOfWork<AppTypeMap>();

      const spy1 = vi.spyOn(clientRepository, 'find').mockImplementation(() => right(some(client))());
      const spy2 = vi.spyOn(roomService, 'withTransaction').mockImplementation(() => left(createRoomError('NoSuchRoom', ''))());
      const spy3 = vi.spyOn(uow, 'update');
      const spy4 = vi.spyOn(publishService, 'toEveryone');
      const spy5 = vi.spyOn(publishService, 'toClient').mockImplementation(() => right(null)());
      const spy6 = vi.spyOn(roomService, 'destroy');
      const spy7 = vi.spyOn(publishService, 'broadcast');

      const either = await leaveRoom.execute(roomID, clientID);

      expect(isLeft(either)).toBe(true);
      expect(spy1).toHaveBeenCalledOnce();
      expect(spy2).toHaveBeenCalledOnce();
      expect(spy3).not.toHaveBeenCalled();
      expect(spy4).not.toHaveBeenCalled();
      expect(spy5).toHaveBeenCalledOnce();
      expect(spy5).toHaveBeenCalledWith(client, PUBLISH.ROOM.LEAVE.FAILURE, {
        cause: 'NOT_FOUND'
      });
      expect(spy6).not.toHaveBeenCalled();
      expect(spy7).not.toHaveBeenCalled();
    });

    it('should publish failure when moderator tries to leave', async () => {
      const roomID = Room.ID.from('4378572d-c541-4e14-8972-31a6f4a5e592');
      const clientID = Client.ID.from('8f060c9d-e346-4c26-a313-d1c6a7482e77');
      const moderator = mockClient({
        id: clientID
      });
      const room = mockRoom({
        id: roomID,
        moderator,
        participants: []
      });
      const uow = new MockUnitOfWork<AppTypeMap>();

      const spy1 = vi.spyOn(clientRepository, 'find').mockImplementation(() => right(some(moderator))());
      const spy2 = vi.spyOn(roomService, 'withTransaction').mockImplementation((_, proc) => {
        return proc({ uow, room });
      });
      const spy3 = vi.spyOn(uow, 'update');
      const spy4 = vi.spyOn(publishService, 'toEveryone');
      const spy5 = vi.spyOn(publishService, 'toClient').mockImplementation(() => right(null)());
      const spy6 = vi.spyOn(roomService, 'destroy');
      const spy7 = vi.spyOn(publishService, 'broadcast');

      const either = await leaveRoom.execute(roomID, clientID);

      expect(isLeft(either)).toBe(true);
      expect(spy1).toHaveBeenCalledOnce();
      expect(spy2).toHaveBeenCalledOnce();
      expect(spy3).not.toHaveBeenCalled();
      expect(spy4).not.toHaveBeenCalled();
      expect(spy5).toHaveBeenCalledOnce();
      expect(spy5).toHaveBeenCalledWith(moderator, PUBLISH.ROOM.LEAVE.FAILURE, {
        cause: 'MODERATOR_CANNOT_LEAVE'
      });
      expect(spy6).not.toHaveBeenCalled();
      expect(spy7).not.toHaveBeenCalled();
    });

    it('should publish failure when client is not joined', async () => {
      const roomID = Room.ID.from('4378572d-c541-4e14-8972-31a6f4a5e592');
      const clientID1 = Client.ID.from('8f060c9d-e346-4c26-a313-d1c6a7482e77');
      const clientID2 = Client.ID.from('676615db-a635-4917-b75d-5aca2a3ddbf7');
      const client1 = mockClient({
        id: clientID1
      });
      const client2 = mockClient({
        id: clientID2
      });
      const room = mockRoom({
        id: roomID,
        moderator: null,
        participants: [client2]
      });
      const uow = new MockUnitOfWork<AppTypeMap>();

      const spy1 = vi.spyOn(clientRepository, 'find').mockImplementation(() => right(some(client1))());
      const spy2 = vi.spyOn(roomService, 'withTransaction').mockImplementation((_, proc) => {
        return proc({ uow, room });
      });
      const spy3 = vi.spyOn(uow, 'update');
      const spy4 = vi.spyOn(publishService, 'toEveryone');
      const spy5 = vi.spyOn(publishService, 'toClient').mockImplementation(() => right(null)());
      const spy6 = vi.spyOn(roomService, 'destroy');
      const spy7 = vi.spyOn(publishService, 'broadcast');

      const either = await leaveRoom.execute(roomID, clientID1);

      expect(isLeft(either)).toBe(true);
      expect(spy1).toHaveBeenCalledOnce();
      expect(spy2).toHaveBeenCalledOnce();
      expect(spy3).not.toHaveBeenCalled();
      expect(spy4).not.toHaveBeenCalled();
      expect(spy5).toHaveBeenCalledOnce();
      expect(spy5).toHaveBeenCalledWith(client1, PUBLISH.ROOM.LEAVE.FAILURE, {
        cause: 'NOT_JOINED'
      });
      expect(spy6).not.toHaveBeenCalled();
      expect(spy7).not.toHaveBeenCalled();
    });
  });
});
