import type { IPublishService } from '@/applications/services/interfaces/IPublishService.js';
import type { IRoomService } from '@/applications/services/interfaces/IRoomService.js';
import { MockPublishService } from '@/applications/services/mocks/MockPublishService.js';
import { MockRoomService } from '@/applications/services/mocks/MockRoomService.js';
import type { AppTypeMap } from '@/applications/unitOfWork/AppTypeMap.js';
import { MockUnitOfWork } from '@/applications/unitOfWork/mocks/MockUnitOfWork.js';
import { PUBLISH } from '@/applications/websocket/PublishEvents.js';
import { Client } from '@/domains/Client/Client.js';
import { mockClient } from '@/domains/Client/mocks/MockClient.js';
import { Identity } from '@/domains/Identity/Identity.js';
import { mockPerson } from '@/domains/Person/mocks/MockPerson.js';
import { Person } from '@/domains/Person/Person.js';
import { Persons } from '@/domains/Person/Persons.js';
import { mockDailyOutcomeRecord } from '@/domains/Room/mocks/MockDailyOutcomeRecord.js';
import { mockGame } from '@/domains/Room/mocks/MockGame.js';
import { mockRoom } from '@/domains/Room/mocks/MockRoom.js';
import { Room } from '@/domains/Room/Room.js';
import type { ILogger } from '@/lib/Logger/ILogger.js';
import { MockLogger } from '@/lib/Logger/mocks/MockLogger.js';
import { isLeft, isRight } from 'fp-ts/lib/Either.js';
import { right } from 'fp-ts/lib/TaskEither.js';
import { PublishMediumOutcome } from '../PublishMediumOutcome.js';

describe('PublishMediumOutcome', () => {
  let roomService: IRoomService;
  let publishService: IPublishService;
  let logger: ILogger;
  let publishMediumOutcome: PublishMediumOutcome;

  beforeEach(() => {
    roomService = new MockRoomService();
    publishService = new MockPublishService();
    logger = new MockLogger();
    publishMediumOutcome = new PublishMediumOutcome(roomService, publishService, logger);
  });

  afterEach(() => {
    vi.clearAllMocks();
  });

  describe('execute', () => {
    it.each`
      role           | expected
      ${'ATONEMENT'} | ${'VILLAGER'}
      ${'CATALYST'}  | ${'WEREWOLF'}
      ${'COWARD'}    | ${'VILLAGER'}
      ${'HUNTER'}    | ${'VILLAGER'}
      ${'INFORMANT'} | ${'VILLAGER'}
      ${'JESTER'}    | ${'VILLAGER'}
      ${'KITSUNE'}   | ${'VILLAGER'}
      ${'KNIGHT'}    | ${'VILLAGER'}
      ${'MEDIUM'}    | ${'VILLAGER'}
      ${'MONASTIC'}  | ${'VILLAGER'}
      ${'ORATOR'}    | ${'VILLAGER'}
      ${'POSSESSED'} | ${'VILLAGER'}
      ${'SEER'}      | ${'VILLAGER'}
      ${'VILLAGER'}  | ${'VILLAGER'}
      ${'WEREWOLF'}  | ${'WEREWOLF'}
    `('should publish $expected when the executed person role is $role', async ({ role, expected }) => {
      const roomID = Room.ID.from('9400e3f1-0a35-4b77-af02-2a41c4325ab8');
      const mediumID = Person.ID.from('728f7a1e-2aca-42ab-be84-29d699df53e7');
      const executedPersonID = Person.ID.from('4494e99c-1a0a-4511-9431-fc3c5041ea38');
      const medium = mockPerson({
        id: mediumID,
        life: 'ALIVE',
        identity: Identity.MEDIUM
      });
      const room = mockRoom({
        id: roomID,
        game: mockGame({
          residents: [
            medium,
            mockPerson({
              id: executedPersonID,
              life: 'DEAD',
              identity: Identity.getByRole(role)
            })
          ],
          records: [
            mockDailyOutcomeRecord({
              days: 1,
              execution: executedPersonID
            })
          ]
        })
      });
      const uow = new MockUnitOfWork<AppTypeMap>();

      const spy1 = vi.spyOn(roomService, 'withTransaction').mockImplementation((_, proc) => {
        return proc({ uow, room });
      });
      const spy2 = vi.spyOn(publishService, 'toPersons').mockImplementation(() => right(null)());
      const spy3 = vi.spyOn(publishService, 'toModerator').mockImplementation(() => right(null)());

      const either = await publishMediumOutcome.execute(roomID);

      expect(isRight(either)).toBe(true);
      expect(spy1).toHaveBeenCalledOnce();
      expect(spy2).toHaveBeenCalledOnce();
      expect(spy2).toHaveBeenCalledWith(Persons.ofArray([medium]), PUBLISH.CHANNEL.OUTCOME, {
        outcome: {
          id: executedPersonID,
          identity: expected
        }
      });
      expect(spy3).toHaveBeenCalledOnce();
      expect(spy3).toHaveBeenCalledWith(room, PUBLISH.CHANNEL.OUTCOME, {
        outcome: {
          id: executedPersonID,
          identity: expected
        }
      });
    });

    it('should not publish event when there is no mediums', async () => {
      const roomID = Room.ID.from('9400e3f1-0a35-4b77-af02-2a41c4325ab8');
      const mediumID = Person.ID.from('728f7a1e-2aca-42ab-be84-29d699df53e7');
      const executedPersonID = Person.ID.from('4494e99c-1a0a-4511-9431-fc3c5041ea38');
      const room = mockRoom({
        id: roomID,
        game: mockGame({
          residents: [
            mockPerson({
              id: mediumID,
              life: 'ALIVE',
              identity: Identity.HUNTER
            }),
            mockPerson({
              id: executedPersonID,
              life: 'DEAD',
              identity: Identity.VILLAGER
            })
          ],
          records: [
            mockDailyOutcomeRecord({
              days: 1,
              execution: executedPersonID
            })
          ]
        })
      });
      const uow = new MockUnitOfWork<AppTypeMap>();

      const spy1 = vi.spyOn(roomService, 'withTransaction').mockImplementation((_, proc) => {
        return proc({ uow, room });
      });
      const spy2 = vi.spyOn(publishService, 'toPersons').mockImplementation(() => right(null)());
      const spy3 = vi.spyOn(publishService, 'toModerator').mockImplementation(() => right(null)());

      const either = await publishMediumOutcome.execute(roomID);

      expect(isRight(either)).toBe(true);
      expect(spy1).toHaveBeenCalledOnce();
      expect(spy2).not.toHaveBeenCalled();
      expect(spy3).not.toHaveBeenCalled();
    });

    it('should publish event when there is no victims', async () => {
      const roomID = Room.ID.from('9400e3f1-0a35-4b77-af02-2a41c4325ab8');
      const mediumID = Person.ID.from('728f7a1e-2aca-42ab-be84-29d699df53e7');
      const medium = mockPerson({
        id: mediumID,
        life: 'ALIVE',
        identity: Identity.MEDIUM
      });
      const room = mockRoom({
        id: roomID,
        game: mockGame({
          residents: [medium],
          records: [
            mockDailyOutcomeRecord({
              days: 1,
              execution: null
            })
          ]
        })
      });
      const uow = new MockUnitOfWork<AppTypeMap>();

      const spy1 = vi.spyOn(roomService, 'withTransaction').mockImplementation((_, proc) => {
        return proc({ uow, room });
      });
      const spy2 = vi.spyOn(publishService, 'toPersons').mockImplementation(() => right(null)());
      const spy3 = vi.spyOn(publishService, 'toModerator').mockImplementation(() => right(null)());

      const either = await publishMediumOutcome.execute(roomID);

      expect(isRight(either)).toBe(true);
      expect(spy1).toHaveBeenCalledOnce();
      expect(spy2).not.toHaveBeenCalled();
      expect(spy3).not.toHaveBeenCalled();
    });

    it('should publish event to all alive mediums', async () => {
      const roomID = Room.ID.from('9400e3f1-0a35-4b77-af02-2a41c4325ab8');
      const mediumID1 = Person.ID.from('728f7a1e-2aca-42ab-be84-29d699df53e7');
      const mediumID2 = Person.ID.from('c5dbf8e9-cd6d-4269-abc9-53643a8da7ba');
      const executedPersonID = Person.ID.from('4494e99c-1a0a-4511-9431-fc3c5041ea38');
      const medium1 = mockPerson({
        id: mediumID1,
        life: 'ALIVE',
        identity: Identity.MEDIUM
      });
      const medium2 = mockPerson({
        id: mediumID2,
        life: 'ALIVE',
        identity: Identity.MEDIUM
      });
      const room = mockRoom({
        id: roomID,
        game: mockGame({
          residents: [
            medium1,
            medium2,
            mockPerson({
              id: executedPersonID,
              life: 'DEAD',
              identity: Identity.VILLAGER
            })
          ],
          records: [
            mockDailyOutcomeRecord({
              days: 1,
              execution: executedPersonID
            })
          ]
        })
      });
      const uow = new MockUnitOfWork<AppTypeMap>();

      const spy1 = vi.spyOn(roomService, 'withTransaction').mockImplementation((_, proc) => {
        return proc({ uow, room });
      });
      const spy2 = vi.spyOn(publishService, 'toPersons').mockImplementation(() => right(null)());
      const spy3 = vi.spyOn(publishService, 'toModerator').mockImplementation(() => right(null)());

      const either = await publishMediumOutcome.execute(roomID);

      expect(isRight(either)).toBe(true);
      expect(spy1).toHaveBeenCalledOnce();
      expect(spy2).toHaveBeenCalledOnce();
      expect(spy2).toHaveBeenCalledWith(Persons.ofArray([medium1, medium2]), PUBLISH.CHANNEL.OUTCOME, {
        outcome: {
          id: executedPersonID,
          identity: 'VILLAGER'
        }
      });
      expect(spy3).toHaveBeenCalledOnce();
      expect(spy3).toHaveBeenCalledWith(room, PUBLISH.CHANNEL.OUTCOME, {
        outcome: {
          id: executedPersonID,
          identity: 'VILLAGER'
        }
      });
    });

    it('should not publish event to dead medium', async () => {
      const roomID = Room.ID.from('9400e3f1-0a35-4b77-af02-2a41c4325ab8');
      const mediumID1 = Person.ID.from('728f7a1e-2aca-42ab-be84-29d699df53e7');
      const mediumID2 = Person.ID.from('c5dbf8e9-cd6d-4269-abc9-53643a8da7ba');
      const executedPersonID = Person.ID.from('4494e99c-1a0a-4511-9431-fc3c5041ea38');
      const medium1 = mockPerson({
        id: mediumID1,
        life: 'ALIVE',
        identity: Identity.MEDIUM
      });
      const medium2 = mockPerson({
        id: mediumID2,
        life: 'DEAD',
        identity: Identity.MEDIUM
      });
      const room = mockRoom({
        id: roomID,
        game: mockGame({
          residents: [
            medium1,
            medium2,
            mockPerson({
              id: executedPersonID,
              life: 'DEAD',
              identity: Identity.VILLAGER
            })
          ],
          records: [
            mockDailyOutcomeRecord({
              days: 1,
              execution: executedPersonID
            })
          ]
        })
      });
      const uow = new MockUnitOfWork<AppTypeMap>();

      const spy1 = vi.spyOn(roomService, 'withTransaction').mockImplementation((_, proc) => {
        return proc({ uow, room });
      });
      const spy2 = vi.spyOn(publishService, 'toPersons').mockImplementation(() => right(null)());
      const spy3 = vi.spyOn(publishService, 'toModerator').mockImplementation(() => right(null)());

      const either = await publishMediumOutcome.execute(roomID);

      expect(isRight(either)).toBe(true);
      expect(spy1).toHaveBeenCalledOnce();
      expect(spy2).toHaveBeenCalledOnce();
      expect(spy2).toHaveBeenCalledWith(Persons.ofArray([medium1]), PUBLISH.CHANNEL.OUTCOME, {
        outcome: {
          id: executedPersonID,
          identity: 'VILLAGER'
        }
      });
      expect(spy3).toHaveBeenCalledOnce();
      expect(spy3).toHaveBeenCalledWith(room, PUBLISH.CHANNEL.OUTCOME, {
        outcome: {
          id: executedPersonID,
          identity: 'VILLAGER'
        }
      });
    });

    it('should publish exception to moderator when the executed person is not dead', async () => {
      const roomID = Room.ID.from('9400e3f1-0a35-4b77-af02-2a41c4325ab8');
      const executedClientID = Client.ID.from('fd183e9f-c971-40d4-83eb-368aef9dc429');
      const mediumID1 = Person.ID.from('728f7a1e-2aca-42ab-be84-29d699df53e7');
      const mediumID2 = Person.ID.from('c5dbf8e9-cd6d-4269-abc9-53643a8da7ba');
      const executedPersonID = Person.ID.from('4494e99c-1a0a-4511-9431-fc3c5041ea38');
      const medium1 = mockPerson({
        id: mediumID1,
        life: 'ALIVE',
        identity: Identity.MEDIUM
      });
      const medium2 = mockPerson({
        id: mediumID2,
        life: 'DEAD',
        identity: Identity.MEDIUM
      });
      const room = mockRoom({
        id: roomID,
        game: mockGame({
          residents: [
            medium1,
            medium2,
            mockPerson({
              id: executedPersonID,
              life: 'ALIVE',
              identity: Identity.VILLAGER,
              client: mockClient({
                id: executedClientID,
                name: 'MeiKong'
              })
            })
          ],
          records: [
            mockDailyOutcomeRecord({
              days: 1,
              execution: executedPersonID
            })
          ]
        })
      });
      const uow = new MockUnitOfWork<AppTypeMap>();

      const spy1 = vi.spyOn(roomService, 'withTransaction').mockImplementation((_, proc) => {
        return proc({ uow, room });
      });
      const spy2 = vi.spyOn(publishService, 'toModerator').mockImplementation(() => right(null)());

      const either = await publishMediumOutcome.execute(roomID);

      expect(isLeft(either)).toBe(true);
      expect(spy1).toHaveBeenCalledOnce();
      expect(spy2).not.toHaveBeenCalled();
    });
  });
});
