import { createEventPublishError } from '@/adapters/presenters/EventPublisher/EventPublishError.js';
import type { IPublishService } from '@/applications/services/interfaces/IPublishService.js';
import type { IRoomService } from '@/applications/services/interfaces/IRoomService.js';
import { MockPublishService } from '@/applications/services/mocks/MockPublishService.js';
import { MockRoomService } from '@/applications/services/mocks/MockRoomService.js';
import type { AppTypeMap } from '@/applications/unitOfWork/AppTypeMap.js';
import { MockUnitOfWork } from '@/applications/unitOfWork/mocks/MockUnitOfWork.js';
import { PUBLISH } from '@/applications/websocket/PublishEvents.js';
import { mockPerson } from '@/domains/Person/mocks/MockPerson.js';
import { Person } from '@/domains/Person/Person.js';
import { mockGame } from '@/domains/Room/mocks/MockGame.js';
import { mockRoom } from '@/domains/Room/mocks/MockRoom.js';
import { Room } from '@/domains/Room/Room.js';
import type { ILogger } from '@/lib/Logger/ILogger.js';
import { MockLogger } from '@/lib/Logger/mocks/MockLogger.js';
import { isLeft, isRight } from 'fp-ts/lib/Either.js';
import { left, right } from 'fp-ts/lib/TaskEither.js';
import { StartEveningPhase } from '../StartEveningPhase.js';

describe('StartEveningPhase', () => {
  let roomService: IRoomService;
  let publishService: IPublishService;
  let logger: ILogger;
  let startEveningPhase: StartEveningPhase;

  beforeEach(() => {
    roomService = new MockRoomService();
    publishService = new MockPublishService();
    logger = new MockLogger();
    startEveningPhase = new StartEveningPhase(roomService, publishService, logger);
  });

  afterEach(() => {
    vi.clearAllMocks();
  });

  describe('execute', () => {
    it('should return right', async () => {
      const roomID = Room.ID.from('8ae5f35e-0962-4a6d-bba5-790afed97905');
      const personID1 = Person.ID.from('843abc0a-15d7-46e6-90b5-c7c845f8200d');
      const personID2 = Person.ID.from('44c4c7f4-88d2-42a0-b3cb-d282fa296593');
      const personID3 = Person.ID.from('302226cd-f676-4754-bd18-b8ca9a93e8ca');
      const personID4 = Person.ID.from('b1a2f0ab-bb31-4e50-9de3-ee5bd3b9936e');
      const personID5 = Person.ID.from('e2950f53-b2f0-4b3f-b9d3-7703fa9bd6e2');
      const room = mockRoom({
        id: roomID,
        game: mockGame({
          residents: [
            mockPerson({
              id: personID1,
              life: 'DEAD'
            }),
            mockPerson({
              id: personID2,
              life: 'ALIVE'
            }),
            mockPerson({
              id: personID3,
              life: 'ALIVE'
            }),
            mockPerson({
              id: personID4,
              life: 'ALIVE'
            }),
            mockPerson({
              id: personID5,
              life: 'DEAD'
            })
          ]
        })
      });
      const uow = new MockUnitOfWork<AppTypeMap>();

      const spy1 = vi.spyOn(roomService, 'withTransaction').mockImplementation((_, proc) => {
        return proc({ uow, room });
      });
      const spy2 = vi.spyOn(publishService, 'toEveryone').mockImplementation(() => right(null)());
      const spy3 = vi.spyOn(publishService, 'toSurvivors').mockImplementation(() => right(null)());

      const either = await startEveningPhase.execute(roomID);

      expect(isRight(either)).toBe(true);
      expect(spy1).toHaveBeenCalledOnce();
      expect(spy2).toHaveBeenCalledOnce();
      expect(spy2).toHaveBeenCalledWith(room, PUBLISH.EVENING.START, {
        seconds: room.game.phaseDuration.evening
      });
      expect(spy3).toHaveBeenCalledOnce();
      expect(spy3).toHaveBeenCalledWith(room, PUBLISH.VOTE.PROMPT, { candidates: [personID2, personID3, personID4] });
    });

    it('should return left when publishService.toEveryone() returns left', async () => {
      const roomID = Room.ID.from('8ae5f35e-0962-4a6d-bba5-790afed97905');
      const personID1 = Person.ID.from('843abc0a-15d7-46e6-90b5-c7c845f8200d');
      const personID2 = Person.ID.from('44c4c7f4-88d2-42a0-b3cb-d282fa296593');
      const personID3 = Person.ID.from('302226cd-f676-4754-bd18-b8ca9a93e8ca');
      const personID4 = Person.ID.from('b1a2f0ab-bb31-4e50-9de3-ee5bd3b9936e');
      const personID5 = Person.ID.from('e2950f53-b2f0-4b3f-b9d3-7703fa9bd6e2');
      const room = mockRoom({
        id: roomID,
        game: mockGame({
          residents: [
            mockPerson({
              id: personID1,
              life: 'DEAD'
            }),
            mockPerson({
              id: personID2,
              life: 'ALIVE'
            }),
            mockPerson({
              id: personID3,
              life: 'ALIVE'
            }),
            mockPerson({
              id: personID4,
              life: 'ALIVE'
            }),
            mockPerson({
              id: personID5,
              life: 'DEAD'
            })
          ]
        })
      });
      const uow = new MockUnitOfWork<AppTypeMap>();

      const spy1 = vi.spyOn(roomService, 'withTransaction').mockImplementation((_, proc) => {
        return proc({ uow, room });
      });
      const spy2 = vi.spyOn(publishService, 'toEveryone').mockImplementation(() => left(createEventPublishError('EmitFailed', ''))());
      const spy3 = vi.spyOn(publishService, 'toSurvivors').mockImplementation(() => right(null)());

      const either = await startEveningPhase.execute(roomID);

      expect(isLeft(either)).toBe(true);
      expect(spy1).toHaveBeenCalledOnce();
      expect(spy2).toHaveBeenCalledOnce();
      expect(spy2).toHaveBeenCalledWith(room, PUBLISH.EVENING.START, {
        seconds: room.game.phaseDuration.evening
      });
      expect(spy3).not.toHaveBeenCalled();
    });

    it('should return left when publishService.toSurvivors() returns left', async () => {
      const roomID = Room.ID.from('8ae5f35e-0962-4a6d-bba5-790afed97905');
      const personID1 = Person.ID.from('843abc0a-15d7-46e6-90b5-c7c845f8200d');
      const personID2 = Person.ID.from('44c4c7f4-88d2-42a0-b3cb-d282fa296593');
      const personID3 = Person.ID.from('302226cd-f676-4754-bd18-b8ca9a93e8ca');
      const personID4 = Person.ID.from('b1a2f0ab-bb31-4e50-9de3-ee5bd3b9936e');
      const personID5 = Person.ID.from('e2950f53-b2f0-4b3f-b9d3-7703fa9bd6e2');
      const room = mockRoom({
        id: roomID,
        game: mockGame({
          residents: [
            mockPerson({
              id: personID1,
              life: 'DEAD'
            }),
            mockPerson({
              id: personID2,
              life: 'ALIVE'
            }),
            mockPerson({
              id: personID3,
              life: 'ALIVE'
            }),
            mockPerson({
              id: personID4,
              life: 'ALIVE'
            }),
            mockPerson({
              id: personID5,
              life: 'DEAD'
            })
          ]
        })
      });
      const uow = new MockUnitOfWork<AppTypeMap>();

      const spy1 = vi.spyOn(roomService, 'withTransaction').mockImplementation((_, proc) => {
        return proc({ uow, room });
      });
      const spy2 = vi.spyOn(publishService, 'toEveryone').mockImplementation(() => right(null)());
      const spy3 = vi.spyOn(publishService, 'toSurvivors').mockImplementation(() => left(createEventPublishError('EmitFailed', ''))());

      const either = await startEveningPhase.execute(roomID);

      expect(isLeft(either)).toBe(true);
      expect(spy1).toHaveBeenCalledOnce();
      expect(spy2).toHaveBeenCalledOnce();
      expect(spy2).toHaveBeenCalledWith(room, PUBLISH.EVENING.START, {
        seconds: room.game.phaseDuration.evening
      });
      expect(spy3).toHaveBeenCalledOnce();
      expect(spy3).toHaveBeenCalledWith(room, PUBLISH.VOTE.PROMPT, { candidates: [personID2, personID3, personID4] });
    });
  });
});
