import { createGatewayError } from '@/adapters/gateways/GatewayError.js';
import type { IPublishService } from '@/applications/services/interfaces/IPublishService.js';
import type { IRoomService } from '@/applications/services/interfaces/IRoomService.js';
import { MockPublishService } from '@/applications/services/mocks/MockPublishService.js';
import { MockRoomService } from '@/applications/services/mocks/MockRoomService.js';
import type { AppTypeMap } from '@/applications/unitOfWork/AppTypeMap.js';
import { MockUnitOfWork } from '@/applications/unitOfWork/mocks/MockUnitOfWork.js';
import { PUBLISH } from '@/applications/websocket/PublishEvents.js';
import { Client } from '@/domains/Client/Client.js';
import type { IClientRepository } from '@/domains/Client/IClientRepository.js';
import { mockClient } from '@/domains/Client/mocks/MockClient.js';
import { MockClientRepository } from '@/domains/Client/mocks/MockClientRepository.js';
import type { GameStatus } from '@/domains/Room/GameStatus.js';
import { mockGame } from '@/domains/Room/mocks/MockGame.js';
import { mockRoom } from '@/domains/Room/mocks/MockRoom.js';
import { Room } from '@/domains/Room/Room.js';
import { createRoomError } from '@/domains/Room/RoomError.js';
import type { ILogger } from '@/lib/Logger/ILogger.js';
import { MockLogger } from '@/lib/Logger/mocks/MockLogger.js';
import { isLeft, isRight } from 'fp-ts/lib/Either.js';
import { none, some } from 'fp-ts/lib/Option.js';
import { left, right } from 'fp-ts/lib/TaskEither.js';
import { JoinRoom } from '../JoinRoom.js';

describe('JoinRoom', () => {
  let clientRepository: IClientRepository;
  let roomService: IRoomService;
  let publishService: IPublishService;
  let logger: ILogger;
  let joinRoom: JoinRoom;

  beforeEach(() => {
    clientRepository = new MockClientRepository();
    roomService = new MockRoomService();
    publishService = new MockPublishService();
    logger = new MockLogger();
    joinRoom = new JoinRoom(clientRepository, roomService, publishService, logger);
  });

  afterEach(() => {
    vi.clearAllMocks();
  });

  describe('execute', () => {
    it('should return right', async () => {
      const roomID = Room.ID.from('fed2aaaf-8bbd-47d2-80e3-9277c9834f04');
      const clientID = Client.ID.from('f6632610-6c04-42ff-9517-5e748f28a7fd');
      const client = mockClient({
        id: clientID
      });
      const password = 'wwHJAgSLQB';
      const moderator = mockClient({
        id: Client.ID.from('ef34ec8b-36b0-4bdc-908c-e051151fb342')
      });
      const room = mockRoom({
        id: roomID,
        password: null,
        quota: 5,
        moderator,
        participants: [
          mockClient({
            id: Client.ID.from('507a2018-cd49-4e87-a962-379f07b30463')
          })
        ],
        game: mockGame({
          status: 'OPEN'
        })
      });
      const uow = new MockUnitOfWork<AppTypeMap>();

      const spy1 = vi.spyOn(clientRepository, 'find').mockImplementation(() => right(some(client))());
      const spy2 = vi.spyOn(roomService, 'withTransaction').mockImplementation((_, proc) => {
        return proc({ uow, room });
      });
      const spy3 = vi.spyOn(uow, 'update').mockImplementation(() => right(null)());
      const spy4 = vi.spyOn(publishService, 'toEveryone').mockImplementation(() => right(null)());
      const spy5 = vi.spyOn(publishService, 'toClient').mockImplementation(() => right(null)());
      const spy6 = vi.spyOn(publishService, 'broadcast').mockImplementation(() => right(null)());

      const either = await joinRoom.execute(roomID, clientID, password);

      expect(isRight(either)).toBe(true);
      expect(spy1).toHaveBeenCalledOnce();
      expect(spy2).toHaveBeenCalledOnce();
      expect(spy3).toHaveBeenCalledOnce();
      expect(spy3).toHaveBeenCalledOnce();
      expect(spy3).toHaveBeenCalledWith(
        'room',
        mockRoom({
          id: roomID,
          password: null,
          quota: 5,
          moderator,
          participants: [
            mockClient({
              id: Client.ID.from('507a2018-cd49-4e87-a962-379f07b30463')
            }),
            client
          ],
          game: mockGame({
            status: 'OPEN'
          })
        })
      );
      expect(spy4).toHaveBeenCalledOnce();
      expect(spy4).toHaveBeenCalledWith(
        mockRoom({
          id: roomID,
          password: null,
          quota: 5,
          moderator,
          participants: [
            mockClient({
              id: Client.ID.from('507a2018-cd49-4e87-a962-379f07b30463')
            }),
            client
          ],
          game: mockGame({
            status: 'OPEN'
          })
        }),
        PUBLISH.ROOM.PARTICIPANT.JOINED,
        {
          client
        }
      );
      expect(spy5).toHaveBeenCalledOnce();
      expect(spy5).toHaveBeenCalledWith(client, PUBLISH.ROOM.JOIN.SUCCESS, {
        roomID,
        moderatorID: moderator.id
      });
      expect(spy6).toHaveBeenCalledOnce();
      expect(spy6).toHaveBeenCalledWith(PUBLISH.ROOM.NOTIFY.JOINED, {
        roomID
      });
    });

    it('should return left if clientRepository.find() returns left', async () => {
      const roomID = Room.ID.from('fed2aaaf-8bbd-47d2-80e3-9277c9834f04');
      const clientID = Client.ID.from('f6632610-6c04-42ff-9517-5e748f28a7fd');
      const password = 'wwHJAgSLQB';
      const uow = new MockUnitOfWork<AppTypeMap>();

      const spy1 = vi.spyOn(clientRepository, 'find').mockImplementation(() => left(createGatewayError('EntityNotFound', ''))());
      const spy2 = vi.spyOn(roomService, 'withTransaction');
      const spy3 = vi.spyOn(uow, 'update');
      const spy4 = vi.spyOn(publishService, 'toEveryone');
      const spy5 = vi.spyOn(publishService, 'toClient');
      const spy6 = vi.spyOn(publishService, 'broadcast');

      const either = await joinRoom.execute(roomID, clientID, password);

      expect(isLeft(either)).toBe(true);
      expect(spy1).toHaveBeenCalledOnce();
      expect(spy2).not.toHaveBeenCalled();
      expect(spy3).not.toHaveBeenCalled();
      expect(spy4).not.toHaveBeenCalled();
      expect(spy5).not.toHaveBeenCalled();
      expect(spy6).not.toHaveBeenCalled();
    });

    it('should return left if clientRepository.find() returns none', async () => {
      const roomID = Room.ID.from('fed2aaaf-8bbd-47d2-80e3-9277c9834f04');
      const clientID = Client.ID.from('f6632610-6c04-42ff-9517-5e748f28a7fd');
      const password = 'wwHJAgSLQB';
      const uow = new MockUnitOfWork<AppTypeMap>();

      const spy1 = vi.spyOn(clientRepository, 'find').mockImplementation(() => right(none)());
      const spy2 = vi.spyOn(roomService, 'withTransaction');
      const spy3 = vi.spyOn(uow, 'update');
      const spy4 = vi.spyOn(publishService, 'toEveryone');
      const spy5 = vi.spyOn(publishService, 'toClient');
      const spy6 = vi.spyOn(publishService, 'broadcast');

      const either = await joinRoom.execute(roomID, clientID, password);

      expect(isLeft(either)).toBe(true);
      expect(spy1).toHaveBeenCalledOnce();
      expect(spy2).not.toHaveBeenCalled();
      expect(spy3).not.toHaveBeenCalled();
      expect(spy4).not.toHaveBeenCalled();
      expect(spy5).not.toHaveBeenCalled();
      expect(spy6).not.toHaveBeenCalled();
    });

    it('should publish failure when room is not found', async () => {
      const roomID = Room.ID.from('fed2aaaf-8bbd-47d2-80e3-9277c9834f04');
      const clientID = Client.ID.from('f6632610-6c04-42ff-9517-5e748f28a7fd');
      const client = mockClient({
        id: clientID
      });
      const password = 'wwHJAgSLQB';
      const uow = new MockUnitOfWork<AppTypeMap>();

      const spy1 = vi.spyOn(clientRepository, 'find').mockImplementation(() => right(some(client))());
      const spy2 = vi.spyOn(roomService, 'withTransaction').mockImplementation(() => left(createRoomError('NoSuchRoom', ''))());
      const spy3 = vi.spyOn(uow, 'update');
      const spy4 = vi.spyOn(publishService, 'toEveryone');
      const spy5 = vi.spyOn(publishService, 'toClient').mockImplementation(() => right(null)());
      const spy6 = vi.spyOn(publishService, 'broadcast');

      const either = await joinRoom.execute(roomID, clientID, password);

      expect(isLeft(either)).toBe(true);
      expect(spy1).toHaveBeenCalledOnce();
      expect(spy2).toHaveBeenCalledOnce();
      expect(spy3).not.toHaveBeenCalled();
      expect(spy4).not.toHaveBeenCalled();
      expect(spy5).toHaveBeenCalledOnce();
      expect(spy5).toHaveBeenCalledWith(client, PUBLISH.ROOM.JOIN.FAILURE, {
        cause: 'NOT_FOUND'
      });
      expect(spy6).not.toHaveBeenCalled();
    });

    it('should publish failure when room is full', async () => {
      const roomID = Room.ID.from('fed2aaaf-8bbd-47d2-80e3-9277c9834f04');
      const clientID = Client.ID.from('f6632610-6c04-42ff-9517-5e748f28a7fd');
      const client = mockClient({
        id: clientID
      });
      const password = 'wwHJAgSLQB';
      const moderator = mockClient({
        id: Client.ID.from('ef34ec8b-36b0-4bdc-908c-e051151fb342')
      });
      const room = mockRoom({
        id: roomID,
        password: null,
        quota: 2,
        moderator,
        participants: [
          mockClient({
            id: Client.ID.from('d43d6d00-a22f-4490-841f-1d1dad10feb5')
          }),
          mockClient({
            id: Client.ID.from('53cb78b2-3482-495b-8da8-6f835b49a04c')
          })
        ],
        game: mockGame({
          status: 'OPEN'
        })
      });
      const uow = new MockUnitOfWork<AppTypeMap>();

      const spy1 = vi.spyOn(clientRepository, 'find').mockImplementation(() => right(some(client))());
      const spy2 = vi.spyOn(roomService, 'withTransaction').mockImplementation((_, proc) => {
        return proc({ uow, room });
      });
      const spy3 = vi.spyOn(uow, 'update');
      const spy4 = vi.spyOn(publishService, 'toEveryone');
      const spy5 = vi.spyOn(publishService, 'toClient').mockImplementation(() => right(null)());
      const spy6 = vi.spyOn(publishService, 'broadcast');

      const either = await joinRoom.execute(roomID, clientID, password);

      expect(isLeft(either)).toBe(true);
      expect(spy1).toHaveBeenCalledOnce();
      expect(spy2).toHaveBeenCalledOnce();
      expect(spy3).not.toHaveBeenCalled();
      expect(spy4).not.toHaveBeenCalled();
      expect(spy5).toHaveBeenCalledOnce();
      expect(spy5).toHaveBeenCalledWith(client, PUBLISH.ROOM.JOIN.FAILURE, {
        cause: 'FULL'
      });
      expect(spy6).not.toHaveBeenCalled();
    });

    it.each`
      status
      ${'IN_PROGRESS'}
      ${'COMPLETED'}
    `('should publish failure when room is not open: status is $status', async ({ status }: { status: GameStatus }) => {
      const roomID = Room.ID.from('fed2aaaf-8bbd-47d2-80e3-9277c9834f04');
      const clientID = Client.ID.from('f6632610-6c04-42ff-9517-5e748f28a7fd');
      const client = mockClient({
        id: clientID
      });
      const password = 'wwHJAgSLQB';
      const moderator = mockClient({
        id: Client.ID.from('ef34ec8b-36b0-4bdc-908c-e051151fb342')
      });
      const room = mockRoom({
        id: roomID,
        password: null,
        quota: 5,
        moderator,
        participants: [
          mockClient({
            id: Client.ID.from('507a2018-cd49-4e87-a962-379f07b30463')
          })
        ],
        game: mockGame({
          status
        })
      });
      const uow = new MockUnitOfWork<AppTypeMap>();

      const spy1 = vi.spyOn(clientRepository, 'find').mockImplementation(() => right(some(client))());
      const spy2 = vi.spyOn(roomService, 'withTransaction').mockImplementation((_, proc) => {
        return proc({ uow, room });
      });
      const spy3 = vi.spyOn(uow, 'update');
      const spy4 = vi.spyOn(publishService, 'toEveryone');
      const spy5 = vi.spyOn(publishService, 'toClient').mockImplementation(() => right(null)());
      const spy6 = vi.spyOn(publishService, 'broadcast');

      const either = await joinRoom.execute(roomID, clientID, password);

      expect(isLeft(either)).toBe(true);
      expect(spy1).toHaveBeenCalledOnce();
      expect(spy2).toHaveBeenCalledOnce();
      expect(spy3).not.toHaveBeenCalled();
      expect(spy4).not.toHaveBeenCalled();
      expect(spy5).toHaveBeenCalledOnce();
      expect(spy5).toHaveBeenCalledWith(client, PUBLISH.ROOM.JOIN.FAILURE, {
        cause: 'NOT_OPEN'
      });
      expect(spy6).not.toHaveBeenCalled();
    });

    it('should publish failure when password is mismatch', async () => {
      const roomID = Room.ID.from('fed2aaaf-8bbd-47d2-80e3-9277c9834f04');
      const clientID = Client.ID.from('f6632610-6c04-42ff-9517-5e748f28a7fd');
      const client = mockClient({
        id: clientID
      });
      const password = 'wwHJAgSLQB';
      const moderator = mockClient({
        id: Client.ID.from('ef34ec8b-36b0-4bdc-908c-e051151fb342')
      });
      const room = mockRoom({
        id: roomID,
        password: 'eYmzZlJnQm',
        quota: 5,
        moderator,
        participants: [
          mockClient({
            id: Client.ID.from('507a2018-cd49-4e87-a962-379f07b30463')
          })
        ],
        game: mockGame({
          status: 'OPEN'
        })
      });
      const uow = new MockUnitOfWork<AppTypeMap>();

      const spy1 = vi.spyOn(clientRepository, 'find').mockImplementation(() => right(some(client))());
      const spy2 = vi.spyOn(roomService, 'withTransaction').mockImplementation((_, proc) => {
        return proc({ uow, room });
      });
      const spy3 = vi.spyOn(uow, 'update');
      const spy4 = vi.spyOn(publishService, 'toEveryone');
      const spy5 = vi.spyOn(publishService, 'toClient').mockImplementation(() => right(null)());
      const spy6 = vi.spyOn(publishService, 'broadcast');

      const either = await joinRoom.execute(roomID, clientID, password);

      expect(isLeft(either)).toBe(true);
      expect(spy1).toHaveBeenCalledOnce();
      expect(spy2).toHaveBeenCalledOnce();
      expect(spy3).not.toHaveBeenCalled();
      expect(spy4).not.toHaveBeenCalled();
      expect(spy5).toHaveBeenCalledOnce();
      expect(spy5).toHaveBeenCalledWith(client, PUBLISH.ROOM.JOIN.FAILURE, {
        cause: 'PASSWORD_MISMATCH'
      });
      expect(spy6).not.toHaveBeenCalled();
    });
  });
});
