import type { IPublishService } from '@/applications/services/interfaces/IPublishService.js';
import type { IRoomService } from '@/applications/services/interfaces/IRoomService.js';
import { MockPublishService } from '@/applications/services/mocks/MockPublishService.js';
import { MockRoomService } from '@/applications/services/mocks/MockRoomService.js';
import type { AppTypeMap } from '@/applications/unitOfWork/AppTypeMap.js';
import { MockUnitOfWork } from '@/applications/unitOfWork/mocks/MockUnitOfWork.js';
import { PUBLISH } from '@/applications/websocket/PublishEvents.js';
import { Identity } from '@/domains/Identity/Identity.js';
import { mockPerson } from '@/domains/Person/mocks/MockPerson.js';
import { Person } from '@/domains/Person/Person.js';
import { mockDailyOutcomeRecord } from '@/domains/Room/mocks/MockDailyOutcomeRecord.js';
import { mockGame } from '@/domains/Room/mocks/MockGame.js';
import { mockRoom } from '@/domains/Room/mocks/MockRoom.js';
import { Room } from '@/domains/Room/Room.js';
import type { ILogger } from '@/lib/Logger/ILogger.js';
import { MockLogger } from '@/lib/Logger/mocks/MockLogger.js';
import { isLeft, isRight } from 'fp-ts/lib/Either.js';
import { right } from 'fp-ts/lib/TaskEither.js';
import type { IPublishOutcome } from '../interfaces/IPublishOutcome.js';
import { MockPublishOutcome } from '../mocks/MockPublishOutcome.js';
import { StartMorningPhase } from '../StartMorningPhase.js';

describe('StartMorningPhase', () => {
  let roomService: IRoomService;
  let publishService: IPublishService;
  let revealNightVictim: IPublishOutcome;
  let publishSeerOutcome: IPublishOutcome;
  let publishMediumOutcome: IPublishOutcome;
  let logger: ILogger;
  let startMorningPhase: StartMorningPhase;

  beforeEach(() => {
    roomService = new MockRoomService();
    publishService = new MockPublishService();
    revealNightVictim = new MockPublishOutcome();
    publishSeerOutcome = new MockPublishOutcome();
    publishMediumOutcome = new MockPublishOutcome();
    logger = new MockLogger();
    startMorningPhase = new StartMorningPhase(roomService, publishService, revealNightVictim, publishSeerOutcome, publishMediumOutcome, logger);
  });

  describe('execute', () => {
    it('should return the result of the previous night phase: not game end', async () => {
      const roomID = Room.ID.from('bf97de8e-471c-4f01-9fb7-42958e11c940');
      const personID1 = Person.ID.from('8ba69365-2147-47d0-994c-ec84687f5e00');
      const personID2 = Person.ID.from('49531b1c-f43c-4093-b80b-7abdb1854779');
      const personID3 = Person.ID.from('8e79c286-300d-46a7-8c16-28b15dbc9cb7');
      const personID4 = Person.ID.from('7047de1b-2a46-4191-89cf-4fd9b4a2d4db');
      const room = mockRoom({
        id: roomID,
        game: mockGame({
          residents: [
            mockPerson({
              id: personID1,
              identity: Identity.VILLAGER,
              life: 'ALIVE'
            }),
            mockPerson({
              id: personID2,
              identity: Identity.VILLAGER,
              life: 'ALIVE'
            }),
            mockPerson({
              id: personID3,
              identity: Identity.WEREWOLF,
              life: 'ALIVE'
            }),
            mockPerson({
              id: personID4,
              identity: Identity.VILLAGER,
              life: 'DEAD'
            })
          ],
          records: [
            mockDailyOutcomeRecord({
              days: 0,
              vote: [
                {
                  source: personID1,
                  destination: personID4
                },
                {
                  source: personID2,
                  destination: personID4
                },
                {
                  source: personID3,
                  destination: personID1
                },
                {
                  source: personID4,
                  destination: personID3
                }
              ]
            })
          ]
        })
      });

      const uow = new MockUnitOfWork<AppTypeMap>();
      const spy1 = vi.spyOn(revealNightVictim, 'execute').mockImplementation(() => right(null)());
      const spy2 = vi.spyOn(publishSeerOutcome, 'execute').mockImplementation(() => right(null)());
      const spy3 = vi.spyOn(publishMediumOutcome, 'execute').mockImplementation(() => right(null)());
      const spy4 = vi.spyOn(roomService, 'withTransaction').mockImplementation((_, proc) => {
        return proc({ room, uow });
      });
      const spy5 = vi.spyOn(uow, 'update').mockImplementation(() => right(null)());
      const spy6 = vi.spyOn(publishService, 'toEveryone').mockImplementation(() => right(null)());

      const either = await startMorningPhase.execute(roomID);

      expect(isRight(either)).toBe(true);

      if (isLeft(either)) {
        throw new Error('This should not happen');
      }

      expect(either.right).toEqual([]);
      expect(spy1).toHaveBeenCalledOnce();
      expect(spy2).toHaveBeenCalledOnce();
      expect(spy3).toHaveBeenCalledOnce();
      expect(spy4).toHaveBeenCalledOnce();
      expect(spy5).toHaveBeenCalledOnce();
      expect(spy6).toHaveBeenCalledOnce();
      expect(spy6).toHaveBeenCalledWith(
        mockRoom({
          id: roomID,
          game: mockGame({
            residents: [
              mockPerson({
                id: personID1,
                identity: Identity.VILLAGER,
                life: 'ALIVE'
              }),
              mockPerson({
                id: personID2,
                identity: Identity.VILLAGER,
                life: 'ALIVE'
              }),
              mockPerson({
                id: personID3,
                identity: Identity.WEREWOLF,
                life: 'ALIVE'
              }),
              mockPerson({
                id: personID4,
                identity: Identity.VILLAGER,
                life: 'DEAD'
              })
            ],
            records: [
              mockDailyOutcomeRecord({
                days: 0,
                vote: [
                  {
                    source: personID1,
                    destination: personID4
                  },
                  {
                    source: personID2,
                    destination: personID4
                  },
                  {
                    source: personID3,
                    destination: personID1
                  },
                  {
                    source: personID4,
                    destination: personID3
                  }
                ]
              }),
              mockDailyOutcomeRecord({
                days: 1
              })
            ]
          })
        }),
        PUBLISH.MORNING.START,
        {
          seconds: room.game.phaseDuration.morning
        }
      );
    });

    it('should return the result of the previous night phase: game end', async () => {
      const roomID = Room.ID.from('bf97de8e-471c-4f01-9fb7-42958e11c940');
      const personID1 = Person.ID.from('8ba69365-2147-47d0-994c-ec84687f5e00');
      const personID2 = Person.ID.from('49531b1c-f43c-4093-b80b-7abdb1854779');
      const personID3 = Person.ID.from('8e79c286-300d-46a7-8c16-28b15dbc9cb7');
      const personID4 = Person.ID.from('7047de1b-2a46-4191-89cf-4fd9b4a2d4db');
      const room = mockRoom({
        id: roomID,
        game: mockGame({
          residents: [
            mockPerson({
              id: personID1,
              identity: Identity.VILLAGER,
              life: 'ALIVE'
            }),
            mockPerson({
              id: personID2,
              identity: Identity.KITSUNE,
              life: 'ALIVE'
            }),
            mockPerson({
              id: personID3,
              identity: Identity.WEREWOLF,
              life: 'ALIVE'
            }),
            mockPerson({
              id: personID4,
              identity: Identity.VILLAGER,
              life: 'DEAD'
            })
          ],
          records: [
            mockDailyOutcomeRecord({
              days: 0,
              vote: [
                {
                  source: personID1,
                  destination: personID4
                },
                {
                  source: personID2,
                  destination: personID4
                },
                {
                  source: personID3,
                  destination: personID1
                },
                {
                  source: personID4,
                  destination: personID3
                }
              ]
            })
          ]
        })
      });

      const uow = new MockUnitOfWork<AppTypeMap>();
      const spy1 = vi.spyOn(revealNightVictim, 'execute').mockImplementation(() => right(null)());
      const spy2 = vi.spyOn(publishSeerOutcome, 'execute').mockImplementation(() => right(null)());
      const spy3 = vi.spyOn(publishMediumOutcome, 'execute').mockImplementation(() => right(null)());
      const spy4 = vi.spyOn(roomService, 'withTransaction').mockImplementation((_, proc) => {
        return proc({ room, uow });
      });
      const spy5 = vi.spyOn(uow, 'update').mockImplementation(() => right(null)());
      const spy6 = vi.spyOn(publishService, 'toEveryone').mockImplementation(() => right(null)());

      const either = await startMorningPhase.execute(roomID);

      expect(isRight(either)).toBe(true);

      if (isLeft(either)) {
        throw new Error('This should not happen');
      }

      expect(either.right).toEqual([
        {
          team: 'KITSUNES',
          terminateGame: true
        }
      ]);
      expect(spy1).toHaveBeenCalledOnce();
      expect(spy2).toHaveBeenCalledOnce();
      expect(spy3).toHaveBeenCalledOnce();
      expect(spy4).toHaveBeenCalledOnce();
      expect(spy5).toHaveBeenCalledOnce();
      expect(spy6).toHaveBeenCalledOnce();
      expect(spy6).toHaveBeenCalledWith(
        mockRoom({
          id: roomID,
          game: mockGame({
            residents: [
              mockPerson({
                id: personID1,
                identity: Identity.VILLAGER,
                life: 'ALIVE'
              }),
              mockPerson({
                id: personID2,
                identity: Identity.KITSUNE,
                life: 'ALIVE'
              }),
              mockPerson({
                id: personID3,
                identity: Identity.WEREWOLF,
                life: 'ALIVE'
              }),
              mockPerson({
                id: personID4,
                identity: Identity.VILLAGER,
                life: 'DEAD'
              })
            ],
            records: [
              mockDailyOutcomeRecord({
                days: 0,
                vote: [
                  {
                    source: personID1,
                    destination: personID4
                  },
                  {
                    source: personID2,
                    destination: personID4
                  },
                  {
                    source: personID3,
                    destination: personID1
                  },
                  {
                    source: personID4,
                    destination: personID3
                  }
                ]
              }),
              mockDailyOutcomeRecord({
                days: 1
              })
            ]
          })
        }),
        PUBLISH.MORNING.START,
        {
          seconds: room.game.phaseDuration.morning
        }
      );
    });
  });
});
