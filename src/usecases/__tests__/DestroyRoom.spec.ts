import { createEventPublishError } from '@/adapters/presenters/EventPublisher/EventPublishError.js';
import type { IPublishService } from '@/applications/services/interfaces/IPublishService.js';
import type { IRoomService } from '@/applications/services/interfaces/IRoomService.js';
import { MockPublishService } from '@/applications/services/mocks/MockPublishService.js';
import { MockRoomService } from '@/applications/services/mocks/MockRoomService.js';
import type { AppTypeMap } from '@/applications/unitOfWork/AppTypeMap.js';
import { createUnitOfWorkError } from '@/applications/unitOfWork/UnitOfWorkError.js';
import { MockUnitOfWork } from '@/applications/unitOfWork/mocks/MockUnitOfWork.js';
import { PUBLISH } from '@/applications/websocket/PublishEvents.js';
import { Client } from '@/domains/Client/Client.js';
import type { IClientRepository } from '@/domains/Client/IClientRepository.js';
import { mockClient } from '@/domains/Client/mocks/MockClient.js';
import { MockClientRepository } from '@/domains/Client/mocks/MockClientRepository.js';
import { Room } from '@/domains/Room/Room.js';
import { mockRoom } from '@/domains/Room/mocks/MockRoom.js';
import type { ILogger } from '@/lib/Logger/ILogger.js';
import { MockLogger } from '@/lib/Logger/mocks/MockLogger.js';
import { isLeft, isRight } from 'fp-ts/lib/Either.js';
import { some } from 'fp-ts/lib/Option.js';
import { left, right } from 'fp-ts/lib/TaskEither.js';
import { DestroyRoom } from '../DestroyRoom.js';

describe('DestroyRoom', () => {
  let clientRepository: IClientRepository;
  let roomService: IRoomService;
  let publishService: IPublishService;
  let logger: ILogger;
  let destroyRoom: DestroyRoom;

  beforeEach(() => {
    clientRepository = new MockClientRepository();
    roomService = new MockRoomService();
    publishService = new MockPublishService();
    logger = new MockLogger();
    destroyRoom = new DestroyRoom(clientRepository, roomService, publishService, logger);
  });

  describe('execute', () => {
    it('should return right', async () => {
      const roomID = Room.ID.from('6c7ee852-2e64-43e9-8d00-7d979273c07e');
      const clientID = Client.ID.from('a1f2cb09-df08-4878-9d8f-0898a534c1f5');
      const client = mockClient({
        id: clientID
      });
      const room = mockRoom({
        id: roomID,
        moderator: client
      });

      const uow = new MockUnitOfWork<AppTypeMap>();
      const spy1 = vi.spyOn(roomService, 'withTransaction').mockImplementation((_, proc) => {
        return proc({ room, uow });
      });
      const spy2 = vi.spyOn(clientRepository, 'find').mockImplementation(() => right(some(client))());
      const spy3 = vi.spyOn(roomService, 'destroy').mockImplementation(() => right(null)());
      const spy4 = vi.spyOn(publishService, 'toClient').mockImplementation(() => right(null)());
      const spy5 = vi.spyOn(publishService, 'toEveryone').mockImplementation(() => right(null)());
      const spy6 = vi.spyOn(publishService, 'broadcast').mockImplementation(() => right(null)());

      const either = await destroyRoom.execute(roomID, clientID);

      expect(isRight(either)).toBe(true);
      expect(spy1).toHaveBeenCalledOnce();
      expect(spy2).toHaveBeenCalledOnce();
      expect(spy3).toHaveBeenCalledOnce();
      expect(spy4).toHaveBeenCalledOnce();
      expect(spy5).toHaveBeenCalledOnce();
      expect(spy5).toHaveBeenCalledOnce();
      expect(spy6).toHaveBeenCalledWith(PUBLISH.ROOM.NOTIFY.DESTROYED, {
        roomID
      });
    });

    it('should publish failure when moderator is absent', async () => {
      const roomID = Room.ID.from('6c7ee852-2e64-43e9-8d00-7d979273c07e');
      const clientID = Client.ID.from('a1f2cb09-df08-4878-9d8f-0898a534c1f5');
      const client = mockClient({
        id: clientID
      });
      const room = mockRoom({
        id: roomID,
        moderator: null
      });

      const uow = new MockUnitOfWork<AppTypeMap>();
      const spy1 = vi.spyOn(roomService, 'withTransaction').mockImplementation((_, proc) => {
        return proc({ room, uow });
      });
      const spy2 = vi.spyOn(clientRepository, 'find').mockImplementation(() => right(some(client))());
      const spy3 = vi.spyOn(roomService, 'destroy');
      const spy4 = vi.spyOn(publishService, 'toClient').mockImplementation(() => right(null)());
      const spy5 = vi.spyOn(publishService, 'toEveryone');
      const spy6 = vi.spyOn(publishService, 'broadcast');

      const either = await destroyRoom.execute(roomID, clientID);

      expect(isLeft(either)).toBe(true);
      expect(spy1).toHaveBeenCalledOnce();
      expect(spy2).toHaveBeenCalled();
      expect(spy3).not.toHaveBeenCalled();
      expect(spy4).toHaveBeenCalledOnce();
      expect(spy4).toHaveBeenCalledWith(client, PUBLISH.ROOM.DESTROY.FAILURE, {});
      expect(spy5).not.toHaveBeenCalled();
      expect(spy6).not.toHaveBeenCalled();
    });

    it('should publish faillure when clientID is different from moderator', async () => {
      const roomID = Room.ID.from('6c7ee852-2e64-43e9-8d00-7d979273c07e');
      const clientID = Client.ID.from('a1f2cb09-df08-4878-9d8f-0898a534c1f5');
      const client = mockClient({
        id: clientID
      });
      const room = mockRoom({
        id: roomID,
        moderator: mockClient({
          id: Client.ID.from('a5d6296f-74f2-4264-8f7b-891e92a35844')
        })
      });

      const uow = new MockUnitOfWork<AppTypeMap>();
      const spy1 = vi.spyOn(roomService, 'withTransaction').mockImplementation((_, proc) => {
        return proc({ room, uow });
      });
      const spy2 = vi.spyOn(clientRepository, 'find').mockImplementation(() => right(some(client))());
      const spy3 = vi.spyOn(roomService, 'destroy');
      const spy4 = vi.spyOn(publishService, 'toClient').mockImplementation(() => right(null)());
      const spy5 = vi.spyOn(publishService, 'toEveryone');
      const spy6 = vi.spyOn(publishService, 'broadcast');

      const either = await destroyRoom.execute(roomID, clientID);

      expect(isLeft(either)).toBe(true);
      expect(spy1).toHaveBeenCalledOnce();
      expect(spy2).toHaveBeenCalled();
      expect(spy3).not.toHaveBeenCalled();
      expect(spy4).toHaveBeenCalledOnce();
      expect(spy4).toHaveBeenCalledWith(client, PUBLISH.ROOM.DESTROY.FAILURE, {});
      expect(spy5).not.toHaveBeenCalled();
      expect(spy6).not.toHaveBeenCalled();
    });

    it('should publish failure when roomService.destroy() returns left', async () => {
      const roomID = Room.ID.from('6c7ee852-2e64-43e9-8d00-7d979273c07e');
      const clientID = Client.ID.from('a1f2cb09-df08-4878-9d8f-0898a534c1f5');
      const client = mockClient({
        id: clientID
      });
      const room = mockRoom({
        id: roomID,
        moderator: mockClient({
          id: clientID
        })
      });

      const uow = new MockUnitOfWork<AppTypeMap>();
      const spy1 = vi.spyOn(roomService, 'withTransaction').mockImplementation((_, proc) => {
        return proc({ room, uow });
      });
      const spy2 = vi.spyOn(clientRepository, 'find').mockImplementation(() => right(some(client))());
      const spy3 = vi.spyOn(roomService, 'destroy').mockImplementation(() => left(createUnitOfWorkError('Commit', ''))());
      const spy4 = vi.spyOn(publishService, 'toClient').mockImplementation(() => right(null)());
      const spy5 = vi.spyOn(publishService, 'toEveryone');
      const spy6 = vi.spyOn(publishService, 'broadcast');

      const either = await destroyRoom.execute(roomID, clientID);

      expect(isLeft(either)).toBe(true);
      expect(spy1).toHaveBeenCalledOnce();
      expect(spy2).toHaveBeenCalledOnce();
      expect(spy3).toHaveBeenCalledOnce();
      expect(spy4).toHaveBeenCalledOnce();
      expect(spy4).toHaveBeenCalledWith(client, PUBLISH.ROOM.DESTROY.FAILURE, {});
      expect(spy5).not.toHaveBeenCalled();
      expect(spy6).not.toHaveBeenCalled();
    });

    it('should publish failure when publishService.toClient() returns left', async () => {
      const roomID = Room.ID.from('6c7ee852-2e64-43e9-8d00-7d979273c07e');
      const clientID = Client.ID.from('a1f2cb09-df08-4878-9d8f-0898a534c1f5');
      const client = mockClient({
        id: clientID
      });
      const room = mockRoom({
        id: roomID,
        moderator: mockClient({
          id: clientID
        })
      });

      const uow = new MockUnitOfWork<AppTypeMap>();
      const spy1 = vi.spyOn(roomService, 'withTransaction').mockImplementation((_, proc) => {
        return proc({ room, uow });
      });
      const spy2 = vi.spyOn(clientRepository, 'find').mockImplementation(() => right(some(client))());
      const spy3 = vi.spyOn(roomService, 'destroy').mockImplementation(() => right(null)());
      const spy4 = vi
        .spyOn(publishService, 'toClient')
        .mockImplementationOnce(() => left(createEventPublishError('EmitFailed', ''))())
        .mockImplementationOnce(() => right(null)());
      const spy5 = vi.spyOn(publishService, 'toEveryone');
      const spy6 = vi.spyOn(publishService, 'broadcast');

      const either = await destroyRoom.execute(roomID, clientID);

      expect(isLeft(either)).toBe(true);
      expect(spy1).toHaveBeenCalledOnce();
      expect(spy2).toHaveBeenCalledOnce();
      expect(spy3).toHaveBeenCalledOnce();
      expect(spy4).toHaveBeenCalledTimes(2);
      expect(spy4).toHaveBeenNthCalledWith(1, client, PUBLISH.ROOM.DESTROY.SUCCESS, {});
      expect(spy4).toHaveBeenNthCalledWith(2, client, PUBLISH.ROOM.DESTROY.FAILURE, {});
      expect(spy5).not.toHaveBeenCalled();
      expect(spy6).not.toHaveBeenCalled();
    });

    it('should publish failure when publishService.toEveryone() returns left', async () => {
      const roomID = Room.ID.from('6c7ee852-2e64-43e9-8d00-7d979273c07e');
      const clientID = Client.ID.from('a1f2cb09-df08-4878-9d8f-0898a534c1f5');
      const client = mockClient({
        id: clientID
      });
      const room = mockRoom({
        id: roomID,
        moderator: mockClient({
          id: clientID
        })
      });

      const uow = new MockUnitOfWork<AppTypeMap>();
      const spy1 = vi.spyOn(roomService, 'withTransaction').mockImplementation((_, proc) => {
        return proc({ room, uow });
      });
      const spy2 = vi.spyOn(clientRepository, 'find').mockImplementation(() => right(some(client))());
      const spy3 = vi.spyOn(roomService, 'destroy').mockImplementation(() => right(null)());
      const spy4 = vi.spyOn(publishService, 'toClient').mockImplementation(() => right(null)());
      const spy5 = vi.spyOn(publishService, 'toEveryone').mockImplementation(() => left(createEventPublishError('EmitFailed', ''))());
      const spy6 = vi.spyOn(publishService, 'broadcast');

      const either = await destroyRoom.execute(roomID, clientID);

      expect(isLeft(either)).toBe(true);
      expect(spy1).toHaveBeenCalledOnce();
      expect(spy2).toHaveBeenCalledOnce();
      expect(spy3).toHaveBeenCalledOnce();
      expect(spy4).toHaveBeenCalledTimes(2);
      expect(spy4).toHaveBeenNthCalledWith(1, client, PUBLISH.ROOM.DESTROY.SUCCESS, {});
      expect(spy4).toHaveBeenNthCalledWith(2, client, PUBLISH.ROOM.DESTROY.FAILURE, {});
      expect(spy5).toHaveBeenCalled();
      expect(spy6).not.toHaveBeenCalled();
    });

    it('should publish failure when publishService.broadcast() returns left', async () => {
      const roomID = Room.ID.from('6c7ee852-2e64-43e9-8d00-7d979273c07e');
      const clientID = Client.ID.from('a1f2cb09-df08-4878-9d8f-0898a534c1f5');
      const client = mockClient({
        id: clientID
      });
      const room = mockRoom({
        id: roomID,
        moderator: mockClient({
          id: clientID
        })
      });

      const uow = new MockUnitOfWork<AppTypeMap>();
      const spy1 = vi.spyOn(roomService, 'withTransaction').mockImplementation((_, proc) => {
        return proc({ room, uow });
      });
      const spy2 = vi.spyOn(clientRepository, 'find').mockImplementation(() => right(some(client))());
      const spy3 = vi.spyOn(roomService, 'destroy').mockImplementation(() => right(null)());
      const spy4 = vi.spyOn(publishService, 'toClient').mockImplementation(() => right(null)());
      const spy5 = vi.spyOn(publishService, 'toEveryone').mockImplementation(() => right(null)());
      const spy6 = vi.spyOn(publishService, 'broadcast').mockImplementationOnce(() => left(createEventPublishError('EmitFailed', ''))());

      const either = await destroyRoom.execute(roomID, clientID);

      expect(isLeft(either)).toBe(true);
      expect(spy1).toHaveBeenCalledOnce();
      expect(spy2).toHaveBeenCalledOnce();
      expect(spy3).toHaveBeenCalledOnce();
      expect(spy4).toHaveBeenCalledTimes(2);
      expect(spy4).toHaveBeenNthCalledWith(1, client, PUBLISH.ROOM.DESTROY.SUCCESS, {});
      expect(spy4).toHaveBeenNthCalledWith(2, client, PUBLISH.ROOM.DESTROY.FAILURE, {});
      expect(spy5).toHaveBeenCalled();
      expect(spy6).toHaveBeenCalled();
    });
  });
});
