import { createGatewayError } from '@/adapters/gateways/GatewayError.js';
import type { IEventPublisher } from '@/adapters/presenters/EventPublisher/IEventPublisher.js';
import { MockEventPublisher } from '@/adapters/presenters/EventPublisher/mocks/MockEventPublisher.js';
import type { IConnectionPool } from '@/applications/connection/IConnectionPool.js';
import { MockConnectionPool } from '@/applications/connection/mocks/MockConnectionPool.js';
import { PUBLISH } from '@/applications/websocket/PublishEvents.js';
import { createClientError } from '@/domains/Client/ClientError.js';
import type { IClientRepository } from '@/domains/Client/IClientRepository.js';
import { mockClient } from '@/domains/Client/mocks/MockClient.js';
import { MockClientRepository } from '@/domains/Client/mocks/MockClientRepository.js';
import { mockWebSocket } from '@/infra/websocket/mocks/MockWebSocket.js';
import type { ILogger } from '@/lib/Logger/ILogger.js';
import { MockLogger } from '@/lib/Logger/mocks/MockLogger.js';
import { isLeft, isRight } from 'fp-ts/lib/Either.js';
import { left, right } from 'fp-ts/lib/TaskEither.js';
import { Connect } from '../Connect.js';

describe('Connect', () => {
  let clientRepository: IClientRepository;
  let pool: IConnectionPool;
  let eventPublisher: IEventPublisher;
  let logger: ILogger;
  let handShake: Connect;

  beforeEach(() => {
    clientRepository = new MockClientRepository();
    pool = new MockConnectionPool();
    eventPublisher = new MockEventPublisher();
    logger = new MockLogger();
    handShake = new Connect(clientRepository, pool, eventPublisher, logger);
  });

  afterEach(() => {
    vi.clearAllMocks();
  });

  describe('execute', () => {
    it('should publish success event when goes well', async () => {
      const client = mockClient();
      const ws = mockWebSocket();

      const spy1 = vi.spyOn(clientRepository, 'create').mockImplementation(() => right(null)());
      const spy2 = vi.spyOn(pool, 'register').mockImplementation(() => null);
      const spy3 = vi.spyOn(eventPublisher, 'toClient').mockImplementation(() => right(null)());
      const spy4 = vi.spyOn(pool, 'unregister').mockImplementation(() => null);

      const either = await handShake.execute(client, ws);

      expect(isRight(either)).toBe(true);
      expect(spy1).toHaveBeenCalledOnce();
      expect(spy2).toHaveBeenCalledOnce();
      expect(spy3).toHaveBeenCalledOnce();
      expect(spy3).toHaveBeenCalledWith(client.id, PUBLISH.CONNECTION.CONNECTED.SUCCESS, {});
      expect(spy4).not.toHaveBeenCalled();
    });

    it('should publish failure event when clientRepository.create() return left', async () => {
      const client = mockClient();
      const ws = mockWebSocket();

      const spy1 = vi.spyOn(clientRepository, 'create').mockImplementation(() => left(createGatewayError('EntityNotFound', ''))());
      const spy2 = vi.spyOn(pool, 'register').mockImplementation(() => null);
      const spy3 = vi.spyOn(eventPublisher, 'toClient').mockImplementation(() => right(null)());
      const spy4 = vi.spyOn(pool, 'unregister').mockImplementation(() => null);

      const either = await handShake.execute(client, ws);

      expect(isLeft(either)).toBe(true);
      expect(spy1).toHaveBeenCalledOnce();
      expect(spy2).not.toHaveBeenCalled();
      expect(spy3).toHaveBeenCalledOnce();
      expect(spy3).toHaveBeenCalledWith(client.id, PUBLISH.CONNECTION.CONNECTED.FAILURE, {});
      expect(spy4).toHaveBeenCalledOnce();
    });

    it('should publish failure event when eventPublisher.toClient() return left', async () => {
      const client = mockClient();
      const ws = mockWebSocket();

      const spy1 = vi.spyOn(clientRepository, 'create').mockImplementation(() => right(null)());
      const spy2 = vi.spyOn(pool, 'register').mockImplementation(() => null);
      const spy3 = vi
        .spyOn(eventPublisher, 'toClient')
        .mockImplementationOnce(() => left(createClientError('NoSuchClient', ''))())
        .mockImplementationOnce(() => right(null)());
      const spy4 = vi.spyOn(pool, 'unregister').mockImplementation(() => null);

      const either = await handShake.execute(client, ws);

      expect(isLeft(either)).toBe(true);
      expect(spy1).toHaveBeenCalledOnce();
      expect(spy2).toHaveBeenCalledOnce();
      expect(spy3).toHaveBeenCalledTimes(2);
      expect(spy3).toHaveBeenNthCalledWith(1, client.id, PUBLISH.CONNECTION.CONNECTED.SUCCESS, {});
      expect(spy4).toHaveBeenCalledOnce();
      expect(spy3).toHaveBeenNthCalledWith(2, client.id, PUBLISH.CONNECTION.CONNECTED.FAILURE, {});
    });

    it('should publish failure event when first and second eventPublisher.toClient() return left', async () => {
      const client = mockClient();
      const ws = mockWebSocket();

      const spy1 = vi.spyOn(clientRepository, 'create').mockImplementation(() => right(null)());
      const spy2 = vi.spyOn(pool, 'register').mockImplementation(() => null);
      const spy3 = vi
        .spyOn(eventPublisher, 'toClient')
        .mockImplementationOnce(() => left(createClientError('NoSuchClient', ''))())
        .mockImplementationOnce(() => left(createClientError('NoSuchClient', ''))());
      const spy4 = vi.spyOn(pool, 'unregister').mockImplementation(() => null);

      const either = await handShake.execute(client, ws);

      expect(isLeft(either)).toBe(true);
      expect(spy1).toHaveBeenCalledOnce();
      expect(spy2).toHaveBeenCalledOnce();
      expect(spy3).toHaveBeenCalledTimes(2);
      expect(spy3).toHaveBeenNthCalledWith(1, client.id, PUBLISH.CONNECTION.CONNECTED.SUCCESS, {});
      expect(spy4).toHaveBeenCalledOnce();
      expect(spy3).toHaveBeenNthCalledWith(2, client.id, PUBLISH.CONNECTION.CONNECTED.FAILURE, {});
    });
  });
});
