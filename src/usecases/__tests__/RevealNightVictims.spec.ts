import type { IPublishService } from '@/applications/services/interfaces/IPublishService.js';
import type { IRoomService } from '@/applications/services/interfaces/IRoomService.js';
import { MockPublishService } from '@/applications/services/mocks/MockPublishService.js';
import { MockRoomService } from '@/applications/services/mocks/MockRoomService.js';
import type { AppTypeMap } from '@/applications/unitOfWork/AppTypeMap.js';
import type { IUnitOfWork } from '@/applications/unitOfWork/IUnitOfWork.js';
import { MockUnitOfWork } from '@/applications/unitOfWork/mocks/MockUnitOfWork.js';
import { PUBLISH } from '@/applications/websocket/PublishEvents.js';
import { Identity } from '@/domains/Identity/Identity.js';
import { mockPerson } from '@/domains/Person/mocks/MockPerson.js';
import { Person, type PersonID } from '@/domains/Person/Person.js';
import { mockDailyOutcomeRecord } from '@/domains/Room/mocks/MockDailyOutcomeRecord.js';
import { mockGame } from '@/domains/Room/mocks/MockGame.js';
import { mockRoom } from '@/domains/Room/mocks/MockRoom.js';
import { Room } from '@/domains/Room/Room.js';
import type { ILogger } from '@/lib/Logger/ILogger.js';
import { MockLogger } from '@/lib/Logger/mocks/MockLogger.js';
import { isRight } from 'fp-ts/lib/Either.js';
import { isNone } from 'fp-ts/lib/Option.js';
import { right } from 'fp-ts/lib/TaskEither.js';
import { RevealNightVictims } from '../RevealNightVictims.js';

describe('RevealNightVictims', () => {
  let roomService: IRoomService;
  let publishService: IPublishService;
  let logger: ILogger;
  let revealNightVictims: RevealNightVictims;

  beforeEach(() => {
    roomService = new MockRoomService();
    publishService = new MockPublishService();
    logger = new MockLogger();
    revealNightVictims = new RevealNightVictims(roomService, publishService, logger);
  });

  describe('execute', () => {
    it('should reveal night victims: wolf attack succeeded', async () => {
      const roomID = Room.ID.from('9400e3f1-0a35-4b77-af02-2a41c4325ab8');
      const werewolfID1 = Person.ID.from('728f7a1e-2aca-42ab-be84-29d699df53e7');
      const werewolfID2 = Person.ID.from('b20d7e00-7c01-48c5-a1c8-03d187d33214');
      const knightID = Person.ID.from('e31ca109-c64f-473d-ae24-ec5d288aa55b');
      const personID = Person.ID.from('4494e99c-1a0a-4511-9431-fc3c5041ea38');
      const werewolf1 = mockPerson({
        id: werewolfID1,
        life: 'ALIVE',
        identity: Identity.WEREWOLF
      });
      const werewolf2 = mockPerson({
        id: werewolfID2,
        life: 'ALIVE',
        identity: Identity.WEREWOLF
      });
      const knight = mockPerson({
        id: knightID,
        life: 'ALIVE',
        identity: Identity.KNIGHT
      });
      const room = mockRoom({
        id: roomID,
        game: mockGame({
          residents: [
            werewolf1,
            werewolf2,
            knight,
            mockPerson({
              id: personID,
              life: 'ALIVE',
              identity: Identity.VILLAGER
            })
          ],
          records: [
            mockDailyOutcomeRecord({
              days: 1,
              attack: [
                {
                  source: werewolfID1,
                  destination: personID
                },
                {
                  source: werewolfID2,
                  destination: personID
                }
              ],
              divination: [],
              protection: [
                {
                  source: knightID,
                  destination: werewolfID1
                }
              ],
              victim: []
            })
          ]
        })
      });
      const modifiedRoom = mockRoom({
        id: roomID,
        game: mockGame({
          residents: [
            werewolf1,
            werewolf2,
            knight,
            mockPerson({
              id: personID,
              life: 'DEAD',
              identity: Identity.VILLAGER
            })
          ],
          records: [
            mockDailyOutcomeRecord({
              days: 1,
              attack: [
                {
                  source: werewolfID1,
                  destination: personID
                },
                {
                  source: werewolfID2,
                  destination: personID
                }
              ],
              divination: [],
              protection: [
                {
                  source: knightID,
                  destination: werewolfID1
                }
              ],
              victim: [personID]
            })
          ]
        })
      });
      const uow = new MockUnitOfWork<AppTypeMap>();

      const spy1 = vi.spyOn(roomService, 'withTransaction').mockImplementation((_, proc) => {
        return proc({ uow, room });
      });
      const spy2 = vi.spyOn(uow, 'update').mockImplementation(() => right(null)());
      const spy3 = vi.spyOn(publishService, 'toEveryone').mockImplementation(() => right(null)());

      const either = await revealNightVictims.execute(roomID);

      expect(isRight(either)).toBe(true);
      expect(spy1).toHaveBeenCalledOnce();
      expect(spy2).toHaveBeenCalledOnce();
      expect(spy3).toHaveBeenCalledOnce();
      expect(spy3).toHaveBeenCalledWith(modifiedRoom, PUBLISH.VICTIMIZED, {
        outcome: {
          id: [personID]
        }
      });
    });

    it('should reveal night victims: most vote collected person is attacked', async () => {
      const roomID = Room.ID.from('9400e3f1-0a35-4b77-af02-2a41c4325ab8');
      const werewolfID1 = Person.ID.from('728f7a1e-2aca-42ab-be84-29d699df53e7');
      const werewolfID2 = Person.ID.from('b20d7e00-7c01-48c5-a1c8-03d187d33214');
      const werewolfID3 = Person.ID.from('4489d461-d9ef-4cf2-a30c-c5b6f8574880');
      const knightID = Person.ID.from('e31ca109-c64f-473d-ae24-ec5d288aa55b');
      const personID = Person.ID.from('4494e99c-1a0a-4511-9431-fc3c5041ea38');
      const werewolf1 = mockPerson({
        id: werewolfID1,
        life: 'ALIVE',
        identity: Identity.WEREWOLF
      });
      const werewolf2 = mockPerson({
        id: werewolfID2,
        life: 'ALIVE',
        identity: Identity.WEREWOLF
      });
      const werewolf3 = mockPerson({
        id: werewolfID3,
        life: 'ALIVE',
        identity: Identity.WEREWOLF
      });
      const knight = mockPerson({
        id: knightID,
        life: 'ALIVE',
        identity: Identity.KNIGHT
      });
      const room = mockRoom({
        id: roomID,
        game: mockGame({
          residents: [
            werewolf1,
            werewolf2,
            werewolf3,
            knight,
            mockPerson({
              id: personID,
              life: 'ALIVE',
              identity: Identity.VILLAGER
            })
          ],
          records: [
            mockDailyOutcomeRecord({
              days: 1,
              attack: [
                {
                  source: werewolfID1,
                  destination: personID
                },
                {
                  source: werewolfID2,
                  destination: personID
                },
                {
                  source: werewolfID3,
                  destination: knightID
                }
              ],
              divination: [],
              protection: [
                {
                  source: knightID,
                  destination: werewolfID1
                }
              ],
              victim: []
            })
          ]
        })
      });
      const modifiedRoom = mockRoom({
        id: roomID,
        game: mockGame({
          residents: [
            werewolf1,
            werewolf2,
            werewolf3,
            knight,
            mockPerson({
              id: personID,
              life: 'DEAD',
              identity: Identity.VILLAGER
            })
          ],
          records: [
            mockDailyOutcomeRecord({
              days: 1,
              attack: [
                {
                  source: werewolfID1,
                  destination: personID
                },
                {
                  source: werewolfID2,
                  destination: personID
                },
                {
                  source: werewolfID3,
                  destination: knightID
                }
              ],
              divination: [],
              protection: [
                {
                  source: knightID,
                  destination: werewolfID1
                }
              ],
              victim: [personID]
            })
          ]
        })
      });
      const uow = new MockUnitOfWork<AppTypeMap>();

      const spy1 = vi.spyOn(roomService, 'withTransaction').mockImplementation((_, proc) => {
        return proc({ uow, room });
      });
      const spy2 = vi.spyOn(uow, 'update').mockImplementation(() => right(null)());
      const spy3 = vi.spyOn(publishService, 'toEveryone').mockImplementation(() => right(null)());

      const either = await revealNightVictims.execute(roomID);

      expect(isRight(either)).toBe(true);
      expect(spy1).toHaveBeenCalledOnce();
      expect(spy2).toHaveBeenCalledOnce();
      expect(spy3).toHaveBeenCalledOnce();
      expect(spy3).toHaveBeenCalledWith(modifiedRoom, PUBLISH.VICTIMIZED, {
        outcome: {
          id: [personID]
        }
      });
    });

    it('should reveal night victims: most vote collected person is attacked (randomly)', async () => {
      const roomID = Room.ID.from('9400e3f1-0a35-4b77-af02-2a41c4325ab8');
      const werewolfID1 = Person.ID.from('728f7a1e-2aca-42ab-be84-29d699df53e7');
      const werewolfID2 = Person.ID.from('b20d7e00-7c01-48c5-a1c8-03d187d33214');
      const knightID = Person.ID.from('e31ca109-c64f-473d-ae24-ec5d288aa55b');
      const personID1 = Person.ID.from('4494e99c-1a0a-4511-9431-fc3c5041ea38');
      const personID2 = Person.ID.from('a92f9cac-5c42-4870-b6a8-f5bc82d58a00');
      const werewolf1 = mockPerson({
        id: werewolfID1,
        life: 'ALIVE',
        identity: Identity.WEREWOLF
      });
      const werewolf2 = mockPerson({
        id: werewolfID2,
        life: 'ALIVE',
        identity: Identity.WEREWOLF
      });
      const knight = mockPerson({
        id: knightID,
        life: 'ALIVE',
        identity: Identity.KNIGHT
      });
      const room = mockRoom({
        id: roomID,
        game: mockGame({
          residents: [
            werewolf1,
            werewolf2,
            knight,
            mockPerson({
              id: personID1,
              life: 'ALIVE',
              identity: Identity.VILLAGER
            }),
            mockPerson({
              id: personID2,
              life: 'ALIVE',
              identity: Identity.VILLAGER
            })
          ],
          records: [
            mockDailyOutcomeRecord({
              days: 1,
              attack: [
                {
                  source: werewolfID1,
                  destination: personID1
                },
                {
                  source: werewolfID2,
                  destination: personID2
                }
              ],
              divination: [],
              protection: [],
              victim: []
            })
          ]
        })
      });
      const uow: IUnitOfWork<AppTypeMap> = new MockUnitOfWork<AppTypeMap>();

      const victimID: Array<PersonID> = [];

      const spy1 = vi.spyOn(roomService, 'withTransaction').mockImplementation((_, proc) => {
        return proc({ uow, room });
      });
      const spy2 = vi.spyOn(uow, 'update').mockImplementation((_, room) => {
        const option = Room.getLatestRecord(room);

        if (isNone(option)) {
          throw new Error('This should not happen');
        }

        victimID.push(...option.value.victim);

        return right(null)();
      });
      const spy3 = vi.spyOn(publishService, 'toEveryone').mockImplementation(() => right(null)());

      const either = await revealNightVictims.execute(roomID);

      const modifiedRoom = mockRoom({
        id: roomID,
        game: mockGame({
          residents: [
            werewolf1,
            werewolf2,
            knight,
            mockPerson({
              id: personID1,
              life: victimID[0] === personID1 ? 'DEAD' : 'ALIVE',
              identity: Identity.VILLAGER
            }),
            mockPerson({
              id: personID2,
              life: victimID[0] === personID2 ? 'DEAD' : 'ALIVE',
              identity: Identity.VILLAGER
            })
          ],
          records: [
            mockDailyOutcomeRecord({
              days: 1,
              attack: [
                {
                  source: werewolfID1,
                  destination: personID1
                },
                {
                  source: werewolfID2,
                  destination: personID2
                }
              ],
              divination: [],
              protection: [],
              victim: victimID
            })
          ]
        })
      });

      expect(isRight(either)).toBe(true);
      expect(spy1).toHaveBeenCalledOnce();
      expect(spy2).toHaveBeenCalledOnce();
      expect(spy3).toHaveBeenCalledOnce();
      expect(spy3).toHaveBeenCalledWith(modifiedRoom, PUBLISH.VICTIMIZED, {
        outcome: {
          id: victimID
        }
      });
    });

    it('should reveal night victims: knight blocked wolf attack', async () => {
      const roomID = Room.ID.from('9400e3f1-0a35-4b77-af02-2a41c4325ab8');
      const werewolfID1 = Person.ID.from('728f7a1e-2aca-42ab-be84-29d699df53e7');
      const werewolfID2 = Person.ID.from('b20d7e00-7c01-48c5-a1c8-03d187d33214');
      const knightID = Person.ID.from('e31ca109-c64f-473d-ae24-ec5d288aa55b');
      const personID = Person.ID.from('4494e99c-1a0a-4511-9431-fc3c5041ea38');
      const werewolf1 = mockPerson({
        id: werewolfID1,
        life: 'ALIVE',
        identity: Identity.WEREWOLF
      });
      const werewolf2 = mockPerson({
        id: werewolfID2,
        life: 'ALIVE',
        identity: Identity.WEREWOLF
      });
      const knight = mockPerson({
        id: knightID,
        life: 'ALIVE',
        identity: Identity.KNIGHT
      });
      const room = mockRoom({
        id: roomID,
        game: mockGame({
          residents: [
            werewolf1,
            werewolf2,
            knight,
            mockPerson({
              id: personID,
              life: 'ALIVE',
              identity: Identity.VILLAGER
            })
          ],
          records: [
            mockDailyOutcomeRecord({
              days: 1,
              attack: [
                {
                  source: werewolfID1,
                  destination: personID
                },
                {
                  source: werewolfID2,
                  destination: personID
                }
              ],
              divination: [],
              protection: [
                {
                  source: knightID,
                  destination: personID
                }
              ],
              victim: []
            })
          ]
        })
      });
      const modifiedRoom = mockRoom({
        id: roomID,
        game: mockGame({
          residents: [
            werewolf1,
            werewolf2,
            knight,
            mockPerson({
              id: personID,
              life: 'ALIVE',
              identity: Identity.VILLAGER
            })
          ],
          records: [
            mockDailyOutcomeRecord({
              days: 1,
              attack: [
                {
                  source: werewolfID1,
                  destination: personID
                },
                {
                  source: werewolfID2,
                  destination: personID
                }
              ],
              divination: [],
              protection: [
                {
                  source: knightID,
                  destination: personID
                }
              ],
              victim: []
            })
          ]
        })
      });
      const uow = new MockUnitOfWork<AppTypeMap>();

      const spy1 = vi.spyOn(roomService, 'withTransaction').mockImplementation((_, proc) => {
        return proc({ uow, room });
      });
      const spy2 = vi.spyOn(uow, 'update').mockImplementation(() => right(null)());
      const spy3 = vi.spyOn(publishService, 'toEveryone').mockImplementation(() => right(null)());

      const either = await revealNightVictims.execute(roomID);

      expect(isRight(either)).toBe(true);
      expect(spy1).toHaveBeenCalledOnce();
      expect(spy2).toHaveBeenCalledOnce();
      expect(spy3).toHaveBeenCalledOnce();
      expect(spy3).toHaveBeenCalledWith(modifiedRoom, PUBLISH.VICTIMIZED, {
        outcome: {
          id: []
        }
      });
    });

    it('should reveal night victims: hunter is attacked and retaliates', async () => {
      const roomID = Room.ID.from('9400e3f1-0a35-4b77-af02-2a41c4325ab8');
      const werewolfID1 = Person.ID.from('728f7a1e-2aca-42ab-be84-29d699df53e7');
      const werewolfID2 = Person.ID.from('b20d7e00-7c01-48c5-a1c8-03d187d33214');
      const knightID = Person.ID.from('e31ca109-c64f-473d-ae24-ec5d288aa55b');
      const hunterID = Person.ID.from('4494e99c-1a0a-4511-9431-fc3c5041ea38');
      const werewolf1 = mockPerson({
        id: werewolfID1,
        life: 'ALIVE',
        identity: Identity.WEREWOLF
      });
      const werewolf2 = mockPerson({
        id: werewolfID2,
        life: 'ALIVE',
        identity: Identity.WEREWOLF
      });
      const knight = mockPerson({
        id: knightID,
        life: 'ALIVE',
        identity: Identity.KNIGHT
      });
      const room = mockRoom({
        id: roomID,
        game: mockGame({
          residents: [
            werewolf1,
            werewolf2,
            knight,
            mockPerson({
              id: hunterID,
              life: 'ALIVE',
              identity: Identity.HUNTER
            })
          ],
          records: [
            mockDailyOutcomeRecord({
              days: 1,
              attack: [
                {
                  source: werewolfID1,
                  destination: hunterID
                },
                {
                  source: werewolfID2,
                  destination: hunterID
                }
              ],
              divination: [],
              protection: [
                {
                  source: knightID,
                  destination: werewolfID1
                }
              ],
              victim: []
            })
          ]
        })
      });
      const uow: IUnitOfWork<AppTypeMap> = new MockUnitOfWork<AppTypeMap>();

      const victimID: Array<PersonID> = [];

      const spy1 = vi.spyOn(roomService, 'withTransaction').mockImplementation((_, proc) => {
        return proc({ uow, room });
      });
      const spy2 = vi.spyOn(uow, 'update').mockImplementation((_, room) => {
        const option = Room.getLatestRecord(room);

        if (isNone(option)) {
          throw new Error('This should not happen');
        }

        victimID.push(...option.value.victim);

        return right(null)();
      });
      const spy3 = vi.spyOn(publishService, 'toEveryone').mockImplementation(() => right(null)());

      const either = await revealNightVictims.execute(roomID);

      const modifiedRoom = mockRoom({
        id: roomID,
        game: mockGame({
          residents: [
            mockPerson({
              id: werewolfID1,
              life: victimID.includes(werewolfID1) ? 'DEAD' : 'ALIVE',
              identity: Identity.WEREWOLF
            }),
            mockPerson({
              id: werewolfID2,
              life: victimID.includes(werewolfID2) ? 'DEAD' : 'ALIVE',
              identity: Identity.WEREWOLF
            }),
            knight,
            mockPerson({
              id: hunterID,
              life: 'DEAD',
              identity: Identity.HUNTER
            })
          ],
          records: [
            mockDailyOutcomeRecord({
              days: 1,
              attack: [
                {
                  source: werewolfID1,
                  destination: hunterID
                },
                {
                  source: werewolfID2,
                  destination: hunterID
                }
              ],
              divination: [],
              protection: [
                {
                  source: knightID,
                  destination: werewolfID1
                }
              ],
              victim: victimID
            })
          ]
        })
      });

      expect(isRight(either)).toBe(true);
      expect(victimID.length).toBe(2);
      expect(spy1).toHaveBeenCalledOnce();
      expect(spy2).toHaveBeenCalledOnce();
      expect(spy3).toHaveBeenCalledOnce();
      expect(spy3).toHaveBeenCalledWith(modifiedRoom, PUBLISH.VICTIMIZED, {
        outcome: {
          id: victimID
        }
      });
    });

    it('should reveal night victims: hunter does not retaliate when the wolf is last one', async () => {
      const roomID = Room.ID.from('9400e3f1-0a35-4b77-af02-2a41c4325ab8');
      const werewolfID = Person.ID.from('728f7a1e-2aca-42ab-be84-29d699df53e7');
      const knightID = Person.ID.from('e31ca109-c64f-473d-ae24-ec5d288aa55b');
      const hunterID = Person.ID.from('4494e99c-1a0a-4511-9431-fc3c5041ea38');
      const werewolf = mockPerson({
        id: werewolfID,
        life: 'ALIVE',
        identity: Identity.WEREWOLF
      });
      const knight = mockPerson({
        id: knightID,
        life: 'ALIVE',
        identity: Identity.KNIGHT
      });
      const room = mockRoom({
        id: roomID,
        game: mockGame({
          residents: [
            werewolf,
            knight,
            mockPerson({
              id: hunterID,
              life: 'ALIVE',
              identity: Identity.HUNTER
            })
          ],
          records: [
            mockDailyOutcomeRecord({
              days: 1,
              attack: [
                {
                  source: werewolfID,
                  destination: hunterID
                }
              ],
              divination: [],
              protection: [
                {
                  source: knightID,
                  destination: werewolfID
                }
              ],
              victim: []
            })
          ]
        })
      });
      const modifiedRoom = mockRoom({
        id: roomID,
        game: mockGame({
          residents: [
            werewolf,
            knight,
            mockPerson({
              id: hunterID,
              life: 'DEAD',
              identity: Identity.HUNTER
            })
          ],
          records: [
            mockDailyOutcomeRecord({
              days: 1,
              attack: [
                {
                  source: werewolfID,
                  destination: hunterID
                }
              ],
              divination: [],
              protection: [
                {
                  source: knightID,
                  destination: werewolfID
                }
              ],
              victim: [hunterID]
            })
          ]
        })
      });
      const uow = new MockUnitOfWork<AppTypeMap>();

      const spy1 = vi.spyOn(roomService, 'withTransaction').mockImplementation((_, proc) => {
        return proc({ uow, room });
      });
      const spy2 = vi.spyOn(uow, 'update').mockImplementation(() => right(null)());
      const spy3 = vi.spyOn(publishService, 'toEveryone').mockImplementation(() => right(null)());

      const either = await revealNightVictims.execute(roomID);

      expect(isRight(either)).toBe(true);
      expect(spy1).toHaveBeenCalledOnce();
      expect(spy2).toHaveBeenCalledOnce();
      expect(spy3).toHaveBeenCalledOnce();
      expect(spy3).toHaveBeenCalledWith(modifiedRoom, PUBLISH.VICTIMIZED, {
        outcome: {
          id: [hunterID]
        }
      });
    });

    it('should reveal night victims: knight protects hunter', async () => {
      const roomID = Room.ID.from('9400e3f1-0a35-4b77-af02-2a41c4325ab8');
      const werewolfID1 = Person.ID.from('728f7a1e-2aca-42ab-be84-29d699df53e7');
      const werewolfID2 = Person.ID.from('b20d7e00-7c01-48c5-a1c8-03d187d33214');
      const knightID = Person.ID.from('e31ca109-c64f-473d-ae24-ec5d288aa55b');
      const hunterID = Person.ID.from('4494e99c-1a0a-4511-9431-fc3c5041ea38');
      const werewolf1 = mockPerson({
        id: werewolfID1,
        life: 'ALIVE',
        identity: Identity.WEREWOLF
      });
      const werewolf2 = mockPerson({
        id: werewolfID2,
        life: 'ALIVE',
        identity: Identity.WEREWOLF
      });
      const knight = mockPerson({
        id: knightID,
        life: 'ALIVE',
        identity: Identity.KNIGHT
      });
      const room = mockRoom({
        id: roomID,
        game: mockGame({
          residents: [
            werewolf1,
            werewolf2,
            knight,
            mockPerson({
              id: hunterID,
              life: 'ALIVE',
              identity: Identity.HUNTER
            })
          ],
          records: [
            mockDailyOutcomeRecord({
              days: 1,
              attack: [
                {
                  source: werewolfID1,
                  destination: hunterID
                },
                {
                  source: werewolfID2,
                  destination: hunterID
                }
              ],
              divination: [],
              protection: [
                {
                  source: knightID,
                  destination: hunterID
                }
              ],
              victim: []
            })
          ]
        })
      });
      const modifiedRoom = mockRoom({
        id: roomID,
        game: mockGame({
          residents: [
            werewolf1,
            werewolf2,
            knight,
            mockPerson({
              id: hunterID,
              life: 'ALIVE',
              identity: Identity.HUNTER
            })
          ],
          records: [
            mockDailyOutcomeRecord({
              days: 1,
              attack: [
                {
                  source: werewolfID1,
                  destination: hunterID
                },
                {
                  source: werewolfID2,
                  destination: hunterID
                }
              ],
              divination: [],
              protection: [
                {
                  source: knightID,
                  destination: hunterID
                }
              ],
              victim: []
            })
          ]
        })
      });
      const uow = new MockUnitOfWork<AppTypeMap>();

      const spy1 = vi.spyOn(roomService, 'withTransaction').mockImplementation((_, proc) => {
        return proc({ uow, room });
      });
      const spy2 = vi.spyOn(uow, 'update').mockImplementation(() => right(null)());
      const spy3 = vi.spyOn(publishService, 'toEveryone').mockImplementation(() => right(null)());

      const either = await revealNightVictims.execute(roomID);

      expect(isRight(either)).toBe(true);
      expect(spy1).toHaveBeenCalledOnce();
      expect(spy2).toHaveBeenCalledOnce();
      expect(spy3).toHaveBeenCalledOnce();
      expect(spy3).toHaveBeenCalledWith(modifiedRoom, PUBLISH.VICTIMIZED, {
        outcome: {
          id: []
        }
      });
    });

    it('should reveal night victims: kitsune is attacked but not dead', async () => {
      const roomID = Room.ID.from('9400e3f1-0a35-4b77-af02-2a41c4325ab8');
      const werewolfID = Person.ID.from('728f7a1e-2aca-42ab-be84-29d699df53e7');
      const kitsuneID = Person.ID.from('e31ca109-c64f-473d-ae24-ec5d288aa55b');
      const personID = Person.ID.from('4494e99c-1a0a-4511-9431-fc3c5041ea38');
      const werewolf = mockPerson({
        id: werewolfID,
        life: 'ALIVE',
        identity: Identity.WEREWOLF
      });
      const kitsune = mockPerson({
        id: kitsuneID,
        life: 'ALIVE',
        identity: Identity.KITSUNE
      });
      const room = mockRoom({
        id: roomID,
        game: mockGame({
          residents: [
            werewolf,
            kitsune,
            mockPerson({
              id: personID,
              life: 'ALIVE',
              identity: Identity.VILLAGER
            })
          ],
          records: [
            mockDailyOutcomeRecord({
              days: 1,
              attack: [
                {
                  source: werewolfID,
                  destination: kitsuneID
                }
              ],
              divination: [],
              protection: [],
              victim: []
            })
          ]
        })
      });
      const modifiedRoom = mockRoom({
        id: roomID,
        game: mockGame({
          residents: [
            werewolf,
            kitsune,
            mockPerson({
              id: personID,
              life: 'ALIVE',
              identity: Identity.VILLAGER
            })
          ],
          records: [
            mockDailyOutcomeRecord({
              days: 1,
              attack: [
                {
                  source: werewolfID,
                  destination: kitsuneID
                }
              ],
              divination: [],
              protection: [],
              victim: []
            })
          ]
        })
      });
      const uow = new MockUnitOfWork<AppTypeMap>();

      const spy1 = vi.spyOn(roomService, 'withTransaction').mockImplementation((_, proc) => {
        return proc({ uow, room });
      });
      const spy2 = vi.spyOn(uow, 'update').mockImplementation(() => right(null)());
      const spy3 = vi.spyOn(publishService, 'toEveryone').mockImplementation(() => right(null)());

      const either = await revealNightVictims.execute(roomID);

      expect(isRight(either)).toBe(true);
      expect(spy1).toHaveBeenCalledOnce();
      expect(spy2).toHaveBeenCalledOnce();
      expect(spy3).toHaveBeenCalledOnce();
      expect(spy3).toHaveBeenCalledWith(modifiedRoom, PUBLISH.VICTIMIZED, {
        outcome: {
          id: []
        }
      });
    });

    it('should reveal night victims: knight protects kitsune', async () => {
      const roomID = Room.ID.from('9400e3f1-0a35-4b77-af02-2a41c4325ab8');
      const werewolfID = Person.ID.from('728f7a1e-2aca-42ab-be84-29d699df53e7');
      const knightID = Person.ID.from('e31ca109-c64f-473d-ae24-ec5d288aa55b');
      const kitsuneID = Person.ID.from('4494e99c-1a0a-4511-9431-fc3c5041ea38');
      const werewolf = mockPerson({
        id: werewolfID,
        life: 'ALIVE',
        identity: Identity.WEREWOLF
      });
      const knight = mockPerson({
        id: knightID,
        life: 'ALIVE',
        identity: Identity.KNIGHT
      });
      const kitsune = mockPerson({
        id: kitsuneID,
        life: 'ALIVE',
        identity: Identity.KNIGHT
      });
      const room = mockRoom({
        id: roomID,
        game: mockGame({
          residents: [werewolf, knight, kitsune],
          records: [
            mockDailyOutcomeRecord({
              days: 1,
              attack: [
                {
                  source: werewolfID,
                  destination: kitsuneID
                }
              ],
              divination: [],
              protection: [
                {
                  source: knightID,
                  destination: kitsuneID
                }
              ],
              victim: []
            })
          ]
        })
      });
      const modifiedRoom = mockRoom({
        id: roomID,
        game: mockGame({
          residents: [werewolf, knight, kitsune],
          records: [
            mockDailyOutcomeRecord({
              days: 1,
              attack: [
                {
                  source: werewolfID,
                  destination: kitsuneID
                }
              ],
              divination: [],
              protection: [
                {
                  source: knightID,
                  destination: kitsuneID
                }
              ],
              victim: []
            })
          ]
        })
      });
      const uow = new MockUnitOfWork<AppTypeMap>();

      const spy1 = vi.spyOn(roomService, 'withTransaction').mockImplementation((_, proc) => {
        return proc({ uow, room });
      });
      const spy2 = vi.spyOn(uow, 'update').mockImplementation(() => right(null)());
      const spy3 = vi.spyOn(publishService, 'toEveryone').mockImplementation(() => right(null)());

      const either = await revealNightVictims.execute(roomID);

      expect(isRight(either)).toBe(true);
      expect(spy1).toHaveBeenCalledOnce();
      expect(spy2).toHaveBeenCalledOnce();
      expect(spy3).toHaveBeenCalledOnce();
      expect(spy3).toHaveBeenCalledWith(modifiedRoom, PUBLISH.VICTIMIZED, {
        outcome: {
          id: []
        }
      });
    });

    it('should reveal night victims: seer divined fox', async () => {
      const roomID = Room.ID.from('9400e3f1-0a35-4b77-af02-2a41c4325ab8');
      const seerID = Person.ID.from('728f7a1e-2aca-42ab-be84-29d699df53e7');
      const kitsuneID = Person.ID.from('4494e99c-1a0a-4511-9431-fc3c5041ea38');
      const seer = mockPerson({
        id: seerID,
        life: 'ALIVE',
        identity: Identity.SEER
      });
      const room = mockRoom({
        id: roomID,
        game: mockGame({
          residents: [
            seer,
            mockPerson({
              id: kitsuneID,
              life: 'ALIVE',
              identity: Identity.KITSUNE
            })
          ],
          records: [
            mockDailyOutcomeRecord({
              days: 1,
              attack: [],
              divination: [
                {
                  source: seerID,
                  destination: kitsuneID
                }
              ],
              protection: [],
              victim: []
            })
          ]
        })
      });
      const modifiedRoom = mockRoom({
        id: roomID,
        game: mockGame({
          residents: [
            seer,
            mockPerson({
              id: kitsuneID,
              life: 'DEAD',
              identity: Identity.KITSUNE
            })
          ],
          records: [
            mockDailyOutcomeRecord({
              days: 1,
              attack: [],
              divination: [
                {
                  source: seerID,
                  destination: kitsuneID
                }
              ],
              protection: [],
              victim: [kitsuneID]
            })
          ]
        })
      });
      const uow = new MockUnitOfWork<AppTypeMap>();

      const spy1 = vi.spyOn(roomService, 'withTransaction').mockImplementation((_, proc) => {
        return proc({ uow, room });
      });
      const spy2 = vi.spyOn(uow, 'update').mockImplementation(() => right(null)());
      const spy3 = vi.spyOn(publishService, 'toEveryone').mockImplementation(() => right(null)());

      const either = await revealNightVictims.execute(roomID);

      expect(isRight(either)).toBe(true);
      expect(spy1).toHaveBeenCalledOnce();
      expect(spy2).toHaveBeenCalledOnce();
      expect(spy3).toHaveBeenCalledOnce();
      expect(spy3).toHaveBeenCalledWith(modifiedRoom, PUBLISH.VICTIMIZED, {
        outcome: {
          id: [kitsuneID]
        }
      });
    });

    it('should reveal night victims: knight cannot block seer divination', async () => {
      const roomID = Room.ID.from('9400e3f1-0a35-4b77-af02-2a41c4325ab8');
      const seerID = Person.ID.from('728f7a1e-2aca-42ab-be84-29d699df53e7');
      const knightID = Person.ID.from('e31ca109-c64f-473d-ae24-ec5d288aa55b');
      const kitsuneID = Person.ID.from('4494e99c-1a0a-4511-9431-fc3c5041ea38');
      const seer = mockPerson({
        id: seerID,
        life: 'ALIVE',
        identity: Identity.SEER
      });
      const knight = mockPerson({
        id: knightID,
        life: 'ALIVE',
        identity: Identity.KNIGHT
      });
      const room = mockRoom({
        id: roomID,
        game: mockGame({
          residents: [
            seer,
            knight,
            mockPerson({
              id: kitsuneID,
              life: 'ALIVE',
              identity: Identity.KITSUNE
            })
          ],
          records: [
            mockDailyOutcomeRecord({
              days: 1,
              attack: [],
              divination: [
                {
                  source: seerID,
                  destination: kitsuneID
                }
              ],
              protection: [
                {
                  source: knightID,
                  destination: kitsuneID
                }
              ],
              victim: []
            })
          ]
        })
      });
      const modifiedRoom = mockRoom({
        id: roomID,
        game: mockGame({
          residents: [
            seer,
            knight,
            mockPerson({
              id: kitsuneID,
              life: 'DEAD',
              identity: Identity.KITSUNE
            })
          ],
          records: [
            mockDailyOutcomeRecord({
              days: 1,
              attack: [],
              divination: [
                {
                  source: seerID,
                  destination: kitsuneID
                }
              ],
              protection: [
                {
                  source: knightID,
                  destination: kitsuneID
                }
              ],
              victim: [kitsuneID]
            })
          ]
        })
      });
      const uow = new MockUnitOfWork<AppTypeMap>();

      const spy1 = vi.spyOn(roomService, 'withTransaction').mockImplementation((_, proc) => {
        return proc({ uow, room });
      });
      const spy2 = vi.spyOn(uow, 'update').mockImplementation(() => right(null)());
      const spy3 = vi.spyOn(publishService, 'toEveryone').mockImplementation(() => right(null)());

      const either = await revealNightVictims.execute(roomID);

      expect(isRight(either)).toBe(true);
      expect(spy1).toHaveBeenCalledOnce();
      expect(spy2).toHaveBeenCalledOnce();
      expect(spy3).toHaveBeenCalledOnce();
      expect(spy3).toHaveBeenCalledWith(modifiedRoom, PUBLISH.VICTIMIZED, {
        outcome: {
          id: [kitsuneID]
        }
      });
    });
  });
});
