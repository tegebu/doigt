import type { IScheduler } from '@/applications/scheduler/IScheduler.js';
import { MockScheduler } from '@/applications/scheduler/mocks/MockScheduler.js';
import type { IPublishService } from '@/applications/services/interfaces/IPublishService.js';
import type { IRoomService } from '@/applications/services/interfaces/IRoomService.js';
import { MockPublishService } from '@/applications/services/mocks/MockPublishService.js';
import { MockRoomService } from '@/applications/services/mocks/MockRoomService.js';
import type { AppTypeMap } from '@/applications/unitOfWork/AppTypeMap.js';
import type { IUnitOfWork } from '@/applications/unitOfWork/IUnitOfWork.js';
import { MockUnitOfWork } from '@/applications/unitOfWork/mocks/MockUnitOfWork.js';
import { PUBLISH } from '@/applications/websocket/PublishEvents.js';
import { Identity } from '@/domains/Identity/Identity.js';
import type { Team } from '@/domains/Identity/Team.js';
import { Person } from '@/domains/Person/Person.js';
import { mockPerson } from '@/domains/Person/mocks/MockPerson.js';
import { Room } from '@/domains/Room/Room.js';
import { mockDailyOutcomeRecord } from '@/domains/Room/mocks/MockDailyOutcomeRecord.js';
import { mockDayPhase } from '@/domains/Room/mocks/MockDayPhase.js';
import { mockGame } from '@/domains/Room/mocks/MockGame.js';
import { mockPhaseDuration } from '@/domains/Room/mocks/MockPhaseDuration.js';
import { mockRoom } from '@/domains/Room/mocks/MockRoom.js';
import type { ILogger } from '@/lib/Logger/ILogger.js';
import { MockLogger } from '@/lib/Logger/mocks/MockLogger.js';
import { isRight } from 'fp-ts/lib/Either.js';
import { right } from 'fp-ts/lib/TaskEither.js';
import { PhaseTransition } from '../PhaseTransition.js';
import type { IEndPhase } from '../interfaces/IEndPhase.js';
import type { IStartPhase } from '../interfaces/IStartPhase.js';
import { MockEndPhase } from '../mocks/MockEndPhase.js';
import { MockStartPhase } from '../mocks/MockStartPhase.js';

describe('PhaseTransition', () => {
  let roomService: IRoomService;
  let publishService: IPublishService;
  let scheduler: IScheduler;
  let startMorningPhase: IStartPhase;
  let startDayPhase: IStartPhase;
  let startEveningPhase: IStartPhase;
  let startNightPhase: IStartPhase;
  let endMorningPhase: IEndPhase;
  let endDayPhase: IEndPhase;
  let endEveningPhase: IEndPhase;
  let endNightPhase: IEndPhase;
  let logger: ILogger;
  let phaseTransition: PhaseTransition;

  beforeEach(() => {
    roomService = new MockRoomService();
    publishService = new MockPublishService();
    scheduler = new MockScheduler();
    startMorningPhase = new MockStartPhase();
    startDayPhase = new MockStartPhase();
    startEveningPhase = new MockStartPhase();
    startNightPhase = new MockStartPhase();
    endMorningPhase = new MockEndPhase();
    endDayPhase = new MockEndPhase();
    endEveningPhase = new MockEndPhase();
    endNightPhase = new MockEndPhase();
    logger = new MockLogger();
    phaseTransition = new PhaseTransition(
      roomService,
      publishService,
      scheduler,
      startMorningPhase,
      startDayPhase,
      startEveningPhase,
      startNightPhase,
      endMorningPhase,
      endDayPhase,
      endEveningPhase,
      endNightPhase,
      logger
    );
  });

  afterEach(() => {
    vi.clearAllMocks();
  });

  describe('execute', () => {
    it('should return finish immediately when the game finished: finish at the start of morning phase', async () => {
      const roomID = Room.ID.from('3cf1a574-e9c2-43b4-92f8-2ca84a53186a');
      const personID1 = Person.ID.from('ebcbec22-cde4-4564-8b9b-5802c9e03313');
      const personID2 = Person.ID.from('c5075ff6-8d3d-4594-8f1e-e3253db28fbf');
      const personID3 = Person.ID.from('ac6fd3d3-59d8-4ab8-b5cb-d6bce42c357c');
      const personID4 = Person.ID.from('81f5d38d-7fd6-4604-b552-656237a899d2');
      const room = mockRoom({
        id: roomID,
        game: mockGame({
          residents: [
            mockPerson({
              id: personID1,
              identity: Identity.VILLAGER,
              life: 'ALIVE'
            }),
            mockPerson({
              id: personID2,
              identity: Identity.VILLAGER,
              life: 'ALIVE'
            }),
            mockPerson({
              id: personID3,
              identity: Identity.WEREWOLF,
              life: 'ALIVE'
            }),
            mockPerson({
              id: personID4,
              identity: Identity.WEREWOLF,
              life: 'ALIVE'
            })
          ],
          records: [
            mockDailyOutcomeRecord({
              days: 0
            }),
            mockDailyOutcomeRecord({
              days: 1
            })
          ],
          dayPhase: mockDayPhase({
            phase: 'MORNING'
          }),
          phaseDuration: mockPhaseDuration({
            morning: 1,
            day: 1,
            evening: 1,
            night: 1
          })
        })
      });

      const uow = new MockUnitOfWork<AppTypeMap>();

      const spy1 = vi.spyOn(roomService, 'withTransaction').mockImplementation((_, proc) => {
        return proc({ room, uow });
      });
      const spy2 = vi.spyOn(startMorningPhase, 'execute').mockImplementation(() =>
        right([
          {
            team: 'WEREWOLVES',
            terminateGame: true
          } satisfies Team
        ])()
      );
      const spy3 = vi.spyOn(scheduler, 'startPhase');
      const spy4 = vi.spyOn(endMorningPhase, 'execute');
      const spy5 = vi.spyOn(scheduler, 'stopPhase').mockImplementation(() => undefined);
      const spy6 = vi.spyOn(publishService, 'toEveryone').mockImplementation(() => right(null)());
      const spy7 = vi.spyOn(publishService, 'broadcast').mockImplementation(() => right(null)());
      const spy8 = vi.spyOn(uow, 'update').mockImplementation(() => right(null)());
      const spy9 = vi.spyOn(endMorningPhase, 'execute');

      const either = await phaseTransition.execute(roomID);

      expect(isRight(either)).toBe(true);
      expect(spy1).toHaveBeenCalledTimes(2);
      expect(spy2).toHaveBeenCalledOnce();
      expect(spy3).not.toHaveBeenCalled();
      expect(spy4).not.toHaveBeenCalled();
      expect(spy5).toHaveBeenCalledOnce();
      expect(spy6).toHaveBeenCalledOnce();
      expect(spy6).toHaveBeenCalledWith(
        mockRoom({
          ...room,
          participants: [...room.participants.values()],
          game: mockGame({
            ...room.game,
            residents: [...room.game.residents.values()],
            records: [...room.game.records.values()],
            status: 'COMPLETED'
          })
        }),
        PUBLISH.GAME.OVER,
        {
          teams: [
            {
              team: 'WEREWOLVES',
              terminateGame: true
            }
          ],
          records: [
            {
              days: 0,
              attack: [],
              corruption: [],
              divination: [],
              execution: null,
              protection: [],
              retaliation: [],
              victim: [],
              vote: []
            },
            {
              days: 1,
              attack: [],
              corruption: [],
              divination: [],
              execution: null,
              protection: [],
              retaliation: [],
              victim: [],
              vote: []
            }
          ]
        }
      );
      expect(spy7).toHaveBeenCalledOnce();
      expect(spy8).toHaveBeenCalledOnce();
      expect(spy8).toHaveBeenLastCalledWith(
        'room',
        mockRoom({
          ...room,
          participants: [...room.participants.values()],
          game: mockGame({
            ...room.game,
            residents: [...room.game.residents.values()],
            records: [...room.game.records.values()],
            status: 'COMPLETED'
          })
        })
      );
      expect(spy9).not.toHaveBeenCalled();
    });

    it('should return finish immediately when the game finished: finish at the enf of morning phase', async () => {
      const roomID = Room.ID.from('3cf1a574-e9c2-43b4-92f8-2ca84a53186a');
      const personID1 = Person.ID.from('ebcbec22-cde4-4564-8b9b-5802c9e03313');
      const personID2 = Person.ID.from('c5075ff6-8d3d-4594-8f1e-e3253db28fbf');
      const personID3 = Person.ID.from('ac6fd3d3-59d8-4ab8-b5cb-d6bce42c357c');
      const personID4 = Person.ID.from('81f5d38d-7fd6-4604-b552-656237a899d2');
      const room = mockRoom({
        id: roomID,
        game: mockGame({
          residents: [
            mockPerson({
              id: personID1,
              identity: Identity.VILLAGER,
              life: 'ALIVE'
            }),
            mockPerson({
              id: personID2,
              identity: Identity.VILLAGER,
              life: 'ALIVE'
            }),
            mockPerson({
              id: personID3,
              identity: Identity.WEREWOLF,
              life: 'ALIVE'
            }),
            mockPerson({
              id: personID4,
              identity: Identity.WEREWOLF,
              life: 'ALIVE'
            })
          ],
          records: [
            mockDailyOutcomeRecord({
              days: 0
            }),
            mockDailyOutcomeRecord({
              days: 1
            })
          ],
          dayPhase: mockDayPhase({
            days: 1,
            phase: 'MORNING'
          }),
          phaseDuration: mockPhaseDuration({
            morning: 1,
            day: 1,
            evening: 1,
            night: 1
          })
        })
      });

      let assertionPromise: Promise<void> = new Promise((reject) => setTimeout(() => reject(), 100));

      const uow = new MockUnitOfWork<AppTypeMap>();

      const spy1 = vi.spyOn(roomService, 'withTransaction').mockImplementation((_, proc) => {
        return proc({ room, uow });
      });
      const spy2 = vi.spyOn(startMorningPhase, 'execute').mockImplementation(() => right([])());
      const spy3 = vi.spyOn(scheduler, 'startPhase').mockImplementation(async (r, _s, proc) => {
        const either = await proc(r);

        assertionPromise = Promise.resolve();

        return either;
      });
      const spy4 = vi.spyOn(endMorningPhase, 'execute').mockImplementation(() =>
        right([
          {
            team: 'WEREWOLVES',
            terminateGame: true
          } satisfies Team
        ])()
      );
      const spy5 = vi.spyOn(scheduler, 'stopPhase').mockImplementation(() => undefined);
      const spy6 = vi.spyOn(publishService, 'toEveryone').mockImplementation((_, _room) => right(null)());
      const spy7 = vi.spyOn(publishService, 'broadcast').mockImplementation(() => right(null)());
      const spy8 = vi.spyOn(uow, 'update').mockImplementation(() => right(null)());

      const either = await phaseTransition.execute(roomID);

      expect(isRight(either)).toBe(true);

      await assertionPromise;

      expect(spy1).toHaveBeenCalledTimes(2);
      expect(spy2).toHaveBeenCalledOnce();
      expect(spy3).toHaveBeenCalledOnce();
      expect(spy4).toHaveBeenCalledOnce();
      expect(spy5).toHaveBeenCalledOnce();
      expect(spy6).toHaveBeenCalledOnce();
      expect(spy6).toHaveBeenCalledWith(
        mockRoom({
          ...room,
          participants: [...room.participants.values()],
          game: mockGame({
            ...room.game,
            residents: [...room.game.residents.values()],
            records: [...room.game.records.values()],
            status: 'COMPLETED'
          })
        }),
        PUBLISH.GAME.OVER,
        {
          teams: [
            {
              team: 'WEREWOLVES',
              terminateGame: true
            }
          ],
          records: [
            {
              days: 0,
              attack: [],
              corruption: [],
              divination: [],
              execution: null,
              protection: [],
              retaliation: [],
              victim: [],
              vote: []
            },
            {
              days: 1,
              attack: [],
              corruption: [],
              divination: [],
              execution: null,
              protection: [],
              retaliation: [],
              victim: [],
              vote: []
            }
          ]
        }
      );
      expect(spy7).toHaveBeenCalledOnce();
      expect(spy8).toHaveBeenCalledOnce();
      expect(spy8).toHaveBeenLastCalledWith(
        'room',
        mockRoom({
          ...room,
          participants: [...room.participants.values()],
          game: mockGame({
            ...room.game,
            residents: [...room.game.residents.values()],
            records: [...room.game.records.values()],
            status: 'COMPLETED'
          })
        })
      );
    });

    it('should return finish when the game finished: finish at next morning phase', async () => {
      const roomID = Room.ID.from('3cf1a574-e9c2-43b4-92f8-2ca84a53186a');
      const personID1 = Person.ID.from('ebcbec22-cde4-4564-8b9b-5802c9e03313');
      const personID2 = Person.ID.from('c5075ff6-8d3d-4594-8f1e-e3253db28fbf');
      const personID3 = Person.ID.from('ac6fd3d3-59d8-4ab8-b5cb-d6bce42c357c');
      const personID4 = Person.ID.from('81f5d38d-7fd6-4604-b552-656237a899d2');
      let room = mockRoom({
        id: roomID,
        game: mockGame({
          residents: [
            mockPerson({
              id: personID1,
              identity: Identity.VILLAGER,
              life: 'ALIVE'
            }),
            mockPerson({
              id: personID2,
              identity: Identity.VILLAGER,
              life: 'ALIVE'
            }),
            mockPerson({
              id: personID3,
              identity: Identity.WEREWOLF,
              life: 'ALIVE'
            }),
            mockPerson({
              id: personID4,
              identity: Identity.WEREWOLF,
              life: 'ALIVE'
            })
          ],
          records: [
            mockDailyOutcomeRecord({
              days: 0
            }),
            mockDailyOutcomeRecord({
              days: 1
            })
          ],
          dayPhase: mockDayPhase({
            days: 1,
            phase: 'MORNING'
          }),
          phaseDuration: mockPhaseDuration({
            morning: 1,
            day: 1,
            evening: 1,
            night: 1
          })
        })
      });

      let assertionPromise: Promise<void> = new Promise((reject) => setTimeout(() => reject(), 100));

      const uow: IUnitOfWork<AppTypeMap> = new MockUnitOfWork<AppTypeMap>();

      const spy1 = vi.spyOn(roomService, 'withTransaction').mockImplementation((_, proc) => {
        return proc({ room, uow });
      });
      const spy2 = vi
        .spyOn(startMorningPhase, 'execute')
        .mockImplementationOnce(() => right([])())
        .mockImplementationOnce(() =>
          right([
            {
              team: 'WEREWOLVES',
              terminateGame: true
            } satisfies Team
          ])()
        );
      const spy3 = vi.spyOn(endMorningPhase, 'execute').mockImplementation(() => right([])());
      const spy4 = vi.spyOn(startDayPhase, 'execute').mockImplementation(() => right([])());
      const spy5 = vi.spyOn(endDayPhase, 'execute').mockImplementation(() => right([])());
      const spy6 = vi.spyOn(startEveningPhase, 'execute').mockImplementation(() => right([])());
      const spy7 = vi.spyOn(publishService, 'broadcast').mockImplementation(() => right(null)());
      const spy8 = vi.spyOn(endEveningPhase, 'execute').mockImplementation(() => right([])());
      const spy9 = vi.spyOn(startNightPhase, 'execute').mockImplementation(() => right([])());
      const spy10 = vi.spyOn(endNightPhase, 'execute').mockImplementation(() => right([])());
      const spy11 = vi
        .spyOn(scheduler, 'startPhase')
        .mockImplementationOnce((r, _s, proc) => proc(r))
        .mockImplementationOnce((r, _s, proc) => proc(r))
        .mockImplementationOnce((r, _s, proc) => proc(r))
        .mockImplementationOnce((r, _s, proc) => proc(r))
        .mockImplementationOnce(async (r, _s, proc) => {
          const either = await proc(r);

          assertionPromise = Promise.resolve();

          return either;
        });
      const spy12 = vi.spyOn(uow, 'update').mockImplementation((_, r) => {
        room = r;
        return right(null)();
      });
      const spy13 = vi.spyOn(scheduler, 'stopPhase').mockImplementation(() => undefined);
      const spy14 = vi.spyOn(publishService, 'toEveryone').mockImplementation(() => right(null)());

      const either = await phaseTransition.execute(roomID);

      expect(isRight(either)).toBe(true);

      await assertionPromise;

      expect(spy1).toHaveBeenCalledTimes(10);
      expect(spy2).toHaveBeenCalledTimes(2);
      expect(spy3).toHaveBeenCalledOnce();
      expect(spy4).toHaveBeenCalledOnce();
      expect(spy5).toHaveBeenCalledOnce();
      expect(spy6).toHaveBeenCalledOnce();
      expect(spy7).toHaveBeenCalledOnce();
      expect(spy8).toHaveBeenCalledOnce();
      expect(spy9).toHaveBeenCalledOnce();
      expect(spy10).toHaveBeenCalledOnce();
      expect(spy11).toHaveBeenCalledTimes(4);
      expect(spy12).toHaveBeenCalledTimes(5);
      expect(spy12).toHaveBeenNthCalledWith(
        5,
        'room',
        mockRoom({
          ...room,
          participants: [...room.participants.values()],
          game: mockGame({
            ...room.game,
            residents: [...room.game.residents.values()],
            records: [...room.game.records.values()],
            status: 'COMPLETED'
          })
        })
      );
      expect(spy13).toHaveBeenCalledOnce();
      expect(spy14).toHaveBeenCalledOnce();
      expect(spy14).toHaveBeenCalledWith(
        mockRoom({
          ...room,
          participants: [...room.participants.values()],
          game: mockGame({
            ...room.game,
            residents: [...room.game.residents.values()],
            records: [...room.game.records.values()],
            status: 'COMPLETED'
          })
        }),
        PUBLISH.GAME.OVER,
        {
          teams: [
            {
              team: 'WEREWOLVES',
              terminateGame: true
            }
          ],
          records: [
            {
              days: 0,
              attack: [],
              corruption: [],
              divination: [],
              execution: null,
              protection: [],
              retaliation: [],
              victim: [],
              vote: []
            },
            {
              days: 1,
              attack: [],
              corruption: [],
              divination: [],
              execution: null,
              protection: [],
              retaliation: [],
              victim: [],
              vote: []
            }
          ]
        }
      );
    });
  });
});
