import { createEventPublishError } from '@/adapters/presenters/EventPublisher/EventPublishError.js';
import type { IPublishService } from '@/applications/services/interfaces/IPublishService.js';
import type { IRoomService } from '@/applications/services/interfaces/IRoomService.js';
import { MockPublishService } from '@/applications/services/mocks/MockPublishService.js';
import { MockRoomService } from '@/applications/services/mocks/MockRoomService.js';
import type { AppTypeMap } from '@/applications/unitOfWork/AppTypeMap.js';
import { MockUnitOfWork } from '@/applications/unitOfWork/mocks/MockUnitOfWork.js';
import { PUBLISH } from '@/applications/websocket/PublishEvents.js';
import { Identity } from '@/domains/Identity/Identity.js';
import { mockPerson } from '@/domains/Person/mocks/MockPerson.js';
import { Person } from '@/domains/Person/Person.js';
import { Persons } from '@/domains/Person/Persons.js';
import { mockGame } from '@/domains/Room/mocks/MockGame.js';
import { mockRoom } from '@/domains/Room/mocks/MockRoom.js';
import { Room } from '@/domains/Room/Room.js';
import type { ILogger } from '@/lib/Logger/ILogger.js';
import { MockLogger } from '@/lib/Logger/mocks/MockLogger.js';
import { isLeft, isRight } from 'fp-ts/lib/Either.js';
import { left, right } from 'fp-ts/lib/TaskEither.js';
import { StartNightPhase } from '../StartNightPhase.js';

describe('StartNightPhase', () => {
  let roomService: IRoomService;
  let publishService: IPublishService;
  let logger: ILogger;
  let startNightPhase: StartNightPhase;

  beforeEach(() => {
    roomService = new MockRoomService();
    publishService = new MockPublishService();
    logger = new MockLogger();
    startNightPhase = new StartNightPhase(roomService, publishService, logger);
  });

  afterEach(() => {
    vi.clearAllMocks();
  });

  describe('execute', () => {
    it('should return right', async () => {
      const roomID = Room.ID.from('8ae5f35e-0962-4a6d-bba5-790afed97905');
      const personID1 = Person.ID.from('ac68033c-2e01-4285-9685-5bb5e64315e7');
      const personID2 = Person.ID.from('b773013e-f1e3-415a-bf43-21954b1a77e6');
      const personID3 = Person.ID.from('9f08e3f6-2afe-4aa1-ae97-1536b118485c');
      const personID4 = Person.ID.from('d681d519-3113-4e15-bb77-60ae3cb221ce');
      const personID5 = Person.ID.from('4787f666-b275-4247-8a48-06ae1a123452');
      const personID6 = Person.ID.from('02f2e30f-9282-4e0b-955a-6db1ad311dce');
      const personID7 = Person.ID.from('c563371e-0828-452e-a604-334e3ea80d1d');
      const personID8 = Person.ID.from('3b4cb53c-b5a0-4702-bc3f-46a24982622d');
      const seer1 = mockPerson({
        id: personID1,
        life: 'ALIVE',
        identity: Identity.SEER
      });
      const seer2 = mockPerson({
        id: personID2,
        life: 'DEAD',
        identity: Identity.SEER
      });
      const knight1 = mockPerson({
        id: personID3,
        life: 'ALIVE',
        identity: Identity.KNIGHT
      });
      const knight2 = mockPerson({
        id: personID4,
        life: 'DEAD',
        identity: Identity.KNIGHT
      });
      const werewolf1 = mockPerson({
        id: personID5,
        life: 'ALIVE',
        identity: Identity.WEREWOLF
      });
      const werewolf2 = mockPerson({
        id: personID6,
        life: 'DEAD',
        identity: Identity.WEREWOLF
      });
      const room = mockRoom({
        id: roomID,
        game: mockGame({
          residents: [
            seer1,
            seer2,
            knight1,
            knight2,
            werewolf1,
            werewolf2,
            mockPerson({
              id: personID7,
              life: 'ALIVE',
              identity: Identity.VILLAGER
            }),
            mockPerson({
              id: personID8,
              life: 'DEAD',
              identity: Identity.VILLAGER
            })
          ]
        })
      });
      const uow = new MockUnitOfWork<AppTypeMap>();
      const events: Array<string> = [];
      let i = 0;

      const spy1 = vi.spyOn(roomService, 'withTransaction').mockImplementation((_, proc) => {
        return proc({ uow, room });
      });
      const spy2 = vi.spyOn(publishService, 'toEveryone').mockImplementation(() => right(null)());
      const spy3 = vi.spyOn(publishService, 'toPersons').mockImplementation((_, event) => {
        events[i] = event;
        i++;

        return right(null)();
      });

      const either = await startNightPhase.execute(roomID);

      expect(isRight(either)).toBe(true);
      expect(spy1).toHaveBeenCalledOnce();
      expect(spy2).toHaveBeenCalledOnce();
      expect(spy2).toHaveBeenCalledWith(room, PUBLISH.NIGHT.START, {
        seconds: room.game.phaseDuration.night
      });
      expect(spy3).toHaveBeenCalledTimes(3);

      let j = 0;

      for (const event of events) {
        j++;

        switch (event) {
          case PUBLISH.DIVINATION.PROMPT: {
            expect(spy3).toHaveBeenNthCalledWith(j, Persons.ofArray([seer1]), PUBLISH.DIVINATION.PROMPT, {
              candidates: [personID1, personID3, personID5, personID7]
            });

            continue;
          }
          case PUBLISH.PROTECTION.PROMPT: {
            expect(spy3).toHaveBeenNthCalledWith(j, Persons.ofArray([knight1]), PUBLISH.PROTECTION.PROMPT, {
              candidates: [personID1, personID3, personID5, personID7]
            });

            continue;
          }
          case PUBLISH.ATTACK.PROMPT: {
            expect(spy3).toHaveBeenNthCalledWith(j, Persons.ofArray([werewolf1]), PUBLISH.ATTACK.PROMPT, {
              candidates: [personID1, personID3, personID5, personID7]
            });

            continue;
          }
          default: {
            throw new Error('This should not happen');
          }
        }
      }
    });

    it('should return left when publishService.toEveryone() returns left', async () => {
      const roomID = Room.ID.from('8ae5f35e-0962-4a6d-bba5-790afed97905');
      const room = mockRoom({
        id: roomID
      });
      const uow = new MockUnitOfWork<AppTypeMap>();

      const spy1 = vi.spyOn(roomService, 'withTransaction').mockImplementation((_, proc) => {
        return proc({ uow, room });
      });
      const spy2 = vi.spyOn(publishService, 'toEveryone').mockImplementation(() => left(createEventPublishError('EmitFailed', ''))());
      const spy3 = vi.spyOn(publishService, 'toPersons');

      const either = await startNightPhase.execute(roomID);

      expect(isLeft(either)).toBe(true);
      expect(spy1).toHaveBeenCalledOnce();
      expect(spy2).toHaveBeenCalledOnce();
      expect(spy3).not.toHaveBeenCalled();
    });

    it('should return left when publishService.toPersons() returns left', async () => {
      const roomID = Room.ID.from('8ae5f35e-0962-4a6d-bba5-790afed97905');
      const room = mockRoom({
        id: roomID
      });
      const uow = new MockUnitOfWork<AppTypeMap>();

      const spy1 = vi.spyOn(roomService, 'withTransaction').mockImplementation((_, proc) => {
        return proc({ uow, room });
      });
      const spy2 = vi.spyOn(publishService, 'toEveryone').mockImplementation(() => right(null)());
      const spy3 = vi.spyOn(publishService, 'toPersons').mockImplementation(() => left(createEventPublishError('EmitFailed', ''))());

      const either = await startNightPhase.execute(roomID);

      expect(isLeft(either)).toBe(true);
      expect(spy1).toHaveBeenCalledOnce();
      expect(spy2).toHaveBeenCalledOnce();
      expect(spy2).toHaveBeenCalledWith(room, PUBLISH.NIGHT.START, {
        seconds: room.game.phaseDuration.night
      });
      expect(spy3).toHaveBeenCalledTimes(3);
    });
  });
});
