import type { IConnectionPool } from '@/applications/connection/IConnectionPool.js';
import { Types } from '@/containers/Types.js';
import { createClientError } from '@/domains/Client/ClientError.js';
import type { ILogger } from '@/lib/Logger/ILogger.js';
import type { Either } from 'fp-ts/lib/Either.js';
import { Do, bindW, fromOption, map } from 'fp-ts/lib/TaskEither.js';
import { pipe } from 'fp-ts/lib/function.js';
import { inject, injectable } from 'inversify';
import type { WebSocket } from 'ws';

/**
 * クライアントとの接続を切断します
 */
@injectable()
export class Disconnect {
  private readonly pool: IConnectionPool;
  private readonly logger: ILogger;

  public constructor(@inject(Types.WebSocketPool) pool: IConnectionPool, @inject(Types.Logger) logger: ILogger) {
    this.pool = pool;
    this.logger = logger;
  }

  public async execute(ws: WebSocket): Promise<Either<unknown, unknown>> {
    this.logger.info('Disconnect.execute()');

    return pipe(
      Do,
      bindW('id', () => {
        return pipe(
          this.pool.getClientID(ws),
          fromOption(() => createClientError('NoSuchClient', 'websocketに対応するクライアントが存在しません'))
        );
      }),
      map(({ id }) => this.pool.disconnect(id)),
      map(() => ws.close())
    )();
  }
}
