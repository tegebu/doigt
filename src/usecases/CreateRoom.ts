import type { GatewayError } from '@/adapters/gateways/GatewayError.js';
import type { IEventPublisher } from '@/adapters/presenters/EventPublisher/IEventPublisher.js';
import { PUBLISH } from '@/applications/websocket/PublishEvents.js';
import { Types } from '@/containers/Types.js';
import type { Client, ClientID } from '@/domains/Client/Client.js';
import type { ClientError } from '@/domains/Client/ClientError.js';
import { Clients } from '@/domains/Client/Clients.js';
import type { IClientRepository } from '@/domains/Client/IClientRepository.js';
import { Game } from '@/domains/Room/Game.js';
import type { IRoomRepository } from '@/domains/Room/IRoomRepository.js';
import { Room, type RoomList } from '@/domains/Room/Room.js';
import type { GenericError } from '@/lib/Error/Errors.js';
import type { ILogger } from '@/lib/Logger/ILogger.js';
import type { Nullable } from '@jamashita/anden/type';
import type { Either } from 'fp-ts/lib/Either.js';
import { pipe } from 'fp-ts/lib/function.js';
import { bindW, Do, flatMap, fromOption, left, map, orElseW, right, type TaskEither } from 'fp-ts/lib/TaskEither.js';
import { inject, injectable } from 'inversify';

/**
 * 部屋を作成します
 * TODO 各種オプション
 * [ ] 死亡時に役職が公開されるかどうか（する場合は霊媒師を除く）
 * [ ] 死亡者は役職を見られるかどうか
 * [ ] 騎士の護衛対象を連続で同じにできるか
 * [ ] 時間の管理
 * [ ] 初日予言
 * [ ] 決戦投票で結果が出ないときの決め方
 * [ ] 投票の票数を表示する
 * [ ] だれがどこに投票したかを表示する
 * [ ] 議論を打ち切りたい
 * [ ] 01:00ごとに時間調整 & 通知
 */
@injectable()
export class CreateRoom {
  private readonly clientRepository: IClientRepository;
  private readonly roomRepository: IRoomRepository;
  private readonly eventPublisher: IEventPublisher;
  private readonly logger: ILogger;

  public constructor(
    @inject(Types.CacheClientRepository) clientRepository: IClientRepository,
    @inject(Types.CacheRoomRepository) roomRepository: IRoomRepository,
    @inject(Types.WSEventPublisher) eventPublisher: IEventPublisher,
    @inject(Types.Logger) logger: ILogger
  ) {
    this.clientRepository = clientRepository;
    this.roomRepository = roomRepository;
    this.eventPublisher = eventPublisher;
    this.logger = logger;
  }

  public async execute(
    name: string,
    password: Nullable<string>,
    quota: number,
    hostID: ClientID,
    needsModerator: boolean
  ): Promise<Either<unknown, unknown>> {
    this.logger.info('CreateRoom.execute()');

    return pipe(
      Do,
      bindW('host', () => this.findClient(hostID)),
      bindW('room', ({ host }) => right(this.forgeRoom(name, password, quota, host, needsModerator))),
      bindW(
        'res1',
        ({ room }) =>
          () =>
            this.roomRepository.create(room)
      ),
      bindW(
        'res2',
        ({ room }) =>
          () =>
            this.eventPublisher.toClient(hostID, PUBLISH.ROOM.CREATE.SUCCESS, {
              roomID: room.id
            })
      ),
      bindW(
        'res3',
        ({ room }) =>
          () =>
            this.eventPublisher.broadcast(PUBLISH.ROOM.NOTIFY.CREATED, {
              room: {
                id: room.id,
                name: room.name,
                moderator: room.moderator,
                quota: room.quota,
                participants: room.participants.size,
                requirePassword: Room.hasPassword(room),
                status: 'OPEN'
              } satisfies RoomList
            })
      ),
      orElseW((e) => {
        return pipe(
          Do,
          bindW('res1', () => () => this.eventPublisher.toClient(hostID, PUBLISH.ROOM.CREATE.FAILURE, {})),
          flatMap(() => left(e))
        );
      })
    )();
  }

  private findClient(id: ClientID): TaskEither<GenericError | GatewayError | ClientError, Client> {
    return pipe(
      Do,
      bindW('option', () => () => this.clientRepository.find(id)),
      bindW('host', ({ option }) => {
        return pipe(
          option,
          fromOption(() => {
            return {
              error: 'ClientError',
              detail: 'NoSuchClient',
              message: `クライアントが見つかりません: ${id}`
            } as ClientError;
          })
        );
      }),
      map(({ host }) => host)
    );
  }

  private forgeRoom(name: string, password: Nullable<string>, quota: number, host: Client, needsModerator: boolean): Room {
    if (needsModerator) {
      return {
        id: Room.ID.generate(),
        name,
        password,
        moderator: host,
        quota,
        participants: Clients.empty(),
        game: Game.init()
      } satisfies Room;
    }

    return {
      id: Room.ID.generate(),
      name,
      password,
      moderator: null,
      participants: Clients.ofArray([host]),
      quota,
      game: Game.init()
    } satisfies Room;
  }
}
