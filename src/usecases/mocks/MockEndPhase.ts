import type { Team } from '@/domains/Identity/Team.js';
import { UnimplementedError } from '@jamashita/anden/error';
import type { Either } from 'fp-ts/lib/Either.js';
import type { IEndPhase } from '../interfaces/IEndPhase.js';

export class MockEndPhase implements IEndPhase {
  public execute(): Promise<Either<unknown, Array<Team>>> {
    throw new UnimplementedError();
  }
}
