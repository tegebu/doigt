import { UnimplementedError } from '@jamashita/anden/error';
import type { Either } from 'fp-ts/lib/Either.js';
import type { IPublishOutcome } from '../interfaces/IPublishOutcome.js';

export class MockPublishOutcome implements IPublishOutcome {
  public execute(): Promise<Either<unknown, unknown>> {
    throw new UnimplementedError();
  }
}
