import type { Team } from '@/domains/Identity/Team.js';
import { UnimplementedError } from '@jamashita/anden/error';
import type { Either } from 'fp-ts/lib/Either.js';
import type { IStartPhase } from '../interfaces/IStartPhase.js';

export class MockStartPhase implements IStartPhase {
  public execute(): Promise<Either<unknown, Array<Team>>> {
    throw new UnimplementedError();
  }
}
