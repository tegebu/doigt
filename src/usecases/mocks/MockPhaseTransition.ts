import type { GatewayError } from '@/adapters/gateways/GatewayError.js';
import type { GenericError } from '@/lib/Error/Errors.js';
import { UnimplementedError } from '@jamashita/anden/error';
import type { Either } from 'fp-ts/lib/Either.js';
import type { IPhaseTransition } from '../interfaces/IPhaseTransition.js';

export class MockPhaseTransition implements IPhaseTransition {
  public execute(): Promise<Either<GenericError | GatewayError, unknown>> {
    throw new UnimplementedError();
  }
}
