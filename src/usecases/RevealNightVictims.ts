import type { EventPublishError } from '@/adapters/presenters/EventPublisher/EventPublishError.js';
import type { IPublishService } from '@/applications/services/interfaces/IPublishService.js';
import type { IRoomService } from '@/applications/services/interfaces/IRoomService.js';
import { PUBLISH } from '@/applications/websocket/PublishEvents.js';
import { Types } from '@/containers/Types.js';
import type { ClientError } from '@/domains/Client/ClientError.js';
import { Identity } from '@/domains/Identity/Identity.js';
import type { VictimOutcome } from '@/domains/Outcome/VictimOutcome.js';
import type { VoteOutcome } from '@/domains/Outcome/VoteOutcome.js';
import { VoteOutcomes } from '@/domains/Outcome/VoteOutcomes.js';
import { Person, type PersonID } from '@/domains/Person/Person.js';
import type { PersonError } from '@/domains/Person/PersonError.js';
import { Persons } from '@/domains/Person/Persons.js';
import { DailyOutcomeRecord } from '@/domains/Room/DailyOutcomeRecord.js';
import { Game } from '@/domains/Room/Game.js';
import { Room, type RoomID } from '@/domains/Room/Room.js';
import type { GenericError } from '@/lib/Error/Errors.js';
import type { ILogger } from '@/lib/Logger/ILogger.js';
import { Kind } from '@jamashita/anden/type';
import type { Either } from 'fp-ts/lib/Either.js';
import { pipe } from 'fp-ts/lib/function.js';
import { bind as oBind, Do as oDo, foldW as oFoldW, isSome, map as oMap, none, type Option, type Some, some } from 'fp-ts/lib/Option.js';
import { apSW, bindW as teBindW, Do as teDo, fromEither, fromOption, map as teMap, right, type TaskEither } from 'fp-ts/lib/TaskEither.js';
import { inject, injectable } from 'inversify';
import type { IPublishOutcome } from './interfaces/IPublishOutcome.js';

/**
 * 夜の犠牲者を公開します
 *
 * 1. 人狼の被害者
 * 2. 人狼が猟師を襲撃した場合、報復を受ける人狼
 * 3. 予言者が妖狐を予言した場合の妖狐
 * 4. TODO 臆病者の避難先が襲撃された場合、臆病者
 */
@injectable()
export class RevealNightVictims implements IPublishOutcome {
  private readonly roomService: IRoomService;
  private readonly publishService: IPublishService;
  private readonly logger: ILogger;

  public constructor(
    @inject(Types.RoomService) roomService: IRoomService,
    @inject(Types.PublishService) publishService: IPublishService,
    @inject(Types.Logger) logger: ILogger
  ) {
    this.roomService = roomService;
    this.publishService = publishService;
    this.logger = logger;
  }

  /**
   * 襲撃の結果をランダム抽選します
   *
   * @param room
   * @param outcomes
   */
  private chooseVictim(room: Room, outcomes: VoteOutcomes): Option<Person> {
    if (outcomes.length === 0) {
      return none;
    }

    const map1 = new Map<PersonID, number>();

    for (const outcome of outcomes) {
      const val = map1.get(outcome.destination) ?? 0;

      map1.set(outcome.destination, val + 1);
    }

    // numberは頻出度
    const map2 = new Map<number, Array<PersonID>>();

    for (const [personID, frequency] of map1) {
      const ids = map2.get(frequency);

      if (Kind.isUndefined(ids)) {
        map2.set(frequency, [personID]);

        continue;
      }

      ids.push(personID);
    }

    // 得票順に並び替え
    const sorted = [...map2.entries()].sort((a, b) => {
      return b[0] - a[0];
    });

    return (
      sorted
        .map(([, ids]) => {
          return Game.randomResident(room.game, ids);
        })
        .find((option: Option<Person>): option is Some<Person> => {
          return isSome(option);
        }) ?? none
    );
  }

  public execute(id: RoomID): Promise<Either<unknown, unknown>> {
    this.logger.info('RevealWolfVictim.execute()');

    return this.roomService.withTransaction(id, ({ room, uow }) => {
      return pipe(
        teDo,
        apSW('victims1', this.getAttackVictims(room)),
        apSW('victims2', this.getDivinationVictims(room)),
        teBindW('res1', ({ victims1, victims2 }) => {
          const victims = [...victims1, ...victims2];
          const victimIDs = victims.map((victim: Person) => {
            return victim.id;
          });
          const outcome = {
            id: victimIDs
          } satisfies VictimOutcome;
          const deceased = victims.map((victim: Person) => {
            return Person.die(victim);
          });

          return pipe(
            teDo,
            teBindW('newRoom', () => {
              return pipe(
                teDo,
                teBindW('newRoom', () => fromEither(Room.addVictim(room, room.game.dayPhase.days, Persons.ofArray(victims)))),
                teMap(({ newRoom }) => Room.exchangePersons(newRoom, Persons.ofArray(deceased)))
              );
            }),
            teBindW(
              'res1',
              ({ newRoom }) =>
                () =>
                  uow.update('room', newRoom)
            ),
            teBindW('res2', ({ newRoom }) => this.publish(newRoom, outcome))
          );
        })
      )();
    });
  }

  /**
   * 人狼の襲撃による被害者を取得します
   *
   * @param room
   */
  private getAttackVictims(room: Room): TaskEither<GenericError | PersonError, Array<Person>> {
    return pipe(
      teDo,
      teBindW('option', () => this.getWerewolfVictim(room)),
      teBindW('victims', ({ option }) => {
        return pipe(
          option,
          oFoldW(
            () => right([]),
            (victim) => {
              return pipe(
                teDo,
                teBindW('knights', () => this.getKnight(room.game)),
                teBindW('outcomes', ({ knights }) => this.getLastKnightVote(room, knights)),
                teBindW('victims', ({ outcomes }) => {
                  const succeeded = outcomes.some((outcome: VoteOutcome) => {
                    return outcome.destination === victim.id;
                  });

                  // 護衛に成功
                  if (succeeded) {
                    return right([]);
                  }
                  switch (victim.identity.role) {
                    // 襲撃先が妖狐の場合、失敗する
                    case 'KITSUNE': {
                      return right([]);
                    }
                    // 護衛に失敗した状態で、被害者が猟師である場合
                    case 'HUNTER': {
                      return pipe(
                        teDo,
                        teBindW('werewolves', () => this.getWerewolf(room.game)),
                        teBindW('victims', ({ werewolves }) => {
                          // 人狼が残り1名の場合は報復をしない
                          if (werewolves.size === 1) {
                            return right([victim]);
                          }

                          // 生存している人狼から1名ランダムに選ぶ
                          const werewolfIDs = [...werewolves.values()].map((werewolf: Person) => {
                            return werewolf.id;
                          });
                          const option = Game.randomResident(room.game, werewolfIDs);

                          if (isSome(option)) {
                            return right([victim, option.value]);
                          }

                          return right([victim]);
                        }),
                        teMap(({ victims }) => victims)
                      );
                    }
                    default: {
                      return right([victim]);
                    }
                  }
                }),
                teMap(({ victims }) => victims)
              );
            }
          )
        );
      }),
      teMap(({ victims }) => victims)
    );
  }

  /**
   * 予言による被害者を取得します
   *
   * @param room
   */
  private getDivinationVictims(room: Room): TaskEither<GenericError | PersonError, Array<Person>> {
    return pipe(
      teDo,
      teBindW('seers', () => this.getSeer(room.game)),
      teBindW('outcomes', ({ seers }) => this.getLastSeerVote(room, seers)),
      teBindW('kitsunes', ({ outcomes }) => {
        const kitsunes = outcomes
          .map((outcome) => {
            return Room.getResident(room, outcome.destination);
          })
          .filter((option: Option<Person>): option is Some<Person> => {
            if (isSome(option)) {
              return option.value.identity === Identity.KITSUNE;
            }

            return false;
          })
          .map((some: Some<Person>) => {
            return some.value;
          });

        return right(kitsunes);
      }),
      teMap(({ kitsunes }) => kitsunes)
    );
  }

  /**
   * 生存している騎士を取得します
   *
   * @param game
   */
  private getKnight(game: Game): TaskEither<PersonError, Persons> {
    const knights = Persons.getByRole(Game.getSurvivors(game), 'KNIGHT');

    return right(Persons.from(knights));
  }

  /**
   * 夜の時間の護衛の投票を取得します
   *
   * @param room
   * @param knights
   */
  private getLastKnightVote(room: Room, knights: Persons): TaskEither<GenericError, VoteOutcomes> {
    return pipe(
      oDo,
      // まだその日の記録を作成してないのでこれで昨日の記録を取ることができる
      oBind('record', () => Room.getLatestRecord(room)),
      oBind('outcomes', ({ record }) => {
        const seq = DailyOutcomeRecord.getProtection(record).filter((outcome) => {
          return knights.has(outcome.source);
        });

        return some(VoteOutcomes.from(seq));
      }),
      oMap(({ outcomes }) => outcomes),
      fromOption(() => {
        return {
          error: 'RuntimeError',
          message: '護衛の結果が存在しません'
        } satisfies GenericError;
      })
    );
  }

  /**
   * 夜の時間の予言の投票を取得します
   *
   * @param room
   * @param seers
   */
  private getLastSeerVote(room: Room, seers: Persons): TaskEither<GenericError, VoteOutcomes> {
    return pipe(
      oDo,
      // まだその日の記録を作成してないのでこれで昨日の記録を取ることができる
      oBind('record', () => Room.getLatestRecord(room)),
      oBind('outcomes', ({ record }) => {
        const seq = DailyOutcomeRecord.getDivination(record).filter((outcome) => {
          return seers.has(outcome.source);
        });

        return some(VoteOutcomes.from(seq));
      }),
      oMap(({ outcomes }) => outcomes),
      fromOption(() => {
        return {
          error: 'RuntimeError',
          message: '予言の結果が存在しません'
        } satisfies GenericError;
      })
    );
  }

  /**
   * 夜の時間の襲撃の投票を取得します
   *
   * @param room
   * @param werewolves
   */
  private getLastWerewolfVote(room: Room, werewolves: Persons): TaskEither<GenericError, VoteOutcomes> {
    return pipe(
      oDo,
      // まだその日の記録を作成してないのでこれで昨日の記録を取ることができる
      oBind('record', () => Room.getLatestRecord(room)),
      oBind('outcomes', ({ record }) => {
        const seq = DailyOutcomeRecord.getAttack(record).filter((outcome) => {
          return werewolves.has(outcome.source);
        });

        return some(VoteOutcomes.from(seq));
      }),
      oMap(({ outcomes }) => outcomes),
      fromOption(() => {
        return {
          error: 'RuntimeError',
          message: '襲撃の結果が存在しません'
        } satisfies GenericError;
      })
    );
  }

  /**
   * 生存している予言者を取得します
   *
   * @param game
   */
  private getSeer(game: Game): TaskEither<PersonError, Persons> {
    const seers = Persons.getByRole(Game.getSurvivors(game), 'SEER');

    return right(Persons.from(seers));
  }

  /**
   * 生存している人狼を取得します
   *
   * @param game
   */
  private getWerewolf(game: Game): TaskEither<PersonError, Persons> {
    const werewolves = Persons.getByRole(Game.getSurvivors(game), 'WEREWOLF');

    return right(Persons.from(werewolves));
  }

  /**
   * 人狼の襲撃による被害者を取得します
   *
   * @param room
   */
  private getWerewolfVictim(room: Room): TaskEither<GenericError | PersonError, Option<Person>> {
    return pipe(
      teDo,
      teBindW('werewolves', () => this.getWerewolf(room.game)),
      teBindW('outcomes', ({ werewolves }) => this.getLastWerewolfVote(room, werewolves)),
      teBindW('victim', ({ outcomes }) => {
        return pipe(
          oDo,
          oBind('victim', () => this.chooseVictim(room, outcomes)),
          oMap(({ victim }) => victim),
          right
        );
      }),
      teMap(({ victim }) => victim)
    );
  }

  /**
   * 全員に通知します
   *
   * @param room
   * @param outcome
   */
  private publish(room: Room, outcome: VictimOutcome): TaskEither<GenericError | EventPublishError | ClientError, unknown> {
    return () =>
      this.publishService.toEveryone(room, PUBLISH.VICTIMIZED, {
        outcome
      });
  }
}
