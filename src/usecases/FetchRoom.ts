import type { IEventPublisher } from '@/adapters/presenters/EventPublisher/IEventPublisher.js';
import { PUBLISH } from '@/applications/websocket/PublishEvents.js';
import { Types } from '@/containers/Types.js';
import type { ClientID } from '@/domains/Client/Client.js';
import type { IRoomRepository } from '@/domains/Room/IRoomRepository.js';
import type { RoomID } from '@/domains/Room/Room.js';
import { createRoomError } from '@/domains/Room/RoomError.js';
import type { ILogger } from '@/lib/Logger/ILogger.js';
import type { Either } from 'fp-ts/lib/Either.js';
import { pipe } from 'fp-ts/lib/function.js';
import { bindW, Do, fromOption, map } from 'fp-ts/lib/TaskEither.js';
import { inject, injectable } from 'inversify';

@injectable()
export class FetchRoom {
  private readonly roomRepository: IRoomRepository;
  private readonly eventPublisher: IEventPublisher;
  private readonly logger: ILogger;

  public constructor(
    @inject(Types.CacheRoomRepository) roomRepository: IRoomRepository,
    @inject(Types.WSEventPublisher) eventPublisher: IEventPublisher,
    @inject(Types.Logger) logger: ILogger
  ) {
    this.roomRepository = roomRepository;
    this.eventPublisher = eventPublisher;
    this.logger = logger;
  }

  public execute(clientID: ClientID, roomID: RoomID): Promise<Either<unknown, unknown>> {
    this.logger.info('FetchRoom.execute()');

    return pipe(
      Do,
      bindW('room', () => {
        return pipe(
          Do,
          bindW('option', () => () => this.roomRepository.find(roomID)),
          bindW('room', ({ option }) => {
            return pipe(
              option,
              fromOption(() => {
                this.logger.error(`NO SUCH ROOM: ${roomID}`);

                return createRoomError('NoSuchRoom', `該当する部屋が存在しません: ${roomID}`);
              })
            );
          }),
          map(({ room }) => room)
        );
      }),
      bindW(
        'res1',
        ({ room }) =>
          () =>
            this.eventPublisher.toClient(clientID, PUBLISH.ROOM.FETCH, {
              room
            })
      )
    )();
  }
}
