import type { EventPublishError } from '@/adapters/presenters/EventPublisher/EventPublishError.js';
import type { RoleAssignmentRequest } from '@/applications/dtos/Role/RoleAssignmentRequest.js';
import type { IScheduler } from '@/applications/scheduler/IScheduler.js';
import type { IPublishService } from '@/applications/services/interfaces/IPublishService.js';
import type { IRoleAssignmentService } from '@/applications/services/interfaces/IRoleAssignmentService.js';
import type { IRoomService } from '@/applications/services/interfaces/IRoomService.js';
import { PUBLISH } from '@/applications/websocket/PublishEvents.js';
import { Types } from '@/containers/Types.js';
import type { ClientError } from '@/domains/Client/ClientError.js';
import type { InformantOutcome } from '@/domains/Outcome/InformantOutcome.js';
import type { Person, PersonBase, PersonID } from '@/domains/Person/Person.js';
import { Persons } from '@/domains/Person/Persons.js';
import { RoleAssignment } from '@/domains/Role/RoleAssignment.js';
import type { RoleError } from '@/domains/Role/RoleError.js';
import { Game } from '@/domains/Room/Game.js';
import { Room, type RoomID } from '@/domains/Room/Room.js';
import { createRoomError, type RoomError } from '@/domains/Room/RoomError.js';
import { Arrays } from '@/lib/Collection/Array/Arrays.js';
import type { GenericError } from '@/lib/Error/Errors.js';
import type { ILogger } from '@/lib/Logger/ILogger.js';
import { Kind } from '@jamashita/anden/type';
import { bindW as eBindW, Do as eDo, type Either, isRight, left as l, map as eMap, right as r } from 'fp-ts/lib/Either.js';
import { pipe } from 'fp-ts/lib/function.js';
import { apSW, bindW as teBindW, Do as teDo, fromEither, right, sequenceArray, type TaskEither } from 'fp-ts/lib/TaskEither.js';
import { inject, injectable } from 'inversify';
import type { IPhaseTransition } from './interfaces/IPhaseTransition.js';

/**
 * ゲームを開始します
 */
@injectable()
export class StartGame {
  private readonly roomService: IRoomService;
  private readonly publishService: IPublishService;
  private readonly roleAssignmentService: IRoleAssignmentService;
  private readonly scheduler: IScheduler;
  private readonly phaseTransition: IPhaseTransition;
  private readonly logger: ILogger;

  constructor(
    @inject(Types.RoomService) roomService: IRoomService,
    @inject(Types.PublishService) publishService: IPublishService,
    @inject(Types.RoleAssignmentService) roleAssignmentService: IRoleAssignmentService,
    @inject(Types.GameScheduler) scheduler: IScheduler,
    @inject(Types.PhaseTransition) phaseTransition: IPhaseTransition,
    @inject(Types.Logger) logger: ILogger
  ) {
    this.roomService = roomService;
    this.publishService = publishService;
    this.roleAssignmentService = roleAssignmentService;
    this.scheduler = scheduler;
    this.phaseTransition = phaseTransition;
    this.logger = logger;
  }

  /**
   * 役割を割り当てます
   *
   * @param room
   */
  private assignRole(room: Room): TaskEither<GenericError | EventPublishError | ClientError, unknown> {
    const tes = [...room.game.residents.values()].map((resident) => {
      return () =>
        this.publishService.toPerson(resident, PUBLISH.ROLE.ASSIGNED, {
          you: resident,
          others: this.getOthers(resident, room.game.residents)
        });
    });

    return sequenceArray([...tes.values()]);
  }

  public execute(id: RoomID, request: RoleAssignmentRequest): Promise<Either<unknown, unknown>> {
    this.logger.info('StartGame.execute()');

    return pipe(
      teDo,
      teBindW(
        'res1',
        () => () =>
          this.roomService.withTransaction(id, ({ room, uow }) => {
            return pipe(
              teDo,
              teBindW('newRoom', () => this.startGame(room, request)),
              teBindW(
                'res1',
                ({ newRoom }) =>
                  () =>
                    uow.update('room', newRoom)
              ),
              teBindW(
                'res2',
                ({ newRoom }) =>
                  () =>
                    this.publishService.toEveryone(newRoom, PUBLISH.GAME.START, {})
              ),
              teBindW(
                'res3',
                ({ newRoom }) =>
                  () =>
                    this.publishService.broadcast(PUBLISH.ROOM.NOTIFY.STATUS_CHANGED, {
                      roomID: newRoom.id,
                      status: 'IN_PROGRESS'
                    })
              ),
              teBindW('res4', ({ newRoom }) => {
                return pipe(
                  teDo,
                  apSW('res1', this.assignRole(newRoom)),
                  apSW('res2', this.notifyAllResidents(newRoom)),
                  teBindW('res3', () => this.publishWerewolvesToDecivers(newRoom)),
                  teBindW('res4', () => {
                    this.scheduler.startPhase(id, Room.getDuration(room), (roomID) => this.phaseTransition.execute(roomID));

                    return right(null);
                  })
                );
              })
            )();
          })
      )
    )();
  }

  /**
   * 生存している内通者を取得します
   *
   * @param game
   */
  private getInformant(game: Game): Persons {
    return Persons.getByRole(Game.getSurvivors(game), 'INFORMANT');
  }

  /**
   * 自分以外の住人を取得します
   *
   * @param me
   * @param residents
   */
  private getOthers(me: Person, residents: Persons): Array<PersonBase> {
    const others = new Map<PersonID, Person>();

    for (const resident of residents.values()) {
      if (resident.id !== me.id) {
        others.set(resident.id, resident);
      }
    }

    return [...others.values()].map((resident) => {
      const { identity, ...rest } = resident;

      return rest satisfies PersonBase;
    });
  }

  /**
   * 生存している予言者を取得します
   *
   * @param game
   */
  private getSeer(game: Game): Persons {
    return Persons.getByRole(Game.getSurvivors(game), 'SEER');
  }

  /**
   * 生存している人狼を取得します
   *
   * @param game
   */
  private getWerewolf(game: Game): Persons {
    return Persons.getByRole(Game.getSurvivors(game), 'WEREWOLF');
  }

  /**
   * moderatorにすべての住人を通知します
   *
   * @param room
   */
  private notifyAllResidents(room: Room): TaskEither<GenericError | EventPublishError | ClientError, unknown> {
    return () =>
      this.publishService.toModerator(room, PUBLISH.ROLE.REVEAL_ALL, {
        residents: [...room.game.residents.values()]
      });
  }

  private prepareGame(room: Room, assignment: RoleAssignment): Either<GenericError | RoomError, Room> {
    return pipe(
      eDo,
      eBindW('newRoom1', () => Room.startGame(room, assignment)),
      eBindW('newRoom2', ({ newRoom1 }) => this.setInnocentRandomly(newRoom1)),
      eMap(({ newRoom2 }) => newRoom2)
    );
  }

  private publishWerewolvesToDecivers(room: Room): TaskEither<GenericError | EventPublishError | ClientError, unknown> {
    const werewolves = this.getWerewolf(room.game);
    const informants = this.getInformant(room.game);
    const ids = [...werewolves.values()].map((werewolf) => {
      return werewolf.id;
    });
    const outcome = {
      ids
    } satisfies InformantOutcome;

    const tes = [...informants.values()].map((informant) => {
      return () =>
        this.publishService.toPerson(informant, PUBLISH.ACKNOWLEDGE.OUTCOME, {
          outcome
        });
    });

    return sequenceArray(tes);
  }

  /**
   * 人狼ではない住人をランダムに設定します
   *
   * @param room
   */
  private setInnocentRandomly(room: Room): Either<GenericError | RoomError, Room> {
    const newRoom = Room.createRecord(room);

    return pipe(
      eDo,
      eBindW('innocent', () => {
        const innocents = Persons.filter(newRoom.game.residents, (resident: Person) => {
          return Game.divine(resident) === 'VILLAGER';
        });
        const innocent = Arrays.shuffle([...innocents.values()])[0];

        if (Kind.isUndefined(innocent)) {
          return l(createRoomError('NoInnocent', '人狼でない住人がいません'));
        }

        return r(innocent);
      }),
      eBindW('newRoom', ({ innocent }) => {
        const seers = this.getSeer(newRoom.game);

        return [...seers.values()].reduce(
          (prev, curr) => {
            if (isRight(prev)) {
              return Room.addDivination(prev.right, 0, curr, innocent);
            }

            return prev;
          },
          r(newRoom) satisfies Either<GenericError, Room>
        );
      }),
      eMap(({ newRoom }) => newRoom)
    );
  }

  private startGame(room: Room, request: RoleAssignmentRequest): TaskEither<GenericError | RoomError | RoleError, Room> {
    return pipe(
      eDo,
      eBindW('assignment', () => this.roleAssignmentService.assign(request, room.quota)),
      eBindW('res1', ({ assignment }) => RoleAssignment.validate(assignment)),
      eBindW('room', ({ assignment }) => this.prepareGame(room, assignment)),
      eMap(({ room }) => room),
      fromEither
    );
  }
}
