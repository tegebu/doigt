import type { EventPublishError } from '@/adapters/presenters/EventPublisher/EventPublishError.js';
import type { IPublishService } from '@/applications/services/interfaces/IPublishService.js';
import type { IRoomService } from '@/applications/services/interfaces/IRoomService.js';
import { PUBLISH } from '@/applications/websocket/PublishEvents.js';
import { Types } from '@/containers/Types.js';
import type { ClientError } from '@/domains/Client/ClientError.js';
import type { Team } from '@/domains/Identity/Team.js';
import type { VoteOutcomes } from '@/domains/Outcome/VoteOutcomes.js';
import { Person, type PersonID } from '@/domains/Person/Person.js';
import { Persons } from '@/domains/Person/Persons.js';
import { DailyOutcomeRecord } from '@/domains/Room/DailyOutcomeRecord.js';
import { Game } from '@/domains/Room/Game.js';
import { Room, type RoomID } from '@/domains/Room/Room.js';
import { createRoomError } from '@/domains/Room/RoomError.js';
import { Arrays } from '@/lib/Collection/Array/Arrays.js';
import type { GenericError } from '@/lib/Error/Errors.js';
import type { ILogger } from '@/lib/Logger/ILogger.js';
import { Kind } from '@jamashita/anden/type';
import type { Either } from 'fp-ts/lib/Either.js';
import { pipe } from 'fp-ts/lib/function.js';
import { bindW, Do, fromOption, map, right, type TaskEither } from 'fp-ts/lib/TaskEither.js';
import { inject, injectable } from 'inversify';
import type { IEndPhase } from './interfaces/IEndPhase.js';

/**
 * 夕の時間を終了します
 * TODO 投票内容が見えるオプション
 * TODO 現状決選投票機能がない
 * TODO 再投票
 * TODO 決戦投票
 */
@injectable()
export class EndEveningPhase implements IEndPhase {
  private readonly roomService: IRoomService;
  private readonly publishService: IPublishService;
  private readonly logger: ILogger;

  public constructor(
    @inject(Types.RoomService) roomService: IRoomService,
    @inject(Types.PublishService) publishService: IPublishService,
    @inject(Types.Logger) logger: ILogger
  ) {
    this.roomService = roomService;
    this.publishService = publishService;
    this.logger = logger;
  }

  /**
   * 最多得票者を決定します
   */
  private determineExecutionResult(votes: VoteOutcomes, residents: Persons): Persons {
    const map = new Map<PersonID, number>();
    const candidates = new Map<PersonID, Person>();

    for (const [, person] of residents) {
      candidates.set(person.id, person);
    }
    for (const vote of votes) {
      const count = map.get(vote.destination) ?? 0;

      map.set(vote.destination, count + 1);
    }

    // 最多得票者を取得する
    const voteResults = Array.from(map.entries());
    // 票数が同じ可能性がある
    let ids: Array<PersonID> = [];
    let max = -1;

    for (const [personID, voteCount] of voteResults) {
      if (voteCount > max) {
        max = voteCount;
        ids = [personID];

        continue;
      }
      if (voteCount === max) {
        ids.push(personID);
      }
    }

    const persons = ids
      .map((id) => {
        return candidates.get(id);
      })
      .filter((person): person is Person => {
        return !Kind.isUndefined(person);
      });

    return Persons.ofArray(persons);
  }

  public async execute(id: RoomID): Promise<Either<unknown, Array<Team>>> {
    this.logger.info('EndEveningPhase.execute()');

    return this.roomService.withTransaction(id, ({ room, uow }) => {
      return pipe(
        Do,
        bindW('record', () => {
          return pipe(
            Room.getLatestRecord(room),
            fromOption(() => {
              this.logger.error('Room.getLatestRecord() RETURNED none');

              return createRoomError('NoSuchRecord', '最新の記録が存在しません。');
            })
          );
        }),
        bindW('newRoom', ({ record }) => {
          const votes = DailyOutcomeRecord.getVote(record);
          const candidates = this.determineExecutionResult(votes, room.game.residents);
          const candidate = Arrays.shuffle([...candidates.values()])[0];

          if (Kind.isUndefined(candidate)) {
            return this.publishNoLynch(room);
          }

          return this.publishLynchVictim(room, candidate);
        }),
        bindW('winners', ({ newRoom }) => right(Game.getWinners(newRoom.game))),
        bindW(
          'res2',
          ({ newRoom }) =>
            () =>
              uow.update('room', newRoom)
        ),
        map(({ winners }) => winners)
      )();
    });
  }

  public publishLynchVictim(room: Room, victim: Person): TaskEither<GenericError | EventPublishError | ClientError, Room> {
    return pipe(
      Do,
      bindW(
        'res1',
        () => () =>
          this.publishService.toEveryone(room, PUBLISH.LYNCHED.SOME, {
            victimID: victim.id
          })
      ),
      bindW('newRoom', () => {
        const v = Person.die(victim);
        const newRoom = Room.exchangePerson(room, v);

        return right(newRoom);
      }),
      bindW('res2', () => () => this.publishService.toEveryone(room, PUBLISH.EVENING.END, {})),
      map(({ newRoom }) => newRoom)
    );
  }

  private publishNoLynch(room: Room): TaskEither<GenericError | EventPublishError | ClientError, Room> {
    return pipe(
      Do,
      bindW('res1', () => () => this.publishService.toEveryone(room, PUBLISH.LYNCHED.NONE, {})),
      map(() => room)
    );
  }
}
