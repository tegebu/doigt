import type { IEventPublisher } from '@/adapters/presenters/EventPublisher/IEventPublisher.js';
import type { IConnectionPool } from '@/applications/connection/IConnectionPool.js';
import { PUBLISH } from '@/applications/websocket/PublishEvents.js';
import { Types } from '@/containers/Types.js';
import type { Client } from '@/domains/Client/Client.js';
import type { IClientRepository } from '@/domains/Client/IClientRepository.js';
import type { ILogger } from '@/lib/Logger/ILogger.js';
import type { Either } from 'fp-ts/lib/Either.js';
import { Do, bindW, flatMap, left, map, orElseW } from 'fp-ts/lib/TaskEither.js';
import { pipe } from 'fp-ts/lib/function.js';
import { inject, injectable } from 'inversify';
import type { WebSocket } from 'ws';

/**
 * クライアントとの接続を確立します
 */
@injectable()
export class Connect {
  private readonly clientRepository: IClientRepository;
  private readonly pool: IConnectionPool;
  private readonly eventPublisher: IEventPublisher;
  private readonly logger: ILogger;

  public constructor(
    @inject(Types.CacheClientRepository) clientRepository: IClientRepository,
    @inject(Types.WebSocketPool) pool: IConnectionPool,
    @inject(Types.WSEventPublisher) eventPublisher: IEventPublisher,
    @inject(Types.Logger) logger: ILogger
  ) {
    this.clientRepository = clientRepository;
    this.pool = pool;
    this.eventPublisher = eventPublisher;
    this.logger = logger;
  }

  public async execute(client: Client, ws: WebSocket): Promise<Either<unknown, unknown>> {
    this.logger.info('Connect.execute()');

    return pipe(
      Do,
      bindW('res1', () => () => this.clientRepository.create(client)),
      map(() => this.pool.register(client.id, ws)),
      bindW('res2', () => () => this.eventPublisher.toClient(client.id, PUBLISH.CONNECTION.CONNECTED.SUCCESS, {})),
      orElseW((e) => {
        return pipe(
          Do,
          map(() => this.pool.unregister(client.id)),
          bindW('res1', () => () => this.eventPublisher.toClient(client.id, PUBLISH.CONNECTION.CONNECTED.FAILURE, {})),
          flatMap(() => left(e))
        );
      })
    )();
  }
}
