import type { EventPublishError } from '@/adapters/presenters/EventPublisher/EventPublishError.js';
import type { IPublishService } from '@/applications/services/interfaces/IPublishService.js';
import type { IRoomService } from '@/applications/services/interfaces/IRoomService.js';
import { PUBLISH } from '@/applications/websocket/PublishEvents.js';
import { Types } from '@/containers/Types.js';
import type { ClientError } from '@/domains/Client/ClientError.js';
import type { SeerOutcome } from '@/domains/Outcome/SeerOutcome.js';
import type { VoteOutcome } from '@/domains/Outcome/VoteOutcome.js';
import { VoteOutcomes } from '@/domains/Outcome/VoteOutcomes.js';
import { Persons } from '@/domains/Person/Persons.js';
import { DailyOutcomeRecord } from '@/domains/Room/DailyOutcomeRecord.js';
import { Game } from '@/domains/Room/Game.js';
import { Room, type RoomID } from '@/domains/Room/Room.js';
import type { GenericError, RuntimeError } from '@/lib/Error/Errors.js';
import type { ILogger } from '@/lib/Logger/ILogger.js';
import type { Either } from 'fp-ts/lib/Either.js';
import { pipe } from 'fp-ts/lib/function.js';
import { bind as oBind, Do as oDo, map as oMap, type Option, some } from 'fp-ts/lib/Option.js';
import { apSW, bindW as teBindW, Do as teDo, flatMap as teFlatMap, fromOption, right, sequenceArray, type TaskEither } from 'fp-ts/lib/TaskEither.js';
import { inject, injectable } from 'inversify';
import type { IPublishOutcome } from './interfaces/IPublishOutcome.js';

/**
 * 予言者の結果を返します
 */
@injectable()
export class PublishSeerOutcome implements IPublishOutcome {
  private readonly roomService: IRoomService;
  private readonly publishService: IPublishService;
  private readonly logger: ILogger;

  public constructor(
    @inject(Types.RoomService) roomService: IRoomService,
    @inject(Types.PublishService) publishService: IPublishService,
    @inject(Types.Logger) logger: ILogger
  ) {
    this.roomService = roomService;
    this.publishService = publishService;
    this.logger = logger;
  }

  public execute(id: RoomID): Promise<Either<unknown, unknown>> {
    this.logger.info('PublishSeerOutcome.execute()');

    return pipe(
      teDo,
      teBindW(
        'room',
        () => () =>
          this.roomService.withTransaction(id, ({ room }) => {
            return right(room)();
          })
      ),
      teBindW('outcomes', ({ room }) => {
        const seers = this.getSeer(room.game);

        return this.getLastSeerVote(room, seers);
      }),
      teFlatMap(({ room, outcomes }) => this.publishOutcome(room, outcomes))
    )();
  }

  /**
   * 夜の時間の予言の投票を取得します
   *
   * @param room
   * @param seers
   */
  private getLastSeerVote(room: Room, seers: Persons): TaskEither<GenericError, VoteOutcomes> {
    return pipe(
      oDo,
      // まだその日の記録を作成してないのでこれで昨日の記録を取ることができる
      oBind('record', () => Room.getLatestRecord(room)),
      oBind('outcomes', ({ record }) => {
        const seq = DailyOutcomeRecord.getDivination(record).filter((outcome) => {
          return seers.has(outcome.source);
        });

        return some(VoteOutcomes.from(seq));
      }),
      oMap(({ outcomes }) => outcomes),
      fromOption(() => {
        return {
          error: 'RuntimeError',
          message: '予言の結果が存在しません'
        } satisfies GenericError;
      })
    );
  }

  /**
   * 予言者の結果を取得します
   * TODO 憑依者の結果を取得し、一致したら反転させる
   * @param room
   * @param outcome
   */
  private getOutcome(room: Room, outcome: VoteOutcome): Option<SeerOutcome> {
    return pipe(
      oDo,
      oBind('person', () => Room.getResident(room, outcome.destination)),
      oMap(({ person }) => {
        return {
          id: outcome.destination,
          identity: Game.divine(person)
        } satisfies SeerOutcome;
      })
    );
  }

  /**
   * 生存している予言者を取得します
   *
   * @param game
   */
  private getSeer(game: Game): Persons {
    return Persons.getByRole(Game.getSurvivors(game), 'SEER');
  }

  /**
   * 予言者に通知します
   *
   * @param room
   * @param outcome
   */
  private publish(room: Room, outcome: VoteOutcome): TaskEither<GenericError | EventPublishError | ClientError, unknown> {
    return pipe(
      teDo,
      teBindW('result', () => {
        return pipe(
          oDo,
          oBind('seer', () => Room.getResident(room, outcome.source)),
          oBind('outcome', () => this.getOutcome(room, outcome)),
          oMap(({ seer, outcome }) => {
            return {
              seer,
              outcome
            };
          }),
          fromOption(() => {
            return {
              error: 'RuntimeError',
              message: '予言者または予言の結果が存在しません'
            } satisfies RuntimeError;
          })
        );
      }),
      teFlatMap(({ result: { seer, outcome } }) => {
        return pipe(
          teDo,
          apSW('res1', () =>
            this.publishService.toPerson(seer, PUBLISH.DIVINATION.OUTCOME, {
              outcome
            })
          ),
          apSW('res2', () =>
            this.publishService.toModerator(room, PUBLISH.DIVINATION.OUTCOME, {
              outcome
            })
          )
        );
      })
    );
  }

  /**
   * 予言者に結果を通知します
   *
   * @param room
   * @param outcomes
   */
  private publishOutcome(room: Room, outcomes: VoteOutcomes): TaskEither<GenericError | EventPublishError | ClientError, unknown> {
    const tes = outcomes.map((outcome: VoteOutcome) => {
      return this.publish(room, outcome);
    });

    return sequenceArray(tes);
  }
}
