import type { GatewayError } from '@/adapters/gateways/GatewayError.js';
import type { EventPublishError } from '@/adapters/presenters/EventPublisher/EventPublishError.js';
import type { IPublishService } from '@/applications/services/interfaces/IPublishService.js';
import type { IRoomService } from '@/applications/services/interfaces/IRoomService.js';
import type { AppTypeMap } from '@/applications/unitOfWork/AppTypeMap.js';
import type { IUnitOfWork } from '@/applications/unitOfWork/IUnitOfWork.js';
import type { UnitOfWorkError } from '@/applications/unitOfWork/UnitOfWorkError.js';
import { type GetPublishEventData, PUBLISH, type PublishEventType } from '@/applications/websocket/PublishEvents.js';
import { Types } from '@/containers/Types.js';
import type { ClientError } from '@/domains/Client/ClientError.js';
import { Person, type PersonID } from '@/domains/Person/Person.js';
import { createPersonError, type PersonError } from '@/domains/Person/PersonError.js';
import { Room, type RoomID } from '@/domains/Room/Room.js';
import type { GenericError } from '@/lib/Error/Errors.js';
import type { ILogger } from '@/lib/Logger/ILogger.js';
import type { Either } from 'fp-ts/lib/Either.js';
import { pipe } from 'fp-ts/lib/function.js';
import { apSW, bindW, Do, flatMap, fromEither, fromOption, left, map, orElseW, right, type TaskEither } from 'fp-ts/lib/TaskEither.js';
import { inject, injectable } from 'inversify';

/**
 * 投票します
 */
@injectable()
export class SetVoteTarget {
  private readonly roomService: IRoomService;
  private readonly publishService: IPublishService;
  private readonly logger: ILogger;

  public constructor(
    @inject(Types.RoomService) roomService: IRoomService,
    @inject(Types.PublishService) publishService: IPublishService,
    @inject(Types.Logger) logger: ILogger
  ) {
    this.roomService = roomService;
    this.publishService = publishService;
    this.logger = logger;
  }

  private emit<T extends PublishEventType>(
    room: Room,
    source: Person,
    event: T,
    data: GetPublishEventData<T>
  ): TaskEither<GenericError | GatewayError | UnitOfWorkError | EventPublishError | ClientError, unknown> {
    return pipe(
      Do,
      apSW('res1', () => this.publishService.toPerson(source, event, data)),
      apSW('res2', () => this.publishService.toModerator(room, event, data))
    );
  }

  public execute(roomID: RoomID, sourceID: PersonID, destinationID: PersonID): Promise<Either<unknown, unknown>> {
    this.logger.info('SetVoteTarget.execute()');

    return this.roomService.withTransaction(roomID, ({ room, uow }) => {
      return pipe(
        Do,
        bindW('person', () => this.validatePersons(room, sourceID, destinationID)),
        bindW('res1', ({ person: { source, destination } }) => this.validatePhase(room, source, destination)),
        bindW('res2', ({ person: { source, destination } }) => {
          return pipe(
            Do,
            bindW('res1', () => this.setVoteTarget(room, source, destination, uow)),
            bindW('res2', () =>
              this.emit(room, source, PUBLISH.VOTE.SET.SUCCESS, {
                sourceID: source.id,
                destinationID: destination.id
              })
            ),
            orElseW((e) => {
              return pipe(
                Do,
                bindW('res1', () =>
                  this.emit(room, source, PUBLISH.VOTE.SET.FAILURE, {
                    cause: null,
                    sourceID: source.id,
                    destinationID: destination.id
                  })
                ),
                flatMap(() => left(e))
              );
            })
          );
        })
      )();
    });
  }

  /**
   * 部屋から住人を取得します
   *
   * @param room
   * @param personID
   */
  private getPerson(room: Room, personID: PersonID): TaskEither<PersonError, Person> {
    return pipe(
      Do,
      bindW('person', () => {
        return pipe(
          Room.getResident(room, personID),
          fromOption(() => {
            this.logger.error(`NO SUCH PERSON. ROOM ID: ${room.id}. PERSON ID: ${personID}`);

            return createPersonError('NoSuchPerson', `該当する住人が存在しません。: ${personID}`);
          })
        );
      }),
      map(({ person }) => person)
    );
  }

  private setVoteTarget(
    room: Room,
    source: Person,
    destination: Person,
    uow: IUnitOfWork<AppTypeMap>
  ): TaskEither<GenericError | UnitOfWorkError, unknown> {
    return pipe(
      Do,
      bindW('newRoom', () => {
        return pipe(
          Do,
          bindW('votedRoom', () => fromEither(Room.addVote(room, room.game.dayPhase.days, source, destination))),
          bindW('newRoom', ({ votedRoom }) => {
            switch (source.identity.role) {
              // 猟師の場合は報復先を設定する
              case 'HUNTER': {
                return fromEither(Room.addRetaliation(votedRoom, votedRoom.game.dayPhase.days, source, destination));
              }
              // 憑依者の場合は汚染先を設定する
              case 'POSSESSED': {
                return fromEither(Room.addCorruption(votedRoom, votedRoom.game.dayPhase.days, source, destination));
              }
              default: {
                return right(votedRoom);
              }
            }
          }),
          map(({ newRoom }) => newRoom)
        );
      }),
      flatMap(
        ({ newRoom }) =>
          () =>
            uow.update('room', newRoom)
      )
    );
  }

  private validatePersons(
    room: Room,
    sourceID: PersonID,
    destinationID: PersonID
  ): TaskEither<
    GenericError | GatewayError | UnitOfWorkError | EventPublishError | ClientError | PersonError,
    {
      source: Person;
      destination: Person;
    }
  > {
    return pipe(
      Do,
      apSW('source', this.getPerson(room, sourceID)),
      apSW('destination', this.getPerson(room, destinationID)),
      bindW('person', ({ source, destination }) => {
        // 元が生存していなければ例外
        if (Person.isDead(source)) {
          return pipe(
            Do,
            bindW('res1', () =>
              this.emit(room, source, PUBLISH.VOTE.SET.FAILURE, {
                cause: 'SOURCE_DEAD',
                sourceID,
                destinationID
              })
            ),
            flatMap(() => left(createPersonError('SourceDead', `生存していません。: ${sourceID}`)))
          );
        }
        // 先が死亡していれば例外
        if (Person.isDead(destination)) {
          return pipe(
            Do,
            bindW('res1', () =>
              this.emit(room, source, PUBLISH.VOTE.SET.FAILURE, {
                cause: 'DESTINATION_DEAD',
                sourceID,
                destinationID
              })
            ),
            flatMap(() => left(createPersonError('DestinationDead', `死亡している住人を指定することはできません。: ${destinationID}`)))
          );
        }

        return right({
          source,
          destination
        });
      }),
      map(({ person }) => person)
    );
  }

  private validatePhase(
    room: Room,
    source: Person,
    destination: Person
  ): TaskEither<GenericError | GatewayError | UnitOfWorkError | EventPublishError | ClientError | PersonError, unknown> {
    return pipe(
      Do,
      bindW('res1', () => {
        if (room.game.dayPhase.phase !== 'EVENING') {
          return pipe(
            Do,
            bindW('res1', () =>
              this.emit(room, source, PUBLISH.VOTE.SET.FAILURE, {
                cause: 'UNACCEPTABLE_PHASE',
                sourceID: source.id,
                destinationID: destination.id
              })
            ),
            flatMap(() => left(createPersonError('ActionImpossible', '夕の時間のみ投票できます。')))
          );
        }

        return right(null);
      })
    );
  }
}
