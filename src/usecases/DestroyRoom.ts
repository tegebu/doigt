import type { GatewayError } from '@/adapters/gateways/GatewayError.js';
import type { IPublishService } from '@/applications/services/interfaces/IPublishService.js';
import type { IRoomService } from '@/applications/services/interfaces/IRoomService.js';
import { PUBLISH } from '@/applications/websocket/PublishEvents.js';
import { Types } from '@/containers/Types.js';
import type { Client, ClientID } from '@/domains/Client/Client.js';
import type { ClientError } from '@/domains/Client/ClientError.js';
import type { IClientRepository } from '@/domains/Client/IClientRepository.js';
import type { Room, RoomID } from '@/domains/Room/Room.js';
import { type RoomError, createRoomError } from '@/domains/Room/RoomError.js';
import type { GenericError } from '@/lib/Error/Errors.js';
import type { ILogger } from '@/lib/Logger/ILogger.js';
import type { Either } from 'fp-ts/lib/Either.js';
import { Do, type TaskEither, bindW, flatMap, fromOption, left, map, orElseW, right } from 'fp-ts/lib/TaskEither.js';
import { pipe } from 'fp-ts/lib/function.js';
import { inject, injectable } from 'inversify';

/**
 * 部屋を破壊します
 *
 * moderatorのみ使用可能です
 * 部屋にいる全てのクライアントを部屋から退出させます
 */
@injectable()
export class DestroyRoom {
  private readonly clientRepository: IClientRepository;
  private readonly roomService: IRoomService;
  private readonly publishService: IPublishService;
  private readonly logger: ILogger;

  public constructor(
    @inject(Types.CacheClientRepository) clientRepository: IClientRepository,
    @inject(Types.RoomService) roomService: IRoomService,
    @inject(Types.PublishService) publishService: IPublishService,
    @inject(Types.Logger) logger: ILogger
  ) {
    this.clientRepository = clientRepository;
    this.roomService = roomService;
    this.publishService = publishService;
    this.logger = logger;
  }

  public execute(roomID: RoomID, clientID: ClientID): Promise<Either<unknown, unknown>> {
    this.logger.info('DestroyRoom.execute()');

    return this.roomService.withTransaction(roomID, ({ room }) => {
      return pipe(
        Do,
        bindW('client', () => this.findClient(clientID)),
        bindW('res1', ({ client }) => {
          return pipe(
            Do,
            bindW('res1', () => this.validateModerator(room, clientID)),
            bindW('res2', () => () => this.roomService.destroy(roomID)),
            bindW('res3', () => () => this.publishService.toClient(client, PUBLISH.ROOM.DESTROY.SUCCESS, {})),
            bindW('res4', () => () => this.publishService.toEveryone(room, PUBLISH.ROOM.DISBAND, {})),
            bindW(
              'res5',
              () => () =>
                this.publishService.broadcast(PUBLISH.ROOM.NOTIFY.DESTROYED, {
                  roomID
                })
            ),
            orElseW((e) => {
              return pipe(
                Do,
                bindW('res1', () => () => this.publishService.toClient(client, PUBLISH.ROOM.DESTROY.FAILURE, {})),
                flatMap(() => left(e))
              );
            })
          );
        })
      )();
    });
  }

  private findClient(id: ClientID): TaskEither<GenericError | GatewayError | ClientError, Client> {
    return pipe(
      Do,
      bindW('option', () => () => this.clientRepository.find(id)),
      bindW('client', ({ option }) => {
        return pipe(
          option,
          fromOption(() => {
            return {
              error: 'ClientError',
              detail: 'NoSuchClient',
              message: `クライアントが見つかりません: ${id}`
            } as ClientError;
          })
        );
      }),
      map(({ client }) => client)
    );
  }

  private validateModerator(room: Room, clientID: ClientID): TaskEither<RoomError, unknown> {
    if (room.moderator?.id === clientID) {
      return right(null);
    }

    return left(createRoomError('ModeratorNotMatch', 'moderatorではないクライアントからのリクエストです'));
  }
}
