import type { IEventPublisher } from '@/adapters/presenters/EventPublisher/IEventPublisher.js';
import { PUBLISH } from '@/applications/websocket/PublishEvents.js';
import { Types } from '@/containers/Types.js';
import type { ClientID } from '@/domains/Client/Client.js';
import type { IRoomRepository } from '@/domains/Room/IRoomRepository.js';
import { Room, type RoomList } from '@/domains/Room/Room.js';
import { Rooms } from '@/domains/Room/Rooms.js';
import type { ILogger } from '@/lib/Logger/ILogger.js';
import type { Either } from 'fp-ts/lib/Either.js';
import { pipe } from 'fp-ts/lib/function.js';
import { bindW, Do, flatMap, left, orElseW, right } from 'fp-ts/lib/TaskEither.js';
import { inject, injectable } from 'inversify';

@injectable()
export class FetchRoomList {
  private readonly roomRepository: IRoomRepository;
  private readonly eventPublisher: IEventPublisher;
  private readonly logger: ILogger;

  public constructor(
    @inject(Types.CacheRoomRepository) roomRepository: IRoomRepository,
    @inject(Types.WSEventPublisher) eventPublisher: IEventPublisher,
    @inject(Types.Logger) logger: ILogger
  ) {
    this.roomRepository = roomRepository;
    this.eventPublisher = eventPublisher;
    this.logger = logger;
  }

  public execute(clientID: ClientID): Promise<Either<unknown, unknown>> {
    this.logger.info('FetchRoomList.execute()');

    return pipe(
      Do,
      bindW('rooms', () => () => this.roomRepository.all()),
      bindW('newRooms', ({ rooms }) => {
        const newRooms = Rooms.filter(rooms, (room) => {
          return Room.isOpen(room);
        });

        return right(newRooms);
      }),
      bindW(
        'res1',
        ({ newRooms }) =>
          () =>
            this.eventPublisher.toClient(clientID, PUBLISH.ROOM.LIST.SUCCESS, {
              rooms: [...newRooms.values()].map((room) => {
                return {
                  id: room.id,
                  name: room.name,
                  moderator: room.moderator,
                  quota: room.quota,
                  participants: room.participants.size,
                  requirePassword: Room.hasPassword(room),
                  status: room.game.status
                } satisfies RoomList;
              })
            })
      ),
      orElseW((e) => {
        return pipe(
          Do,
          bindW('res1', () => () => this.eventPublisher.toClient(clientID, PUBLISH.ROOM.LIST.FAILURE, {})),
          flatMap(() => left(e))
        );
      })
    )();
  }
}
