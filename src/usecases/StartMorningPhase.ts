import type { IPublishService } from '@/applications/services/interfaces/IPublishService.js';
import type { IRoomService } from '@/applications/services/interfaces/IRoomService.js';
import type { AppTypeMap } from '@/applications/unitOfWork/AppTypeMap.js';
import type { IUnitOfWork } from '@/applications/unitOfWork/IUnitOfWork.js';
import type { UnitOfWorkError } from '@/applications/unitOfWork/UnitOfWorkError.js';
import { PUBLISH } from '@/applications/websocket/PublishEvents.js';
import { Types } from '@/containers/Types.js';
import type { Team } from '@/domains/Identity/Team.js';
import { Game } from '@/domains/Room/Game.js';
import { Room, type RoomID } from '@/domains/Room/Room.js';
import type { GenericError } from '@/lib/Error/Errors.js';
import type { ILogger } from '@/lib/Logger/ILogger.js';
import type { Either } from 'fp-ts/lib/Either.js';
import { pipe } from 'fp-ts/lib/function.js';
import { bindW, Do, map, right, type TaskEither } from 'fp-ts/lib/TaskEither.js';
import { inject, injectable } from 'inversify';
import type { IPublishOutcome } from './interfaces/IPublishOutcome.js';
import type { IStartPhase } from './interfaces/IStartPhase.js';

/**
 * 朝の時間を開始します
 */
@injectable()
export class StartMorningPhase implements IStartPhase {
  private readonly roomService: IRoomService;
  private readonly publishService: IPublishService;
  private readonly revealNightVictim: IPublishOutcome;
  private readonly publishSeerOutcome: IPublishOutcome;
  private readonly publishMediumOutcome: IPublishOutcome;
  private readonly logger: ILogger;

  public constructor(
    @inject(Types.RoomService) roomService: IRoomService,
    @inject(Types.PublishService) publishService: IPublishService,
    @inject(Types.RevealNightVictims) revealNightVictim: IPublishOutcome,
    @inject(Types.PublishSeerOutcome) publishSeerOutcome: IPublishOutcome,
    @inject(Types.PublishMediumOutcome) publishMediumOutcome: IPublishOutcome,
    @inject(Types.Logger) logger: ILogger
  ) {
    this.roomService = roomService;
    this.publishService = publishService;
    this.revealNightVictim = revealNightVictim;
    this.publishSeerOutcome = publishSeerOutcome;
    this.publishMediumOutcome = publishMediumOutcome;
    this.logger = logger;
  }

  public execute(id: RoomID): Promise<Either<unknown, Array<Team>>> {
    this.logger.info('StartMorningPhase.execute()');

    return pipe(
      Do,
      bindW('res1', () => this.publishEachOutcome(id)),
      bindW(
        'winners',
        () => () =>
          this.roomService.withTransaction(id, ({ room, uow }) => {
            return pipe(
              Do,
              bindW('winners', () => right(Game.getWinners(room.game))),
              bindW('newRoom', () => this.saveNewRecord(room, uow)),
              bindW(
                'res2',
                ({ newRoom }) =>
                  () =>
                    this.publishService.toEveryone(newRoom, PUBLISH.MORNING.START, {
                      seconds: newRoom.game.phaseDuration.morning
                    })
              ),
              map(({ winners }) => winners)
            )();
          })
      ),
      map(({ winners }) => winners)
    )();
  }

  private publishEachOutcome(id: RoomID): TaskEither<unknown, unknown> {
    return pipe(
      Do,
      bindW('res1', () => () => this.revealNightVictim.execute(id)),
      bindW('res2', () => () => this.publishSeerOutcome.execute(id)),
      bindW('res3', () => () => this.publishMediumOutcome.execute(id))
    );
  }

  /**
   * 新しいDailyRecordを保存します
   *
   * @param room
   * @param uow
   */
  private saveNewRecord(room: Room, uow: IUnitOfWork<AppTypeMap>): TaskEither<GenericError | UnitOfWorkError, Room> {
    const newRoom = Room.createRecord(room);

    return pipe(
      Do,
      bindW('res1', () => () => uow.update('room', newRoom)),
      map(() => newRoom)
    );
  }
}
