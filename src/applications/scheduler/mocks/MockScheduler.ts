import { UnimplementedError } from '@jamashita/anden/error';
import type { IScheduler } from '../IScheduler.js';

export class MockScheduler implements IScheduler {
  public hasTimer(): boolean {
    throw new UnimplementedError();
  }

  public startPhase(): void {
    throw new UnimplementedError();
  }

  public stopPhase(): void {
    throw new UnimplementedError();
  }
}
