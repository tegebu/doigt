import type { RoomID } from '@/domains/Room/Room.js';
import type { UnaryFunction } from '@jamashita/anden/type';
import type { Either } from 'fp-ts/lib/Either.js';

export interface IScheduler {
  hasTimer(roomID: RoomID): boolean;

  startPhase(roomID: RoomID, second: number, cb: UnaryFunction<RoomID, Promise<Either<unknown, unknown>>>): void;

  stopPhase(roomID: RoomID): void;
}
