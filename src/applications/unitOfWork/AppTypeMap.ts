import type { Room, RoomID } from '@/domains/Room/Room.js';

export type AppTypeMap = Readonly<{
  room: {
    id: RoomID;
    entity: Room;
  };
}>;
