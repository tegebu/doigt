import type { ILogger } from '@/lib/Logger/ILogger.js';
import type { IUnitOfWork, RepositoryTypeMap } from './IUnitOfWork.js';

export interface IUnitOfWorkFactory<T extends RepositoryTypeMap> {
  forge(logger: ILogger): IUnitOfWork<T>;
}
