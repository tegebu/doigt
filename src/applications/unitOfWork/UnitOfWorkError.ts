type Detail = 'StartTransaction' | 'Commit' | 'Rollback' | 'Create' | 'Delete' | 'Update' | 'Find';

export type UnitOfWorkError = Readonly<{
  error: 'UnitOfWorkError';
  detail: Detail;
  message: string;
}>;

export const createUnitOfWorkError = (detail: Detail, message: string): UnitOfWorkError => {
  return {
    error: 'UnitOfWorkError',
    detail,
    message
  };
};
