import type { GatewayError } from '@/adapters/gateways/GatewayError.js';
import type { GenericError } from '@/lib/Error/Errors.js';
import type { Either } from 'fp-ts/lib/Either.js';
import type { Option } from 'fp-ts/lib/Option.js';

export interface InTransactionRepository<K, E> {
  create(entity: E): Promise<Either<GenericError | GatewayError, unknown>>;

  delete(id: K): Promise<Either<GenericError | GatewayError, unknown>>;

  find(id: K): Promise<Either<GenericError | GatewayError, Option<E>>>;

  update(entity: E): Promise<Either<GenericError | GatewayError, unknown>>;
}
