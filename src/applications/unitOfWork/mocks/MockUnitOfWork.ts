import type { GatewayError } from '@/adapters/gateways/GatewayError.js';
import type { GenericError } from '@/lib/Error/Errors.js';
import { UnimplementedError } from '@jamashita/anden/error';
import type { Either } from 'fp-ts/lib/Either.js';
import type { Option } from 'fp-ts/lib/Option.js';
import type { IUnitOfWork, RepositoryTypeMap } from '../IUnitOfWork.js';
import type { UnitOfWorkError } from '../UnitOfWorkError.js';

export class MockUnitOfWork<T extends RepositoryTypeMap> implements IUnitOfWork<T> {
  public commit(): Promise<Either<GenericError | UnitOfWorkError | GatewayError, unknown>> {
    throw new UnimplementedError();
  }

  public create(): Promise<Either<GenericError | UnitOfWorkError, unknown>> {
    throw new UnimplementedError();
  }

  public delete(): Promise<Either<GenericError | UnitOfWorkError, unknown>> {
    throw new UnimplementedError();
  }

  public find<K extends keyof T>(): Promise<Either<GenericError | UnitOfWorkError | GatewayError, Option<T[K]['entity']>>> {
    throw new UnimplementedError();
  }

  public isLocked(): boolean {
    throw new UnimplementedError();
  }

  public registerRepository(): void {
    throw new UnimplementedError();
  }

  public rollback(): Promise<Either<GenericError | UnitOfWorkError | GatewayError, unknown>> {
    throw new UnimplementedError();
  }

  public startTransaction(): Promise<Either<GenericError | UnitOfWorkError, unknown>> {
    throw new UnimplementedError();
  }

  public update(): Promise<Either<GenericError | UnitOfWorkError, unknown>> {
    throw new UnimplementedError();
  }
}
