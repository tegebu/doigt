import { UnimplementedError } from '@jamashita/anden/error';
import type { IUnitOfWork, RepositoryTypeMap } from '../IUnitOfWork.js';
import type { IUnitOfWorkFactory } from '../IUnitOfWorkFactory.js';

export class MockUnitOfWorkFactory<T extends RepositoryTypeMap> implements IUnitOfWorkFactory<T> {
  public forge(): IUnitOfWork<T> {
    throw new UnimplementedError();
  }
}
