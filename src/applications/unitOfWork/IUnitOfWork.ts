import type { GatewayError } from '@/adapters/gateways/GatewayError.js';
import type { GenericError } from '@/lib/Error/Errors.js';
import type { Either } from 'fp-ts/lib/Either.js';
import type { Option } from 'fp-ts/lib/Option.js';
import type { InTransactionRepository } from './InTransactionRepository.js';
import type { UnitOfWorkError } from './UnitOfWorkError.js';

export type RepositoryTypeMap<K = unknown> = Readonly<{
  [key: string]: {
    id: K;
    entity: {
      id: K;
    };
  };
}>;

export interface IUnitOfWork<T extends RepositoryTypeMap> {
  commit(): Promise<Either<GenericError | UnitOfWorkError | GatewayError, unknown>>;

  create<K extends keyof T>(key: K, entity: T[K]['entity']): Promise<Either<GenericError | UnitOfWorkError, unknown>>;

  delete<K extends keyof T>(key: K, id: T[K]['id']): Promise<Either<GenericError | UnitOfWorkError, unknown>>;

  find<K extends keyof T>(key: K, id: T[K]['id']): Promise<Either<GenericError | UnitOfWorkError | GatewayError, Option<T[K]['entity']>>>;

  isLocked(): boolean;

  registerRepository<K extends keyof T>(key: K, repository: InTransactionRepository<T[K]['id'], T[K]['entity']>): void;

  rollback(): Promise<Either<GenericError | UnitOfWorkError | GatewayError, unknown>>;

  startTransaction(): Promise<Either<GenericError | UnitOfWorkError, unknown>>;

  update<K extends keyof T>(key: K, entity: T[K]['entity']): Promise<Either<GenericError | UnitOfWorkError, unknown>>;
}
