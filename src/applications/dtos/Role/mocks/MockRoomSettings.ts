import { mockRoleAssignmentRequest } from '@/applications/dtos/Role/mocks/MockRoleAssignmentRequest.js';
import type { RoleAssignmentRequest } from '@/applications/dtos/Role/RoleAssignmentRequest.js';
import type { RoomSettings } from '@/applications/dtos/Role/RoleSettings.js';
import type { ClientID } from '@/domains/Client/Client.js';

type RoomSettingsArgs = Partial<
  Readonly<{
    hostID: string;
    name: string;
    password: string;
    quota: number;
    needsModerator: boolean;
    roleAssignment: RoleAssignmentRequest;
  }>
>;

export const mockRoomSettings = ({
  hostID = '8a8e9c21-6282-4c5f-97ac-ce5cd845cb16',
  name = 'room',
  password = 'password',
  quota = 10,
  needsModerator = true,
  roleAssignment = mockRoleAssignmentRequest()
}: RoomSettingsArgs = {}): RoomSettings => {
  return {
    hostID: hostID as ClientID,
    name,
    password,
    quota,
    needsModerator,
    roleAssignment
  };
};
