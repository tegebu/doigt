import type { RoleValue } from '../RoleAssignmentRequest.js';

type RoleAssignmentRequestArgs = Partial<
  Readonly<{
    atonement: RoleValue;
    catalyst: RoleValue;
    coward: RoleValue;
    hunter: RoleValue;
    informant: RoleValue;
    jester: RoleValue;
    kitsune: RoleValue;
    knight: RoleValue;
    medium: RoleValue;
    monastic: RoleValue;
    orator: RoleValue;
    possessed: RoleValue;
    seer: RoleValue;
    werewolf: RoleValue;
  }>
>;

export const mockRoleAssignmentRequest = ({
  atonement = { type: 'SINGLE', value: 0 },
  catalyst = { type: 'SINGLE', value: 0 },
  coward = { type: 'SINGLE', value: 0 },
  hunter = { type: 'SINGLE', value: 0 },
  informant = { type: 'SINGLE', value: 0 },
  jester = { type: 'SINGLE', value: 0 },
  kitsune = { type: 'SINGLE', value: 0 },
  knight = { type: 'SINGLE', value: 0 },
  medium = { type: 'SINGLE', value: 0 },
  monastic = { type: 'SINGLE', value: 0 },
  orator = { type: 'SINGLE', value: 0 },
  possessed = { type: 'SINGLE', value: 0 },
  seer = { type: 'SINGLE', value: 0 },
  werewolf = { type: 'SINGLE', value: 0 }
}: RoleAssignmentRequestArgs = {}) => {
  return {
    atonement,
    catalyst,
    coward,
    hunter,
    informant,
    jester,
    kitsune,
    knight,
    medium,
    monastic,
    orator,
    possessed,
    seer,
    werewolf
  };
};
