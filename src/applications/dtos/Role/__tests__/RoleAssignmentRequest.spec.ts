import { RoleAssignmentRequest } from '../RoleAssignmentRequest.js';

describe('RoleAssignmentRequest', () => {
  describe('update', () => {
    it('should return a new RoleAssignmentRequest with the updated values', () => {
      const original = {
        atonement: {
          type: 'SINGLE',
          value: 1
        },
        catalyst: {
          type: 'SINGLE',
          value: 1
        },
        coward: {
          type: 'SINGLE',
          value: 1
        },
        hunter: {
          type: 'SINGLE',
          value: 1
        },
        informant: {
          type: 'SINGLE',
          value: 1
        },
        jester: {
          type: 'SINGLE',
          value: 1
        },
        kitsune: {
          type: 'SINGLE',
          value: 1
        },
        knight: {
          type: 'SINGLE',
          value: 1
        },
        medium: {
          type: 'SINGLE',
          value: 1
        },
        monastic: {
          type: 'SINGLE',
          value: 1
        },
        orator: {
          type: 'SINGLE',
          value: 1
        },
        possessed: {
          type: 'SINGLE',
          value: 1
        },
        seer: {
          type: 'SINGLE',
          value: 1
        },
        werewolf: {
          type: 'SINGLE',
          value: 1
        }
      } as RoleAssignmentRequest;

      const diff = {
        orator: {
          type: 'SINGLE',
          value: 2
        }
      } as Partial<RoleAssignmentRequest>;

      const updated = RoleAssignmentRequest.update(diff, original);

      expect(original).toEqual({
        atonement: {
          type: 'SINGLE',
          value: 1
        },
        catalyst: {
          type: 'SINGLE',
          value: 1
        },
        coward: {
          type: 'SINGLE',
          value: 1
        },
        hunter: {
          type: 'SINGLE',
          value: 1
        },
        informant: {
          type: 'SINGLE',
          value: 1
        },
        jester: {
          type: 'SINGLE',
          value: 1
        },
        kitsune: {
          type: 'SINGLE',
          value: 1
        },
        knight: {
          type: 'SINGLE',
          value: 1
        },
        medium: {
          type: 'SINGLE',
          value: 1
        },
        monastic: {
          type: 'SINGLE',
          value: 1
        },
        orator: {
          type: 'SINGLE',
          value: 1
        },
        possessed: {
          type: 'SINGLE',
          value: 1
        },
        seer: {
          type: 'SINGLE',
          value: 1
        },
        werewolf: {
          type: 'SINGLE',
          value: 1
        }
      });
      expect(updated).toEqual({
        atonement: {
          type: 'SINGLE',
          value: 1
        },
        catalyst: {
          type: 'SINGLE',
          value: 1
        },
        coward: {
          type: 'SINGLE',
          value: 1
        },
        hunter: {
          type: 'SINGLE',
          value: 1
        },
        informant: {
          type: 'SINGLE',
          value: 1
        },
        jester: {
          type: 'SINGLE',
          value: 1
        },
        kitsune: {
          type: 'SINGLE',
          value: 1
        },
        knight: {
          type: 'SINGLE',
          value: 1
        },
        medium: {
          type: 'SINGLE',
          value: 1
        },
        monastic: {
          type: 'SINGLE',
          value: 1
        },
        orator: {
          type: 'SINGLE',
          value: 2
        },
        possessed: {
          type: 'SINGLE',
          value: 1
        },
        seer: {
          type: 'SINGLE',
          value: 1
        },
        werewolf: {
          type: 'SINGLE',
          value: 1
        }
      });
    });
  });

  describe('validate', () => {
    it('should return true when the sum of the value is less than or equal to the quota: single', () => {
      const request = {
        atonement: {
          type: 'SINGLE',
          value: 1
        },
        catalyst: {
          type: 'SINGLE',
          value: 1
        },
        coward: {
          type: 'SINGLE',
          value: 1
        },
        hunter: {
          type: 'SINGLE',
          value: 1
        },
        informant: {
          type: 'SINGLE',
          value: 1
        },
        jester: {
          type: 'SINGLE',
          value: 1
        },
        kitsune: {
          type: 'SINGLE',
          value: 1
        },
        knight: {
          type: 'SINGLE',
          value: 1
        },
        medium: {
          type: 'SINGLE',
          value: 1
        },
        monastic: {
          type: 'SINGLE',
          value: 1
        },
        orator: {
          type: 'SINGLE',
          value: 1
        },
        possessed: {
          type: 'SINGLE',
          value: 1
        },
        seer: {
          type: 'SINGLE',
          value: 1
        },
        werewolf: {
          type: 'SINGLE',
          value: 1
        }
      } satisfies RoleAssignmentRequest;

      expect(RoleAssignmentRequest.validate(request, 13)).toBe(false);
      expect(RoleAssignmentRequest.validate(request, 14)).toBe(true);
      expect(RoleAssignmentRequest.validate(request, 15)).toBe(true);
    });

    it('should return true when the sum of the min is less than or equal to the quota: range', () => {
      const request = {
        atonement: {
          type: 'RANGE',
          min: 1,
          max: 3
        },
        catalyst: {
          type: 'RANGE',
          min: 1,
          max: 3
        },
        coward: {
          type: 'RANGE',
          min: 1,
          max: 3
        },
        hunter: {
          type: 'RANGE',
          min: 1,
          max: 3
        },
        informant: {
          type: 'RANGE',
          min: 1,
          max: 3
        },
        jester: {
          type: 'RANGE',
          min: 1,
          max: 3
        },
        kitsune: {
          type: 'RANGE',
          min: 1,
          max: 3
        },
        knight: {
          type: 'RANGE',
          min: 1,
          max: 3
        },
        medium: {
          type: 'RANGE',
          min: 1,
          max: 3
        },
        monastic: {
          type: 'RANGE',
          min: 1,
          max: 3
        },
        orator: {
          type: 'RANGE',
          min: 1,
          max: 3
        },
        possessed: {
          type: 'RANGE',
          min: 1,
          max: 3
        },
        seer: {
          type: 'RANGE',
          min: 1,
          max: 3
        },
        werewolf: {
          type: 'RANGE',
          min: 1,
          max: 3
        }
      } satisfies RoleAssignmentRequest;

      expect(RoleAssignmentRequest.validate(request, 13)).toBe(false);
      expect(RoleAssignmentRequest.validate(request, 14)).toBe(true);
      expect(RoleAssignmentRequest.validate(request, 15)).toBe(true);
    });

    it('should return true when the minimum value of the values is less than or equal to the quota: complex', () => {
      const request = {
        atonement: {
          type: 'COMPLEX',
          values: [3, 1, 2]
        },
        catalyst: {
          type: 'COMPLEX',
          values: [3, 1, 2]
        },
        coward: {
          type: 'COMPLEX',
          values: [3, 1, 2]
        },
        hunter: {
          type: 'COMPLEX',
          values: [3, 1, 2]
        },
        informant: {
          type: 'COMPLEX',
          values: [3, 1, 2]
        },
        jester: {
          type: 'COMPLEX',
          values: [3, 1, 2]
        },
        kitsune: {
          type: 'COMPLEX',
          values: [3, 1, 2]
        },
        knight: {
          type: 'COMPLEX',
          values: [3, 1, 2]
        },
        medium: {
          type: 'COMPLEX',
          values: [3, 1, 2]
        },
        monastic: {
          type: 'COMPLEX',
          values: [3, 1, 2]
        },
        orator: {
          type: 'COMPLEX',
          values: [3, 1, 2]
        },
        possessed: {
          type: 'COMPLEX',
          values: [3, 1, 2]
        },
        seer: {
          type: 'COMPLEX',
          values: [3, 1, 2]
        },
        werewolf: {
          type: 'COMPLEX',
          values: [3, 1, 2]
        }
      } satisfies RoleAssignmentRequest;

      expect(RoleAssignmentRequest.validate(request, 13)).toBe(false);
      expect(RoleAssignmentRequest.validate(request, 14)).toBe(true);
      expect(RoleAssignmentRequest.validate(request, 15)).toBe(true);
    });
  });
});
