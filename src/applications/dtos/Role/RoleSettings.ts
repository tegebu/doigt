// 部屋の設定
import { RoleAssignmentRequest } from '@/applications/dtos/Role/RoleAssignmentRequest.js';
import { Client } from '@/domains/Client/Client.js';
import { z } from 'zod';

const RoomSettingsSchema = z.object({
  hostID: Client.ID.SCHEMA,
  name: z.string().trim().min(1, '部屋名は必須です'),
  password: z
    .string({
      required_error: 'パスワードは必須です'
    })
    .trim()
    .nullable(),
  quota: z
    .number({
      invalid_type_error: '定員は必須です'
    })
    .int('整数の値を入力してください')
    .positive('定員は1人以上に設定してください'),
  needsModerator: z.boolean(),
  roleAssignment: RoleAssignmentRequest.SCHEMA
});

export type RoomSettings = Readonly<z.infer<typeof RoomSettingsSchema>>;

export namespace RoomSettings {
  export const SCHEMA = RoomSettingsSchema;
}
