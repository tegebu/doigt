import type { ParseError } from '@/lib/Error/ParseError.js';
import { ExhaustiveError } from '@jamashita/anden/error';
import type { Either } from 'fp-ts/lib/Either.js';
import { left, right } from 'fp-ts/lib/Either.js';
import { z } from 'zod';

const SingleRoleValueSchema = z.object({
  type: z.literal('SINGLE'),
  value: z
    .number({
      invalid_type_error: '数値を入力してください'
    })
    .int('整数を入力してください')
    .nonnegative('0以上の値を入力してください')
});
const RangeRoleValueSchema = z
  .object({
    type: z.literal('RANGE'),
    min: z
      .number({
        invalid_type_error: '数値を入力してください'
      })
      .int('整数を入力してください')
      .nonnegative('0以上の値を入力してください'),
    max: z
      .number({
        invalid_type_error: '数値を入力してください'
      })
      .int('整数を入力してください')
      .nonnegative('0以上の値を入力してください')
  })
  .refine(
    (data) => {
      return data.min <= data.max;
    },
    {
      message: '最小値は最大値以下にしてください'
    }
  );
const ComplexRoleValueSchema = z.object({
  type: z.literal('COMPLEX'),
  values: z
    .array(
      z
        .number({
          invalid_type_error: '数値を入力してください'
        })
        .int('整数を入力してください')
        .nonnegative('0以上の値を入力してください')
    )
    .min(1, '1つ以上の値を入力してください')
    .refine(
      (arr) => {
        const removeDuplicateSet = new Set(arr);

        return removeDuplicateSet.size === arr.length;
      },
      {
        message: '値が重複しています'
      }
    )
});
const RoleValueSchema = z.union([SingleRoleValueSchema, RangeRoleValueSchema, ComplexRoleValueSchema]);
// あまりを村人にするのでvillagerを含めない
const RoleAssignmentRequestSchema = z.object({
  atonement: RoleValueSchema,
  catalyst: RoleValueSchema,
  coward: RoleValueSchema,
  hunter: RoleValueSchema,
  informant: RoleValueSchema,
  jester: RoleValueSchema,
  kitsune: RoleValueSchema,
  knight: RoleValueSchema,
  medium: RoleValueSchema,
  monastic: RoleValueSchema,
  orator: RoleValueSchema,
  possessed: RoleValueSchema,
  seer: RoleValueSchema,
  werewolf: RoleValueSchema
});

export type RoleValue = Readonly<z.infer<typeof RoleValueSchema>>;
export type RoleAssignmentRequest = Readonly<z.infer<typeof RoleAssignmentRequestSchema>>;

export namespace RoleAssignmentRequest {
  export const SCHEMA = RoleAssignmentRequestSchema;

  export const parse = (value: unknown): Either<ParseError, RoleAssignmentRequest> => {
    const result = RoleAssignmentRequest.SCHEMA.safeParse(value);

    if (result.success) {
      return right(result.data);
    }

    return left({
      error: 'ParseError',
      message: `INVALID VALUE: GIVEN ${String(value)}`
    } satisfies ParseError);
  };

  export const update = (diff: Partial<RoleAssignmentRequest>, original: RoleAssignmentRequest): RoleAssignmentRequest => {
    return {
      ...original,
      ...diff
    } satisfies RoleAssignmentRequest;
  };

  /**
   * その役職の割り当てリクエスト通りに割り当てできるかを検証します
   *
   * @param request
   * @param quota
   */
  export const validate = (request: RoleAssignmentRequest, quota: number): boolean => {
    const total = Object.values(request).reduce((prev, curr) => {
      switch (curr.type) {
        case 'SINGLE': {
          return prev + curr.value;
        }
        case 'RANGE': {
          return prev + curr.min;
        }
        case 'COMPLEX': {
          return prev + Math.min(...curr.values);
        }
        default: {
          throw new ExhaustiveError(curr);
        }
      }
    }, 0);

    return total <= quota;
  };
}
