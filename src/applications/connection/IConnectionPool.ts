import type { ClientID } from '@/domains/Client/Client.js';
import type { Option } from 'fp-ts/lib/Option.js';

export interface IConnectionPool<T = unknown> {
  disconnect(id: ClientID): void;

  getAllActiveConnections(): Array<T>;

  getClientID(connection: T): Option<ClientID>;

  getConnection(id: ClientID): Option<T>;

  isDisconnected(id: ClientID): boolean;

  reconnect(id: ClientID, connection: T): void;

  register(id: ClientID, connection: T): void;

  unregister(id: ClientID): void;
}
