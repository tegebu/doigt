import type { ClientID } from '@/domains/Client/Client.js';
import { UnimplementedError } from '@jamashita/anden/error';
import type { Option } from 'fp-ts/lib/Option.js';
import type { IConnectionPool } from '../IConnectionPool.js';

export class MockConnectionPool implements IConnectionPool {
  public disconnect(): void {
    throw new UnimplementedError();
  }

  public getAllActiveConnections(): Array<unknown> {
    throw new UnimplementedError();
  }

  public getClientID(): Option<ClientID> {
    throw new UnimplementedError();
  }

  public getConnection(): Option<unknown> {
    throw new UnimplementedError();
  }

  public isDisconnected(): boolean {
    throw new UnimplementedError();
  }

  public reconnect(): void {
    throw new UnimplementedError();
  }

  public register(): void {
    throw new UnimplementedError();
  }

  public unregister(): void {
    throw new UnimplementedError();
  }
}
