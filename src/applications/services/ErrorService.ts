import { type GatewayError, GatewayErrorSchema } from '@/adapters/gateways/GatewayError.js';
import { Types } from '@/containers/Types.js';
import { type GenericError, GenericErrorSchema } from '@/lib/Error/Errors.js';
import type { ILogger } from '@/lib/Logger/ILogger.js';
import { RuntimeError } from '@jamashita/anden/error';
import { inject, injectable } from 'inversify';
import {
  type DetailMessageError,
  DetailMessageErrorSchema,
  type IErrorService,
  type MessageError,
  MessageErrorSchema
} from './interfaces/IErrorService.js';

@injectable()
export class ErrorService implements IErrorService {
  private readonly logger: ILogger;

  public constructor(@inject(Types.Logger) logger: ILogger) {
    this.logger = logger;
  }

  public handle(error: GenericError | GatewayError | MessageError | DetailMessageError): void {
    const result1 = GenericErrorSchema.safeParse(error);

    if (result1.success) {
      this.logger.error(result1.data.error);

      return;
    }

    const result2 = GatewayErrorSchema.safeParse(error);

    if (result2.success) {
      this.logger.error(result2.data.error);

      return;
    }

    const result3 = DetailMessageErrorSchema.safeParse(error);

    if (result3.success) {
      this.logger.error(result3.data.detail);
      this.logger.error(result3.data.message);

      return;
    }

    const result4 = MessageErrorSchema.safeParse(error);

    if (result4.success) {
      this.logger.error(result4.data.message);

      return;
    }

    throw new RuntimeError(`UNKNOWN ERROR: ${error.error}`);
  }
}
