import type { EventPublishError } from '@/adapters/presenters/EventPublisher/EventPublishError.js';
import type { ClientError } from '@/domains/Client/ClientError.js';
import type { GenericError } from '@/lib/Error/Errors.js';
import { UnimplementedError } from '@jamashita/anden/error';
import type { Either } from 'fp-ts/lib/Either.js';
import type { IPublishService } from '../interfaces/IPublishService.js';

export class MockPublishService implements IPublishService {
  public broadcast(): Promise<Either<GenericError | EventPublishError, unknown>> {
    throw new UnimplementedError();
  }

  public toClient(): Promise<Either<GenericError | EventPublishError | ClientError, unknown>> {
    throw new UnimplementedError();
  }

  public toDeceased(): Promise<Either<GenericError | EventPublishError | ClientError, unknown>> {
    throw new UnimplementedError();
  }

  public toEveryone(): Promise<Either<GenericError | EventPublishError | ClientError, unknown>> {
    throw new UnimplementedError();
  }

  public toModerator(): Promise<Either<GenericError | EventPublishError | ClientError, unknown>> {
    throw new UnimplementedError();
  }

  public toPerson(): Promise<Either<GenericError | EventPublishError | ClientError, unknown>> {
    throw new UnimplementedError();
  }

  public toPersons(): Promise<Either<GenericError | EventPublishError | ClientError, unknown>> {
    throw new UnimplementedError();
  }

  public toSurvivors(): Promise<Either<GenericError | EventPublishError | ClientError, unknown>> {
    throw new UnimplementedError();
  }
}
