import type { GatewayError } from '@/adapters/gateways/GatewayError.js';
import type { IUnitOfWork } from '@/applications/unitOfWork/IUnitOfWork.js';
import type { UnitOfWorkError } from '@/applications/unitOfWork/UnitOfWorkError.js';
import type { RoomError } from '@/domains/Room/RoomError.js';
import type { GenericError } from '@/lib/Error/Errors.js';
import { UnimplementedError } from '@jamashita/anden/error';
import type { Either } from 'fp-ts/lib/Either.js';
import type { IRoomService, RoomTypeMap } from '../interfaces/IRoomService.js';

export class MockRoomService implements IRoomService {
  public cleanup(): Promise<Either<GenericError | GatewayError | UnitOfWorkError, unknown>> {
    throw new UnimplementedError();
  }

  public destroy(): Promise<Either<GenericError | GatewayError | UnitOfWorkError, unknown>> {
    throw new UnimplementedError();
  }

  public getUOW(): IUnitOfWork<RoomTypeMap> {
    throw new UnimplementedError();
  }

  public withTransaction<L, R>(): Promise<Either<GenericError | GatewayError | UnitOfWorkError | RoomError | L, R>> {
    throw new UnimplementedError();
  }
}
