import type { RoleAssignment } from '@/domains/Role/RoleAssignment.js';
import type { RoleError } from '@/domains/Role/RoleError.js';
import { UnimplementedError } from '@jamashita/anden/error';
import type { Either } from 'fp-ts/lib/Either.js';
import type { IRoleAssignmentService } from '../interfaces/IRoleAssignmentService.js';

export class MockRoleAssignmentService implements IRoleAssignmentService {
  public assign(): Either<RoleError, RoleAssignment> {
    throw new UnimplementedError();
  }
}
