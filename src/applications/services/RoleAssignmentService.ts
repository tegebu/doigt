import { Types } from '@/containers/Types.js';
import type { RoleAssignment, RoleAssignmentKey } from '@/domains/Role/RoleAssignment.js';
import type { RoleError } from '@/domains/Role/RoleError.js';
import { Arrays } from '@/lib/Collection/Array/Arrays.js';
import type { ILogger } from '@/lib/Logger/ILogger.js';
import type { IRandomGenerator } from '@/lib/Random/interfaces/IRandomGenerator.js';
import { ExhaustiveError, RuntimeError } from '@jamashita/anden/error';
import { Kind } from '@jamashita/anden/type';
import { type Either, left, right } from 'fp-ts/lib/Either.js';
import { inject, injectable } from 'inversify';
import type { RoleAssignmentRequest, RoleValue } from '../dtos/Role/RoleAssignmentRequest.js';

export type SingleRoleValue = Readonly<{
  role: RoleAssignmentKey;
  value: number;
}>;
export type RangeRoleValue = Readonly<{
  role: RoleAssignmentKey;
  min: number;
  max: number;
}>;
export type ComplexRoleValue = Readonly<{
  role: RoleAssignmentKey;
  values: ReadonlyArray<number>;
}>;
export type AssignmentAccumulator = Readonly<{
  single: Array<SingleRoleValue>;
  range: Array<RangeRoleValue>;
  complex: Array<ComplexRoleValue>;
}>;
type WritableRoleAssignment = {
  [P in RoleAssignmentKey]: RoleAssignment[P];
};

@injectable()
export class RoleAssignmentService {
  private readonly random: IRandomGenerator;
  private readonly logger: ILogger;

  public constructor(@inject(Types.RandomGenerator) random: IRandomGenerator, @inject(Types.Logger) logger: ILogger) {
    this.random = random;
    this.logger = logger;
  }

  private allocateRemainingRoles(remainingSlots: number, request: RoleAssignmentRequest): RoleAssignment {
    const assignableSlots = this.calculateAssignableSlots(request);
    const totalSlots = this.sumAssignment(assignableSlots);

    if (totalSlots <= remainingSlots) {
      return {
        atonement: assignableSlots.atonement,
        catalyst: assignableSlots.catalyst,
        coward: assignableSlots.coward,
        hunter: assignableSlots.hunter,
        informant: assignableSlots.informant,
        jester: assignableSlots.jester,
        kitsune: assignableSlots.kitsune,
        knight: assignableSlots.knight,
        medium: assignableSlots.medium,
        monastic: assignableSlots.monastic,
        orator: assignableSlots.orator,
        possessed: assignableSlots.possessed,
        seer: assignableSlots.seer,
        villager: remainingSlots - totalSlots,
        werewolf: assignableSlots.werewolf
      } satisfies RoleAssignment;
    }

    // 先に選ばれる役職のほうが有利のため、順番をランダムにする
    const randomizedRoles = this.shuffleRolesForAllocation();
    let remaining = remainingSlots;
    const roleAssignment = {} as Partial<WritableRoleAssignment>;

    for (const role of randomizedRoles) {
      remaining -= assignableSlots[role];

      if (remaining > 0) {
        roleAssignment[role] = assignableSlots[role];
      }
      if (remaining === 0) {
        roleAssignment[role] = assignableSlots[role];

        break;
      }
      if (remaining <= 0) {
        roleAssignment.villager = assignableSlots[role] + remaining;

        break;
      }
    }

    return {
      atonement: roleAssignment.atonement ?? 0,
      catalyst: roleAssignment.catalyst ?? 0,
      coward: roleAssignment.coward ?? 0,
      hunter: roleAssignment.hunter ?? 0,
      informant: roleAssignment.informant ?? 0,
      jester: roleAssignment.jester ?? 0,
      kitsune: roleAssignment.kitsune ?? 0,
      knight: roleAssignment.knight ?? 0,
      medium: roleAssignment.medium ?? 0,
      monastic: roleAssignment.monastic ?? 0,
      orator: roleAssignment.orator ?? 0,
      possessed: roleAssignment.possessed ?? 0,
      seer: roleAssignment.seer ?? 0,
      villager: roleAssignment.villager ?? 0,
      werewolf: roleAssignment.werewolf ?? 0
    } satisfies RoleAssignment;
  }

  /**
   * 配役を行います
   *
   * @param request
   * @param quota
   */
  public assign(request: RoleAssignmentRequest, quota: number): Either<RoleError, RoleAssignment> {
    this.logger.info('RoleAssignmentService.assign()');

    const requestByType = Object.entries(request).reduce(
      (prev, [k, v]) => {
        const key = this.toKey(k);

        switch (v.type) {
          case 'SINGLE': {
            prev.single.push({
              role: key,
              value: v.value
            });

            return prev;
          }
          case 'RANGE': {
            prev.range.push({
              role: key,
              min: v.min,
              max: v.max
            });

            return prev;
          }
          case 'COMPLEX': {
            prev.complex.push({
              role: key,
              values: v.values
            });

            return prev;
          }
          default: {
            throw new ExhaustiveError(v);
          }
        }
      },
      {
        single: [],
        range: [],
        complex: []
      } as AssignmentAccumulator
    );

    // 必須の割り当て数を取り除く
    let remainingSlots = quota;

    // なんとかした
    remainingSlots = requestByType.single.reduce((prev, curr) => {
      return prev - curr.value;
    }, remainingSlots);
    remainingSlots = requestByType.range.reduce((prev, curr) => {
      return prev - curr.min;
    }, remainingSlots);
    remainingSlots = requestByType.complex.reduce((prev, curr) => {
      return prev - Math.min(...curr.values);
    }, remainingSlots);

    if (remainingSlots < 0) {
      return left({
        error: 'RoleError',
        detail: 'QuotaExceeded',
        message: `配役しきれない人数です: ${quota}`
      } satisfies RoleError);
    }

    let roleAssignment = {} as Partial<WritableRoleAssignment>;

    roleAssignment = requestByType.single.reduce((prev, curr) => {
      prev[curr.role] = curr.value;

      return prev;
    }, roleAssignment);
    roleAssignment = requestByType.range.reduce((prev, curr) => {
      prev[curr.role] = curr.min;

      return prev;
    }, roleAssignment);
    roleAssignment = requestByType.complex.reduce((prev, curr) => {
      prev[curr.role] = Math.min(...curr.values);

      return prev;
    }, roleAssignment);

    const minimumAssignment = {
      atonement: roleAssignment.atonement ?? 0,
      catalyst: roleAssignment.catalyst ?? 0,
      coward: roleAssignment.coward ?? 0,
      hunter: roleAssignment.hunter ?? 0,
      informant: roleAssignment.informant ?? 0,
      jester: roleAssignment.jester ?? 0,
      kitsune: roleAssignment.kitsune ?? 0,
      knight: roleAssignment.knight ?? 0,
      medium: roleAssignment.medium ?? 0,
      monastic: roleAssignment.monastic ?? 0,
      orator: roleAssignment.orator ?? 0,
      possessed: roleAssignment.possessed ?? 0,
      seer: roleAssignment.seer ?? 0,
      villager: 0,
      werewolf: roleAssignment.werewolf ?? 0
    } satisfies RoleAssignment;

    // ピッタリの場合は最低値で割り当てる
    if (remainingSlots === 0) {
      return right(minimumAssignment);
    }

    const availableSlots = this.allocateRemainingRoles(remainingSlots, request);

    return right({
      atonement: minimumAssignment.atonement + availableSlots.atonement,
      catalyst: minimumAssignment.catalyst + availableSlots.catalyst,
      coward: minimumAssignment.coward + availableSlots.coward,
      hunter: minimumAssignment.hunter + availableSlots.hunter,
      informant: minimumAssignment.informant + availableSlots.informant,
      jester: minimumAssignment.jester + availableSlots.jester,
      kitsune: minimumAssignment.kitsune + availableSlots.kitsune,
      knight: minimumAssignment.knight + availableSlots.knight,
      medium: minimumAssignment.medium + availableSlots.medium,
      monastic: minimumAssignment.monastic + availableSlots.monastic,
      orator: minimumAssignment.orator + availableSlots.orator,
      possessed: minimumAssignment.possessed + availableSlots.possessed,
      seer: minimumAssignment.seer + availableSlots.seer,
      villager: minimumAssignment.villager + availableSlots.villager,
      werewolf: minimumAssignment.werewolf + availableSlots.werewolf
    } satisfies RoleAssignment);
  }

  private calculateAssignableSlots(request: RoleAssignmentRequest): RoleAssignment {
    const partial = Object.entries(request).reduce(
      (prev, [k, v]) => {
        const key = this.toKey(k);

        switch (key) {
          case 'atonement':
          case 'catalyst':
          case 'coward':
          case 'informant':
          case 'hunter':
          case 'jester':
          case 'kitsune':
          case 'knight':
          case 'medium':
          case 'monastic':
          case 'orator':
          case 'possessed':
          case 'seer':
          case 'werewolf': {
            prev[key] = this.calculateRoleSlot(v);

            return prev;
          }
          case 'villager': {
            throw new RuntimeError('Villager should not be calculated');
          }
          default: {
            throw new ExhaustiveError(key);
          }
        }
      },
      {} as Partial<WritableRoleAssignment>
    );

    return {
      atonement: partial.atonement ?? 0,
      catalyst: partial.catalyst ?? 0,
      coward: partial.coward ?? 0,
      hunter: partial.hunter ?? 0,
      informant: partial.informant ?? 0,
      jester: partial.jester ?? 0,
      kitsune: partial.kitsune ?? 0,
      knight: partial.knight ?? 0,
      medium: partial.medium ?? 0,
      monastic: partial.monastic ?? 0,
      orator: partial.orator ?? 0,
      possessed: partial.possessed ?? 0,
      seer: partial.seer ?? 0,
      villager: 0,
      werewolf: partial.werewolf ?? 0
    } satisfies RoleAssignment;
  }

  private calculateRoleSlot(assignment: RoleValue): number {
    switch (assignment.type) {
      case 'SINGLE':
        return 0;
      case 'RANGE':
        return this.random.integer(0, assignment.max - assignment.min);
      case 'COMPLEX': {
        const value = assignment.values[this.random.integer(0, assignment.values.length - 1)];

        if (Kind.isUndefined(value)) {
          throw new RuntimeError('Complex value is undefined');
        }

        return value - Math.min(...assignment.values);
      }
      default:
        throw new ExhaustiveError(assignment);
    }
  }

  private shuffleRolesForAllocation(): Array<RoleAssignmentKey> {
    return Arrays.shuffle([
      'atonement',
      'catalyst',
      'coward',
      'informant',
      'hunter',
      'jester',
      'kitsune',
      'knight',
      'medium',
      'monastic',
      'orator',
      'possessed',
      'seer',
      'werewolf'
    ] satisfies Array<RoleAssignmentKey>);
  }

  private sumAssignment(assignment: RoleAssignment): number {
    return Object.values(assignment).reduce((prev, curr) => {
      return prev + curr;
    }, 0);
  }

  private toKey(key: string): RoleAssignmentKey {
    return key as RoleAssignmentKey;
  }
}
