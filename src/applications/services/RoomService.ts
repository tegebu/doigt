import type { GatewayError } from '@/adapters/gateways/GatewayError.js';
import { Types } from '@/containers/Types.js';
import type { IRoomRepository } from '@/domains/Room/IRoomRepository.js';
import type { Room, RoomID } from '@/domains/Room/Room.js';
import { type RoomError, createRoomError } from '@/domains/Room/RoomError.js';
import type { GenericError } from '@/lib/Error/Errors.js';
import type { ILogger } from '@/lib/Logger/ILogger.js';
import { Kind, type UnaryFunction } from '@jamashita/anden/type';
import type { Either } from 'fp-ts/lib/Either.js';
import { Do, type TaskEither, bindW, flatMap, fromOption, left, map, orElseW, right } from 'fp-ts/lib/TaskEither.js';
import { pipe } from 'fp-ts/lib/function.js';
import { inject, injectable } from 'inversify';
import type { IUnitOfWork } from '../unitOfWork/IUnitOfWork.js';
import type { IUnitOfWorkFactory } from '../unitOfWork/IUnitOfWorkFactory.js';
import type { UnitOfWorkError } from '../unitOfWork/UnitOfWorkError.js';
import type { IRoomService, RoomAndUOW, RoomTypeMap } from './interfaces/IRoomService.js';

@injectable()
export class RoomService implements IRoomService {
  private readonly uowMap: Map<RoomID, IUnitOfWork<RoomTypeMap>>;
  private readonly uowFactory: IUnitOfWorkFactory<RoomTypeMap>;
  private readonly roomRepository: IRoomRepository;
  private readonly logger: ILogger;

  public constructor(
    @inject(Types.CacheUnitOfWorkFactory) uowFactory: IUnitOfWorkFactory<RoomTypeMap>,
    @inject(Types.CacheRoomRepository) roomRepository: IRoomRepository,
    @inject(Types.Logger) logger: ILogger
  ) {
    this.uowMap = new Map();
    this.uowFactory = uowFactory;
    this.roomRepository = roomRepository;
    this.logger = logger;
  }

  public async cleanup(id: RoomID): Promise<Either<GenericError | GatewayError | UnitOfWorkError, unknown>> {
    const uow = this.getUOW(id);

    if (uow.isLocked()) {
      const either = await uow.rollback();

      this.uowMap.delete(id);

      return either;
    }

    this.uowMap.delete(id);

    return right(null)();
  }

  public destroy(id: RoomID): Promise<Either<GenericError | GatewayError | UnitOfWorkError, unknown>> {
    const uow = this.getUOW(id);

    return pipe(
      Do,
      bindW('res1', () => () => uow.delete('room', id)),
      bindW('res2', () => {
        this.uowMap.delete(id);

        return right(null);
      }),
      bindW('res3', () => () => uow.commit())
    )();
  }

  private findRoom(roomID: RoomID, uow: IUnitOfWork<RoomTypeMap>): TaskEither<RoomError | GatewayError | GenericError | UnitOfWorkError, Room> {
    return pipe(
      Do,
      bindW('option', () => () => uow.find('room', roomID)),
      flatMap(({ option }) => {
        return pipe(
          option,
          fromOption(() => {
            this.logger.error(`RoomRepository.find() RETURNED none. ROOM ID: ${roomID}`);

            return createRoomError('NoSuchRoom', `該当する部屋が存在しません。: ${roomID}`);
          })
        );
      })
    );
  }

  public getUOW(id: RoomID): IUnitOfWork<RoomTypeMap> {
    const uow = this.uowMap.get(id);

    if (Kind.isUndefined(uow)) {
      const u = this.uowFactory.forge(this.logger);

      this.uowMap.set(id, u);
      u.registerRepository('room', this.roomRepository);

      return u;
    }

    return uow;
  }

  public withTransaction<L, R>(
    id: RoomID,
    proc: UnaryFunction<RoomAndUOW, Promise<Either<L, R>>>
  ): Promise<Either<GenericError | GatewayError | UnitOfWorkError | RoomError | L, R>> {
    const uow = this.getUOW(id);

    return pipe(
      Do,
      flatMap(() => () => uow.startTransaction()),
      bindW('room', () => this.findRoom(id, uow)),
      bindW(
        'result',
        ({ room }) =>
          () =>
            proc({ room, uow })
      ),
      bindW('res1', () => () => uow.commit()),
      map(({ result }) => result),
      orElseW((e) => {
        return pipe(
          Do,
          flatMap(() => () => uow.rollback()),
          flatMap(() => left(e))
        );
      })
    )();
  }
}
