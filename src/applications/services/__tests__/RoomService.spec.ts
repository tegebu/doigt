import type { AppTypeMap } from '@/applications/unitOfWork/AppTypeMap.js';
import type { IUnitOfWorkFactory } from '@/applications/unitOfWork/IUnitOfWorkFactory.js';
import { MockUnitOfWork } from '@/applications/unitOfWork/mocks/MockUnitOfWork.js';
import { MockUnitOfWorkFactory } from '@/applications/unitOfWork/mocks/MockUnitOfWorkFactory.js';
import type { IRoomRepository } from '@/domains/Room/IRoomRepository.js';
import { Room } from '@/domains/Room/Room.js';
import { createRoomError } from '@/domains/Room/RoomError.js';
import { mockRoom } from '@/domains/Room/mocks/MockRoom.js';
import { MockRoomRepository } from '@/domains/Room/mocks/MockRoomRepository.js';
import type { ILogger } from '@/lib/Logger/ILogger.js';
import { MockLogger } from '@/lib/Logger/mocks/MockLogger.js';
import { isLeft, isRight } from 'fp-ts/lib/Either.js';
import { none, some } from 'fp-ts/lib/Option.js';
import { left, right } from 'fp-ts/lib/TaskEither.js';
import { RoomService } from '../RoomService.js';

describe('RoomService', () => {
  let uowFactory: IUnitOfWorkFactory<AppTypeMap>;
  let roomRepository: IRoomRepository;
  let logger: ILogger;
  let roomService: RoomService;

  beforeEach(() => {
    uowFactory = new MockUnitOfWorkFactory();
    roomRepository = new MockRoomRepository();
    logger = new MockLogger();
    roomService = new RoomService(uowFactory, roomRepository, logger);
  });

  afterEach(() => {
    vi.clearAllMocks();
  });

  describe('destroy', () => {
    it('should delete the room and the unit of work', async () => {
      const roomID = Room.ID.from('d986e1f4-af74-453f-8c98-39e8e06b730a');

      const uow = new MockUnitOfWork<AppTypeMap>();
      const spy1 = vi.spyOn(uowFactory, 'forge').mockImplementation(() => uow);
      const spy2 = vi.spyOn(uow, 'registerRepository').mockImplementation(() => null);
      const spy3 = vi.spyOn(uow, 'delete').mockImplementation(() => right(null)());
      const spy4 = vi.spyOn(uow, 'commit').mockImplementation(() => right(null)());

      const either = await roomService.destroy(roomID);

      expect(isRight(either)).toBe(true);
      expect(spy1).toHaveBeenCalledOnce();
      expect(spy2).toHaveBeenCalledOnce();
      expect(spy3).toHaveBeenCalledOnce();
      expect(spy4).toHaveBeenCalledOnce();
    });
  });

  describe('getUOW', () => {
    it('should return a unit of work', () => {
      const roomID = Room.ID.from('d986e1f4-af74-453f-8c98-39e8e06b730a');

      const uow = new MockUnitOfWork<AppTypeMap>();
      const spy1 = vi.spyOn(uowFactory, 'forge').mockImplementation(() => uow);
      const spy2 = vi.spyOn(uow, 'registerRepository').mockImplementation(() => null);

      const uow1 = roomService.getUOW(roomID);

      expect(spy1).toHaveBeenCalledOnce();
      expect(spy2).toHaveBeenCalledOnce();

      const uow2 = roomService.getUOW(roomID);

      expect(spy1).toHaveBeenCalledOnce();
      expect(spy2).toHaveBeenCalledOnce();

      expect(uow1).toBe(uow);
      expect(uow1).toBe(uow2);
    });
  });

  describe('withTransaction', () => {
    it('should commit the transaction if the callback returns right', async () => {
      const roomID = Room.ID.from('d986e1f4-af74-453f-8c98-39e8e06b730a');
      const room = mockRoom({
        id: roomID
      });

      const uow = new MockUnitOfWork<AppTypeMap>();
      const spy1 = vi.spyOn(uowFactory, 'forge').mockImplementation(() => uow);
      const spy2 = vi.spyOn(uow, 'registerRepository').mockImplementation(() => null);
      const spy3 = vi.spyOn(uow, 'startTransaction').mockImplementation(() => right(null)());
      const spy4 = vi.spyOn(uow, 'find').mockImplementation(() => right(some(room))());
      const spy5 = vi.spyOn(uow, 'commit').mockImplementation(() => right(null)());
      const spy6 = vi.spyOn(uow, 'rollback').mockImplementation(() => right(null)());

      const either = await roomService.withTransaction(roomID, ({ room: r }) => {
        expect(r).toBe(room);

        return right(null)();
      });

      expect(isRight(either)).toBe(true);
      expect(spy1).toHaveBeenCalledOnce();
      expect(spy2).toHaveBeenCalledOnce();
      expect(spy3).toHaveBeenCalledOnce();
      expect(spy4).toHaveBeenCalledOnce();
      expect(spy5).toHaveBeenCalledOnce();
      expect(spy6).not.toHaveBeenCalled();
    });

    it('should rollback the transaction if the room is not found', async () => {
      const roomID = Room.ID.from('d986e1f4-af74-453f-8c98-39e8e06b730a');

      const uow = new MockUnitOfWork<AppTypeMap>();
      const spy1 = vi.spyOn(uowFactory, 'forge').mockImplementation(() => uow);
      const spy2 = vi.spyOn(uow, 'registerRepository').mockImplementation(() => null);
      const spy3 = vi.spyOn(uow, 'startTransaction').mockImplementation(() => right(null)());
      const spy4 = vi.spyOn(uow, 'find').mockImplementation(() => right(none)());
      const spy5 = vi.spyOn(uow, 'commit').mockImplementation(() => right(null)());
      const spy6 = vi.spyOn(uow, 'rollback').mockImplementation(() => right(null)());

      const either = await roomService.withTransaction(roomID, ({ room: r }) => {
        expect(r).toBeNull();

        return right(null)();
      });

      expect(isLeft(either)).toBe(true);

      if (isRight(either)) {
        throw new Error('This should not happen');
      }

      expect(either.left.error).toBe('RoomError');

      if (either.left.error !== 'RoomError') {
        throw new Error('This should not happen');
      }

      expect(either.left.detail).toBe('NoSuchRoom');
      expect(spy1).toHaveBeenCalledOnce();
      expect(spy2).toHaveBeenCalledOnce();
      expect(spy3).toHaveBeenCalledOnce();
      expect(spy4).toHaveBeenCalledOnce();
      expect(spy5).not.toHaveBeenCalled();
      expect(spy6).toHaveBeenCalledOnce();
    });

    it('should rollback the transaction if the callback returns left', async () => {
      const roomID = Room.ID.from('d986e1f4-af74-453f-8c98-39e8e06b730a');
      const room = mockRoom({
        id: roomID
      });
      const error = createRoomError('NoSuchRoom', '');

      const uow = new MockUnitOfWork<AppTypeMap>();
      const spy1 = vi.spyOn(uowFactory, 'forge').mockImplementation(() => uow);
      const spy2 = vi.spyOn(uow, 'registerRepository').mockImplementation(() => null);
      const spy3 = vi.spyOn(uow, 'startTransaction').mockImplementation(() => right(null)());
      const spy4 = vi.spyOn(uow, 'find').mockImplementation(() => right(some(room))());
      const spy5 = vi.spyOn(uow, 'commit').mockImplementation(() => right(null)());
      const spy6 = vi.spyOn(uow, 'rollback').mockImplementation(() => right(null)());

      const either = await roomService.withTransaction(roomID, ({ room: r }) => {
        expect(r).toBe(room);

        return left(error)();
      });

      expect(isLeft(either)).toBe(true);

      if (isRight(either)) {
        throw new Error('This should not happen');
      }

      expect(either.left).toBe(error);
      expect(spy1).toHaveBeenCalledOnce();
      expect(spy2).toHaveBeenCalledOnce();
      expect(spy3).toHaveBeenCalledOnce();
      expect(spy4).toHaveBeenCalledOnce();
      expect(spy5).not.toHaveBeenCalled();
      expect(spy6).toHaveBeenCalledOnce();
    });
  });
});
