import type { RoleAssignmentRequest } from '@/applications/dtos/Role/RoleAssignmentRequest.js';
import type { ILogger } from '@/lib/Logger/ILogger.js';
import { MockLogger } from '@/lib/Logger/mocks/MockLogger.js';
import type { IRandomGenerator } from '@/lib/Random/interfaces/IRandomGenerator.js';
import { MockRandomGenerator } from '@/lib/Random/mocks/MockRandomGenerator.js';
import { Random } from '@jamashita/steckdose/random';
import { isLeft, isRight } from 'fp-ts/lib/Either.js';
import { RoleAssignmentService } from '../RoleAssignmentService.js';

describe('RoleAssignmentService', () => {
  let random: IRandomGenerator;
  let logger: ILogger;

  beforeEach(() => {
    random = new MockRandomGenerator();
    logger = new MockLogger();
  });

  afterEach(() => {
    vi.clearAllMocks();
  });

  describe('assign', () => {
    it('should return left when the number of slots is less than the number of players', () => {
      const request = {
        atonement: {
          type: 'SINGLE',
          value: 0
        },
        catalyst: {
          type: 'SINGLE',
          value: 0
        },
        coward: {
          type: 'SINGLE',
          value: 0
        },
        hunter: {
          type: 'SINGLE',
          value: 0
        },
        informant: {
          type: 'SINGLE',
          value: 0
        },
        jester: {
          type: 'SINGLE',
          value: 0
        },
        kitsune: {
          type: 'SINGLE',
          value: 0
        },
        knight: {
          type: 'SINGLE',
          value: 0
        },
        medium: {
          type: 'SINGLE',
          value: 0
        },
        monastic: {
          type: 'SINGLE',
          value: 0
        },
        orator: {
          type: 'SINGLE',
          value: 0
        },
        possessed: {
          type: 'SINGLE',
          value: 0
        },
        seer: {
          type: 'SINGLE',
          value: 1
        },
        werewolf: {
          type: 'SINGLE',
          value: 1
        }
      } satisfies RoleAssignmentRequest;

      const spy1 = vi.spyOn(random, 'integer');

      const roleAssignmentService = new RoleAssignmentService(random, logger);

      const either = roleAssignmentService.assign(request, 1);

      expect(isLeft(either)).toBe(true);
      expect(spy1).not.toHaveBeenCalled();
    });

    it('should return right when the number of slots is equal to the number of players', () => {
      const request = {
        atonement: {
          type: 'SINGLE',
          value: 0
        },
        catalyst: {
          type: 'SINGLE',
          value: 0
        },
        coward: {
          type: 'SINGLE',
          value: 0
        },
        hunter: {
          type: 'SINGLE',
          value: 0
        },
        informant: {
          type: 'SINGLE',
          value: 0
        },
        jester: {
          type: 'SINGLE',
          value: 0
        },
        kitsune: {
          type: 'SINGLE',
          value: 0
        },
        knight: {
          type: 'SINGLE',
          value: 0
        },
        medium: {
          type: 'SINGLE',
          value: 0
        },
        monastic: {
          type: 'SINGLE',
          value: 0
        },
        orator: {
          type: 'SINGLE',
          value: 0
        },
        possessed: {
          type: 'SINGLE',
          value: 0
        },
        seer: {
          type: 'SINGLE',
          value: 1
        },
        werewolf: {
          type: 'SINGLE',
          value: 1
        }
      } satisfies RoleAssignmentRequest;

      const spy1 = vi.spyOn(random, 'integer');

      const roleAssignmentService = new RoleAssignmentService(random, logger);

      const either = roleAssignmentService.assign(request, 2);

      expect(isRight(either)).toBe(true);

      if (isLeft(either)) {
        throw new Error('This should not happen');
      }

      const assignment = either.right;

      expect(spy1).not.toHaveBeenCalled();
      expect(assignment).toEqual({
        atonement: 0,
        catalyst: 0,
        coward: 0,
        hunter: 0,
        informant: 0,
        jester: 0,
        kitsune: 0,
        knight: 0,
        medium: 0,
        monastic: 0,
        orator: 0,
        possessed: 0,
        seer: 1,
        villager: 0,
        werewolf: 1
      });
    });

    it('should assign villagers when there is more quota than the number of players', () => {
      const request = {
        atonement: {
          type: 'SINGLE',
          value: 0
        },
        catalyst: {
          type: 'SINGLE',
          value: 0
        },
        coward: {
          type: 'SINGLE',
          value: 0
        },
        hunter: {
          type: 'SINGLE',
          value: 0
        },
        informant: {
          type: 'SINGLE',
          value: 0
        },
        jester: {
          type: 'SINGLE',
          value: 0
        },
        kitsune: {
          type: 'SINGLE',
          value: 0
        },
        knight: {
          type: 'SINGLE',
          value: 0
        },
        medium: {
          type: 'SINGLE',
          value: 0
        },
        monastic: {
          type: 'SINGLE',
          value: 0
        },
        orator: {
          type: 'SINGLE',
          value: 0
        },
        possessed: {
          type: 'SINGLE',
          value: 0
        },
        seer: {
          type: 'SINGLE',
          value: 1
        },
        werewolf: {
          type: 'SINGLE',
          value: 1
        }
      } satisfies RoleAssignmentRequest;

      const spy1 = vi.spyOn(random, 'integer');

      const roleAssignmentService = new RoleAssignmentService(random, logger);

      const either = roleAssignmentService.assign(request, 4);

      expect(isRight(either)).toBe(true);

      if (isLeft(either)) {
        throw new Error('This should not happen');
      }

      const assignment = either.right;

      expect(spy1).not.toHaveBeenCalled();
      expect(assignment).toEqual({
        atonement: 0,
        catalyst: 0,
        coward: 0,
        hunter: 0,
        informant: 0,
        jester: 0,
        kitsune: 0,
        knight: 0,
        medium: 0,
        monastic: 0,
        orator: 0,
        possessed: 0,
        seer: 1,
        villager: 2,
        werewolf: 1
      });
    });

    it('should invoke random.integer when at least one role has a range assignment', () => {
      const request = {
        atonement: {
          type: 'SINGLE',
          value: 0
        },
        catalyst: {
          type: 'SINGLE',
          value: 0
        },
        coward: {
          type: 'SINGLE',
          value: 0
        },
        hunter: {
          type: 'SINGLE',
          value: 0
        },
        informant: {
          type: 'SINGLE',
          value: 0
        },
        jester: {
          type: 'SINGLE',
          value: 0
        },
        kitsune: {
          type: 'SINGLE',
          value: 0
        },
        knight: {
          type: 'SINGLE',
          value: 0
        },
        medium: {
          type: 'SINGLE',
          value: 0
        },
        monastic: {
          type: 'SINGLE',
          value: 0
        },
        orator: {
          type: 'SINGLE',
          value: 0
        },
        possessed: {
          type: 'SINGLE',
          value: 0
        },
        seer: {
          type: 'SINGLE',
          value: 1
        },
        werewolf: {
          type: 'RANGE',
          min: 1,
          max: 2
        }
      } satisfies RoleAssignmentRequest;

      const spy1 = vi.spyOn(random, 'integer').mockImplementation(() => 0);

      const roleAssignmentService = new RoleAssignmentService(random, logger);

      const either = roleAssignmentService.assign(request, 3);

      expect(isRight(either)).toBe(true);

      if (isLeft(either)) {
        throw new Error('This should not happen');
      }

      const assignment = either.right;

      expect(spy1).toHaveBeenCalledOnce();
      expect(assignment).toEqual({
        atonement: 0,
        catalyst: 0,
        coward: 0,
        hunter: 0,
        informant: 0,
        jester: 0,
        kitsune: 0,
        knight: 0,
        medium: 0,
        monastic: 0,
        orator: 0,
        possessed: 0,
        seer: 1,
        villager: 1,
        werewolf: 1
      });
    });

    it('should invoke random.integer when at least one role has a complex assignment', () => {
      const request = {
        atonement: {
          type: 'SINGLE',
          value: 0
        },
        catalyst: {
          type: 'SINGLE',
          value: 0
        },
        coward: {
          type: 'SINGLE',
          value: 0
        },
        hunter: {
          type: 'SINGLE',
          value: 0
        },
        informant: {
          type: 'SINGLE',
          value: 0
        },
        jester: {
          type: 'SINGLE',
          value: 0
        },
        kitsune: {
          type: 'SINGLE',
          value: 0
        },
        knight: {
          type: 'SINGLE',
          value: 0
        },
        medium: {
          type: 'SINGLE',
          value: 0
        },
        monastic: {
          type: 'SINGLE',
          value: 0
        },
        orator: {
          type: 'SINGLE',
          value: 0
        },
        possessed: {
          type: 'SINGLE',
          value: 0
        },
        seer: {
          type: 'SINGLE',
          value: 1
        },
        werewolf: {
          type: 'COMPLEX',
          values: [1, 2]
        }
      } satisfies RoleAssignmentRequest;

      const spy1 = vi.spyOn(random, 'integer').mockImplementation(() => 0);

      const roleAssignmentService = new RoleAssignmentService(random, logger);

      const either = roleAssignmentService.assign(request, 3);

      expect(isRight(either)).toBe(true);

      if (isLeft(either)) {
        throw new Error('This should not happen');
      }

      const assignment = either.right;

      expect(spy1).toHaveBeenCalledOnce();
      expect(assignment).toEqual({
        atonement: 0,
        catalyst: 0,
        coward: 0,
        hunter: 0,
        informant: 0,
        jester: 0,
        kitsune: 0,
        knight: 0,
        medium: 0,
        monastic: 0,
        orator: 0,
        possessed: 0,
        seer: 1,
        villager: 1,
        werewolf: 1
      });
    });

    it('should assign villagers if there is more quota than the number of players', () => {
      const request = {
        atonement: {
          type: 'SINGLE',
          value: 0
        },
        catalyst: {
          type: 'SINGLE',
          value: 0
        },
        coward: {
          type: 'SINGLE',
          value: 0
        },
        hunter: {
          type: 'SINGLE',
          value: 0
        },
        informant: {
          type: 'SINGLE',
          value: 0
        },
        jester: {
          type: 'SINGLE',
          value: 0
        },
        kitsune: {
          type: 'SINGLE',
          value: 0
        },
        knight: {
          type: 'SINGLE',
          value: 0
        },
        medium: {
          type: 'SINGLE',
          value: 0
        },
        monastic: {
          type: 'SINGLE',
          value: 0
        },
        orator: {
          type: 'SINGLE',
          value: 0
        },
        possessed: {
          type: 'SINGLE',
          value: 0
        },
        seer: {
          type: 'SINGLE',
          value: 2
        },
        werewolf: {
          type: 'COMPLEX',
          values: [1, 2]
        }
      } satisfies RoleAssignmentRequest;

      const spy1 = vi.spyOn(random, 'integer').mockImplementation(() => 1);

      const roleAssignmentService = new RoleAssignmentService(random, logger);

      const either = roleAssignmentService.assign(request, 7);

      expect(isRight(either)).toBe(true);

      if (isLeft(either)) {
        throw new Error('This should not happen');
      }

      const assignment = either.right;

      expect(spy1).toHaveBeenCalledOnce();
      expect(assignment).toEqual({
        atonement: 0,
        catalyst: 0,
        coward: 0,
        hunter: 0,
        informant: 0,
        jester: 0,
        kitsune: 0,
        knight: 0,
        medium: 0,
        monastic: 0,
        orator: 0,
        possessed: 0,
        seer: 2,
        villager: 3,
        werewolf: 2
      });
    });

    it.each(Array.from({ length: 100 }, (_, i) => i + 1))(
      'should assign correctly when there are multiple roles with range assignment: #%i attempt',
      () => {
        const request = {
          atonement: {
            type: 'SINGLE',
            value: 0
          },
          catalyst: {
            type: 'SINGLE',
            value: 0
          },
          coward: {
            type: 'SINGLE',
            value: 0
          },
          hunter: {
            type: 'SINGLE',
            value: 0
          },
          informant: {
            type: 'SINGLE',
            value: 0
          },
          jester: {
            type: 'SINGLE',
            value: 0
          },
          kitsune: {
            type: 'SINGLE',
            value: 0
          },
          knight: {
            type: 'SINGLE',
            value: 0
          },
          medium: {
            type: 'SINGLE',
            value: 0
          },
          monastic: {
            type: 'SINGLE',
            value: 0
          },
          orator: {
            type: 'SINGLE',
            value: 0
          },
          possessed: {
            type: 'SINGLE',
            value: 0
          },
          seer: {
            type: 'RANGE',
            min: 2,
            max: 4
          },
          werewolf: {
            type: 'COMPLEX',
            values: [3, 4]
          }
        } satisfies RoleAssignmentRequest;

        const roleAssignmentService = new RoleAssignmentService(random, logger);

        const range: [called: boolean, value: number] = [false, 0];
        const complex: [called: boolean, value: number] = [false, 0];

        const spy1 = vi.spyOn(random, 'integer').mockImplementation((min, max) => {
          if (max === 2) {
            const value = Random.integer(min, max);

            range[0] = true;
            range[1] = value;

            return value;
          }
          if (max === 1) {
            const value = Random.integer(min, max - 1);

            complex[0] = true;
            complex[1] = value;

            return value;
          }

          console.log(max);
          throw new Error('This should not happen');
        });

        const either = roleAssignmentService.assign(request, 9);

        expect(isRight(either)).toBe(true);

        if (isLeft(either)) {
          throw new Error('This should not happen');
        }

        const assignment = either.right;

        expect(spy1).toHaveBeenCalledTimes(2);
        expect(range[0]).toBe(true);
        expect(complex[0]).toBe(true);

        switch (range[1]) {
          case 0: {
            switch (complex[1]) {
              case 0: {
                expect(assignment.seer).toBe(2);
                expect(assignment.villager).toBe(4);
                expect(assignment.werewolf).toBe(3);

                break;
              }
              case 1: {
                expect(assignment.seer).toBe(2);
                expect(assignment.villager).toBe(3);
                expect(assignment.werewolf).toBe(4);

                break;
              }
              default: {
                console.log('illegal complex[1]');
                console.log(complex[1]);

                throw new Error('This should not happen');
              }
            }

            break;
          }
          case 1: {
            switch (complex[1]) {
              case 0: {
                expect(assignment.seer).toBe(3);
                expect(assignment.villager).toBe(3);
                expect(assignment.werewolf).toBe(3);

                break;
              }
              case 1: {
                expect(assignment.seer).toBe(3);
                expect(assignment.villager).toBe(2);
                expect(assignment.werewolf).toBe(4);

                break;
              }
              default: {
                console.log('illegal complex[1]!');
                console.log(complex[1]);

                throw new Error('This should not happen');
              }
            }

            break;
          }
          case 2: {
            switch (complex[1]) {
              case 0: {
                expect(assignment.seer).toBe(4);
                expect(assignment.villager).toBe(2);
                expect(assignment.werewolf).toBe(3);

                break;
              }
              case 1: {
                expect(assignment.seer).toBe(4);
                expect(assignment.villager).toBe(1);
                expect(assignment.werewolf).toBe(4);

                break;
              }
              default: {
                console.log('illegal complex[1]!');
                console.log(complex[1]);

                throw new Error('This should not happen');
              }
            }

            break;
          }
          default: {
            console.log('illegal range[1]');
            console.log(range[1]);

            throw new Error('This should not happen');
          }
        }
      }
    );

    it.each(Array.from({ length: 100 }, (_, i) => i + 1))('should assign villagers when there is not enough divisor assignment: #%i attempt', () => {
      const request = {
        atonement: {
          type: 'SINGLE',
          value: 0
        },
        catalyst: {
          type: 'SINGLE',
          value: 0
        },
        coward: {
          type: 'SINGLE',
          value: 0
        },
        hunter: {
          type: 'SINGLE',
          value: 0
        },
        informant: {
          type: 'SINGLE',
          value: 0
        },
        jester: {
          type: 'SINGLE',
          value: 0
        },
        kitsune: {
          type: 'SINGLE',
          value: 0
        },
        knight: {
          type: 'SINGLE',
          value: 0
        },
        medium: {
          type: 'SINGLE',
          value: 0
        },
        monastic: {
          type: 'SINGLE',
          value: 0
        },
        orator: {
          type: 'SINGLE',
          value: 0
        },
        possessed: {
          type: 'SINGLE',
          value: 0
        },
        seer: {
          type: 'RANGE',
          min: 1,
          max: 6
        },
        werewolf: {
          type: 'RANGE',
          min: 1,
          max: 5
        }
      } satisfies RoleAssignmentRequest;

      const spy1 = vi.spyOn(random, 'integer').mockImplementation((_, max) => max);

      const roleAssignmentService = new RoleAssignmentService(random, logger);

      const either = roleAssignmentService.assign(request, 8);

      expect(isRight(either)).toBe(true);
      expect(spy1).toHaveBeenCalledTimes(2);

      if (isLeft(either)) {
        throw new Error('This should not happen');
      }

      const assignment = either.right;

      if (assignment.seer === 6) {
        expect(assignment).toEqual({
          atonement: 0,
          catalyst: 0,
          coward: 0,
          hunter: 0,
          informant: 0,
          jester: 0,
          kitsune: 0,
          knight: 0,
          medium: 0,
          monastic: 0,
          orator: 0,
          possessed: 0,
          seer: 6,
          villager: 1,
          werewolf: 1
        });
      } else {
        expect(assignment).toEqual({
          atonement: 0,
          catalyst: 0,
          coward: 0,
          hunter: 0,
          informant: 0,
          jester: 0,
          kitsune: 0,
          knight: 0,
          medium: 0,
          monastic: 0,
          orator: 0,
          possessed: 0,
          seer: 1,
          villager: 2,
          werewolf: 5
        });
      }
    });
  });
});
