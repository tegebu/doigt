import type { GatewayError } from '@/adapters/gateways/GatewayError.js';
import type { IUnitOfWork } from '@/applications/unitOfWork/IUnitOfWork.js';
import type { UnitOfWorkError } from '@/applications/unitOfWork/UnitOfWorkError.js';
import type { Room, RoomID } from '@/domains/Room/Room.js';
import type { RoomError } from '@/domains/Room/RoomError.js';
import type { GenericError } from '@/lib/Error/Errors.js';
import type { UnaryFunction } from '@jamashita/anden/type';
import type { Either } from 'fp-ts/lib/Either.js';

export type RoomTypeMap = Readonly<{
  room: {
    id: RoomID;
    entity: Room;
  };
}>;

export type RoomAndUOW = Readonly<{
  room: Room;
  uow: IUnitOfWork<RoomTypeMap>;
}>;

export interface IRoomService {
  cleanup(id: RoomID): Promise<Either<GenericError | GatewayError | UnitOfWorkError, unknown>>;

  destroy(id: RoomID): Promise<Either<GenericError | GatewayError | UnitOfWorkError, unknown>>;

  getUOW(id: RoomID): IUnitOfWork<RoomTypeMap>;

  withTransaction<L, R>(
    id: RoomID,
    proc: UnaryFunction<RoomAndUOW, Promise<Either<L, R>>>
  ): Promise<Either<GenericError | GatewayError | UnitOfWorkError | RoomError | L, R>>;
}
