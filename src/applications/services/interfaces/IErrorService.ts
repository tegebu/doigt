import type { GatewayError } from '@/adapters/gateways/GatewayError.js';
import type { GenericError } from '@/lib/Error/Errors.js';
import { z } from 'zod';

export const MessageErrorSchema = z.object({
  error: z.string(),
  message: z.string()
});
export const DetailMessageErrorSchema = z.object({
  error: z.string(),
  detail: z.string(),
  message: z.string()
});

export type MessageError = Readonly<z.infer<typeof MessageErrorSchema>>;
export type DetailMessageError = Readonly<z.infer<typeof DetailMessageErrorSchema>>;

export interface IErrorService {
  handle(error: GenericError | GatewayError | MessageError | DetailMessageError): void;
}
