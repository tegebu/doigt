import type { RoleAssignmentRequest } from '@/applications/dtos/Role/RoleAssignmentRequest.js';
import type { RoleAssignment } from '@/domains/Role/RoleAssignment.js';
import type { RoleError } from '@/domains/Role/RoleError.js';
import type { Either } from 'fp-ts/lib/Either.js';

export interface IRoleAssignmentService {
  assign(request: RoleAssignmentRequest, quota: number): Either<RoleError, RoleAssignment>;
}
