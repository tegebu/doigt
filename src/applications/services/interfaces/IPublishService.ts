import type { EventPublishError } from '@/adapters/presenters/EventPublisher/EventPublishError.js';
import type { GetPublishEventData, PublishEventType } from '@/applications/websocket/PublishEvents.js';
import type { Client } from '@/domains/Client/Client.js';
import type { ClientError } from '@/domains/Client/ClientError.js';
import type { Person } from '@/domains/Person/Person.js';
import type { Persons } from '@/domains/Person/Persons.js';
import type { Room } from '@/domains/Room/Room.js';
import type { GenericError } from '@/lib/Error/Errors.js';
import type { Either } from 'fp-ts/lib/Either.js';

export interface IPublishService {
  broadcast<T extends PublishEventType>(event: T, data: GetPublishEventData<T>): Promise<Either<GenericError | EventPublishError, unknown>>;

  toClient<T extends PublishEventType>(
    client: Client,
    event: T,
    data: GetPublishEventData<T>
  ): Promise<Either<GenericError | EventPublishError | ClientError, unknown>>;

  toDeceased<T extends PublishEventType>(
    room: Room,
    event: T,
    data: GetPublishEventData<T>
  ): Promise<Either<GenericError | EventPublishError | ClientError, unknown>>;

  toEveryone<T extends PublishEventType>(
    room: Room,
    event: T,
    data: GetPublishEventData<T>
  ): Promise<Either<GenericError | EventPublishError | ClientError, unknown>>;

  toModerator<T extends PublishEventType>(
    room: Room,
    event: T,
    data: GetPublishEventData<T>
  ): Promise<Either<GenericError | EventPublishError | ClientError, unknown>>;

  toPerson<T extends PublishEventType>(
    person: Person,
    event: T,
    data: GetPublishEventData<T>
  ): Promise<Either<GenericError | EventPublishError | ClientError, unknown>>;

  toPersons<T extends PublishEventType>(
    persons: Persons,
    event: T,
    data: GetPublishEventData<T>
  ): Promise<Either<GenericError | EventPublishError | ClientError, unknown>>;

  toSurvivors<T extends PublishEventType>(
    room: Room,
    event: T,
    data: GetPublishEventData<T>
  ): Promise<Either<GenericError | EventPublishError | ClientError, unknown>>;
}
