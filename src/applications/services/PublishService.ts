import type { EventPublishError } from '@/adapters/presenters/EventPublisher/EventPublishError.js';
import type { IEventPublisher } from '@/adapters/presenters/EventPublisher/IEventPublisher.js';
import { Types } from '@/containers/Types.js';
import type { Client } from '@/domains/Client/Client.js';
import type { ClientError } from '@/domains/Client/ClientError.js';
import type { Person } from '@/domains/Person/Person.js';
import type { Persons } from '@/domains/Person/Persons.js';
import { Game } from '@/domains/Room/Game.js';
import type { Room } from '@/domains/Room/Room.js';
import type { GenericError } from '@/lib/Error/Errors.js';
import type { ILogger } from '@/lib/Logger/ILogger.js';
import { Kind } from '@jamashita/anden/type';
import type { Either } from 'fp-ts/lib/Either.js';
import { right } from 'fp-ts/lib/TaskEither.js';
import { inject, injectable } from 'inversify';
import type { GetPublishEventData, PublishEventType } from '../websocket/PublishEvents.js';
import type { IPublishService } from './interfaces/IPublishService.js';

/**
 * TODO メッセージを送れなかった人に対してメッセージをキューイングし、再接続時に送信する
 */
@injectable()
export class PublishService implements IPublishService {
  private readonly eventPublisher: IEventPublisher;
  private readonly logger: ILogger;

  public constructor(@inject(Types.WSEventPublisher) eventPublisher: IEventPublisher, @inject(Types.Logger) logger: ILogger) {
    this.eventPublisher = eventPublisher;
    this.logger = logger;
  }

  public broadcast<T extends PublishEventType>(event: T, data: GetPublishEventData<T>): Promise<Either<GenericError | EventPublishError, unknown>> {
    return this.eventPublisher.broadcast(event, data);
  }

  /**
   * クライアントにイベントを送信します
   *
   * @param client
   * @param event
   * @param data
   */
  public toClient<T extends PublishEventType>(
    client: Client,
    event: T,
    data: GetPublishEventData<T>
  ): Promise<Either<GenericError | EventPublishError | ClientError, unknown>> {
    this.logger.info('PublishService.toClient()');

    return this.eventPublisher.toClient(client.id, event, data);
  }

  /**
   * 死者にイベントを送信します
   *
   * @param room
   * @param event
   * @param data
   */
  public async toDeceased<T extends PublishEventType>(
    room: Room,
    event: T,
    data: GetPublishEventData<T>
  ): Promise<Either<GenericError | EventPublishError | ClientError, unknown>> {
    this.logger.info('PublishService.toDeceased()');

    const deceased = Game.getDeceased(room.game);

    return this.toPersons(deceased, event, data);
  }

  /**
   * moderatorを含めた全員にイベントを送信します
   *
   * @param room
   * @param event
   * @param data
   */
  public async toEveryone<T extends PublishEventType>(
    room: Room,
    event: T,
    data: GetPublishEventData<T>
  ): Promise<Either<GenericError | EventPublishError | ClientError, unknown>> {
    this.logger.info('PublishService.toDeceased()');

    const ids = [...room.participants.values()].map((c: Client) => {
      return c.id;
    });

    return this.eventPublisher.toClients(ids, event, data);
  }

  /**
   * moderatorがいる場合、イベントを送信します
   *
   * @param room
   * @param event
   * @param data
   */
  public async toModerator<T extends PublishEventType>(
    room: Room,
    event: T,
    data: GetPublishEventData<T>
  ): Promise<Either<GenericError | EventPublishError | ClientError, unknown>> {
    this.logger.info('PublishService.toModerator()');

    // moderatorがいない場合は何もしない
    if (Kind.isNull(room.moderator)) {
      return right(null)();
    }

    return this.eventPublisher.toClient(room.moderator.id, event, data);
  }

  /**
   * 人物にイベントを送信します
   * 生死を問いません
   *
   * @param person
   * @param event
   * @param data
   */
  public async toPerson<T extends PublishEventType>(
    person: Person,
    event: T,
    data: GetPublishEventData<T>
  ): Promise<Either<GenericError | EventPublishError | ClientError, unknown>> {
    this.logger.info('PublishService.toPerson()');

    return this.eventPublisher.toClient(person.client.id, event, data);
  }

  /**
   * 人物にイベントを送信します
   * 生死を問いません
   *
   * @param persons
   * @param event
   * @param data
   */
  public async toPersons<T extends PublishEventType>(
    persons: Persons,
    event: T,
    data: GetPublishEventData<T>
  ): Promise<Either<GenericError | EventPublishError | ClientError, unknown>> {
    this.logger.info('PublishService.toPerson()');

    const ids = [...persons.values()].map((p: Person) => {
      return p.client.id;
    });

    return this.eventPublisher.toClients(ids, event, data);
  }

  /**
   * 生存者にイベントを送信します
   *
   * @param room
   * @param event
   * @param data
   */
  public async toSurvivors<T extends PublishEventType>(
    room: Room,
    event: T,
    data: GetPublishEventData<T>
  ): Promise<Either<GenericError | EventPublishError | ClientError, unknown>> {
    this.logger.info('PublishService.toSurvivors()');

    const survivors = Game.getSurvivors(room.game);

    return this.toPersons(survivors, event, data);
  }
}
