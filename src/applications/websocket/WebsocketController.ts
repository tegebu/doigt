import type { RuntimeError as RunError } from '@/lib/Error/Errors.js';
import { WSEvent } from '@/lib/Event/WSEvent.js';
import type { ILogger } from '@/lib/Logger/ILogger.js';
import { RuntimeError } from '@jamashita/anden/error';
import { Kind, type ObjectLiteral, type UnaryFunction } from '@jamashita/anden/type';
import type { Either } from 'fp-ts/lib/Either.js';
import { tryCatch as eTryCatch } from 'fp-ts/lib/Either.js';
import { pipe } from 'fp-ts/lib/function.js';
import { bindW, Do, fromEither, map, mapError } from 'fp-ts/lib/TaskEither.js';
import type { WebSocket } from 'ws';
import { ErrorService } from '../services/ErrorService.js';
import type { IErrorService } from '../services/interfaces/IErrorService.js';
import { type GetSubmitEventData, SubmitEvent, type SubmitEventType } from './SubmitEvents.js';

type EmitArgs = Readonly<{
  obj: ObjectLiteral;
  ws: WebSocket;
}>;
type RouterArgs<T extends SubmitEventType> = Readonly<{
  data: GetSubmitEventData<T>;
  ws: WebSocket;
}>;
type RouterCallback<T extends SubmitEventType> = UnaryFunction<RouterArgs<T>, Promise<Either<unknown, unknown>>>;

export class WebsocketController {
  private readonly routers: Map<SubmitEventType, RouterCallback<SubmitEventType>>;
  private readonly errorService: IErrorService;

  public static empty(logger: ILogger): WebsocketController {
    return WebsocketController.of(new Map(), logger);
  }

  public static of(routers: ReadonlyMap<SubmitEventType, RouterCallback<SubmitEventType>>, logger: ILogger): WebsocketController {
    return new WebsocketController(new Map(routers), logger);
  }

  protected constructor(routers: Map<SubmitEventType, RouterCallback<SubmitEventType>>, logger: ILogger) {
    this.routers = routers;
    this.errorService = new ErrorService(logger);
  }

  public emit({ obj, ws }: EmitArgs): Promise<Either<unknown, unknown>> {
    return pipe(
      Do,
      bindW('event', () => fromEither(WSEvent.parse(obj))),
      bindW('submitEvent', ({ event }) => {
        return pipe(
          eTryCatch(
            () => {
              if (SubmitEvent.is(event.type as SubmitEventType, event.data)) {
                return event as SubmitEvent;
              }

              throw new RuntimeError('');
            },
            () => {
              return {
                error: 'RuntimeError',
                message: `event.typeがSubmitEventに適合しません: ${event.type}`
              } satisfies RunError;
            }
          ),
          fromEither
        );
      }),
      map(({ submitEvent }) => {
        const cb = this.routers.get(submitEvent.type);

        if (Kind.isUndefined(cb)) {
          return {
            error: 'RuntimeError',
            message: `購読されていません: ${submitEvent.type}`
          } satisfies RunError;
        }

        return cb({
          data: submitEvent.data,
          ws
        });
      }),
      mapError((e) => {
        this.errorService.handle(e);

        return e;
      })
    )();
  }

  public merge(...controllers: ReadonlyArray<WebsocketController>): void {
    for (const c of controllers) {
      c.routers.forEach((cb, event) => {
        this.subscribe(event, cb);
      });
    }
  }

  public subscribe<T extends SubmitEventType>(event: T, cb: RouterCallback<T>): void {
    if (this.routers.has(event)) {
      throw new RuntimeError(`Event ${event} is already subscribed.`);
    }

    this.routers.set(event, cb as unknown as RouterCallback<SubmitEventType>);
  }
}
