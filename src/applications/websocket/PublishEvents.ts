import { Client } from '@/domains/Client/Client.js';
import { Team } from '@/domains/Identity/Team.js';
import { InformantOutcome } from '@/domains/Outcome/InformantOutcome.js';
import { MediumOutcome } from '@/domains/Outcome/MediumOutcome.js';
import { SeerOutcome } from '@/domains/Outcome/SeerOutcome.js';
import { VictimOutcome } from '@/domains/Outcome/VictimOutcome.js';
import { Person } from '@/domains/Person/Person.js';
import { DailyOutcomeRecord } from '@/domains/Room/DailyOutcomeRecord.js';
import { GameStatus } from '@/domains/Room/GameStatus.js';
import { Room as RoomDomain } from '@/domains/Room/Room.js';
import { ExhaustiveError } from '@jamashita/anden/error';
import { z } from 'zod';

export const PUBLISH = {
  PONG: 'XiangXiang',
  ERROR: 'UsmanJiao',
  CONNECTION: {
    CONNECTED: {
      SUCCESS: 'SergeySardar',
      FAILURE: 'KhalidFerreira'
    },
    DISCONNECTED: 'ElenaKaur',
    RECONNECTED: {
      SUCCESS: 'FranciscaKone',
      FAILURE: 'JaimeHao'
    }
  },
  ROOM: {
    CREATE: {
      SUCCESS: 'ChenDelgado',
      FAILURE: 'AlanAli'
    },
    DESTROY: {
      SUCCESS: 'JianhuaSaid',
      FAILURE: 'MamadouAbdel'
    },
    JOIN: {
      SUCCESS: 'SimonVazquez',
      FAILURE: 'AntonioMoore'
    },
    LEAVE: {
      SUCCESS: 'LongSaad',
      FAILURE: 'NykolaiBegum'
    },
    PARTICIPANT: {
      JOINED: 'AngelLima',
      LEFT: 'StevenWilson'
    },
    DISBAND: 'AndreyMejia',
    LIST: {
      SUCCESS: 'NurSolomon',
      FAILURE: 'NataliaXia'
    },
    NOTIFY: {
      CREATED: 'MarkMahato',
      DESTROYED: 'OscarKaur',
      JOINED: 'XiaolingAraujo',
      LEFT: 'PabloPradhan',
      STATUS_CHANGED: 'MahdiMartins'
    },
    FETCH: 'NadiaChang'
  },
  GAME: {
    START: 'MercyKumar',
    OVER: 'YuliyaSardar'
  },
  ROLE: {
    ASSIGNED: 'MosesMondal',
    REVEAL_ALL: 'XiaofengNiu'
  },
  MORNING: {
    START: 'IrinaMajhi',
    END: 'CharlesLeon'
  },
  DAY: {
    START: 'ValentinaAkhtar',
    END: 'GitaSo'
  },
  EVENING: {
    START: 'MunniRashid',
    END: 'YolandadeJesus'
  },
  NIGHT: {
    START: 'NikolayMoore',
    END: 'KhaledKarim'
  },
  RESIDENT: {
    LIST: 'JenniferTakahashi'
  },
  VICTIMIZED: 'AshaSah',
  LYNCHED: {
    NONE: 'UrmilaSoares',
    SOME: 'SalehLeon'
  },
  VOTE: {
    SET: {
      SUCCESS: 'LihuaRahman',
      FAILURE: 'NykolaiJiang'
    },
    PROMPT: 'FelixHassan'
  },
  ATTACK: {
    SET: {
      SUCCESS: 'EdgarParamar',
      FAILURE: 'AnthonySolanki'
    },
    PROMPT: 'EvaHaji'
  },
  ACKNOWLEDGE: {
    OUTCOME: 'UshaDelgado'
  },
  DIVINATION: {
    SET: {
      SUCCESS: 'MohammadDias',
      FAILURE: 'MoussaAraujo'
    },
    OUTCOME: 'DeniseBarry',
    PROMPT: 'ZainabAdam'
  },
  CHANNEL: {
    OUTCOME: 'SalehGong'
  },
  PROTECTION: {
    SET: {
      SUCCESS: 'AnastasiyaShi',
      FAILURE: 'GabrielaKim'
    },
    PROMPT: 'VictorMondal'
  },
  EVACUATION: {
    SET: {
      SUCCESS: 'SavitriGeng',
      FAILURE: 'MichelYue'
    },
    PROMPT: 'YuYan'
  }
} as const;

export namespace PublishEvent {
  export namespace Pong {
    export const DATA_SCHEMA = z.object({
      message: z.string()
    });
    export const SCHEMA = z.object({
      type: z.literal(PUBLISH.PONG),
      data: DATA_SCHEMA
    });

    export type Event = Readonly<z.infer<typeof SCHEMA>>;
  }

  export namespace Connection {
    export namespace Connected {
      export namespace Success {
        export const DATA_SCHEMA = z.object({});
        export const SCHEMA = z.object({
          type: z.literal(PUBLISH.CONNECTION.CONNECTED.SUCCESS),
          data: DATA_SCHEMA
        });

        export type Event = Readonly<z.infer<typeof SCHEMA>>;
      }

      export namespace Failure {
        export const DATA_SCHEMA = z.object({});
        export const SCHEMA = z.object({
          type: z.literal(PUBLISH.CONNECTION.CONNECTED.FAILURE),
          data: DATA_SCHEMA
        });

        export type Event = Readonly<z.infer<typeof SCHEMA>>;
      }
    }

    export namespace Disconnected {
      export const DATA_SCHEMA = z.object({});
      export const SCHEMA = z.object({
        type: z.literal(PUBLISH.CONNECTION.DISCONNECTED),
        data: DATA_SCHEMA
      });

      export type Event = Readonly<z.infer<typeof SCHEMA>>;
    }

    export namespace Reconnected {
      export namespace Success {
        export const DATA_SCHEMA = z.object({});
        export const SCHEMA = z.object({
          type: z.literal(PUBLISH.CONNECTION.RECONNECTED.SUCCESS),
          data: DATA_SCHEMA
        });

        export type Event = Readonly<z.infer<typeof SCHEMA>>;
      }

      export namespace Failure {
        export const DATA_SCHEMA = z.object({});
        export const SCHEMA = z.object({
          type: z.literal(PUBLISH.CONNECTION.RECONNECTED.FAILURE),
          data: DATA_SCHEMA
        });

        export type Event = Readonly<z.infer<typeof SCHEMA>>;
      }
    }
  }

  export namespace Room {
    export namespace Create {
      export namespace Success {
        export const DATA_SCHEMA = z.object({
          roomID: RoomDomain.ID.SCHEMA
        });
        export const SCHEMA = z.object({
          type: z.literal(PUBLISH.ROOM.CREATE.SUCCESS),
          data: DATA_SCHEMA
        });

        export type Event = Readonly<z.infer<typeof SCHEMA>>;
      }

      export namespace Failure {
        export const DATA_SCHEMA = z.object({});
        export const SCHEMA = z.object({
          type: z.literal(PUBLISH.ROOM.CREATE.FAILURE),
          data: DATA_SCHEMA
        });

        export type Event = Readonly<z.infer<typeof SCHEMA>>;
      }
    }

    export namespace Destroy {
      export namespace Success {
        export const DATA_SCHEMA = z.object({});
        export const SCHEMA = z.object({
          type: z.literal(PUBLISH.ROOM.DESTROY.SUCCESS),
          data: DATA_SCHEMA
        });

        export type Event = Readonly<z.infer<typeof SCHEMA>>;
      }

      export namespace Failure {
        export const DATA_SCHEMA = z.object({});
        export const SCHEMA = z.object({
          type: z.literal(PUBLISH.ROOM.DESTROY.FAILURE),
          data: DATA_SCHEMA
        });

        export type Event = Readonly<z.infer<typeof SCHEMA>>;
      }
    }

    export namespace Join {
      export namespace Success {
        export const DATA_SCHEMA = z.object({
          roomID: RoomDomain.ID.SCHEMA,
          moderatorID: Client.ID.SCHEMA.nullable()
        });
        export const SCHEMA = z.object({
          type: z.literal(PUBLISH.ROOM.JOIN.SUCCESS),
          data: DATA_SCHEMA
        });

        export type Event = Readonly<z.infer<typeof SCHEMA>>;
      }

      export namespace Failure {
        export const DATA_SCHEMA = z.object({
          cause: z.union([
            z.literal('FULL'),
            z.literal('NOT_FOUND'),
            z.literal('PASSWORD_MISMATCH'),
            z.literal('ALREADY_JOINED'),
            z.literal('NOT_OPEN')
          ])
        });
        export const SCHEMA = z.object({
          type: z.literal(PUBLISH.ROOM.JOIN.FAILURE),
          data: DATA_SCHEMA
        });

        export type Event = Readonly<z.infer<typeof SCHEMA>>;
      }
    }

    export namespace Leave {
      export namespace Success {
        export const DATA_SCHEMA = z.object({});
        export const SCHEMA = z.object({
          type: z.literal(PUBLISH.ROOM.LEAVE.SUCCESS),
          data: DATA_SCHEMA
        });

        export type Event = Readonly<z.infer<typeof SCHEMA>>;
      }

      export namespace Failure {
        export const DATA_SCHEMA = z.object({
          cause: z.union([z.literal('NOT_FOUND'), z.literal('NOT_JOINED'), z.literal('MODERATOR_CANNOT_LEAVE')])
        });
        export const SCHEMA = z.object({
          type: z.literal(PUBLISH.ROOM.LEAVE.FAILURE),
          data: DATA_SCHEMA
        });

        export type Event = Readonly<z.infer<typeof SCHEMA>>;
      }
    }

    export namespace Participant {
      export namespace Joined {
        export const DATA_SCHEMA = z.object({
          client: Client.SCHEMA
        });
        export const SCHEMA = z.object({
          type: z.literal(PUBLISH.ROOM.PARTICIPANT.JOINED),
          data: DATA_SCHEMA
        });

        export type Event = Readonly<z.infer<typeof SCHEMA>>;
      }

      export namespace Left {
        export const DATA_SCHEMA = z.object({
          clientID: Client.ID.SCHEMA
        });
        export const SCHEMA = z.object({
          type: z.literal(PUBLISH.ROOM.PARTICIPANT.LEFT),
          data: DATA_SCHEMA
        });

        export type Event = Readonly<z.infer<typeof SCHEMA>>;
      }
    }

    export namespace Disband {
      export const DATA_SCHEMA = z.object({});
      export const SCHEMA = z.object({
        type: z.literal(PUBLISH.ROOM.DISBAND),
        data: DATA_SCHEMA
      });

      export type Event = Readonly<z.infer<typeof SCHEMA>>;
    }

    export namespace List {
      export namespace Success {
        export const DATA_SCHEMA = z.object({
          rooms: z.array(RoomDomain.List.SCHEMA)
        });
        export const SCHEMA = z.object({
          type: z.literal(PUBLISH.ROOM.LIST.SUCCESS),
          data: DATA_SCHEMA
        });

        export type Event = Readonly<z.infer<typeof SCHEMA>>;
      }

      export namespace Failure {
        export const DATA_SCHEMA = z.object({});
        export const SCHEMA = z.object({
          type: z.literal(PUBLISH.ROOM.LIST.FAILURE),
          data: DATA_SCHEMA
        });

        export type Event = Readonly<z.infer<typeof SCHEMA>>;
      }
    }

    export namespace Notify {
      export namespace Created {
        export const DATA_SCHEMA = z.object({
          room: RoomDomain.List.SCHEMA
        });
        export const SCHEMA = z.object({
          type: z.literal(PUBLISH.ROOM.NOTIFY.CREATED),
          data: DATA_SCHEMA
        });

        export type Event = Readonly<z.infer<typeof SCHEMA>>;
      }

      export namespace Destroyed {
        export const DATA_SCHEMA = z.object({
          roomID: RoomDomain.ID.SCHEMA
        });
        export const SCHEMA = z.object({
          type: z.literal(PUBLISH.ROOM.NOTIFY.DESTROYED),
          data: DATA_SCHEMA
        });

        export type Event = Readonly<z.infer<typeof SCHEMA>>;
      }

      export namespace Joined {
        export const DATA_SCHEMA = z.object({
          roomID: RoomDomain.ID.SCHEMA
        });
        export const SCHEMA = z.object({
          type: z.literal(PUBLISH.ROOM.NOTIFY.JOINED),
          data: DATA_SCHEMA
        });

        export type Event = Readonly<z.infer<typeof SCHEMA>>;
      }

      export namespace Left {
        export const DATA_SCHEMA = z.object({
          roomID: RoomDomain.ID.SCHEMA
        });
        export const SCHEMA = z.object({
          type: z.literal(PUBLISH.ROOM.NOTIFY.LEFT),
          data: DATA_SCHEMA
        });

        export type Event = Readonly<z.infer<typeof SCHEMA>>;
      }

      export namespace StatusChanged {
        export const DATA_SCHEMA = z.object({
          roomID: RoomDomain.ID.SCHEMA,
          status: GameStatus.SCHEMA
        });
        export const SCHEMA = z.object({
          type: z.literal(PUBLISH.ROOM.NOTIFY.STATUS_CHANGED),
          data: DATA_SCHEMA
        });

        export type Event = Readonly<z.infer<typeof SCHEMA>>;
      }
    }

    export namespace Fetch {
      export const DATA_SCHEMA = z.object({
        room: RoomDomain.SCHEMA
      });
      export const SCHEMA = z.object({
        type: z.literal(PUBLISH.ROOM.FETCH),
        data: DATA_SCHEMA
      });

      export type Event = Readonly<z.infer<typeof SCHEMA>>;
    }
  }

  export namespace Game {
    export namespace Start {
      export const DATA_SCHEMA = z.object({});
      export const SCHEMA = z.object({
        type: z.literal(PUBLISH.GAME.START),
        data: DATA_SCHEMA
      });

      export type Event = Readonly<z.infer<typeof SCHEMA>>;
    }

    export namespace Over {
      export const DATA_SCHEMA = z.object({
        teams: z.array(Team.SCHEMA),
        records: z.array(DailyOutcomeRecord.SCHEMA)
      });
      export const SCHEMA = z.object({
        type: z.literal(PUBLISH.GAME.OVER),
        data: DATA_SCHEMA
      });

      export type Event = Readonly<z.infer<typeof SCHEMA>>;
    }
  }

  export namespace Role {
    export namespace Assigned {
      export const DATA_SCHEMA = z.object({
        you: Person.SCHEMA,
        others: z.array(Person.Base.SCHEMA)
      });
      export const SCHEMA = z.object({
        type: z.literal(PUBLISH.ROLE.ASSIGNED),
        data: DATA_SCHEMA
      });

      export type Event = Readonly<z.infer<typeof SCHEMA>>;
    }

    export namespace RevealAll {
      export const DATA_SCHEMA = z.object({
        residents: z.array(Person.SCHEMA)
      });
      export const SCHEMA = z.object({
        type: z.literal(PUBLISH.ROLE.REVEAL_ALL),
        data: DATA_SCHEMA
      });

      export type Event = Readonly<z.infer<typeof SCHEMA>>;
    }
  }

  export namespace Morning {
    export namespace Start {
      export const DATA_SCHEMA = z.object({
        seconds: z.number()
      });
      export const SCHEMA = z.object({
        type: z.literal(PUBLISH.MORNING.START),
        data: DATA_SCHEMA
      });

      export type Event = Readonly<z.infer<typeof SCHEMA>>;
    }

    export namespace End {
      export const DATA_SCHEMA = z.object({});
      export const SCHEMA = z.object({
        type: z.literal(PUBLISH.MORNING.END),
        data: DATA_SCHEMA
      });

      export type Event = Readonly<z.infer<typeof SCHEMA>>;
    }
  }

  export namespace Day {
    export namespace Start {
      export const DATA_SCHEMA = z.object({
        seconds: z.number()
      });
      export const SCHEMA = z.object({
        type: z.literal(PUBLISH.DAY.START),
        data: DATA_SCHEMA
      });

      export type Event = Readonly<z.infer<typeof SCHEMA>>;
    }

    export namespace End {
      export const DATA_SCHEMA = z.object({});
      export const SCHEMA = z.object({
        type: z.literal(PUBLISH.DAY.END),
        data: DATA_SCHEMA
      });

      export type Event = Readonly<z.infer<typeof SCHEMA>>;
    }
  }

  export namespace Evening {
    export namespace Start {
      export const DATA_SCHEMA = z.object({
        seconds: z.number()
      });
      export const SCHEMA = z.object({
        type: z.literal(PUBLISH.EVENING.START),
        data: DATA_SCHEMA
      });

      export type Event = Readonly<z.infer<typeof SCHEMA>>;
    }

    export namespace End {
      export const DATA_SCHEMA = z.object({});
      export const SCHEMA = z.object({
        type: z.literal(PUBLISH.EVENING.END),
        data: DATA_SCHEMA
      });

      export type Event = Readonly<z.infer<typeof SCHEMA>>;
    }
  }

  export namespace Night {
    export namespace Start {
      export const DATA_SCHEMA = z.object({
        seconds: z.number()
      });
      export const SCHEMA = z.object({
        type: z.literal(PUBLISH.NIGHT.START),
        data: DATA_SCHEMA
      });

      export type Event = Readonly<z.infer<typeof SCHEMA>>;
    }

    export namespace End {
      export const DATA_SCHEMA = z.object({});
      export const SCHEMA = z.object({
        type: z.literal(PUBLISH.NIGHT.END),
        data: DATA_SCHEMA
      });

      export type Event = Readonly<z.infer<typeof SCHEMA>>;
    }
  }

  export namespace Resident {
    export namespace List {
      export const DATA_SCHEMA = z.object({
        survivors: z.array(Person.ID.SCHEMA),
        deceased: z.array(Person.ID.SCHEMA)
      });
      export const SCHEMA = z.object({
        type: z.literal(PUBLISH.RESIDENT.LIST),
        data: DATA_SCHEMA
      });

      export type Event = Readonly<z.infer<typeof SCHEMA>>;
    }
  }

  export namespace Victimized {
    export const DATA_SCHEMA = z.object({
      outcome: VictimOutcome.SCHEMA
    });
    export const SCHEMA = z.object({
      type: z.literal(PUBLISH.VICTIMIZED),
      data: DATA_SCHEMA
    });

    export type Event = Readonly<z.infer<typeof SCHEMA>>;
  }

  export namespace Lynched {
    export namespace None {
      export const DATA_SCHEMA = z.object({});
      export const SCHEMA = z.object({
        type: z.literal(PUBLISH.LYNCHED.NONE),
        data: DATA_SCHEMA
      });

      export type Event = Readonly<z.infer<typeof SCHEMA>>;
    }

    export namespace Some {
      export const DATA_SCHEMA = z.object({
        victimID: Person.ID.SCHEMA
      });
      export const SCHEMA = z.object({
        type: z.literal(PUBLISH.LYNCHED.SOME),
        data: DATA_SCHEMA
      });

      export type Event = Readonly<z.infer<typeof SCHEMA>>;
    }
  }

  export namespace Vote {
    export namespace Set {
      export namespace Success {
        export const DATA_SCHEMA = z.object({
          sourceID: Person.ID.SCHEMA,
          destinationID: Person.ID.SCHEMA
        });
        export const SCHEMA = z.object({
          type: z.literal(PUBLISH.VOTE.SET.SUCCESS),
          data: DATA_SCHEMA
        });

        export type Event = Readonly<z.infer<typeof SCHEMA>>;
      }

      export namespace Failure {
        export const DATA_SCHEMA = z.object({
          cause: z
            .union([z.literal('SOURCE_DEAD'), z.literal('DESTINATION_DEAD'), z.literal('UNACCEPTABLE_PHASE'), z.literal('UNACCEPTABLE_ROLE')])
            .nullable(),
          sourceID: Person.ID.SCHEMA,
          destinationID: Person.ID.SCHEMA
        });
        export const SCHEMA = z.object({
          type: z.literal(PUBLISH.VOTE.SET.FAILURE),
          data: DATA_SCHEMA
        });

        export type Event = Readonly<z.infer<typeof SCHEMA>>;
      }
    }

    export namespace Prompt {
      export const DATA_SCHEMA = z.object({
        candidates: z.array(Person.ID.SCHEMA)
      });
      export const SCHEMA = z.object({
        type: z.literal(PUBLISH.VOTE.PROMPT),
        data: DATA_SCHEMA
      });

      export type Event = Readonly<z.infer<typeof SCHEMA>>;
    }
  }

  export namespace Acknowledge {
    export namespace Outcome {
      export const DATA_SCHEMA = z.object({
        outcome: InformantOutcome.SCHEMA
      });
      export const SCHEMA = z.object({
        type: z.literal(PUBLISH.ACKNOWLEDGE.OUTCOME),
        data: DATA_SCHEMA
      });

      export type Event = Readonly<z.infer<typeof SCHEMA>>;
    }
  }

  export namespace Attack {
    export namespace Set {
      export namespace Success {
        export const DATA_SCHEMA = z.object({
          sourceID: Person.ID.SCHEMA,
          destinationID: Person.ID.SCHEMA
        });
        export const SCHEMA = z.object({
          type: z.literal(PUBLISH.ATTACK.SET.SUCCESS),
          data: DATA_SCHEMA
        });

        export type Event = Readonly<z.infer<typeof SCHEMA>>;
      }

      export namespace Failure {
        export const DATA_SCHEMA = z.object({
          cause: z
            .union([z.literal('SOURCE_DEAD'), z.literal('DESTINATION_DEAD'), z.literal('UNACCEPTABLE_PHASE'), z.literal('UNACCEPTABLE_ROLE')])
            .nullable(),
          sourceID: Person.ID.SCHEMA,
          destinationID: Person.ID.SCHEMA
        });
        export const SCHEMA = z.object({
          type: z.literal(PUBLISH.ATTACK.SET.FAILURE),
          data: DATA_SCHEMA
        });

        export type Event = Readonly<z.infer<typeof SCHEMA>>;
      }
    }

    export namespace Prompt {
      export const DATA_SCHEMA = z.object({
        candidates: z.array(Person.ID.SCHEMA)
      });
      export const SCHEMA = z.object({
        type: z.literal(PUBLISH.ATTACK.PROMPT),
        data: DATA_SCHEMA
      });

      export type Event = Readonly<z.infer<typeof SCHEMA>>;
    }
  }

  export namespace Divination {
    export namespace Set {
      export namespace Success {
        export const DATA_SCHEMA = z.object({
          sourceID: Person.ID.SCHEMA,
          destinationID: Person.ID.SCHEMA
        });
        export const SCHEMA = z.object({
          type: z.literal(PUBLISH.DIVINATION.SET.SUCCESS),
          data: DATA_SCHEMA
        });

        export type Event = Readonly<z.infer<typeof SCHEMA>>;
      }

      export namespace Failure {
        export const DATA_SCHEMA = z.object({
          cause: z
            .union([z.literal('SOURCE_DEAD'), z.literal('DESTINATION_DEAD'), z.literal('UNACCEPTABLE_PHASE'), z.literal('UNACCEPTABLE_ROLE')])
            .nullable(),
          sourceID: Person.ID.SCHEMA,
          destinationID: Person.ID.SCHEMA
        });
        export const SCHEMA = z.object({
          type: z.literal(PUBLISH.DIVINATION.SET.FAILURE),
          data: DATA_SCHEMA
        });

        export type Event = Readonly<z.infer<typeof SCHEMA>>;
      }
    }

    export namespace Outcome {
      export const DATA_SCHEMA = z.object({
        outcome: SeerOutcome.SCHEMA
      });
      export const SCHEMA = z.object({
        type: z.literal(PUBLISH.DIVINATION.OUTCOME),
        data: DATA_SCHEMA
      });

      export type Event = Readonly<z.infer<typeof SCHEMA>>;
    }

    export namespace Prompt {
      export const DATA_SCHEMA = z.object({
        candidates: z.array(Person.ID.SCHEMA)
      });
      export const SCHEMA = z.object({
        type: z.literal(PUBLISH.DIVINATION.PROMPT),
        data: DATA_SCHEMA
      });

      export type Event = Readonly<z.infer<typeof PublishEvent.Divination.Prompt.SCHEMA>>;
    }
  }

  export namespace Channel {
    export namespace Outcome {
      export const DATA_SCHEMA = z.object({
        outcome: MediumOutcome.SCHEMA
      });
      export const SCHEMA = z.object({
        type: z.literal(PUBLISH.CHANNEL.OUTCOME),
        data: DATA_SCHEMA
      });

      export type Event = Readonly<z.infer<typeof SCHEMA>>;
    }
  }

  export namespace Protection {
    export namespace Set {
      export namespace Success {
        export const DATA_SCHEMA = z.object({
          sourceID: Person.ID.SCHEMA,
          destinationID: Person.ID.SCHEMA
        });
        export const SCHEMA = z.object({
          type: z.literal(PUBLISH.PROTECTION.SET.SUCCESS),
          data: DATA_SCHEMA
        });

        export type Event = Readonly<z.infer<typeof SCHEMA>>;
      }

      export namespace Failure {
        export const DATA_SCHEMA = z.object({
          cause: z
            .union([z.literal('SOURCE_DEAD'), z.literal('DESTINATION_DEAD'), z.literal('UNACCEPTABLE_PHASE'), z.literal('UNACCEPTABLE_ROLE')])
            .nullable(),
          sourceID: Person.ID.SCHEMA,
          destinationID: Person.ID.SCHEMA
        });
        export const SCHEMA = z.object({
          type: z.literal(PUBLISH.PROTECTION.SET.FAILURE),
          data: DATA_SCHEMA
        });

        export type Event = Readonly<z.infer<typeof SCHEMA>>;
      }
    }

    export namespace Prompt {
      export const DATA_SCHEMA = z.object({
        candidates: z.array(Person.ID.SCHEMA)
      });
      export const SCHEMA = z.object({
        type: z.literal(PUBLISH.PROTECTION.PROMPT),
        data: DATA_SCHEMA
      });

      export type Event = Readonly<z.infer<typeof SCHEMA>>;
    }
  }

  export namespace Evacuation {
    export namespace Set {
      export namespace Success {
        export const DATA_SCHEMA = z.object({});
        export const SCHEMA = z.object({
          type: z.literal(PUBLISH.EVACUATION.SET.SUCCESS),
          data: DATA_SCHEMA
        });

        export type Event = Readonly<z.infer<typeof SCHEMA>>;
      }

      export namespace Failure {
        export const DATA_SCHEMA = z.object({
          cause: z
            .union([z.literal('SOURCE_DEAD'), z.literal('DESTINATION_DEAD'), z.literal('UNACCEPTABLE_PHASE'), z.literal('UNACCEPTABLE_ROLE')])
            .nullable(),
          sourceID: Person.ID.SCHEMA,
          destinationID: Person.ID.SCHEMA
        });
        export const SCHEMA = z.object({
          type: z.literal(PUBLISH.EVACUATION.SET.FAILURE),
          data: DATA_SCHEMA
        });

        export type Event = Readonly<z.infer<typeof SCHEMA>>;
      }
    }

    export namespace Prompt {
      export const DATA_SCHEMA = z.object({
        candidates: z.array(Person.ID.SCHEMA)
      });
      export const SCHEMA = z.object({
        type: z.literal(PUBLISH.EVACUATION.PROMPT),
        data: DATA_SCHEMA
      });

      export type Event = Readonly<z.infer<typeof SCHEMA>>;
    }
  }

  export const is = <T extends PublishEventType>(event: T, data: unknown): data is GetPublishEventData<T> => {
    switch (event) {
      case PUBLISH.PONG: {
        return PublishEvent.Pong.DATA_SCHEMA.safeParse(data).success;
      }
      case PUBLISH.CONNECTION.CONNECTED.SUCCESS: {
        return PublishEvent.Connection.Connected.Success.DATA_SCHEMA.safeParse(data).success;
      }
      case PUBLISH.CONNECTION.CONNECTED.FAILURE: {
        return PublishEvent.Connection.Connected.Failure.DATA_SCHEMA.safeParse(data).success;
      }
      case PUBLISH.CONNECTION.DISCONNECTED: {
        return PublishEvent.Connection.Disconnected.DATA_SCHEMA.safeParse(data).success;
      }
      case PUBLISH.CONNECTION.RECONNECTED.SUCCESS: {
        return PublishEvent.Connection.Reconnected.Success.DATA_SCHEMA.safeParse(data).success;
      }
      case PUBLISH.CONNECTION.RECONNECTED.FAILURE: {
        return PublishEvent.Connection.Reconnected.Failure.DATA_SCHEMA.safeParse(data).success;
      }
      case PUBLISH.ROOM.CREATE.SUCCESS: {
        return PublishEvent.Room.Create.Success.DATA_SCHEMA.safeParse(data).success;
      }
      case PUBLISH.ROOM.CREATE.FAILURE: {
        return PublishEvent.Room.Create.Failure.DATA_SCHEMA.safeParse(data).success;
      }
      case PUBLISH.ROOM.DESTROY.SUCCESS: {
        return PublishEvent.Room.Destroy.Success.DATA_SCHEMA.safeParse(data).success;
      }
      case PUBLISH.ROOM.DESTROY.FAILURE: {
        return PublishEvent.Room.Destroy.Failure.DATA_SCHEMA.safeParse(data).success;
      }
      case PUBLISH.ROOM.JOIN.SUCCESS: {
        return PublishEvent.Room.Join.Success.DATA_SCHEMA.safeParse(data).success;
      }
      case PUBLISH.ROOM.JOIN.FAILURE: {
        return PublishEvent.Room.Join.Failure.DATA_SCHEMA.safeParse(data).success;
      }
      case PUBLISH.ROOM.LEAVE.SUCCESS: {
        return PublishEvent.Room.Leave.Success.DATA_SCHEMA.safeParse(data).success;
      }
      case PUBLISH.ROOM.LEAVE.FAILURE: {
        return PublishEvent.Room.Leave.Failure.DATA_SCHEMA.safeParse(data).success;
      }
      case PUBLISH.ROOM.PARTICIPANT.JOINED: {
        return PublishEvent.Room.Participant.Joined.DATA_SCHEMA.safeParse(data).success;
      }
      case PUBLISH.ROOM.PARTICIPANT.LEFT: {
        return PublishEvent.Room.Participant.Left.DATA_SCHEMA.safeParse(data).success;
      }
      case PUBLISH.ROOM.DISBAND: {
        return PublishEvent.Room.Disband.DATA_SCHEMA.safeParse(data).success;
      }
      case PUBLISH.ROOM.LIST.SUCCESS: {
        return PublishEvent.Room.List.Success.DATA_SCHEMA.safeParse(data).success;
      }
      case PUBLISH.ROOM.LIST.FAILURE: {
        return PublishEvent.Room.List.Failure.DATA_SCHEMA.safeParse(data).success;
      }
      case PUBLISH.ROOM.NOTIFY.CREATED: {
        return PublishEvent.Room.Notify.Created.DATA_SCHEMA.safeParse(data).success;
      }
      case PUBLISH.ROOM.NOTIFY.DESTROYED: {
        return PublishEvent.Room.Notify.Destroyed.DATA_SCHEMA.safeParse(data).success;
      }
      case PUBLISH.ROOM.NOTIFY.JOINED: {
        return PublishEvent.Room.Notify.Joined.DATA_SCHEMA.safeParse(data).success;
      }
      case PUBLISH.ROOM.NOTIFY.LEFT: {
        return PublishEvent.Room.Notify.Left.DATA_SCHEMA.safeParse(data).success;
      }
      case PUBLISH.ROOM.NOTIFY.STATUS_CHANGED: {
        return PublishEvent.Room.Notify.StatusChanged.DATA_SCHEMA.safeParse(data).success;
      }
      case PUBLISH.ROOM.FETCH: {
        return PublishEvent.Room.Fetch.DATA_SCHEMA.safeParse(data).success;
      }
      case PUBLISH.GAME.START: {
        return PublishEvent.Game.Start.DATA_SCHEMA.safeParse(data).success;
      }
      case PUBLISH.GAME.OVER: {
        return PublishEvent.Game.Over.DATA_SCHEMA.safeParse(data).success;
      }
      case PUBLISH.ROLE.ASSIGNED: {
        return PublishEvent.Role.Assigned.DATA_SCHEMA.safeParse(data).success;
      }
      case PUBLISH.ROLE.REVEAL_ALL: {
        return PublishEvent.Role.RevealAll.DATA_SCHEMA.safeParse(data).success;
      }
      case PUBLISH.MORNING.START: {
        return PublishEvent.Morning.Start.DATA_SCHEMA.safeParse(data).success;
      }
      case PUBLISH.MORNING.END: {
        return PublishEvent.Morning.End.DATA_SCHEMA.safeParse(data).success;
      }
      case PUBLISH.DAY.START: {
        return PublishEvent.Day.Start.DATA_SCHEMA.safeParse(data).success;
      }
      case PUBLISH.DAY.END: {
        return PublishEvent.Day.End.DATA_SCHEMA.safeParse(data).success;
      }
      case PUBLISH.EVENING.START: {
        return PublishEvent.Evening.Start.DATA_SCHEMA.safeParse(data).success;
      }
      case PUBLISH.EVENING.END: {
        return PublishEvent.Evening.End.DATA_SCHEMA.safeParse(data).success;
      }
      case PUBLISH.NIGHT.START: {
        return PublishEvent.Night.Start.DATA_SCHEMA.safeParse(data).success;
      }
      case PUBLISH.NIGHT.END: {
        return PublishEvent.Night.End.DATA_SCHEMA.safeParse(data).success;
      }
      case PUBLISH.RESIDENT.LIST: {
        return PublishEvent.Resident.List.DATA_SCHEMA.safeParse(data).success;
      }
      case PUBLISH.VICTIMIZED: {
        return PublishEvent.Victimized.DATA_SCHEMA.safeParse(data).success;
      }
      case PUBLISH.LYNCHED.NONE: {
        return PublishEvent.Lynched.None.DATA_SCHEMA.safeParse(data).success;
      }
      case PUBLISH.LYNCHED.SOME: {
        return PublishEvent.Lynched.Some.DATA_SCHEMA.safeParse(data).success;
      }
      case PUBLISH.VOTE.SET.SUCCESS: {
        return PublishEvent.Vote.Set.Success.DATA_SCHEMA.safeParse(data).success;
      }
      case PUBLISH.VOTE.SET.FAILURE: {
        return PublishEvent.Vote.Set.Failure.DATA_SCHEMA.safeParse(data).success;
      }
      case PUBLISH.VOTE.PROMPT: {
        return PublishEvent.Vote.Prompt.DATA_SCHEMA.safeParse(data).success;
      }
      case PUBLISH.ACKNOWLEDGE.OUTCOME: {
        return PublishEvent.Acknowledge.Outcome.DATA_SCHEMA.safeParse(data).success;
      }
      case PUBLISH.ATTACK.SET.SUCCESS: {
        return PublishEvent.Attack.Set.Success.DATA_SCHEMA.safeParse(data).success;
      }
      case PUBLISH.ATTACK.SET.FAILURE: {
        return PublishEvent.Attack.Set.Failure.DATA_SCHEMA.safeParse(data).success;
      }
      case PUBLISH.ATTACK.PROMPT: {
        return PublishEvent.Attack.Prompt.DATA_SCHEMA.safeParse(data).success;
      }
      case PUBLISH.DIVINATION.SET.SUCCESS: {
        return PublishEvent.Divination.Set.Success.DATA_SCHEMA.safeParse(data).success;
      }
      case PUBLISH.DIVINATION.SET.FAILURE: {
        return PublishEvent.Divination.Set.Failure.DATA_SCHEMA.safeParse(data).success;
      }
      case PUBLISH.DIVINATION.OUTCOME: {
        return PublishEvent.Divination.Outcome.DATA_SCHEMA.safeParse(data).success;
      }
      case PUBLISH.DIVINATION.PROMPT: {
        return PublishEvent.Divination.Prompt.DATA_SCHEMA.safeParse(data).success;
      }
      case PUBLISH.CHANNEL.OUTCOME: {
        return PublishEvent.Channel.Outcome.DATA_SCHEMA.safeParse(data).success;
      }
      case PUBLISH.PROTECTION.SET.SUCCESS: {
        return PublishEvent.Protection.Set.Success.DATA_SCHEMA.safeParse(data).success;
      }
      case PUBLISH.PROTECTION.SET.FAILURE: {
        return PublishEvent.Protection.Set.Failure.DATA_SCHEMA.safeParse(data).success;
      }
      case PUBLISH.PROTECTION.PROMPT: {
        return PublishEvent.Protection.Prompt.DATA_SCHEMA.safeParse(data).success;
      }
      case PUBLISH.EVACUATION.SET.SUCCESS: {
        return PublishEvent.Evacuation.Set.Success.DATA_SCHEMA.safeParse(data).success;
      }
      case PUBLISH.EVACUATION.SET.FAILURE: {
        return PublishEvent.Evacuation.Set.Failure.DATA_SCHEMA.safeParse(data).success;
      }
      case PUBLISH.EVACUATION.PROMPT: {
        return PublishEvent.Evacuation.Prompt.DATA_SCHEMA.safeParse(data).success;
      }
      default: {
        throw new ExhaustiveError(event);
      }
    }
  };
}

export type PublishEvent =
  | PublishEvent.Pong.Event
  | PublishEvent.Connection.Connected.Success.Event
  | PublishEvent.Connection.Connected.Failure.Event
  | PublishEvent.Connection.Disconnected.Event
  | PublishEvent.Connection.Reconnected.Success.Event
  | PublishEvent.Connection.Reconnected.Failure.Event
  | PublishEvent.Room.Create.Success.Event
  | PublishEvent.Room.Create.Failure.Event
  | PublishEvent.Room.Destroy.Success.Event
  | PublishEvent.Room.Destroy.Failure.Event
  | PublishEvent.Room.Join.Success.Event
  | PublishEvent.Room.Join.Failure.Event
  | PublishEvent.Room.Leave.Success.Event
  | PublishEvent.Room.Leave.Failure.Event
  | PublishEvent.Room.Participant.Joined.Event
  | PublishEvent.Room.Participant.Left.Event
  | PublishEvent.Room.Disband.Event
  | PublishEvent.Room.List.Success.Event
  | PublishEvent.Room.List.Failure.Event
  | PublishEvent.Room.Notify.Created.Event
  | PublishEvent.Room.Notify.Destroyed.Event
  | PublishEvent.Room.Notify.Joined.Event
  | PublishEvent.Room.Notify.Left.Event
  | PublishEvent.Room.Notify.StatusChanged.Event
  | PublishEvent.Room.Fetch.Event
  | PublishEvent.Game.Start.Event
  | PublishEvent.Game.Over.Event
  | PublishEvent.Role.Assigned.Event
  | PublishEvent.Role.RevealAll.Event
  | PublishEvent.Morning.Start.Event
  | PublishEvent.Morning.End.Event
  | PublishEvent.Day.Start.Event
  | PublishEvent.Day.End.Event
  | PublishEvent.Evening.Start.Event
  | PublishEvent.Evening.End.Event
  | PublishEvent.Night.Start.Event
  | PublishEvent.Night.End.Event
  | PublishEvent.Resident.List.Event
  | PublishEvent.Victimized.Event
  | PublishEvent.Lynched.None.Event
  | PublishEvent.Lynched.Some.Event
  | PublishEvent.Vote.Set.Success.Event
  | PublishEvent.Vote.Set.Failure.Event
  | PublishEvent.Vote.Prompt.Event
  | PublishEvent.Acknowledge.Outcome.Event
  | PublishEvent.Attack.Set.Success.Event
  | PublishEvent.Attack.Set.Failure.Event
  | PublishEvent.Attack.Prompt.Event
  | PublishEvent.Divination.Set.Success.Event
  | PublishEvent.Divination.Set.Failure.Event
  | PublishEvent.Divination.Outcome.Event
  | PublishEvent.Divination.Prompt.Event
  | PublishEvent.Channel.Outcome.Event
  | PublishEvent.Protection.Set.Success.Event
  | PublishEvent.Protection.Set.Failure.Event
  | PublishEvent.Protection.Prompt.Event
  | PublishEvent.Evacuation.Set.Success.Event
  | PublishEvent.Evacuation.Set.Failure.Event
  | PublishEvent.Evacuation.Prompt.Event;

export type PublishEventType = PublishEvent['type'];
export type GetPublishEventData<T extends PublishEventType> = Extract<PublishEvent, { type: T }>['data'];
