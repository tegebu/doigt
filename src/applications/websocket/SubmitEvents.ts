import { RoomSettings } from '@/applications/dtos/Role/RoleSettings.js';
import { Client } from '@/domains/Client/Client.js';
import { Person } from '@/domains/Person/Person.js';
import { Room as RoomDomain } from '@/domains/Room/Room.js';
import { ExhaustiveError } from '@jamashita/anden/error';
import { z } from 'zod';

export const SUBMIT = {
  PING: 'AnhMorales',
  CONNECTION: {
    CONNECT: 'HugoSahu',
    DISCONNECT: 'MohammedMao',
    RECONNECT: 'BenjaminSoto'
  },
  ROOM: {
    CREATE: 'MuhammedGupta',
    DESTROY: 'RahulPatel',
    JOIN: 'ShanSaha',
    LEAVE: 'TuanAbdel',
    LIST: 'DanielRana',
    FETCH: 'AmitDo'
  },
  VOTE: 'TatianaNgoy',
  ATTACK: 'RaviDan',
  DIVINATION: 'WaiVu',
  PROTECTION: 'PatriciaAguilar',
  EVACUATION: 'LongLeon'
} as const;

export namespace SubmitEvent {
  export namespace Ping {
    export const DATA_SCHEMA = z.object({
      message: z.string().min(1)
    });
    export const SCHEMA = z.object({
      type: z.literal(SUBMIT.PING),
      data: DATA_SCHEMA
    });

    export type Event = Readonly<z.infer<typeof SCHEMA>>;
  }

  export namespace Connection {
    export namespace Connect {
      export const DATA_SCHEMA = z.object({
        client: Client.SCHEMA
      });
      export const SCHEMA = z.object({
        type: z.literal(SUBMIT.CONNECTION.CONNECT),
        data: DATA_SCHEMA
      });

      export type Event = Readonly<z.infer<typeof SCHEMA>>;
    }

    export namespace Disconnect {
      export const DATA_SCHEMA = z.object({});
      export const SCHEMA = z.object({
        type: z.literal(SUBMIT.CONNECTION.DISCONNECT),
        data: DATA_SCHEMA
      });

      export type Event = Readonly<z.infer<typeof SCHEMA>>;
    }

    export namespace Reconnect {
      export const DATA_SCHEMA = z.object({
        clientID: Client.ID.SCHEMA
      });
      export const SCHEMA = z.object({
        type: z.literal(SUBMIT.CONNECTION.RECONNECT),
        data: DATA_SCHEMA
      });

      export type Event = Readonly<z.infer<typeof SCHEMA>>;
    }
  }

  export namespace Room {
    export namespace Create {
      export const DATA_SCHEMA = z.object({
        settings: RoomSettings.SCHEMA
      });
      export const SCHEMA = z.object({
        type: z.literal(SUBMIT.ROOM.CREATE),
        data: DATA_SCHEMA
      });

      export type Event = Readonly<z.infer<typeof SCHEMA>>;
    }

    export namespace Destroy {
      export const DATA_SCHEMA = z.object({
        roomID: RoomDomain.ID.SCHEMA,
        clientID: Client.ID.SCHEMA
      });
      export const SCHEMA = z.object({
        type: z.literal(SUBMIT.ROOM.DESTROY),
        data: DATA_SCHEMA
      });

      export type Event = Readonly<z.infer<typeof SCHEMA>>;
    }

    export namespace Join {
      export const DATA_SCHEMA = z.object({
        roomID: RoomDomain.ID.SCHEMA,
        clientID: Client.ID.SCHEMA,
        password: z.string().nullable()
      });
      export const SCHEMA = z.object({
        type: z.literal(SUBMIT.ROOM.JOIN),
        data: DATA_SCHEMA
      });

      export type Event = Readonly<z.infer<typeof SCHEMA>>;
    }

    export namespace Leave {
      export const DATA_SCHEMA = z.object({
        roomID: RoomDomain.ID.SCHEMA,
        clientID: Client.ID.SCHEMA
      });
      export const SCHEMA = z.object({
        type: z.literal(SUBMIT.ROOM.LEAVE),
        data: DATA_SCHEMA
      });

      export type Event = Readonly<z.infer<typeof SCHEMA>>;
    }

    export namespace List {
      export const DATA_SCHEMA = z.object({
        clientID: Client.ID.SCHEMA
      });
      export const SCHEMA = z.object({
        type: z.literal(SUBMIT.ROOM.LIST),
        data: DATA_SCHEMA
      });

      export type Event = Readonly<z.infer<typeof SCHEMA>>;
    }

    export namespace Fetch {
      export const DATA_SCHEMA = z.object({
        clientID: Client.ID.SCHEMA,
        roomID: RoomDomain.ID.SCHEMA
      });
      export const SCHEMA = z.object({
        type: z.literal(SUBMIT.ROOM.FETCH),
        data: DATA_SCHEMA
      });

      export type Event = Readonly<z.infer<typeof SCHEMA>>;
    }
  }

  export namespace Vote {
    export const DATA_SCHEMA = z.object({
      roomID: RoomDomain.ID.SCHEMA,
      sourceID: Person.ID.SCHEMA,
      destinationID: Person.ID.SCHEMA
    });
    export const SCHEMA = z.object({
      type: z.literal(SUBMIT.VOTE),
      data: DATA_SCHEMA
    });

    export type Event = Readonly<z.infer<typeof SCHEMA>>;
  }

  export namespace Attack {
    export const DATA_SCHEMA = z.object({
      roomID: RoomDomain.ID.SCHEMA,
      sourceID: Person.ID.SCHEMA,
      destinationID: Person.ID.SCHEMA
    });
    export const SCHEMA = z.object({
      type: z.literal(SUBMIT.ATTACK),
      data: DATA_SCHEMA
    });

    export type Event = Readonly<z.infer<typeof SCHEMA>>;
  }

  export namespace Divination {
    export const DATA_SCHEMA = z.object({
      roomID: RoomDomain.ID.SCHEMA,
      sourceID: Person.ID.SCHEMA,
      destinationID: Person.ID.SCHEMA
    });
    export const SCHEMA = z.object({
      type: z.literal(SUBMIT.DIVINATION),
      data: DATA_SCHEMA
    });

    export type Event = Readonly<z.infer<typeof SCHEMA>>;
  }

  export namespace Protection {
    export const DATA_SCHEMA = z.object({
      roomID: RoomDomain.ID.SCHEMA,
      sourceID: Person.ID.SCHEMA,
      destinationID: Person.ID.SCHEMA
    });
    export const SCHEMA = z.object({
      type: z.literal(SUBMIT.PROTECTION),
      data: DATA_SCHEMA
    });

    export type Event = Readonly<z.infer<typeof SCHEMA>>;
  }

  export namespace Evacuation {
    export const DATA_SCHEMA = z.object({
      roomID: RoomDomain.ID.SCHEMA,
      sourceID: Person.ID.SCHEMA,
      destinationID: Person.ID.SCHEMA
    });
    export const SCHEMA = z.object({
      type: z.literal(SUBMIT.EVACUATION),
      data: DATA_SCHEMA
    });

    export type Event = Readonly<z.infer<typeof SCHEMA>>;
  }

  export const is = <T extends SubmitEventType>(event: T, data: unknown): data is GetSubmitEventData<T> => {
    switch (event) {
      case SUBMIT.PING: {
        return SubmitEvent.Ping.DATA_SCHEMA.safeParse(data).success;
      }
      case SUBMIT.CONNECTION.CONNECT: {
        return SubmitEvent.Connection.Connect.DATA_SCHEMA.safeParse(data).success;
      }
      case SUBMIT.CONNECTION.DISCONNECT: {
        return SubmitEvent.Connection.Disconnect.DATA_SCHEMA.safeParse(data).success;
      }
      case SUBMIT.CONNECTION.RECONNECT: {
        return SubmitEvent.Connection.Reconnect.DATA_SCHEMA.safeParse(data).success;
      }
      case SUBMIT.ROOM.CREATE: {
        return SubmitEvent.Room.Create.DATA_SCHEMA.safeParse(data).success;
      }
      case SUBMIT.ROOM.DESTROY: {
        return SubmitEvent.Room.Destroy.DATA_SCHEMA.safeParse(data).success;
      }
      case SUBMIT.ROOM.JOIN: {
        return SubmitEvent.Room.Join.DATA_SCHEMA.safeParse(data).success;
      }
      case SUBMIT.ROOM.LEAVE: {
        return SubmitEvent.Room.Leave.DATA_SCHEMA.safeParse(data).success;
      }
      case SUBMIT.ROOM.LIST: {
        return SubmitEvent.Room.List.DATA_SCHEMA.safeParse(data).success;
      }
      case SUBMIT.ROOM.FETCH: {
        return SubmitEvent.Room.Fetch.DATA_SCHEMA.safeParse(data).success;
      }
      case SUBMIT.VOTE: {
        return SubmitEvent.Vote.DATA_SCHEMA.safeParse(data).success;
      }
      case SUBMIT.ATTACK: {
        return SubmitEvent.Attack.DATA_SCHEMA.safeParse(data).success;
      }
      case SUBMIT.DIVINATION: {
        return SubmitEvent.Divination.DATA_SCHEMA.safeParse(data).success;
      }
      case SUBMIT.PROTECTION: {
        return SubmitEvent.Protection.DATA_SCHEMA.safeParse(data).success;
      }
      case SUBMIT.EVACUATION: {
        return SubmitEvent.Evacuation.DATA_SCHEMA.safeParse(data).success;
      }
      default: {
        throw new ExhaustiveError(event);
      }
    }
  };
}

export type SubmitEvent =
  | SubmitEvent.Ping.Event
  | SubmitEvent.Connection.Connect.Event
  | SubmitEvent.Connection.Disconnect.Event
  | SubmitEvent.Connection.Reconnect.Event
  | SubmitEvent.Room.Create.Event
  | SubmitEvent.Room.Destroy.Event
  | SubmitEvent.Room.Join.Event
  | SubmitEvent.Room.Leave.Event
  | SubmitEvent.Room.List.Event
  | SubmitEvent.Room.Fetch.Event
  | SubmitEvent.Vote.Event
  | SubmitEvent.Attack.Event
  | SubmitEvent.Divination.Event
  | SubmitEvent.Protection.Event
  | SubmitEvent.Evacuation.Event;

export type SubmitEventType = SubmitEvent['type'];
export type GetSubmitEventData<T extends SubmitEventType> = Extract<SubmitEvent, { type: T }>['data'];
