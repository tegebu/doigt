import { Team } from '@/domains/Identity/Team.js';
import type { Identity } from '../Identity.js';

type IdentityArgs = Partial<Identity>;

export const mockIdentity = ({ name = '村人', role = 'VILLAGER', team = Team.VILLAGERS }: IdentityArgs = {}) => {
  return {
    name,
    role,
    team
  } satisfies Identity;
};
