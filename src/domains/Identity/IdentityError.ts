type Detail = string;

export type IdentityError = Readonly<{
  error: 'IdentityError';
  detail: Detail;
  message: string;
}>;
