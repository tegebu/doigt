import { Identity } from '../Identity.js';

describe('Identity', () => {
  describe('getByRole', () => {
    it.each`
      role             | expected
      ${'ACCUSER'}     | ${Identity.ACCUSER}
      ${'APOSTATE'}    | ${Identity.APOSTATE}
      ${'ATONEMENT'}   | ${Identity.ATONEMENT}
      ${'CATALYST'}    | ${Identity.CATALYST}
      ${'COUNT'}       | ${Identity.COUNT}
      ${'COWARD'}      | ${Identity.COWARD}
      ${'CULT_LEADER'} | ${Identity.CULT_LEADER}
      ${'HUNTER'}      | ${Identity.HUNTER}
      ${'INFORMANT'}   | ${Identity.INFORMANT}
      ${'JESTER'}      | ${Identity.JESTER}
      ${'KITSUNE'}     | ${Identity.KITSUNE}
      ${'KNIGHT'}      | ${Identity.KNIGHT}
      ${'MEDIUM'}      | ${Identity.MEDIUM}
      ${'MONASTIC'}    | ${Identity.MONASTIC}
      ${'ORATOR'}      | ${Identity.ORATOR}
      ${'POSSESSED'}   | ${Identity.POSSESSED}
      ${'SEER'}        | ${Identity.SEER}
      ${'VILLAGER'}    | ${Identity.VILLAGER}
      ${'WEREWOLF'}    | ${Identity.WEREWOLF}
    `('should return $expected when $role is passed', ({ role, expected }) => {
      expect(Identity.getByRole(role)).toBe(expected);
    });
  });
});
