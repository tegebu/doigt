import { ExhaustiveError } from '@jamashita/anden/error';
import { z } from 'zod';
import { Role } from '../Role/Role.js';
import { Team } from './Team.js';

const IdentitySchema = z.object({
  name: z.string().min(1),
  role: Role.SCHEMA,
  team: Team.SCHEMA
});

export type Identity = Readonly<z.infer<typeof IdentitySchema>>;

export namespace Identity {
  export const SCHEMA = IdentitySchema;

  export const ACCUSER = {
    name: '告発人',
    role: 'ACCUSER',
    team: Team.VILLAGERS
  } satisfies Identity;

  export const APOSTATE = {
    name: '背教者',
    role: 'APOSTATE',
    team: Team.VILLAGERS // TODO 途中で入れ替わるのをどう表現するか
  } satisfies Identity;

  export const ATONEMENT = {
    name: '贖罪者',
    role: 'ATONEMENT',
    team: Team.VILLAGERS
  } satisfies Identity;

  export const CATALYST = {
    name: '触媒',
    role: 'CATALYST',
    team: Team.WEREWOLVES
  } satisfies Identity;

  export const COUNT = {
    name: '伯爵',
    role: 'COUNT',
    team: Team.VILLAGERS
  } satisfies Identity;

  export const COWARD = {
    name: '臆病者',
    role: 'COWARD',
    team: Team.VILLAGERS
  } satisfies Identity;

  export const CULT_LEADER = {
    name: '教祖',
    role: 'CULT_LEADER',
    team: Team.VILLAGERS // TODO 独自陣営
  } satisfies Identity;

  export const HUNTER = {
    name: '猟師',
    role: 'HUNTER',
    team: Team.VILLAGERS
  } satisfies Identity;

  export const INFORMANT = {
    name: '内通者',
    role: 'INFORMANT',
    team: Team.WEREWOLVES
  } satisfies Identity;

  export const JESTER = {
    name: '道化',
    role: 'JESTER',
    team: Team.JESTERS
  } satisfies Identity;

  export const KITSUNE = {
    name: '妖狐',
    role: 'KITSUNE',
    team: Team.KITSUNES
  } satisfies Identity;

  export const KNIGHT = {
    name: '騎士',
    role: 'KNIGHT',
    team: Team.VILLAGERS
  } satisfies Identity;

  export const MEDIUM = {
    name: '霊媒師',
    role: 'MEDIUM',
    team: Team.VILLAGERS
  } satisfies Identity;

  export const MONASTIC = {
    name: '僧侶',
    role: 'MONASTIC',
    team: Team.VILLAGERS
  } satisfies Identity;

  export const ORATOR = {
    name: '雄弁家',
    role: 'ORATOR',
    team: Team.VILLAGERS
  } satisfies Identity;

  export const POSSESSED = {
    name: '憑依者',
    role: 'POSSESSED',
    team: Team.VILLAGERS
  } satisfies Identity;

  export const SEER = {
    name: '予言者',
    role: 'SEER',
    team: Team.VILLAGERS
  } satisfies Identity;

  export const VILLAGER = {
    name: '村人',
    role: 'VILLAGER',
    team: Team.VILLAGERS
  } satisfies Identity;

  export const WEREWOLF = {
    name: '人狼',
    role: 'WEREWOLF',
    team: Team.WEREWOLVES
  } satisfies Identity;

  export const getByRole = (role: Role): Identity => {
    switch (role) {
      case 'ACCUSER': {
        return Identity.ACCUSER;
      }
      case 'APOSTATE': {
        return Identity.APOSTATE;
      }
      case 'ATONEMENT': {
        return Identity.ATONEMENT;
      }
      case 'CATALYST': {
        return Identity.CATALYST;
      }
      case 'COUNT': {
        return Identity.COUNT;
      }
      case 'COWARD': {
        return Identity.COWARD;
      }
      case 'CULT_LEADER': {
        return Identity.CULT_LEADER;
      }
      case 'HUNTER': {
        return Identity.HUNTER;
      }
      case 'INFORMANT': {
        return Identity.INFORMANT;
      }
      case 'JESTER': {
        return Identity.JESTER;
      }
      case 'KITSUNE': {
        return Identity.KITSUNE;
      }
      case 'KNIGHT': {
        return Identity.KNIGHT;
      }
      case 'MEDIUM': {
        return Identity.MEDIUM;
      }
      case 'MONASTIC': {
        return Identity.MONASTIC;
      }
      case 'ORATOR': {
        return Identity.ORATOR;
      }
      case 'POSSESSED': {
        return Identity.POSSESSED;
      }
      case 'SEER': {
        return Identity.SEER;
      }
      case 'VILLAGER': {
        return Identity.VILLAGER;
      }
      case 'WEREWOLF': {
        return Identity.WEREWOLF;
      }
      default: {
        throw new ExhaustiveError(role);
      }
    }
  };
}
