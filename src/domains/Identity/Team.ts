import { z } from 'zod';

const TeamSchema = z.object({
  team: z.union([z.literal('VILLAGERS'), z.literal('WEREWOLVES'), z.literal('KITSUNES'), z.literal('JESTERS')]),
  // これがtrueだと勝利条件を満たしたときにゲームを終了させる
  terminateGame: z.boolean()
});

export type Team = Readonly<z.infer<typeof TeamSchema>>;

export namespace Team {
  export const SCHEMA = TeamSchema;

  export const JESTERS = {
    team: 'JESTERS',
    terminateGame: false
  } as const satisfies Team;

  export const KITSUNES = {
    team: 'KITSUNES',
    terminateGame: true
  } as const satisfies Team;

  export const VILLAGERS = {
    team: 'VILLAGERS',
    terminateGame: true
  } as const satisfies Team;

  export const WEREWOLVES = {
    team: 'WEREWOLVES',
    terminateGame: true
  } as const satisfies Team;
}
