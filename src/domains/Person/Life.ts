import { z } from 'zod';

const LifeSchema = z.union([z.literal('ALIVE'), z.literal('DEAD')]);

export type Life = z.infer<typeof LifeSchema>;

export namespace Life {
  export const SCHEMA = LifeSchema;

  export const isAlive = (life: Life): boolean => {
    return life === 'ALIVE';
  };

  export const isDead = (life: Life): boolean => {
    return life === 'DEAD';
  };
}
