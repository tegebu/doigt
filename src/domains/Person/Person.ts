import { Kind } from '@jamashita/anden/type';
import { UUID } from '@jamashita/anden/uuid';
import type { Tagged } from 'type-fest';
import { z } from 'zod';
import { Client } from '../Client/Client.js';
import { Identity } from '../Identity/Identity.js';
import { Life } from './Life.js';

// TODO Residentに名前を変更すr
export type PersonID = Tagged<string, 'Person'>;

const IDSchema = z.custom<PersonID>((value: unknown) => {
  return Kind.isString(value) && UUID.validate(value);
});
const PersonBaseSchema = z.object({
  id: IDSchema,
  life: Life.SCHEMA,
  client: Client.SCHEMA
});
const PersonSchema = PersonBaseSchema.extend({
  identity: Identity.SCHEMA
});

export type PersonBase = Readonly<z.infer<typeof PersonBaseSchema>>;
export type Person = Readonly<z.infer<typeof PersonSchema>>;

export namespace Person {
  export const SCHEMA = PersonSchema;

  export namespace Base {
    export const SCHEMA = PersonBaseSchema;
  }

  export namespace ID {
    export const SCHEMA = IDSchema;

    export const from = (value: string): PersonID => {
      return value as PersonID;
    };

    export const generate = (): PersonID => {
      return Person.ID.from(UUID.v7().get());
    };
  }

  export const die = (person: Person): Person => {
    if (Life.isAlive(person.life)) {
      return {
        ...person,
        life: 'DEAD'
      } satisfies Person;
    }

    return person;
  };

  export const isAlive = (person: Person): boolean => {
    return Life.isAlive(person.life);
  };

  export const isDead = (person: Person): boolean => {
    return Life.isDead(person.life);
  };

  export const revive = (person: Person): Person => {
    if (Life.isDead(person.life)) {
      return {
        ...person,
        life: 'ALIVE'
      } satisfies Person;
    }

    return person;
  };
}
