import type { Client } from '@/domains/Client/Client.js';
import { mockClient } from '@/domains/Client/mocks/MockClient.js';
import { Identity } from '@/domains/Identity/Identity.js';
import type { Life } from '../Life.js';
import { Person } from '../Person.js';

type PersonArgs = Partial<{
  id: string;
  identity: Identity;
  life: Life;
  client: Client;
}>;

export const mockPerson = ({
  id = '8febaac0-33a9-4fd3-be77-f63b7b49ecff',
  identity = Identity.VILLAGER,
  life = 'ALIVE',
  client = mockClient()
}: PersonArgs = {}) => {
  return {
    id: Person.ID.from(id),
    identity,
    life,
    client
  } satisfies Person;
};
