type Detail =
  | 'SourceDead'
  | 'DestinationDead'
  | 'StillAlive'
  | 'NoSuchPerson'
  | 'NotKnight'
  | 'NotMedium'
  | 'NotSeer'
  | 'NotWerewolf'
  | 'ActionImpossible';

export type PersonError = Readonly<{
  error: 'PersonError';
  detail: Detail;
  message: string;
}>;

export const createPersonError = (detail: Detail, message: string): PersonError => {
  return {
    error: 'PersonError',
    detail,
    message
  };
};
