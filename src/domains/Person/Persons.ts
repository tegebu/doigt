import { Maps } from '@/lib/Collection/Map/Maps.js';
import { Kind, type Predicate } from '@jamashita/anden/type';
import { Random } from '@jamashita/steckdose/random';
import { none, type Option, some } from 'fp-ts/lib/Option.js';
import type { Tagged } from 'type-fest';
import { z } from 'zod';
import type { Role } from '../Role/Role.js';
import { Person, type PersonID } from './Person.js';

export type Persons = Tagged<ReadonlyMap<PersonID, Person>, 'Persons'>;

export const PersonsSchema = z.custom<Persons>((value) => z.map(Person.ID.SCHEMA, Person.SCHEMA).safeParse(value));

export namespace Persons {
  export const SCHEMA = PersonsSchema;

  export const empty = (): Persons => {
    return Persons.from(new Map());
  };

  export const exchange = (persons: Persons, person: Person): Persons => {
    const map = new Map(persons);

    return Persons.from(map.set(person.id, person));
  };

  export const from = (persons: ReadonlyMap<PersonID, Person>): Persons => {
    return persons as Persons;
  };

  export const filter = (persons: Persons, predicate: Predicate<Person>): Persons => {
    return Persons.from(Maps.filter(persons, predicate));
  };

  export const getByRole = (persons: Persons, role: Role): Persons => {
    const map = new Map<PersonID, Person>();

    for (const person of persons.values()) {
      if (person.identity.role === role) {
        map.set(person.id, person);
      }
    }

    return Persons.from(map);
  };

  export const join = (persons: Persons, person: Person): Persons => {
    const map = new Map(persons);

    return Persons.from(map.set(person.id, person));
  };

  export const leave = (persons: Persons, person: Person): Persons => {
    const map = new Map(persons);

    map.delete(person.id);

    return Persons.from(map);
  };

  export const ofArray = (persons: ReadonlyArray<Person>): Persons => {
    if (persons.length === 0) {
      return Persons.empty();
    }

    const map = new Map<PersonID, Person>();

    for (const person of persons) {
      map.set(person.id, person);
    }

    return Persons.from(map);
  };

  const randomInner = (persons: ReadonlyArray<Person>): Option<Person> => {
    const index = Random.integer(0, persons.length - 1);
    const person = persons[index];

    if (Kind.isUndefined(person)) {
      return randomInner(persons);
    }

    return some(person);
  };

  export const random = (persons: Persons): Option<Person> => {
    if (persons.size === 0) {
      return none;
    }

    return randomInner([...persons.values()]);
  };
}
