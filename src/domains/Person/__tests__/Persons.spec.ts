import { Identity } from '@/domains/Identity/Identity.js';
import { isNone, isSome } from 'fp-ts/lib/Option.js';
import { mockPerson } from '../mocks/MockPerson.js';
import { Persons } from '../Persons.js';

describe('Persons', () => {
  describe('exchange', () => {
    it('should return exchanged persons', () => {
      const person1 = mockPerson({
        id: '0ce82d50-c1c3-467d-855f-55736176ced5'
      });
      const person2 = mockPerson({
        id: 'a93f3373-c976-4a89-820c-18a6e1b7f734'
      });

      const persons = Persons.ofArray([person1, person2]);

      expect(persons.size).toBe(2);

      const person3 = mockPerson({
        id: '0ce82d50-c1c3-467d-855f-55736176ced5'
      });

      const exchangedPersons = Persons.exchange(persons, person3);

      expect(persons.size).toBe(2);
      expect(persons.get(person1.id)).toBe(person1);
      expect(persons.get(person2.id)).toBe(person2);
      expect(persons.get(person3.id)).toBe(person1);
      expect(exchangedPersons.size).toBe(2);
      expect(exchangedPersons.get(person1.id)).toBe(person3);
      expect(exchangedPersons.get(person2.id)).toBe(person2);
      expect(exchangedPersons.get(person3.id)).toBe(person3);
    });
  });

  describe('getByRole', () => {
    it('should return persons by role', () => {
      const person1 = mockPerson({
        id: '0ce82d50-c1c3-467d-855f-55736176ced5',
        identity: Identity.VILLAGER
      });
      const person2 = mockPerson({
        id: 'a93f3373-c976-4a89-820c-18a6e1b7f734',
        identity: Identity.WEREWOLF
      });
      const person3 = mockPerson({
        id: '72bd2897-4c96-486d-bb80-89d289f00a80',
        identity: Identity.WEREWOLF
      });
      const person4 = mockPerson({
        id: '0372c268-2e17-40bb-9806-1018f1ebdda0',
        identity: Identity.SEER
      });

      const persons = Persons.ofArray([person1, person2, person3, person4]);

      expect(persons.size).toBe(4);

      const werewolves = Persons.getByRole(persons, 'WEREWOLF');

      expect(persons.size).toBe(4);
      expect(werewolves.size).toBe(2);
      expect(werewolves.get(person1.id)).toBe(undefined);
      expect(werewolves.get(person2.id)).toBe(person2);
      expect(werewolves.get(person3.id)).toBe(person3);
      expect(werewolves.get(person4.id)).toBe(undefined);
    });
  });

  describe('join', () => {
    it('should return joined persons', () => {
      const person1 = mockPerson({
        id: '0ce82d50-c1c3-467d-855f-55736176ced5'
      });
      const person2 = mockPerson({
        id: 'a93f3373-c976-4a89-820c-18a6e1b7f734'
      });

      const persons = Persons.ofArray([person1]);

      expect(persons.size).toBe(1);

      const joinedPersons = Persons.join(persons, person2);

      expect(persons.size).toBe(1);
      expect(persons.get(person1.id)).toBe(person1);
      expect(persons.get(person2.id)).toBe(undefined);
      expect(joinedPersons.size).toBe(2);
      expect(joinedPersons.get(person1.id)).toBe(person1);
      expect(joinedPersons.get(person2.id)).toBe(person2);
    });
  });

  describe('leave', () => {
    it('should return left persons', () => {
      const person1 = mockPerson({
        id: '0ce82d50-c1c3-467d-855f-55736176ced5'
      });
      const person2 = mockPerson({
        id: 'a93f3373-c976-4a89-820c-18a6e1b7f734'
      });

      const persons = Persons.ofArray([person1, person2]);

      expect(persons.size).toBe(2);

      const leftPersons = Persons.leave(persons, person1);

      expect(persons.size).toBe(2);
      expect(persons.get(person1.id)).toBe(person1);
      expect(persons.get(person2.id)).toBe(person2);
      expect(leftPersons.size).toBe(1);
      expect(leftPersons.get(person1.id)).toBe(undefined);
      expect(leftPersons.get(person2.id)).toBe(person2);
    });
  });

  describe('random', () => {
    it('should return none if there is no element', () => {
      const persons = Persons.empty();

      const option = Persons.random(persons);

      expect(isNone(option)).toBe(true);
    });

    it('should return the element if there is only one element', () => {
      const person1 = mockPerson({
        id: '0ce82d50-c1c3-467d-855f-55736176ced5'
      });

      const persons = Persons.ofArray([person1]);

      const option = Persons.random(persons);

      expect(isSome(option)).toBe(true);

      if (isNone(option)) {
        throw new Error('This should not happen');
      }

      expect(option.value).toBe(person1);
    });

    // 要素が複数あればランダムに選ばれる
    it('should return the element randomly if there are multiple elements', () => {
      const person1 = mockPerson({
        id: '0ce82d50-c1c3-467d-855f-55736176ced5'
      });
      const person2 = mockPerson({
        id: 'a93f3373-c976-4a89-820c-18a6e1b7f734'
      });

      const persons = Persons.ofArray([person1, person2]);

      const option = Persons.random(persons);

      expect(isSome(option)).toBe(true);

      if (isNone(option)) {
        throw new Error('This should not happen');
      }

      expect(persons.has(option.value.id)).toBe(true);
    });
  });
});
