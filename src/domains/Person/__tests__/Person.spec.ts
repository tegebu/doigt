import { mockClient } from '@/domains/Client/mocks/MockClient.js';
import { Identity } from '@/domains/Identity/Identity.js';
import { Person } from '../Person.js';

describe('Person', () => {
  describe('die', () => {
    it('should return dead person', () => {
      const person = {
        id: Person.ID.from('b3f508c3-5507-4e8e-b002-0798ef2ea0d4'),
        identity: Identity.VILLAGER,
        life: 'ALIVE',
        client: mockClient()
      } satisfies Person;

      expect(Person.isAlive(person)).toBe(true);

      const deadPerson = Person.die(person);

      expect(Person.isDead(deadPerson)).toBe(true);
    });

    it('should not revive dead person', () => {
      const person = {
        id: Person.ID.from('b3f508c3-5507-4e8e-b002-0798ef2ea0d4'),
        identity: Identity.VILLAGER,
        life: 'DEAD',
        client: mockClient()
      } satisfies Person;

      expect(Person.isDead(person)).toBe(true);

      const deadPerson = Person.die(person);

      expect(Person.isDead(deadPerson)).toBe(true);
    });
  });

  describe('revive', () => {
    it('should return alive person', () => {
      const person = {
        id: Person.ID.from('b3f508c3-5507-4e8e-b002-0798ef2ea0d4'),
        identity: Identity.VILLAGER,
        life: 'DEAD',
        client: mockClient()
      } satisfies Person;

      expect(Person.isDead(person)).toBe(true);

      const alivePerson = Person.revive(person);

      expect(Person.isAlive(alivePerson)).toBe(true);
    });

    it('should not kill alive person', () => {
      const person = {
        id: Person.ID.from('b3f508c3-5507-4e8e-b002-0798ef2ea0d4'),
        identity: Identity.VILLAGER,
        life: 'ALIVE',
        client: mockClient()
      } satisfies Person;

      expect(Person.isAlive(person)).toBe(true);

      const alivePerson = Person.revive(person);

      expect(Person.isAlive(alivePerson)).toBe(true);
    });
  });
});
