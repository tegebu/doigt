import { Life } from '../Life.js';

describe('Life', () => {
  describe('isAlive', () => {
    it('should return true when life is alive', () => {
      expect(Life.isAlive('ALIVE')).toBe(true);
    });

    it('should return false when life is dead', () => {
      expect(Life.isAlive('DEAD')).toBe(false);
    });
  });

  describe('isDead', () => {
    it('should return true when life is dead', () => {
      expect(Life.isDead('DEAD')).toBe(true);
    });

    it('should return false when life is alive', () => {
      expect(Life.isDead('ALIVE')).toBe(false);
    });
  });
});
