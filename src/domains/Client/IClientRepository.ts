import type { GatewayError } from '@/adapters/gateways/GatewayError.js';
import type { GenericError } from '@/lib/Error/Errors.js';
import type { Either } from 'fp-ts/lib/Either.js';
import type { Option } from 'fp-ts/lib/Option.js';
import type { Client, ClientID } from './Client.js';

export interface IClientRepository {
  create(client: Client): Promise<Either<GenericError | GatewayError, unknown>>;

  delete(id: ClientID): Promise<Either<GenericError | GatewayError, unknown>>;

  find(id: ClientID): Promise<Either<GenericError | GatewayError, Option<Client>>>;

  update(client: Client): Promise<Either<GenericError | GatewayError, unknown>>;
}
