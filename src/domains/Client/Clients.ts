import type { Tagged } from 'type-fest';
import { z } from 'zod';
import { Client, type ClientID } from './Client.js';

export type Clients = Tagged<ReadonlyMap<ClientID, Client>, 'Clients'>;

const ClientsSchema = z.custom<Clients>((value) => z.map(Client.ID.SCHEMA, Client.SCHEMA).safeParse(value).success);

export namespace Clients {
  export const SCHEMA = ClientsSchema;

  export const empty = (): Clients => {
    return Clients.from(new Map());
  };

  export const from = (clients: ReadonlyMap<ClientID, Client>): Clients => {
    return clients as Clients;
  };

  export const join = (clients: Clients, client: Client): Clients => {
    const map = new Map(clients);

    return Clients.from(map.set(client.id, client));
  };

  export const leave = (clients: Clients, client: Client): Clients => {
    const map = new Map(clients);

    map.delete(client.id);

    return Clients.from(map);
  };

  export const ofArray = (clients: ReadonlyArray<Client>): Clients => {
    if (clients.length === 0) {
      return Clients.empty();
    }

    const map = new Map<ClientID, Client>();

    for (const individual of clients) {
      map.set(individual.id, individual);
    }

    return Clients.from(map);
  };
}
