type Detail = 'NoSuchClient' | 'ClientClosed';

export type ClientError = Readonly<{
  error: 'ClientError';
  detail: Detail;
  message: string;
}>;

export const createClientError = (detail: Detail, message: string): ClientError => {
  return {
    error: 'ClientError',
    detail,
    message
  };
};
