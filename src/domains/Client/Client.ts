import type { ParseError } from '@/lib/Error/ParseError.js';
import { Kind } from '@jamashita/anden/type';
import { UUID } from '@jamashita/anden/uuid';
import { type Either, left, right } from 'fp-ts/lib/Either.js';
import type { Tagged } from 'type-fest';
import { z } from 'zod';

export type ClientID = Tagged<string, 'ClientID'>;

const IDSchema = z.custom<ClientID>((value: unknown) => {
  return Kind.isString(value) && UUID.validate(value);
});
const ClientSchema = z.object({
  id: IDSchema,
  name: z.string().min(1)
});

export type Client = Readonly<z.infer<typeof ClientSchema>>;

export namespace Client {
  export namespace ID {
    export const SCHEMA = IDSchema;

    export const from = (id: string): ClientID => {
      return id as ClientID;
    };

    export const generate = (): ClientID => {
      return Client.ID.from(UUID.v7().get());
    };
  }

  export const SCHEMA = ClientSchema;

  export const parse = (value: unknown): Either<ParseError, Client> => {
    const result = Client.SCHEMA.safeParse(value);

    if (result.success) {
      return right(result.data);
    }

    return left({
      error: 'ParseError',
      message: `INVALID VALUE. GIVEN: ${String(value)}`
    } satisfies ParseError);
  };
}
