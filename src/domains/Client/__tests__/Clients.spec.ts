import { Clients } from '@/domains/Client/Clients.js';
import { mockClient } from '@/domains/Client/mocks/MockClient.js';

describe('Clients', () => {
  describe('join', () => {
    it('should return joined clients', () => {
      const client1 = mockClient({
        id: '0ce82d50-c1c3-467d-855f-55736176ced5'
      });
      const client2 = mockClient({
        id: 'a93f3373-c976-4a89-820c-18a6e1b7f734'
      });

      const clients = Clients.ofArray([client1]);

      expect(clients.size).toBe(1);

      const joinedClients = Clients.join(clients, client2);

      expect(clients.size).toBe(1);
      expect(clients.get(client1.id)).toBe(client1);
      expect(clients.get(client2.id)).toBe(undefined);
      expect(joinedClients.size).toBe(2);
      expect(joinedClients.get(client1.id)).toBe(client1);
      expect(joinedClients.get(client2.id)).toBe(client2);
    });
  });

  describe('leave', () => {
    it('should return left clients', () => {
      const client1 = mockClient({
        id: '0ce82d50-c1c3-467d-855f-55736176ced5'
      });
      const client2 = mockClient({
        id: 'a93f3373-c976-4a89-820c-18a6e1b7f734'
      });

      const clients = Clients.ofArray([client1, client2]);

      expect(clients.size).toBe(2);

      const leftClients = Clients.leave(clients, client1);

      expect(clients.size).toBe(2);
      expect(clients.get(client1.id)).toBe(client1);
      expect(clients.get(client2.id)).toBe(client2);
      expect(leftClients.size).toBe(1);
      expect(leftClients.get(client1.id)).toBe(undefined);
      expect(leftClients.get(client2.id)).toBe(client2);
    });
  });
});
