import { Client } from '../Client.js';

type ClientArgs = Partial<
  Readonly<{
    id: string;
    name: string;
  }>
>;

export const mockClient = ({ id = 'e0372bc5-fe31-49b8-9b3c-0c85c1a00a59', name = 'mockClient' }: ClientArgs = {}): Client => {
  return {
    id: Client.ID.from(id),
    name
  } satisfies Client;
};
