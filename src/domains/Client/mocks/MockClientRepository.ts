import type { GatewayError } from '@/adapters/gateways/GatewayError.js';
import type { GenericError } from '@/lib/Error/Errors.js';
import { UnimplementedError } from '@jamashita/anden/error';
import type { Either } from 'fp-ts/lib/Either.js';
import type { Option } from 'fp-ts/lib/Option.js';
import type { Client } from '../Client.js';
import type { IClientRepository } from '../IClientRepository.js';

export class MockClientRepository implements IClientRepository {
  public create(): Promise<Either<GenericError | GatewayError, unknown>> {
    throw new UnimplementedError();
  }

  public delete(): Promise<Either<GenericError | GatewayError, unknown>> {
    throw new UnimplementedError();
  }

  public find(): Promise<Either<GenericError | GatewayError, Option<Client>>> {
    throw new UnimplementedError();
  }

  public update(): Promise<Either<GenericError | GatewayError, unknown>> {
    throw new UnimplementedError();
  }
}
