import { Maps } from '@/lib/Collection/Map/Maps.js';
import { fromNullable, type Option } from 'fp-ts/lib/Option.js';
import type { Tagged } from 'type-fest';
import { z } from 'zod';
import { Room, type RoomID, type RoomList } from './Room.js';

export type Rooms = Tagged<ReadonlyMap<RoomID, Room>, 'Room'>;
export type RoomLists = Tagged<ReadonlyMap<RoomID, RoomList>, 'RoomList'>;

const RoomsSchema = z.custom<Rooms>((value: unknown) => z.map(Room.ID.SCHEMA, Room.SCHEMA).safeParse(value).success);
const RoomListsSchema = z.custom<Rooms>((value: unknown) => z.map(Room.ID.SCHEMA, Room.List.SCHEMA).safeParse(value).success);

export namespace Rooms {
  export const SCHEMA = RoomsSchema;

  export namespace List {
    export const SCHEMA = RoomListsSchema;

    /**
     * すでに存在する場合は何もしない
     *
     * @param rooms
     * @param room
     */
    export const add = (rooms: RoomLists, room: RoomList): RoomLists => {
      if (rooms.has(room.id)) {
        return rooms;
      }

      const map = new Map<RoomID, RoomList>(rooms);

      return Rooms.List.from(map.set(room.id, room));
    };

    export const empty = (): RoomLists => {
      return Rooms.List.from(new Map());
    };

    export const filter = (rooms: RoomLists, predicate: (room: RoomList) => boolean): RoomLists => {
      return Rooms.List.from(Maps.filter(rooms, predicate));
    };

    export const from = (rooms: ReadonlyMap<RoomID, RoomList>): RoomLists => {
      return rooms as RoomLists;
    };

    export const ofArray = (rooms: ReadonlyArray<RoomList>): RoomLists => {
      const map = new Map<RoomID, RoomList>();

      for (const room of rooms) {
        map.set(room.id, room);
      }

      return Rooms.List.from(map);
    };

    /**
     * 存在しない場合は何もしない
     *
     * @param rooms
     * @param roomID
     */
    export const remove = (rooms: RoomLists, roomID: RoomID): RoomLists => {
      if (!rooms.has(roomID)) {
        return rooms;
      }

      const map = new Map<RoomID, RoomList>(rooms);

      map.delete(roomID);

      return Rooms.List.from(map);
    };

    /**
     * 存在しない場合は何もしない
     *
     * @param rooms
     * @param room
     */
    export const replace = (rooms: RoomLists, room: RoomList): RoomLists => {
      if (!rooms.has(room.id)) {
        return rooms;
      }

      const map = new Map<RoomID, RoomList>(rooms);

      return Rooms.List.from(map.set(room.id, room));
    };
  }

  export const close = (rooms: Rooms, roomID: RoomID): Rooms => {
    const map = new Map<RoomID, Room>(rooms);

    map.delete(roomID);

    return Rooms.from(map);
  };

  export const create = (rooms: Rooms, room: Room): Rooms => {
    const map = new Map<RoomID, Room>(rooms);

    return Rooms.from(map.set(room.id, room));
  };

  export const empty = (): Rooms => {
    return Rooms.from(new Map());
  };

  export const filter = (rooms: Rooms, predicate: (room: Room) => boolean): Rooms => {
    return Rooms.from(Maps.filter(rooms, predicate));
  };

  export const from = (rooms: ReadonlyMap<RoomID, Room>): Rooms => {
    return rooms as Rooms;
  };

  export const get = (rooms: Rooms, roomID: RoomID): Option<Room> => {
    return fromNullable(rooms.get(roomID));
  };

  export const ofArray = (rooms: ReadonlyArray<Room>): Rooms => {
    const map = new Map<RoomID, Room>();

    for (const room of rooms) {
      map.set(room.id, room);
    }

    return Rooms.from(map);
  };
}
