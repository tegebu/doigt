import { z } from 'zod';

const GameStatusSchema = z.union([z.literal('OPEN'), z.literal('IN_PROGRESS'), z.literal('COMPLETED')]);

export type GameStatus = z.infer<typeof GameStatusSchema>;

export namespace GameStatus {
  export const SCHEMA = GameStatusSchema;
}
