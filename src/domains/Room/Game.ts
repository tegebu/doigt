import { Arrays } from '@/lib/Collection/Array/Arrays.js';
import type { RuntimeError } from '@/lib/Error/Errors.js';
import { ExhaustiveError } from '@jamashita/anden/error';
import { Kind } from '@jamashita/anden/type';
import type { Either } from 'fp-ts/lib/Either.js';
import { map } from 'fp-ts/lib/Either.js';
import { pipe } from 'fp-ts/lib/function.js';
import { fromNullable, isSome, none, type Option, type Some, some } from 'fp-ts/lib/Option.js';
import { z } from 'zod';
import { Team } from '../Identity/Team.js';
import type { MediumOutcomeType } from '../Outcome/MediumOutcome.js';
import type { SeerOutcomeType } from '../Outcome/SeerOutcome.js';
import { Person, type PersonID } from '../Person/Person.js';
import { Persons } from '../Person/Persons.js';
import type { Role } from '../Role/Role.js';
import type { DailyOutcomeRecord } from './DailyOutcomeRecord.js';
import { DailyOutcomeRecords } from './DailyOutcomeRecords.js';
import { DayPhase } from './DayPhase.js';
import { GameStatus } from './GameStatus.js';
import { PhaseDuration } from './PhaseDuration.js';
import { TeamSize } from './TeamSize.js';

const GameSchema = z.object({
  dayPhase: DayPhase.SCHEMA,
  phaseDuration: PhaseDuration.SCHEMA,
  residents: Persons.SCHEMA,
  records: DailyOutcomeRecords.SCHEMA,
  status: GameStatus.SCHEMA
});

export type Game = Readonly<z.infer<typeof GameSchema>>;

export namespace Game {
  export const SCHEMA = GameSchema;

  export const addAttack = (game: Game, days: number, source: Person, destination: Person): Either<RuntimeError, Game> => {
    return pipe(
      DailyOutcomeRecords.addAttack(game.records, days, source, destination),
      map((records: DailyOutcomeRecords) => {
        return {
          ...game,
          records
        } satisfies Game;
      })
    );
  };

  export const addCorruption = (game: Game, days: number, source: Person, destination: Person): Either<RuntimeError, Game> => {
    return pipe(
      DailyOutcomeRecords.addCorruption(game.records, days, source, destination),
      map((records: DailyOutcomeRecords) => {
        return {
          ...game,
          records
        } satisfies Game;
      })
    );
  };

  export const addDivination = (game: Game, days: number, source: Person, destination: Person): Either<RuntimeError, Game> => {
    return pipe(
      DailyOutcomeRecords.addDivination(game.records, days, source, destination),
      map((records: DailyOutcomeRecords) => {
        return {
          ...game,
          records
        } satisfies Game;
      })
    );
  };

  export const addExecution = (game: Game, days: number, target: Person): Either<RuntimeError, Game> => {
    return pipe(
      DailyOutcomeRecords.addExecution(game.records, days, target),
      map((records: DailyOutcomeRecords) => {
        return {
          ...game,
          records
        } satisfies Game;
      })
    );
  };

  export const addProtection = (game: Game, days: number, source: Person, destination: Person): Either<RuntimeError, Game> => {
    return pipe(
      DailyOutcomeRecords.addProtection(game.records, days, source, destination),
      map((records: DailyOutcomeRecords) => {
        return {
          ...game,
          records
        } satisfies Game;
      })
    );
  };

  export const addRetaliation = (game: Game, days: number, source: Person, destination: Person): Either<RuntimeError, Game> => {
    return pipe(
      DailyOutcomeRecords.addRetaliation(game.records, days, source, destination),
      map((records: DailyOutcomeRecords) => {
        return {
          ...game,
          records
        } satisfies Game;
      })
    );
  };

  export const addVictim = (game: Game, days: number, target: Persons): Either<RuntimeError, Game> => {
    return pipe(
      DailyOutcomeRecords.addVictim(game.records, days, target),
      map((records: DailyOutcomeRecords) => {
        return {
          ...game,
          records
        } satisfies Game;
      })
    );
  };

  export const addVote = (game: Game, days: number, source: Person, destination: Person): Either<RuntimeError, Game> => {
    return pipe(
      DailyOutcomeRecords.addVote(game.records, days, source, destination),
      map((records: DailyOutcomeRecords) => {
        return {
          ...game,
          records
        } satisfies Game;
      })
    );
  };

  export const calculateTeamSize = (game: Game): TeamSize => {
    return TeamSize.calculateTeamSize(Game.getSurvivors(game));
  };

  export const channel = (person: Person): MediumOutcomeType => {
    switch (person.identity.role) {
      case 'ACCUSER':
      case 'APOSTATE':
      case 'ATONEMENT':
      case 'CULT_LEADER':
      case 'COUNT':
      case 'COWARD':
      case 'HUNTER':
      case 'INFORMANT':
      case 'JESTER':
      case 'KITSUNE':
      case 'KNIGHT':
      case 'MEDIUM':
      case 'MONASTIC':
      case 'ORATOR':
      case 'POSSESSED':
      case 'SEER':
      case 'VILLAGER': {
        return 'VILLAGER';
      }
      case 'CATALYST':
      case 'WEREWOLF': {
        return 'WEREWOLF';
      }
      default: {
        throw new ExhaustiveError(person.identity.role);
      }
    }
  };

  export const createRecord = (game: Game): Game => {
    return {
      ...game,
      records: DailyOutcomeRecords.create(game.records)
    } satisfies Game;
  };

  export const divine = (person: Person): SeerOutcomeType => {
    switch (person.identity.role) {
      case 'ACCUSER':
      case 'APOSTATE':
      case 'ATONEMENT':
      case 'CATALYST':
      case 'CULT_LEADER':
      case 'COUNT':
      case 'COWARD':
      case 'HUNTER':
      case 'INFORMANT':
      case 'JESTER':
      case 'KITSUNE':
      case 'KNIGHT':
      case 'MEDIUM':
      case 'MONASTIC':
      case 'ORATOR':
      case 'SEER':
      case 'VILLAGER': {
        return 'VILLAGER';
      }
      case 'POSSESSED':
      case 'WEREWOLF': {
        return 'WEREWOLF';
      }
      default: {
        throw new ExhaustiveError(person.identity.role);
      }
    }
  };

  export const exchangePerson = (game: Game, person: Person): Game => {
    return {
      ...game,
      residents: Persons.exchange(game.residents, person)
    } satisfies Game;
  };

  export const exchangePersons = (game: Game, persons: Persons): Game => {
    const newResidents = [...persons.values()].reduce((prev, curr) => {
      return Persons.exchange(prev, curr);
    }, game.residents);

    return {
      ...game,
      residents: newResidents
    } satisfies Game;
  };

  export const getByRole = (game: Game, role: Role): Persons => {
    return Persons.getByRole(game.residents, role);
  };

  export const getDeceased = (game: Game): Persons => {
    return Persons.filter(game.residents, (person: Person) => {
      return Person.isDead(person);
    });
  };

  export const getDuration = (game: Game): number => {
    switch (game.dayPhase.phase) {
      case 'MORNING': {
        return game.phaseDuration.morning;
      }
      case 'DAY': {
        return game.phaseDuration.day;
      }
      case 'EVENING': {
        return game.phaseDuration.evening;
      }
      case 'NIGHT': {
        return game.phaseDuration.night;
      }
      default: {
        throw new ExhaustiveError(game.dayPhase.phase);
      }
    }
  };

  export const getLatestRecord = (game: Game): Option<DailyOutcomeRecord> => {
    return DailyOutcomeRecords.getLatest(game.records);
  };

  export const getMaxDays = (game: Game): number => {
    return DailyOutcomeRecords.getMaxDays(game.records);
  };

  export const getResident = (game: Game, personID: PersonID): Option<Person> => {
    return fromNullable(game.residents.get(personID));
  };

  const getSpecifiedResidents = (game: Game, ids: ReadonlyArray<PersonID>): Array<Person> => {
    // 全員から選ぶ
    if (ids.length === 0) {
      return [...game.residents.values()];
    }

    return ids
      .map((id: PersonID) => {
        return Game.getResident(game, id);
      })
      .filter((option: Option<Person>): option is Some<Person> => {
        return isSome(option);
      })
      .map((some: Some<Person>) => {
        return some.value;
      });
  };

  export const getSurvivors = (game: Game): Persons => {
    return Persons.filter(game.residents, (person: Person) => {
      return Person.isAlive(person);
    });
  };

  /**
   * 勝者を取得します
   * 勝利チームは複数いる場合があります。現時点で勝利チームがいない場合は空配列を返します。
   *
   * @param game
   */
  export const getWinners = (game: Game): Array<Team> => {
    const winners: Array<Team> = [];
    const jesters = Persons.getByRole(Game.getDeceased(game), 'JESTER');

    // 道化がいて、死亡していたら道化は勝利
    if (jesters.size > 0) {
      winners.push(Team.JESTERS);
    }

    const size = Game.calculateTeamSize(game);

    if (size.werewolves <= 0) {
      if (size.kitsunes > 0) {
        winners.push(Team.KITSUNES);

        return winners;
      }

      winners.push(Team.VILLAGERS);

      return winners;
    }
    if (size.villagers <= size.werewolves) {
      if (size.kitsunes > 0) {
        winners.push(Team.KITSUNES);

        return winners;
      }

      winners.push(Team.WEREWOLVES);

      return winners;
    }

    return [];
  };

  export const init = (): Game => {
    return {
      dayPhase: DayPhase.init(),
      phaseDuration: PhaseDuration.init(),
      residents: Persons.empty(),
      records: DailyOutcomeRecords.empty(),
      status: 'OPEN'
    } satisfies Game;
  };

  export const isCompleted = (game: Game): boolean => {
    return game.status === 'COMPLETED';
  };

  export const isInProgress = (game: Game): boolean => {
    return game.status === 'IN_PROGRESS';
  };

  export const isOpen = (game: Game): boolean => {
    return game.status === 'OPEN';
  };

  export const joinResidents = (game: Game, residents: Persons): Game => {
    return {
      ...game,
      residents
    } satisfies Game;
  };

  export const nextPhase = (game: Game): Game => {
    return {
      ...game,
      dayPhase: DayPhase.next(game.dayPhase)
    } satisfies Game;
  };

  /**
   * IDを指定した場合はそのIDの中で存在する住人から選ぶ
   * 指定しなかった場合は全員から選ぶ
   *
   * @param ids
   */
  export const randomResident = (game: Game, ids: ReadonlyArray<PersonID> = []): Option<Person> => {
    if (game.residents.size === 0) {
      return none;
    }

    const residents = getSpecifiedResidents(game, ids);
    const resident = Arrays.shuffle(residents)[0];

    if (Kind.isUndefined(resident)) {
      return none;
    }

    return some(resident);
  };
}
