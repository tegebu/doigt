import { z } from 'zod';
import { Phase } from './Phase.js';

const DayPhaseSchema = z.object({
  days: z.number().int().nonnegative(),
  phase: Phase.SCHEMA
});

export type DayPhase = Readonly<z.infer<typeof DayPhaseSchema>>;

export namespace DayPhase {
  export const SCHEMA = DayPhaseSchema;

  export const init = (): DayPhase => {
    return {
      days: 0,
      phase: 'NIGHT'
    } satisfies DayPhase;
  };

  export const next = (dayPhase: DayPhase): DayPhase => {
    const nextPhase = Phase.next(dayPhase.phase);

    switch (nextPhase) {
      case 'MORNING': {
        return {
          days: dayPhase.days + 1,
          phase: 'MORNING'
        } satisfies DayPhase;
      }
      default: {
        return {
          days: dayPhase.days,
          phase: nextPhase
        } satisfies DayPhase;
      }
    }
  };
}
