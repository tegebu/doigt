import { z } from 'zod';

// 秒数です
const PhaseDurationSchema = z.object({
  morning: z.number().int().nonnegative(),
  day: z.number().int().nonnegative(),
  evening: z.number().int().nonnegative(),
  night: z.number().int().nonnegative()
});

export type PhaseDuration = Readonly<z.infer<typeof PhaseDurationSchema>>;

export namespace PhaseDuration {
  export const SCHEMA = PhaseDurationSchema;

  // デフォルト設定
  export const init = (): PhaseDuration => {
    return {
      morning: 30,
      day: 240,
      evening: 30,
      night: 30
    } satisfies PhaseDuration;
  };
}
