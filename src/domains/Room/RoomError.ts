type Detail =
  | 'RoomFull'
  | 'NoSuchRoom'
  | 'NoSuchRecord'
  | 'RoomNotOpen'
  | 'PasswordMismatch'
  | 'ModeratorCannotLeave'
  | 'NotJoined'
  | 'RoleAssignmentImpossible'
  | 'NoInnocent'
  | 'ModeratorNotMatch'
  | 'Unknown';

export type RoomError = Readonly<{
  error: 'RoomError';
  detail: Detail;
  message: string;
}>;

export const createRoomError = (detail: Detail, message: string): RoomError => {
  return {
    error: 'RoomError',
    detail,
    message
  };
};
