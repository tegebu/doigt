import { z } from 'zod';

const OutcomeRecordTypeSchema = z.union([
  z.literal('ATTACK'),
  z.literal('DIVINATION'),
  z.literal('EXECUTION'),
  z.literal('PROTECTION'),
  z.literal('VOTE')
]);

export type OutcomeRecordType = z.infer<typeof OutcomeRecordTypeSchema>;

export namespace OutcomeRecordType {
  export const SCHEMA = OutcomeRecordTypeSchema;
}
