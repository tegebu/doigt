import { GameStatus } from '@/domains/Room/GameStatus.js';
import { Arrays } from '@/lib/Collection/Array/Arrays.js';
import type { RuntimeError } from '@/lib/Error/Errors.js';
import { RuntimeError as RE } from '@jamashita/anden/error';
import { Kind } from '@jamashita/anden/type';
import { UUID } from '@jamashita/anden/uuid';
import { type Either, left, map, right } from 'fp-ts/lib/Either.js';
import { pipe } from 'fp-ts/lib/function.js';
import { fromNullable, type Option } from 'fp-ts/lib/Option.js';
import type { Tagged } from 'type-fest';
import { z } from 'zod';
import { Client, type ClientID } from '../Client/Client.js';
import { Clients } from '../Client/Clients.js';
import { Identity } from '../Identity/Identity.js';
import { Person, type PersonID } from '../Person/Person.js';
import { Persons } from '../Person/Persons.js';
import { RoleAssignment } from '../Role/RoleAssignment.js';
import type { DailyOutcomeRecord } from './DailyOutcomeRecord.js';
import { DailyOutcomeRecords } from './DailyOutcomeRecords.js';
import { DayPhase } from './DayPhase.js';
import { Game } from './Game.js';
import { PhaseDuration } from './PhaseDuration.js';
import { createRoomError, type RoomError } from './RoomError.js';

export type RoomID = Tagged<string, 'RoomID'>;

const IDSchema = z.custom<RoomID>((value: unknown) => {
  return Kind.isString(value) && UUID.validate(value);
});

const RoomSchema = z.object({
  id: IDSchema,
  name: z.string().min(1),
  password: z.string().nullable(),
  moderator: Client.SCHEMA.nullable(),
  quota: z.number().int().positive(),
  participants: Clients.SCHEMA,
  game: Game.SCHEMA
});
// 一覧で表示する1部屋の情報
const RoomListSchema = z.object({
  id: IDSchema,
  name: z.string().min(1),
  moderator: Client.SCHEMA.nullable(),
  quota: z.number().int().positive(),
  participants: z.number().int().nonnegative(),
  requirePassword: z.boolean(),
  status: GameStatus.SCHEMA
});

export type Room = Readonly<z.infer<typeof RoomSchema>>;
export type RoomList = Readonly<z.infer<typeof RoomListSchema>>;

export namespace Room {
  export const SCHEMA = RoomSchema;

  export namespace ID {
    export const SCHEMA = IDSchema;

    export const from = (value: string): RoomID => {
      return value as RoomID;
    };

    export const generate = (): RoomID => {
      return Room.ID.from(UUID.v7().get());
    };
  }

  export namespace List {
    export const SCHEMA = RoomListSchema;
  }

  export const addAttack = (room: Room, days: number, source: Person, destination: Person): Either<RuntimeError, Room> => {
    return pipe(
      Game.addAttack(room.game, days, source, destination),
      map((game) => {
        return {
          ...room,
          game
        } satisfies Room;
      })
    );
  };

  export const addCorruption = (room: Room, days: number, source: Person, destination: Person): Either<RuntimeError, Room> => {
    return pipe(
      Game.addCorruption(room.game, days, source, destination),
      map((game) => {
        return {
          ...room,
          game
        } satisfies Room;
      })
    );
  };

  export const addDivination = (room: Room, days: number, source: Person, destination: Person): Either<RuntimeError, Room> => {
    return pipe(
      Game.addDivination(room.game, days, source, destination),
      map((game) => {
        return {
          ...room,
          game
        } satisfies Room;
      })
    );
  };

  export const addExecution = (room: Room, days: number, target: Person): Either<RuntimeError, Room> => {
    return pipe(
      Game.addExecution(room.game, days, target),
      map((game) => {
        return {
          ...room,
          game
        } satisfies Room;
      })
    );
  };

  export const addProtection = (room: Room, days: number, source: Person, destination: Person): Either<RuntimeError, Room> => {
    return pipe(
      Game.addProtection(room.game, days, source, destination),
      map((game) => {
        return {
          ...room,
          game
        } satisfies Room;
      })
    );
  };

  export const addRetaliation = (room: Room, days: number, source: Person, destination: Person): Either<RuntimeError, Room> => {
    return pipe(
      Game.addRetaliation(room.game, days, source, destination),
      map((game) => {
        return {
          ...room,
          game
        } satisfies Room;
      })
    );
  };

  export const addVictim = (room: Room, days: number, target: Persons): Either<RuntimeError, Room> => {
    return pipe(
      Game.addVictim(room.game, days, target),
      map((game) => {
        return {
          ...room,
          game
        } satisfies Room;
      })
    );
  };

  export const addVote = (room: Room, days: number, source: Person, destination: Person): Either<RuntimeError, Room> => {
    return pipe(
      Game.addVote(room.game, days, source, destination),
      map((game) => {
        return {
          ...room,
          game
        } satisfies Room;
      })
    );
  };

  export const createRecord = (room: Room): Room => {
    return {
      ...room,
      game: Game.createRecord(room.game)
    } satisfies Room;
  };

  export const exchangePerson = (room: Room, person: Person): Room => {
    return {
      ...room,
      game: Game.exchangePerson(room.game, person)
    } satisfies Room;
  };

  export const exchangePersons = (room: Room, persons: Persons): Room => {
    return {
      ...room,
      game: Game.exchangePersons(room.game, persons)
    } satisfies Room;
  };

  export const getDuration = (room: Room): number => {
    return Game.getDuration(room.game);
  };

  export const getLatestRecord = (room: Room): Option<DailyOutcomeRecord> => {
    return Game.getLatestRecord(room.game);
  };

  export const getResident = (room: Room, personID: PersonID): Option<Person> => {
    return Game.getResident(room.game, personID);
  };

  export const getParticipant = (room: Room, clientID: ClientID): Option<Client> => {
    return fromNullable(room.participants.get(clientID));
  };

  export const hasBeenJoined = (room: Room, client: Client): boolean => {
    return room.participants.has(client.id);
  };

  export const hasPassword = (room: Room): boolean => {
    return Kind.isString(room.password);
  };

  // ゲームが終了しているかどうかを取得する
  export const isCompleted = (room: Room): boolean => {
    return Game.isCompleted(room.game);
  };

  export const isEmpty = (room: Room): boolean => {
    return room.participants.size === 0;
  };

  export const isFull = (room: Room): boolean => {
    return room.quota <= room.participants.size;
  };

  // ゲームが進行中かどうかを取得する
  export const isInProgress = (room: Room): boolean => {
    return Game.isInProgress(room.game);
  };

  export const isModerator = (room: Room, client: Client): boolean => {
    return room.moderator?.id === client.id;
  };

  export const isModeratorAbsent = (room: Room): boolean => {
    return Kind.isNull(room.moderator);
  };

  // ゲームが開始前かどうかを取得する
  export const isOpen = (room: Room): boolean => {
    return Game.isOpen(room.game);
  };

  // 人数制限チェックを行っていないので気をつけてください
  export const joinParticipant = (room: Room, client: Client): Room => {
    return {
      ...room,
      participants: Clients.join(room.participants, client)
    } satisfies Room;
  };

  // 全員がいなくなったら部屋を閉じてください
  export const leaveParticipant = (room: Room, client: Client): Room => {
    return {
      ...room,
      participants: Clients.leave(room.participants, client)
    } satisfies Room;
  };

  export const nextPhase = (room: Room): Room => {
    if (Room.isOpen(room)) {
      return room;
    }

    return {
      ...room,
      game: Game.nextPhase(room.game)
    } satisfies Room;
  };

  // 部屋の人数を返す
  export const occupants = (room: Room): number => {
    return room.participants.size;
  };

  export const startGame = (room: Room, assignment: RoleAssignment): Either<RoomError, Room> => {
    const roles = Arrays.shuffle(RoleAssignment.expandRoles(assignment));

    if (room.participants.size !== roles.length) {
      return left(createRoomError('RoleAssignmentImpossible', `参加者の数と役職の数が一致しません: ${room.participants.size} !== ${roles.length}`));
    }

    let i = 0;
    const persons = [...room.participants.values()].map((client) => {
      const role = roles[i];

      if (Kind.isUndefined(role)) {
        throw new RE('Role array is shorter than the number of participants');
      }

      i++;

      return {
        id: Person.ID.generate(),
        identity: Identity.getByRole(role),
        life: 'ALIVE',
        client
      } satisfies Person;
    });
    const game = {
      dayPhase: DayPhase.init(),
      phaseDuration: PhaseDuration.init(),
      residents: Persons.ofArray([...persons]),
      records: DailyOutcomeRecords.empty(),
      status: 'IN_PROGRESS'
    } satisfies Game;

    return right({
      ...room,
      game
    } satisfies Room);
  };

  export const terminateGame = (room: Room): Room => {
    return {
      ...room,
      game: {
        ...room.game,
        status: 'COMPLETED'
      }
    } satisfies Room;
  };
}
