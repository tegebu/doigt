import { Phase } from '../Phase.js';

describe('Phase', () => {
  describe('next', () => {
    it.each`
      phase        | expected
      ${'MORNING'} | ${'DAY'}
      ${'DAY'}     | ${'EVENING'}
      ${'EVENING'} | ${'NIGHT'}
      ${'NIGHT'}   | ${'MORNING'}
    `('should return $expected when $phase is given', ({ phase, expected }) => {
      expect(Phase.next(phase)).toBe(expected);
    });
  });
});
