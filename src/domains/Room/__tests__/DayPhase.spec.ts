import { DayPhase } from '../DayPhase.js';

describe('DayPhase', () => {
  describe('init', () => {
    it('should return start phase', () => {
      const result = DayPhase.init();

      expect(result.days).toBe(0);
      expect(result.phase).toBe('NIGHT');
    });
  });

  describe('next', () => {
    it('should return next phase', () => {
      const dayPhase = {
        days: 0,
        phase: 'NIGHT'
      } satisfies DayPhase;

      const result1 = DayPhase.next(dayPhase);

      expect(result1.days).toBe(1);
      expect(result1.phase).toBe('MORNING');

      const result2 = DayPhase.next(result1);

      expect(result2.days).toBe(1);
      expect(result2.phase).toBe('DAY');

      const result3 = DayPhase.next(result2);

      expect(result3.days).toBe(1);
      expect(result3.phase).toBe('EVENING');

      const result4 = DayPhase.next(result3);

      expect(result4.days).toBe(1);
      expect(result4.phase).toBe('NIGHT');

      const result5 = DayPhase.next(result4);

      expect(result5.days).toBe(2);
      expect(result5.phase).toBe('MORNING');
    });
  });
});
