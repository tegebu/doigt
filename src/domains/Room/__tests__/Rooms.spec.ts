import { mockClient } from '@/domains/Client/mocks/MockClient.js';
import { mockRoom, mockRoomList } from '../mocks/MockRoom.js';
import { Rooms } from '../Rooms.js';

describe('Rooms', () => {
  describe('List', () => {
    describe('add', () => {
      it('should add room', () => {
        const rooms = Rooms.List.ofArray([
          mockRoomList({
            id: '2d692a33-31ce-4803-95aa-48d44d4a14ac'
          })
        ]);

        expect(rooms.size).toBe(1);

        const list = mockRoomList({
          id: 'c12aac47-10eb-4723-938c-896339282087'
        });

        const addedLists = Rooms.List.add(rooms, list);

        expect(rooms.size).toBe(1);
        expect(addedLists.size).toBe(2);
      });

      it('should not add existing room', () => {
        const list = mockRoomList({
          id: '2d692a33-31ce-4803-95aa-48d44d4a14ac'
        });

        const rooms = Rooms.List.ofArray([list]);

        expect(rooms.size).toBe(1);

        const addedRooms = Rooms.List.add(rooms, list);

        expect(rooms.size).toBe(1);
        expect(addedRooms.size).toBe(1);
      });
    });

    describe('remove', () => {
      it('should remove room', () => {
        const list = mockRoomList({
          id: '2d692a33-31ce-4803-95aa-48d44d4a14ac'
        });

        const rooms = Rooms.List.ofArray([list]);

        expect(rooms.size).toBe(1);

        const removedRooms = Rooms.List.remove(rooms, list.id);

        expect(rooms.size).toBe(1);
        expect(removedRooms.size).toBe(0);
      });

      it('should not remove non-existent room', () => {
        const rooms = Rooms.List.ofArray([
          mockRoomList({
            id: '2d692a33-31ce-4803-95aa-48d44d4a14ac'
          })
        ]);

        expect(rooms.size).toBe(1);

        const list = mockRoomList({
          id: 'c12aac47-10eb-4723-938c-896339282087'
        });

        const removedRooms = Rooms.List.remove(rooms, list.id);

        expect(rooms.size).toBe(1);
        expect(removedRooms.size).toBe(1);
      });
    });

    describe('replace', () => {
      it('should replace room', () => {
        const list1 = mockRoomList({
          id: '2d692a33-31ce-4803-95aa-48d44d4a14ac',
          name: 'ROOM NAME',
          requirePassword: false,
          quota: 10,
          moderator: null,
          participants: 0,
          status: 'OPEN'
        });
        const list2 = mockRoomList({
          id: '2d692a33-31ce-4803-95aa-48d44d4a14ac',
          name: 'DIFFERENT ROOM NAME',
          requirePassword: true,
          quota: 20,
          moderator: mockClient({
            id: 'd286d3a9-c95c-4d2c-a7b0-75f76609b41c'
          }),
          participants: 1,
          status: 'COMPLETED'
        });

        const rooms = Rooms.List.ofArray([list1]);

        expect(rooms.size).toBe(1);

        const replacedRooms = Rooms.List.replace(rooms, list2);

        expect(rooms.size).toBe(1);
        expect(replacedRooms.size).toBe(1);

        const list = replacedRooms.get(list2.id);

        expect(list?.id).toBe(list2.id);
        expect(list?.name).toBe(list2.name);
        expect(list?.requirePassword).toBe(list2.requirePassword);
        expect(list?.quota).toBe(list2.quota);
        expect(list?.moderator).toBe(list2.moderator);
        expect(list?.participants).toBe(list2.participants);
        expect(list?.status).toBe(list2.status);
      });

      it('should not replace non-existent room', () => {
        const rooms = Rooms.List.ofArray([
          mockRoomList({
            id: '2d692a33-31ce-4803-95aa-48d44d4a14ac'
          })
        ]);

        expect(rooms.size).toBe(1);

        const list = mockRoomList({
          id: 'c12aac47-10eb-4723-938c-896339282087'
        });

        const replacedRooms = Rooms.List.replace(rooms, list);

        expect(rooms.size).toBe(1);
        expect(replacedRooms.size).toBe(1);
        expect(replacedRooms.get(list.id)).toBe(undefined);
      });
    });
  });

  describe('close', () => {
    it('should close room', () => {
      const room = mockRoom({
        id: 'c12aac47-10eb-4723-938c-896339282087'
      });

      const rooms = Rooms.ofArray([room]);

      expect(rooms.size).toBe(1);

      const closedRooms = Rooms.close(rooms, room.id);

      expect(rooms.size).toBe(1);
      expect(closedRooms.size).toBe(0);
    });

    it('should not close non-existent room', () => {
      const rooms = Rooms.ofArray([
        mockRoom({
          id: '2d692a33-31ce-4803-95aa-48d44d4a14ac'
        })
      ]);

      expect(rooms.size).toBe(1);

      const room = mockRoom({
        id: 'c12aac47-10eb-4723-938c-896339282087'
      });

      const closedRooms = Rooms.close(rooms, room.id);

      expect(rooms.size).toBe(1);
      expect(closedRooms.size).toBe(1);
    });
  });

  describe('create', () => {
    it('should create room', () => {
      const rooms = Rooms.empty();

      expect(rooms.size).toBe(0);

      const room = mockRoom();

      const createdRooms = Rooms.create(rooms, room);

      expect(rooms.size).toBe(0);
      expect(createdRooms.size).toBe(1);
    });
  });
});
