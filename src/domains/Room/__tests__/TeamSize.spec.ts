import { Identity } from '@/domains/Identity/Identity.js';
import { mockPerson } from '@/domains/Person/mocks/MockPerson.js';
import { Persons } from '@/domains/Person/Persons.js';
import { TeamSize } from '../TeamSize.js';

describe('TeamSize', () => {
  describe('calculateTeamSize', () => {
    it('should return the number of each team', () => {
      const persons = Persons.ofArray([
        mockPerson({
          id: '0b186068-4e94-4481-9923-af995c753bd4',
          identity: Identity.VILLAGER
        }),
        mockPerson({
          id: '81de30a0-0b23-4c8b-a33b-3713fbfd9d86',
          identity: Identity.VILLAGER
        }),
        mockPerson({
          id: 'ed10d594-ade8-4667-ab33-aa07c3b19d77',
          identity: Identity.SEER
        }),
        mockPerson({
          id: 'aa76841e-3df6-4462-8563-28adda4cd903',
          identity: Identity.MEDIUM
        }),
        mockPerson({
          id: 'b6759361-ad28-4f88-9762-787024f6254f',
          identity: Identity.WEREWOLF
        }),
        mockPerson({
          id: '410fbd32-af29-4119-80c6-5b587a6f246d',
          identity: Identity.WEREWOLF
        }),
        mockPerson({
          id: 'a952e25d-d998-4b27-956b-e7100e111085',
          identity: Identity.INFORMANT
        }),
        mockPerson({
          id: '952a9fa6-d5ac-4d34-a097-03e9f37563b7',
          identity: Identity.KITSUNE
        })
      ]);

      const result = TeamSize.calculateTeamSize(persons);

      expect(result.villagers).toBe(5);
      expect(result.werewolves).toBe(2);
      expect(result.kitsunes).toBe(1);
      expect(result.jesters).toBe(0);
    });

    it('should return the number of each team: catalyst will be counted as werewolf', () => {
      const persons = Persons.ofArray([
        mockPerson({
          id: '0b186068-4e94-4481-9923-af995c753bd4',
          identity: Identity.VILLAGER
        }),
        mockPerson({
          id: '81de30a0-0b23-4c8b-a33b-3713fbfd9d86',
          identity: Identity.VILLAGER
        }),
        mockPerson({
          id: 'ed10d594-ade8-4667-ab33-aa07c3b19d77',
          identity: Identity.SEER
        }),
        mockPerson({
          id: 'aa76841e-3df6-4462-8563-28adda4cd903',
          identity: Identity.MEDIUM
        }),
        mockPerson({
          id: 'b6759361-ad28-4f88-9762-787024f6254f',
          identity: Identity.WEREWOLF
        }),
        mockPerson({
          id: '410fbd32-af29-4119-80c6-5b587a6f246d',
          identity: Identity.WEREWOLF
        }),
        mockPerson({
          id: 'a952e25d-d998-4b27-956b-e7100e111085',
          identity: Identity.CATALYST
        }),
        mockPerson({
          id: '952a9fa6-d5ac-4d34-a097-03e9f37563b7',
          identity: Identity.KITSUNE
        })
      ]);

      const result = TeamSize.calculateTeamSize(persons);

      expect(result.villagers).toBe(4);
      expect(result.werewolves).toBe(3);
      expect(result.kitsunes).toBe(1);
      expect(result.jesters).toBe(0);
    });

    it('should return the number of each team: there are no werewolf', () => {
      const persons = Persons.ofArray([
        mockPerson({
          id: '0b186068-4e94-4481-9923-af995c753bd4',
          identity: Identity.VILLAGER
        }),
        mockPerson({
          id: '81de30a0-0b23-4c8b-a33b-3713fbfd9d86',
          identity: Identity.VILLAGER
        }),
        mockPerson({
          id: 'ed10d594-ade8-4667-ab33-aa07c3b19d77',
          identity: Identity.SEER
        }),
        mockPerson({
          id: 'aa76841e-3df6-4462-8563-28adda4cd903',
          identity: Identity.MEDIUM
        }),
        mockPerson({
          id: 'a952e25d-d998-4b27-956b-e7100e111085',
          identity: Identity.CATALYST
        }),
        mockPerson({
          id: '952a9fa6-d5ac-4d34-a097-03e9f37563b7',
          identity: Identity.KITSUNE
        })
      ]);

      const result = TeamSize.calculateTeamSize(persons);

      expect(result.villagers).toBe(4);
      expect(result.werewolves).toBe(0);
      expect(result.kitsunes).toBe(1);
      expect(result.jesters).toBe(0);
    });

    it('should return the number of each team: jester will be counted as jesters', () => {
      const persons = Persons.ofArray([
        mockPerson({
          id: '0b186068-4e94-4481-9923-af995c753bd4',
          identity: Identity.VILLAGER
        }),
        mockPerson({
          id: '81de30a0-0b23-4c8b-a33b-3713fbfd9d86',
          identity: Identity.VILLAGER
        }),
        mockPerson({
          id: 'ed10d594-ade8-4667-ab33-aa07c3b19d77',
          identity: Identity.SEER
        }),
        mockPerson({
          id: 'aa76841e-3df6-4462-8563-28adda4cd903',
          identity: Identity.MEDIUM
        }),
        mockPerson({
          id: 'a952e25d-d998-4b27-956b-e7100e111085',
          identity: Identity.JESTER
        }),
        mockPerson({
          id: '952a9fa6-d5ac-4d34-a097-03e9f37563b7',
          identity: Identity.KITSUNE
        })
      ]);

      const result = TeamSize.calculateTeamSize(persons);

      expect(result.villagers).toBe(4);
      expect(result.werewolves).toBe(0);
      expect(result.kitsunes).toBe(1);
      expect(result.jesters).toBe(1);
    });
  });
});
