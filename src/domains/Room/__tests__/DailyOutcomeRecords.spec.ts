import { mockPerson } from '@/domains/Person/mocks/MockPerson.js';
import { Persons } from '@/domains/Person/Persons.js';
import { isLeft, isRight } from 'fp-ts/lib/Either.js';
import { isNone, isSome } from 'fp-ts/lib/Option.js';
import { DailyOutcomeRecords } from '../DailyOutcomeRecords.js';

describe('DailyOutcomeRecords', () => {
  describe('addAttack', () => {
    it('should add a person to attack', () => {
      const source = mockPerson({
        id: '79f16d66-8549-43f5-a8ff-d8aa4b21c282'
      });
      const destination = mockPerson({
        id: '145249b6-55be-4ca8-91e8-d5224049e196'
      });
      const records1 = DailyOutcomeRecords.empty();
      const records2 = DailyOutcomeRecords.create(records1);
      const records3 = DailyOutcomeRecords.create(records2);
      const records4 = DailyOutcomeRecords.create(records3);
      const either = DailyOutcomeRecords.addAttack(records4, 1, source, destination);

      expect(isRight(either)).toBe(true);

      if (isLeft(either)) {
        throw new Error('This should not happen');
      }

      const record = either.right.get(1);

      expect(record?.attack.length).toBe(1);
      expect(record?.divination.length).toBe(0);
      expect(record?.execution).toBe(null);
      expect(record?.protection.length).toBe(0);
      expect(record?.victim.length).toBe(0);
      expect(record?.vote.length).toBe(0);
    });

    it('should return Left(RuntimeError) when the record does not exist', () => {
      const source = mockPerson({
        id: '79f16d66-8549-43f5-a8ff-d8aa4b21c282'
      });
      const destination = mockPerson({
        id: '145249b6-55be-4ca8-91e8-d5224049e196'
      });
      const records = DailyOutcomeRecords.empty();
      const either = DailyOutcomeRecords.addAttack(records, 1, source, destination);

      expect(isLeft(either)).toBe(true);

      if (isRight(either)) {
        throw new Error('This should not happen');
      }

      expect(either.left.error).toBe('RuntimeError');
    });
  });

  describe('addDivination', () => {
    it('should add a person to divination', () => {
      const source = mockPerson({
        id: '79f16d66-8549-43f5-a8ff-d8aa4b21c282'
      });
      const destination = mockPerson({
        id: '145249b6-55be-4ca8-91e8-d5224049e196'
      });
      const records1 = DailyOutcomeRecords.empty();
      const records2 = DailyOutcomeRecords.create(records1);
      const records3 = DailyOutcomeRecords.create(records2);
      const records4 = DailyOutcomeRecords.create(records3);
      const either = DailyOutcomeRecords.addDivination(records4, 1, source, destination);

      expect(isRight(either)).toBe(true);

      if (isLeft(either)) {
        throw new Error('This should not happen');
      }

      const record = either.right.get(1);

      expect(record?.attack.length).toBe(0);
      expect(record?.divination.length).toBe(1);
      expect(record?.execution).toBe(null);
      expect(record?.protection.length).toBe(0);
      expect(record?.victim.length).toBe(0);
      expect(record?.vote.length).toBe(0);
    });

    it('should return Left(RuntimeError) when the record does not exist', () => {
      const source = mockPerson({
        id: '79f16d66-8549-43f5-a8ff-d8aa4b21c282'
      });
      const destination = mockPerson({
        id: '145249b6-55be-4ca8-91e8-d5224049e196'
      });
      const records = DailyOutcomeRecords.empty();
      const either = DailyOutcomeRecords.addDivination(records, 1, source, destination);

      expect(isLeft(either)).toBe(true);

      if (isRight(either)) {
        throw new Error('This should not happen');
      }

      expect(either.left.error).toBe('RuntimeError');
    });
  });

  describe('addExecution', () => {
    it('should add a person to execution', () => {
      const target = mockPerson({
        id: '79f16d66-8549-43f5-a8ff-d8aa4b21c282'
      });
      const records1 = DailyOutcomeRecords.empty();
      const records2 = DailyOutcomeRecords.create(records1);
      const records3 = DailyOutcomeRecords.create(records2);
      const records4 = DailyOutcomeRecords.create(records3);
      const either = DailyOutcomeRecords.addExecution(records4, 1, target);

      expect(isRight(either)).toBe(true);

      if (isLeft(either)) {
        throw new Error('This should not happen');
      }

      const record = either.right.get(1);

      expect(record?.attack.length).toBe(0);
      expect(record?.divination.length).toBe(0);
      expect(record?.execution).toBe(target.id);
      expect(record?.protection.length).toBe(0);
      expect(record?.victim.length).toBe(0);
      expect(record?.vote.length).toBe(0);
    });

    it('should return Left(RuntimeError) when the record does not exist', () => {
      const target = mockPerson({
        id: '79f16d66-8549-43f5-a8ff-d8aa4b21c282'
      });
      const records = DailyOutcomeRecords.empty();
      const either = DailyOutcomeRecords.addExecution(records, 1, target);

      expect(isLeft(either)).toBe(true);

      if (isRight(either)) {
        throw new Error('This should not happen');
      }

      expect(either.left.error).toBe('RuntimeError');
    });
  });

  describe('addProtection', () => {
    it('should add a person to protection', () => {
      const source = mockPerson({
        id: '79f16d66-8549-43f5-a8ff-d8aa4b21c282'
      });
      const destination = mockPerson({
        id: '145249b6-55be-4ca8-91e8-d5224049e196'
      });
      const records1 = DailyOutcomeRecords.empty();
      const records2 = DailyOutcomeRecords.create(records1);
      const records3 = DailyOutcomeRecords.create(records2);
      const records4 = DailyOutcomeRecords.create(records3);
      const either = DailyOutcomeRecords.addProtection(records4, 1, source, destination);

      expect(isRight(either)).toBe(true);

      if (isLeft(either)) {
        throw new Error('This should not happen');
      }

      const record = either.right.get(1);

      expect(record?.attack.length).toBe(0);
      expect(record?.divination.length).toBe(0);
      expect(record?.execution).toBe(null);
      expect(record?.protection.length).toBe(1);
      expect(record?.victim.length).toBe(0);
      expect(record?.vote.length).toBe(0);
    });

    it('should return Left(RuntimeError) when the record does not exist', () => {
      const source = mockPerson({
        id: '79f16d66-8549-43f5-a8ff-d8aa4b21c282'
      });
      const destination = mockPerson({
        id: '145249b6-55be-4ca8-91e8-d5224049e196'
      });
      const records = DailyOutcomeRecords.empty();
      const either = DailyOutcomeRecords.addProtection(records, 1, source, destination);

      expect(isLeft(either)).toBe(true);

      if (isRight(either)) {
        throw new Error('This should not happen');
      }

      expect(either.left.error).toBe('RuntimeError');
    });
  });

  describe('addVictim', () => {
    it('should add a person to victim', () => {
      const target1 = mockPerson({
        id: '79f16d66-8549-43f5-a8ff-d8aa4b21c282'
      });
      const target2 = mockPerson({
        id: '5f7dc393-0910-4484-a2b5-031d1b911cc6'
      });
      const records1 = DailyOutcomeRecords.empty();
      const records2 = DailyOutcomeRecords.create(records1);
      const records3 = DailyOutcomeRecords.create(records2);
      const records4 = DailyOutcomeRecords.create(records3);
      const either = DailyOutcomeRecords.addVictim(records4, 1, Persons.ofArray([target1, target2]));

      expect(isRight(either)).toBe(true);

      if (isLeft(either)) {
        throw new Error('This should not happen');
      }

      const record = either.right.get(1);

      expect(record?.attack.length).toBe(0);
      expect(record?.divination.length).toBe(0);
      expect(record?.execution).toBe(null);
      expect(record?.protection.length).toBe(0);
      expect(record?.victim.length).toBe(2);
      expect(record?.vote.length).toBe(0);
    });

    it('should return Left(RuntimeError) when the record does not exist', () => {
      const target1 = mockPerson({
        id: '79f16d66-8549-43f5-a8ff-d8aa4b21c282'
      });
      const target2 = mockPerson({
        id: '5f7dc393-0910-4484-a2b5-031d1b911cc6'
      });
      const records = DailyOutcomeRecords.empty();
      const either = DailyOutcomeRecords.addVictim(records, 1, Persons.ofArray([target1, target2]));

      expect(isLeft(either)).toBe(true);

      if (isRight(either)) {
        throw new Error('This should not happen');
      }

      expect(either.left.error).toBe('RuntimeError');
    });
  });

  describe('addVote', () => {
    it('should add a person to protection', () => {
      const source = mockPerson({
        id: '79f16d66-8549-43f5-a8ff-d8aa4b21c282'
      });
      const destination = mockPerson({
        id: '145249b6-55be-4ca8-91e8-d5224049e196'
      });
      const records1 = DailyOutcomeRecords.empty();
      const records2 = DailyOutcomeRecords.create(records1);
      const records3 = DailyOutcomeRecords.create(records2);
      const records4 = DailyOutcomeRecords.create(records3);
      const either = DailyOutcomeRecords.addVote(records4, 1, source, destination);

      expect(isRight(either)).toBe(true);

      if (isLeft(either)) {
        throw new Error('This should not happen');
      }

      const record = either.right.get(1);

      expect(record?.attack.length).toBe(0);
      expect(record?.divination.length).toBe(0);
      expect(record?.execution).toBe(null);
      expect(record?.protection.length).toBe(0);
      expect(record?.victim.length).toBe(0);
      expect(record?.vote.length).toBe(1);
    });

    it('should return Left(RuntimeError) when the record does not exist', () => {
      const source = mockPerson({
        id: '79f16d66-8549-43f5-a8ff-d8aa4b21c282'
      });
      const destination = mockPerson({
        id: '145249b6-55be-4ca8-91e8-d5224049e196'
      });
      const records = DailyOutcomeRecords.empty();
      const either = DailyOutcomeRecords.addVote(records, 1, source, destination);

      expect(isLeft(either)).toBe(true);

      if (isRight(either)) {
        throw new Error('This should not happen');
      }

      expect(either.left.error).toBe('RuntimeError');
    });
  });

  describe('create', () => {
    it('should create a new record with days 0', () => {
      const records = DailyOutcomeRecords.create(DailyOutcomeRecords.empty());

      expect(records.size).toBe(1);
      expect(records.get(0)).not.toBe(null);
    });

    it('should create a new record with the next day of max days', () => {
      const records1 = DailyOutcomeRecords.create(DailyOutcomeRecords.empty());
      const records2 = DailyOutcomeRecords.create(records1);

      expect(records1.size).toBe(1);
      expect(records1.get(0)).not.toBe(null);
      expect(records2.size).toBe(2);
      expect(records2.get(0)).not.toBe(null);
      expect(records2.get(1)).not.toBe(null);
      expect(records1.get(0)).toBe(records2.get(0));
      expect(records2.get(0)).not.toBe(records2.get(1));
    });
  });

  describe('getChronologically', () => {
    it('should return records in chronological order', () => {
      const records1 = DailyOutcomeRecords.create(DailyOutcomeRecords.empty());
      const records2 = DailyOutcomeRecords.create(records1);
      const records3 = DailyOutcomeRecords.create(records2);
      const records4 = DailyOutcomeRecords.create(records3);
      const chronologically = DailyOutcomeRecords.getChronologically(records4);

      expect(chronologically.length).toBe(4);
      expect(chronologically[0]?.days).toBe(0);
      expect(chronologically[1]?.days).toBe(1);
      expect(chronologically[2]?.days).toBe(2);
      expect(chronologically[3]?.days).toBe(3);
    });
  });

  describe('getLatest', () => {
    it('should return the latest record', () => {
      const records1 = DailyOutcomeRecords.create(DailyOutcomeRecords.empty());
      const option1 = DailyOutcomeRecords.getLatest(records1);

      expect(isSome(option1)).toBe(true);

      if (isNone(option1)) {
        throw new Error('This should not happen');
      }

      expect(option1.value).toBe(records1.get(0));

      const records2 = DailyOutcomeRecords.create(records1);
      const option2 = DailyOutcomeRecords.getLatest(records2);

      expect(isSome(option2)).toBe(true);

      if (isNone(option2)) {
        throw new Error('This should not happen');
      }

      expect(option2.value).toBe(records2.get(1));
    });

    it('should return None when there is no record', () => {
      const records = DailyOutcomeRecords.empty();
      const option = DailyOutcomeRecords.getLatest(records);

      expect(isNone(option)).toBe(true);
    });
  });
});
