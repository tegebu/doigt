import { mockPerson } from '@/domains/Person/mocks/MockPerson.js';
import { Persons } from '@/domains/Person/Persons.js';
import { DailyOutcomeRecord } from '../DailyOutcomeRecord.js';

describe('DailyOutcomeRecord', () => {
  describe('addAttack', () => {
    it('should add a person to attack', () => {
      const record = DailyOutcomeRecord.create(1);
      const source = mockPerson({
        id: '5b898f56-9c29-4662-b73f-d5da0189e6e2'
      });
      const destination = mockPerson({
        id: '5b898f56-9c29-4662-b73f-d5da0189e6e2'
      });

      expect(record.attack.length).toBe(0);
      expect(record.corruption.length).toBe(0);
      expect(record.divination.length).toBe(0);
      expect(record.execution).toBe(null);
      expect(record.protection.length).toBe(0);
      expect(record.retaliation.length).toBe(0);
      expect(record.victim.length).toBe(0);
      expect(record.vote.length).toBe(0);

      const result = DailyOutcomeRecord.addAttack(record, source, destination);

      expect(record.attack.length).toBe(0);
      expect(record.corruption.length).toBe(0);
      expect(record.divination.length).toBe(0);
      expect(record.execution).toBe(null);
      expect(record.protection.length).toBe(0);
      expect(record.retaliation.length).toBe(0);
      expect(record.victim.length).toBe(0);
      expect(record.vote.length).toBe(0);
      expect(result.attack.length).toBe(1);
      expect(result.corruption.length).toBe(0);
      expect(result.divination.length).toBe(0);
      expect(result.execution).toBe(null);
      expect(result.protection.length).toBe(0);
      expect(result.retaliation.length).toBe(0);
      expect(result.victim.length).toBe(0);
      expect(result.vote.length).toBe(0);

      expect(result.attack[0]?.source).toBe(source.id);
      expect(result.attack[0]?.destination).toBe(destination.id);
    });
  });

  describe('addCorruption', () => {
    it('should add a person to corruption', () => {
      const record = DailyOutcomeRecord.create(1);
      const source = mockPerson({
        id: '5b898f56-9c29-4662-b73f-d5da0189e6e2'
      });
      const destination = mockPerson({
        id: '5b898f56-9c29-4662-b73f-d5da0189e6e2'
      });

      expect(record.attack.length).toBe(0);
      expect(record.corruption.length).toBe(0);
      expect(record.divination.length).toBe(0);
      expect(record.execution).toBe(null);
      expect(record.protection.length).toBe(0);
      expect(record.retaliation.length).toBe(0);
      expect(record.victim.length).toBe(0);
      expect(record.vote.length).toBe(0);

      const result = DailyOutcomeRecord.addCorruption(record, source, destination);

      expect(record.attack.length).toBe(0);
      expect(record.corruption.length).toBe(0);
      expect(record.divination.length).toBe(0);
      expect(record.execution).toBe(null);
      expect(record.protection.length).toBe(0);
      expect(record.retaliation.length).toBe(0);
      expect(record.victim.length).toBe(0);
      expect(record.vote.length).toBe(0);
      expect(result.attack.length).toBe(0);
      expect(result.corruption.length).toBe(1);
      expect(result.divination.length).toBe(0);
      expect(result.execution).toBe(null);
      expect(result.protection.length).toBe(0);
      expect(result.retaliation.length).toBe(0);
      expect(result.victim.length).toBe(0);
      expect(result.vote.length).toBe(0);

      expect(result.corruption[0]?.source).toBe(source.id);
      expect(result.corruption[0]?.destination).toBe(destination.id);
    });
  });

  describe('getDeviation', () => {
    it('should add a person to divination', () => {
      const record = DailyOutcomeRecord.create(1);
      const source = mockPerson({
        id: '5b898f56-9c29-4662-b73f-d5da0189e6e2'
      });
      const destination = mockPerson({
        id: '5b898f56-9c29-4662-b73f-d5da0189e6e2'
      });

      expect(record.attack.length).toBe(0);
      expect(record.corruption.length).toBe(0);
      expect(record.divination.length).toBe(0);
      expect(record.execution).toBe(null);
      expect(record.protection.length).toBe(0);
      expect(record.retaliation.length).toBe(0);
      expect(record.victim.length).toBe(0);
      expect(record.vote.length).toBe(0);

      const result = DailyOutcomeRecord.addDivination(record, source, destination);

      expect(record.attack.length).toBe(0);
      expect(record.corruption.length).toBe(0);
      expect(record.divination.length).toBe(0);
      expect(record.execution).toBe(null);
      expect(record.protection.length).toBe(0);
      expect(record.retaliation.length).toBe(0);
      expect(record.victim.length).toBe(0);
      expect(record.vote.length).toBe(0);
      expect(result.attack.length).toBe(0);
      expect(result.corruption.length).toBe(0);
      expect(result.divination.length).toBe(1);
      expect(result.execution).toBe(null);
      expect(result.protection.length).toBe(0);
      expect(result.retaliation.length).toBe(0);
      expect(result.victim.length).toBe(0);
      expect(result.vote.length).toBe(0);

      expect(result.divination[0]?.source).toBe(source.id);
      expect(result.divination[0]?.destination).toBe(destination.id);
    });
  });

  describe('addExecution', () => {
    it('should add a person to execution', () => {
      const record = DailyOutcomeRecord.create(1);
      const target = mockPerson({
        id: '5b898f56-9c29-4662-b73f-d5da0189e6e2'
      });

      expect(record.attack.length).toBe(0);
      expect(record.corruption.length).toBe(0);
      expect(record.divination.length).toBe(0);
      expect(record.execution).toBe(null);
      expect(record.protection.length).toBe(0);
      expect(record.retaliation.length).toBe(0);
      expect(record.victim.length).toBe(0);
      expect(record.vote.length).toBe(0);

      const result = DailyOutcomeRecord.addExecution(record, target);

      expect(record.attack.length).toBe(0);
      expect(record.corruption.length).toBe(0);
      expect(record.divination.length).toBe(0);
      expect(record.execution).toBe(null);
      expect(record.protection.length).toBe(0);
      expect(record.retaliation.length).toBe(0);
      expect(record.victim.length).toBe(0);
      expect(record.vote.length).toBe(0);
      expect(result.attack.length).toBe(0);
      expect(result.corruption.length).toBe(0);
      expect(result.divination.length).toBe(0);
      expect(result.execution).toBe(target.id);
      expect(result.protection.length).toBe(0);
      expect(result.retaliation.length).toBe(0);
      expect(result.victim.length).toBe(0);
      expect(result.vote.length).toBe(0);
    });
  });

  describe('addProtection', () => {
    it('should add a person to protection', () => {
      const record = DailyOutcomeRecord.create(1);
      const source = mockPerson({
        id: '5b898f56-9c29-4662-b73f-d5da0189e6e2'
      });
      const destination = mockPerson({
        id: '5b898f56-9c29-4662-b73f-d5da0189e6e2'
      });

      expect(record.attack.length).toBe(0);
      expect(record.corruption.length).toBe(0);
      expect(record.divination.length).toBe(0);
      expect(record.execution).toBe(null);
      expect(record.protection.length).toBe(0);
      expect(record.retaliation.length).toBe(0);
      expect(record.victim.length).toBe(0);
      expect(record.vote.length).toBe(0);

      const result = DailyOutcomeRecord.addProtection(record, source, destination);

      expect(record.attack.length).toBe(0);
      expect(record.corruption.length).toBe(0);
      expect(record.divination.length).toBe(0);
      expect(record.execution).toBe(null);
      expect(record.protection.length).toBe(0);
      expect(record.retaliation.length).toBe(0);
      expect(record.victim.length).toBe(0);
      expect(record.vote.length).toBe(0);
      expect(result.attack.length).toBe(0);
      expect(result.corruption.length).toBe(0);
      expect(result.divination.length).toBe(0);
      expect(result.execution).toBe(null);
      expect(result.protection.length).toBe(1);
      expect(result.retaliation.length).toBe(0);
      expect(result.victim.length).toBe(0);
      expect(result.vote.length).toBe(0);

      expect(result.protection[0]?.source).toBe(source.id);
      expect(result.protection[0]?.destination).toBe(destination.id);
    });
  });

  describe('addRetaliation', () => {
    it('should add a person to retaliation', () => {
      const record = DailyOutcomeRecord.create(1);
      const source = mockPerson({
        id: '5b898f56-9c29-4662-b73f-d5da0189e6e2'
      });
      const destination = mockPerson({
        id: '5b898f56-9c29-4662-b73f-d5da0189e6e2'
      });

      expect(record.attack.length).toBe(0);
      expect(record.corruption.length).toBe(0);
      expect(record.divination.length).toBe(0);
      expect(record.execution).toBe(null);
      expect(record.protection.length).toBe(0);
      expect(record.retaliation.length).toBe(0);
      expect(record.victim.length).toBe(0);
      expect(record.vote.length).toBe(0);

      const result = DailyOutcomeRecord.addRetaliation(record, source, destination);

      expect(record.attack.length).toBe(0);
      expect(record.corruption.length).toBe(0);
      expect(record.divination.length).toBe(0);
      expect(record.execution).toBe(null);
      expect(record.protection.length).toBe(0);
      expect(record.retaliation.length).toBe(0);
      expect(record.victim.length).toBe(0);
      expect(record.vote.length).toBe(0);
      expect(result.attack.length).toBe(0);
      expect(result.corruption.length).toBe(0);
      expect(result.divination.length).toBe(0);
      expect(result.execution).toBe(null);
      expect(result.protection.length).toBe(0);
      expect(result.retaliation.length).toBe(1);
      expect(result.victim.length).toBe(0);
      expect(result.vote.length).toBe(0);

      expect(result.retaliation[0]?.source).toBe(source.id);
      expect(result.retaliation[0]?.destination).toBe(destination.id);
    });
  });

  describe('addVictim', () => {
    it('should add a person to victim', () => {
      const record = DailyOutcomeRecord.create(1);
      const target1 = mockPerson({
        id: '5b898f56-9c29-4662-b73f-d5da0189e6e2'
      });
      const target2 = mockPerson({
        id: '38354893-1a44-4b69-91e8-ee541c86a7b6'
      });

      expect(record.attack.length).toBe(0);
      expect(record.corruption.length).toBe(0);
      expect(record.divination.length).toBe(0);
      expect(record.execution).toBe(null);
      expect(record.protection.length).toBe(0);
      expect(record.retaliation.length).toBe(0);
      expect(record.victim.length).toBe(0);
      expect(record.vote.length).toBe(0);

      const result = DailyOutcomeRecord.addVictim(record, Persons.ofArray([target1, target2]));

      expect(record.attack.length).toBe(0);
      expect(record.corruption.length).toBe(0);
      expect(record.divination.length).toBe(0);
      expect(record.execution).toBe(null);
      expect(record.protection.length).toBe(0);
      expect(record.retaliation.length).toBe(0);
      expect(record.victim.length).toBe(0);
      expect(record.vote.length).toBe(0);
      expect(result.attack.length).toBe(0);
      expect(result.corruption.length).toBe(0);
      expect(result.divination.length).toBe(0);
      expect(result.execution).toBe(null);
      expect(result.protection.length).toBe(0);
      expect(result.retaliation.length).toBe(0);
      expect(result.victim.length).toBe(2);
      expect(result.vote.length).toBe(0);

      expect(result.victim[0]).toBe(target1.id);
      expect(result.victim[1]).toBe(target2.id);
    });
  });

  describe('addVote', () => {
    it('should add a person to vote', () => {
      const record = DailyOutcomeRecord.create(1);
      const source = mockPerson({
        id: '5b898f56-9c29-4662-b73f-d5da0189e6e2'
      });
      const destination = mockPerson({
        id: '5b898f56-9c29-4662-b73f-d5da0189e6e2'
      });

      expect(record.attack.length).toBe(0);
      expect(record.corruption.length).toBe(0);
      expect(record.divination.length).toBe(0);
      expect(record.execution).toBe(null);
      expect(record.protection.length).toBe(0);
      expect(record.retaliation.length).toBe(0);
      expect(record.victim.length).toBe(0);
      expect(record.vote.length).toBe(0);

      const result = DailyOutcomeRecord.addVote(record, source, destination);

      expect(record.attack.length).toBe(0);
      expect(record.corruption.length).toBe(0);
      expect(record.divination.length).toBe(0);
      expect(record.execution).toBe(null);
      expect(record.protection.length).toBe(0);
      expect(record.retaliation.length).toBe(0);
      expect(record.victim.length).toBe(0);
      expect(record.vote.length).toBe(0);
      expect(result.attack.length).toBe(0);
      expect(result.divination.length).toBe(0);
      expect(result.execution).toBe(null);
      expect(result.protection.length).toBe(0);
      expect(result.victim.length).toBe(0);
      expect(result.vote.length).toBe(1);

      expect(result.vote[0]?.source).toBe(source.id);
      expect(result.vote[0]?.destination).toBe(destination.id);
    });
  });

  describe('getAttack', () => {
    it('should get the correct persons from attack', () => {
      const record = DailyOutcomeRecord.create(1);
      const source = mockPerson({
        id: '5b898f56-9c29-4662-b73f-d5da0189e6e2'
      });
      const destination = mockPerson({
        id: '5b898f56-9c29-4662-b73f-d5da0189e6e2'
      });

      const result = DailyOutcomeRecord.addAttack(record, source, destination);

      expect(DailyOutcomeRecord.getAttack(result)[0]?.source).toBe(source.id);
      expect(DailyOutcomeRecord.getAttack(result)[0]?.destination).toBe(destination.id);
    });
  });

  describe('getCorruption', () => {
    it('should get the correct persons from corruption', () => {
      const record = DailyOutcomeRecord.create(1);
      const source = mockPerson({
        id: '5b898f56-9c29-4662-b73f-d5da0189e6e2'
      });
      const destination = mockPerson({
        id: '5b898f56-9c29-4662-b73f-d5da0189e6e2'
      });

      const result = DailyOutcomeRecord.addCorruption(record, source, destination);

      expect(DailyOutcomeRecord.getCorruption(result)[0]?.source).toBe(source.id);
      expect(DailyOutcomeRecord.getCorruption(result)[0]?.destination).toBe(destination.id);
    });
  });

  describe('getDivination', () => {
    it('should get the correct persons from divination', () => {
      const record = DailyOutcomeRecord.create(1);
      const source = mockPerson({
        id: '5b898f56-9c29-4662-b73f-d5da0189e6e2'
      });
      const destination = mockPerson({
        id: '5b898f56-9c29-4662-b73f-d5da0189e6e2'
      });

      const result = DailyOutcomeRecord.addDivination(record, source, destination);

      expect(DailyOutcomeRecord.getDivination(result)[0]?.source).toBe(source.id);
      expect(DailyOutcomeRecord.getDivination(result)[0]?.destination).toBe(destination.id);
    });
  });

  describe('getExecution', () => {
    it('should get the correct persons from execution', () => {
      const record = DailyOutcomeRecord.create(1);
      const target = mockPerson({
        id: '5b898f56-9c29-4662-b73f-d5da0189e6e2'
      });

      const result = DailyOutcomeRecord.addExecution(record, target);

      expect(DailyOutcomeRecord.getExecution(result)).toBe(target.id);
    });
  });

  describe('getProtection', () => {
    it('should get the correct persons from protection', () => {
      const record = DailyOutcomeRecord.create(1);
      const source = mockPerson({
        id: '5b898f56-9c29-4662-b73f-d5da0189e6e2'
      });
      const destination = mockPerson({
        id: '5b898f56-9c29-4662-b73f-d5da0189e6e2'
      });

      const result = DailyOutcomeRecord.addProtection(record, source, destination);

      expect(DailyOutcomeRecord.getProtection(result)[0]?.source).toBe(source.id);
      expect(DailyOutcomeRecord.getProtection(result)[0]?.destination).toBe(destination.id);
    });
  });

  describe('getRetaliation', () => {
    it('should get the correct persons from retaliation', () => {
      const record = DailyOutcomeRecord.create(1);
      const source = mockPerson({
        id: '5b898f56-9c29-4662-b73f-d5da0189e6e2'
      });
      const destination = mockPerson({
        id: '5b898f56-9c29-4662-b73f-d5da0189e6e2'
      });

      const result = DailyOutcomeRecord.addRetaliation(record, source, destination);

      expect(DailyOutcomeRecord.getRetaliation(result)[0]?.source).toBe(source.id);
      expect(DailyOutcomeRecord.getRetaliation(result)[0]?.destination).toBe(destination.id);
    });
  });

  describe('getVictim', () => {
    it('should get the correct persons from victim', () => {
      const record = DailyOutcomeRecord.create(1);
      const target1 = mockPerson({
        id: '5b898f56-9c29-4662-b73f-d5da0189e6e2'
      });
      const target2 = mockPerson({
        id: '38354893-1a44-4b69-91e8-ee541c86a7b6'
      });

      const result = DailyOutcomeRecord.addVictim(record, Persons.ofArray([target1, target2]));
      const victims = DailyOutcomeRecord.getVictim(result);

      expect(victims.length).toBe(2);
      expect(victims.includes(target1.id)).toBe(true);
      expect(victims.includes(target2.id)).toBe(true);
    });
  });

  describe('getVote', () => {
    it('should get the correct persons from vote', () => {
      const record = DailyOutcomeRecord.create(1);
      const source = mockPerson({
        id: '5b898f56-9c29-4662-b73f-d5da0189e6e2'
      });
      const destination = mockPerson({
        id: '5b898f56-9c29-4662-b73f-d5da0189e6e2'
      });

      const result = DailyOutcomeRecord.addVote(record, source, destination);

      expect(DailyOutcomeRecord.getVote(result)[0]?.source).toBe(source.id);
      expect(DailyOutcomeRecord.getVote(result)[0]?.destination).toBe(destination.id);
    });
  });
});
