import { Client } from '@/domains/Client/Client.js';
import { Clients } from '@/domains/Client/Clients.js';
import { mockClient } from '@/domains/Client/mocks/MockClient.js';
import { Identity } from '@/domains/Identity/Identity.js';
import type { Person, PersonID } from '@/domains/Person/Person.js';
import { Persons } from '@/domains/Person/Persons.js';
import { mockRoleAssignment } from '@/domains/Role/mocks/MockRoleAssignment.js';
import { isLeft, isRight } from 'fp-ts/lib/Either.js';
import { mockGame } from '../mocks/MockGame.js';
import { Room } from '../Room.js';

describe('Room', () => {
  describe('isFull', () => {
    it('should return true if the room is full', () => {
      const room = {
        id: Room.ID.from('6c09e7d8-132c-4ee8-9b05-9d3dacf1cf85'),
        name: 'room',
        password: null,
        moderator: null,
        quota: 4,
        participants: Clients.ofArray([
          mockClient({
            id: 'af950fd9-22fc-49c1-9c84-7c3c62da0c27'
          }),
          mockClient({
            id: '420159d2-6bbc-4597-9418-8fa0619ebaab'
          }),
          mockClient({
            id: 'f3d6f1f4-2a0b-4f0d-8f0b-0d6e7b4c8f3b'
          }),
          mockClient({
            id: 'c50fbc9a-5d07-4fee-8287-159cedfcebad'
          })
        ]),
        game: mockGame()
      } satisfies Room;

      expect(Room.isFull(room)).toBe(true);
    });

    it('should return true if the occupants are more than the quota', () => {
      const room = {
        id: Room.ID.from('6c09e7d8-132c-4ee8-9b05-9d3dacf1cf85'),
        name: 'room',
        password: null,
        moderator: null,
        quota: 4,
        participants: Clients.ofArray([
          mockClient({
            id: 'af950fd9-22fc-49c1-9c84-7c3c62da0c27'
          }),
          mockClient({
            id: '420159d2-6bbc-4597-9418-8fa0619ebaab'
          }),
          mockClient({
            id: 'f3d6f1f4-2a0b-4f0d-8f0b-0d6e7b4c8f3b'
          }),
          mockClient({
            id: 'c50fbc9a-5d07-4fee-8287-159cedfcebad'
          }),
          mockClient({
            id: 'bdfde888-b115-4901-8387-78d85410714c'
          })
        ]),
        game: mockGame()
      } satisfies Room;

      expect(Room.isFull(room)).toBe(true);
    });

    it('should return false if the room is not full', () => {
      const room = {
        id: Room.ID.from('6c09e7d8-132c-4ee8-9b05-9d3dacf1cf85'),
        name: 'room',
        password: null,
        moderator: null,
        quota: 4,
        participants: Clients.ofArray([
          mockClient({
            id: 'af950fd9-22fc-49c1-9c84-7c3c62da0c27'
          }),
          mockClient({
            id: '420159d2-6bbc-4597-9418-8fa0619ebaab'
          }),
          mockClient({
            id: 'f3d6f1f4-2a0b-4f0d-8f0b-0d6e7b4c8f3b'
          })
        ]),
        game: mockGame()
      } satisfies Room;

      expect(Room.isFull(room)).toBe(false);
    });
  });

  describe('isModerator', () => {
    it('should return true if the client is the moderator of the room', () => {
      const clientID = Client.ID.from('af950fd9-22fc-49c1-9c84-7c3c62da0c27');
      const client = mockClient({
        id: clientID
      });

      const room = {
        id: Room.ID.from('6c09e7d8-132c-4ee8-9b05-9d3dacf1cf85'),
        name: 'room',
        password: null,
        moderator: client,
        quota: 4,
        participants: Clients.empty(),
        game: mockGame()
      } satisfies Room;

      expect(Room.isModerator(room, client)).toBe(true);
    });

    it('should return false if the client is not the moderator of the room', () => {
      const clientID = Client.ID.from('af950fd9-22fc-49c1-9c84-7c3c62da0c27');
      const client = mockClient({
        id: clientID
      });

      const room = {
        id: Room.ID.from('6c09e7d8-132c-4ee8-9b05-9d3dacf1cf85'),
        name: 'room',
        password: null,
        moderator: mockClient({
          id: Client.ID.from('420159d2-6bbc-4597-9418-8fa0619ebaab')
        }),
        quota: 4,
        participants: Clients.empty(),
        game: mockGame()
      } satisfies Room;

      expect(Room.isModerator(room, client)).toBe(false);
    });

    it('should return false if the room has no moderator', () => {
      const clientID = Client.ID.from('af950fd9-22fc-49c1-9c84-7c3c62da0c27');
      const client = mockClient({
        id: clientID
      });

      const room = {
        id: Room.ID.from('6c09e7d8-132c-4ee8-9b05-9d3dacf1cf85'),
        name: 'room',
        password: null,
        moderator: null,
        quota: 4,
        participants: Clients.empty(),
        game: mockGame()
      } satisfies Room;

      expect(Room.isModerator(room, client)).toBe(false);
    });
  });

  describe('joinParticipant', () => {
    it('should return room with individual', () => {
      const room = {
        id: Room.ID.from('6c09e7d8-132c-4ee8-9b05-9d3dacf1cf85'),
        name: 'room',
        password: null,
        moderator: null,
        quota: 4,
        participants: Clients.empty(),
        game: mockGame()
      } satisfies Room;

      const individual = mockClient({
        id: 'af950fd9-22fc-49c1-9c84-7c3c62da0c27'
      });

      expect(Room.occupants(room)).toBe(0);

      const joinedRoom = Room.joinParticipant(room, individual);

      expect(Room.occupants(room)).toBe(0);
      expect(Room.occupants(joinedRoom)).toBe(1);
    });
  });

  describe('leaveParticipant', () => {
    it('should not decrease the number of occupants if the individual is not in the room', () => {
      const room = {
        id: Room.ID.from('6c09e7d8-132c-4ee8-9b05-9d3dacf1cf85'),
        name: 'room',
        password: null,
        moderator: null,
        quota: 4,
        participants: Clients.ofArray([
          mockClient({
            id: 'af950fd9-22fc-49c1-9c84-7c3c62da0c27'
          })
        ]),
        game: mockGame()
      } satisfies Room;

      const individual = mockClient({
        id: '420159d2-6bbc-4597-9418-8fa0619ebaab'
      });

      expect(Room.occupants(room)).toBe(1);

      const leftRoom = Room.leaveParticipant(room, individual);

      expect(Room.occupants(room)).toBe(1);
      expect(Room.occupants(leftRoom)).toBe(1);
    });

    it('should return room without individual', () => {
      const individual = mockClient({
        id: 'af950fd9-22fc-49c1-9c84-7c3c62da0c27'
      });
      const room = {
        id: Room.ID.from('6c09e7d8-132c-4ee8-9b05-9d3dacf1cf85'),
        name: 'room',
        password: null,
        moderator: null,
        quota: 4,
        participants: Clients.ofArray([
          individual,
          mockClient({
            id: '420159d2-6bbc-4597-9418-8fa0619ebaab'
          })
        ]),
        game: mockGame()
      } satisfies Room;

      expect(Room.occupants(room)).toBe(2);

      const leftRoom = Room.leaveParticipant(room, individual);

      expect(Room.occupants(room)).toBe(2);
      expect(Room.occupants(leftRoom)).toBe(1);
    });
  });

  describe('startGame', () => {
    it('should return right room with residents who have been assigned roles', () => {
      const room = {
        id: Room.ID.from('6c09e7d8-132c-4ee8-9b05-9d3dacf1cf85'),
        name: 'room',
        password: null,
        moderator: null,
        quota: 4,
        participants: Clients.ofArray([
          mockClient({
            id: 'af950fd9-22fc-49c1-9c84-7c3c62da0c27'
          }),
          mockClient({
            id: '420159d2-6bbc-4597-9418-8fa0619ebaab'
          }),
          mockClient({
            id: 'f3d6f1f4-2a0b-4f0d-8f0b-0d6e7b4c8f3b'
          }),
          mockClient({
            id: 'c50fbc9a-5d07-4fee-8287-159cedfcebad'
          })
        ]),
        game: mockGame({
          status: 'OPEN'
        })
      } satisfies Room;
      const assignment = mockRoleAssignment({
        seer: 1,
        villager: 2,
        werewolf: 1
      });

      const either = Room.startGame(room, assignment);

      expect(isRight(either)).toBe(true);

      if (isLeft(either)) {
        throw new Error('This should not happen');
      }

      const newRoom = either.right;

      const filter = (residents: Persons, identity: Identity): Persons => {
        const map = new Map<PersonID, Person>();

        for (const resident of residents.values()) {
          if (resident.identity === identity) {
            map.set(resident.id, resident);
          }
        }

        return Persons.from(map);
      };

      expect(newRoom.game.dayPhase.days).toBe(0);
      expect(newRoom.game.dayPhase.phase).toBe('NIGHT');
      expect(newRoom.game.status).toBe('IN_PROGRESS');
      expect(newRoom.game.residents.size).toBe(4);
      expect(filter(newRoom.game.residents, Identity.SEER).size).toBe(1);
      expect(filter(newRoom.game.residents, Identity.VILLAGER).size).toBe(2);
      expect(filter(newRoom.game.residents, Identity.WEREWOLF).size).toBe(1);
    });

    it('should return left when the number of participants and the number of roles do not match', () => {
      const room = {
        id: Room.ID.from('6c09e7d8-132c-4ee8-9b05-9d3dacf1cf85'),
        name: 'room',
        password: null,
        moderator: null,
        quota: 4,
        participants: Clients.ofArray([
          mockClient({
            id: 'af950fd9-22fc-49c1-9c84-7c3c62da0c27'
          }),
          mockClient({
            id: '420159d2-6bbc-4597-9418-8fa0619ebaab'
          }),
          mockClient({
            id: 'f3d6f1f4-2a0b-4f0d-8f0b-0d6e7b4c8f3b'
          }),
          mockClient({
            id: 'c50fbc9a-5d07-4fee-8287-159cedfcebad'
          })
        ]),
        game: mockGame({
          status: 'OPEN'
        })
      } satisfies Room;
      const assignment = mockRoleAssignment({
        seer: 1,
        villager: 2,
        werewolf: 2
      });

      const either = Room.startGame(room, assignment);

      expect(isLeft(either)).toBe(true);
    });
  });
});
