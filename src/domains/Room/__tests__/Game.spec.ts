import { Identity } from '@/domains/Identity/Identity.js';
import { Team } from '@/domains/Identity/Team.js';
import { mockPerson } from '@/domains/Person/mocks/MockPerson.js';
import { Person } from '@/domains/Person/Person.js';
import { Persons } from '@/domains/Person/Persons.js';
import { isNone, isSome } from 'fp-ts/lib/Option.js';
import { DailyOutcomeRecords } from '../DailyOutcomeRecords.js';
import { Game } from '../Game.js';
import { mockDayPhase } from '../mocks/MockDayPhase.js';
import { mockPhaseDuration } from '../mocks/MockPhaseDuration.js';
import type { Phase } from '../Phase.js';

describe('Games', () => {
  describe('channel', () => {
    it.each`
      role           | expected
      ${'ATONEMENT'} | ${'VILLAGER'}
      ${'CATALYST'}  | ${'WEREWOLF'}
      ${'COWARD'}    | ${'VILLAGER'}
      ${'HUNTER'}    | ${'VILLAGER'}
      ${'INFORMANT'} | ${'VILLAGER'}
      ${'JESTER'}    | ${'VILLAGER'}
      ${'KITSUNE'}   | ${'VILLAGER'}
      ${'KNIGHT'}    | ${'VILLAGER'}
      ${'MEDIUM'}    | ${'VILLAGER'}
      ${'MONASTIC'}  | ${'VILLAGER'}
      ${'ORATOR'}    | ${'VILLAGER'}
      ${'POSSESSED'} | ${'VILLAGER'}
      ${'SEER'}      | ${'VILLAGER'}
      ${'VILLAGER'}  | ${'VILLAGER'}
      ${'WEREWOLF'}  | ${'WEREWOLF'}
    `('should return $expected when the person is $role', async ({ role, expected }) => {
      const identity = Identity.getByRole(role);
      const person = mockPerson({
        identity
      });

      expect(Game.channel(person)).toBe(expected);
    });
  });

  describe('divine', () => {
    it.each`
      role           | expected
      ${'ATONEMENT'} | ${'VILLAGER'}
      ${'CATALYST'}  | ${'VILLAGER'}
      ${'COWARD'}    | ${'VILLAGER'}
      ${'HUNTER'}    | ${'VILLAGER'}
      ${'INFORMANT'} | ${'VILLAGER'}
      ${'JESTER'}    | ${'VILLAGER'}
      ${'KITSUNE'}   | ${'VILLAGER'}
      ${'KNIGHT'}    | ${'VILLAGER'}
      ${'MEDIUM'}    | ${'VILLAGER'}
      ${'MONASTIC'}  | ${'VILLAGER'}
      ${'ORATOR'}    | ${'VILLAGER'}
      ${'POSSESSED'} | ${'WEREWOLF'}
      ${'SEER'}      | ${'VILLAGER'}
      ${'VILLAGER'}  | ${'VILLAGER'}
      ${'WEREWOLF'}  | ${'WEREWOLF'}
    `('should return $expected when the person is $role', async ({ role, expected }) => {
      const identity = Identity.getByRole(role);
      const person = mockPerson({
        identity
      });

      expect(Game.divine(person)).toBe(expected);
    });
  });

  describe('exchangePersons', () => {
    it('should return a game with exchanged persons', () => {
      const person1 = mockPerson({
        id: 'a49ae316-fc6a-4db1-89a1-b7fcc0206986',
        life: 'ALIVE'
      });
      const person2 = mockPerson({
        id: '01b7c746-a7a4-4418-abe7-75292c7c5514',
        life: 'ALIVE',
        identity: Identity.HUNTER
      });
      const person3 = mockPerson({
        id: 'a49ae316-fc6a-4db1-89a1-b7fcc0206986',
        life: 'DEAD'
      });
      const person4 = mockPerson({
        id: '01b7c746-a7a4-4418-abe7-75292c7c5514',
        life: 'ALIVE',
        identity: Identity.KNIGHT
      });

      const game = {
        dayPhase: mockDayPhase(),
        phaseDuration: mockPhaseDuration(),
        residents: Persons.ofArray([person1, person2]),
        records: DailyOutcomeRecords.empty(),
        status: 'IN_PROGRESS'
      } satisfies Game;

      const exchanged = Game.exchangePersons(game, Persons.ofArray([person3, person4]));

      expect(game.residents.get(person3.id)?.life).toBe('ALIVE');
      expect(game.residents.get(person4.id)?.identity).toBe(Identity.HUNTER);
      expect(exchanged.residents.get(person3.id)?.life).toBe('DEAD');
      expect(exchanged.residents.get(person4.id)?.identity).toBe(Identity.KNIGHT);
    });
  });

  describe('getDeceased', () => {
    it('should return deceased persons', () => {
      const person1 = mockPerson({
        id: 'a49ae316-fc6a-4db1-89a1-b7fcc0206986',
        life: 'DEAD'
      });
      const person2 = mockPerson({
        id: '01b7c746-a7a4-4418-abe7-75292c7c5514',
        life: 'ALIVE'
      });
      const person3 = mockPerson({
        id: 'e7539ead-f8ae-472f-afad-19996b7580a6',
        life: 'DEAD'
      });
      const person4 = mockPerson({
        id: 'b59df5e0-7038-44e8-baef-09214d51ecf7',
        life: 'ALIVE'
      });
      const person5 = mockPerson({
        id: 'c33c8fb6-c2bc-4e70-9d2f-f916eae03d5a',
        life: 'DEAD'
      });

      const game = {
        dayPhase: mockDayPhase(),
        phaseDuration: mockPhaseDuration(),
        residents: Persons.ofArray([person1, person2, person3, person4, person5]),
        records: DailyOutcomeRecords.empty(),
        status: 'IN_PROGRESS'
      } satisfies Game;

      const deceased = Game.getDeceased(game);

      expect(deceased.has(person1.id)).toBe(true);
      expect(deceased.has(person3.id)).toBe(true);
      expect(deceased.has(person5.id)).toBe(true);
    });

    it('should return empty persons', () => {
      const person1 = mockPerson({
        id: 'a49ae316-fc6a-4db1-89a1-b7fcc0206986',
        life: 'ALIVE'
      });
      const person2 = mockPerson({
        id: '01b7c746-a7a4-4418-abe7-75292c7c5514',
        life: 'ALIVE'
      });
      const person3 = mockPerson({
        id: 'e7539ead-f8ae-472f-afad-19996b7580a6',
        life: 'ALIVE'
      });
      const person4 = mockPerson({
        id: 'b59df5e0-7038-44e8-baef-09214d51ecf7',
        life: 'ALIVE'
      });
      const person5 = mockPerson({
        id: 'c33c8fb6-c2bc-4e70-9d2f-f916eae03d5a',
        life: 'ALIVE'
      });

      const game = {
        dayPhase: mockDayPhase(),
        phaseDuration: mockPhaseDuration(),
        residents: Persons.ofArray([person1, person2, person3, person4, person5]),
        records: DailyOutcomeRecords.empty(),
        status: 'IN_PROGRESS'
      } satisfies Game;

      const deceased = Game.getDeceased(game);

      expect(deceased.size).toBe(0);
    });
  });

  describe('getDuration', () => {
    it.each`
      phase        | expected
      ${'MORNING'} | ${30}
      ${'DAY'}     | ${240}
      ${'EVENING'} | ${30}
      ${'NIGHT'}   | ${30}
    `('should return $expected when the phase is $phase', ({ phase, expected }: { phase: Phase; expected: number }) => {
      const game = {
        dayPhase: mockDayPhase({
          days: 1,
          phase
        }),
        phaseDuration: mockPhaseDuration({
          morning: 30,
          day: 240,
          evening: 30,
          night: 30
        }),
        residents: Persons.empty(),
        records: DailyOutcomeRecords.empty(),
        status: 'IN_PROGRESS'
      } satisfies Game;

      expect(Game.getDuration(game)).toBe(expected);
    });
  });

  describe('getSurvivors', () => {
    it('should return survivors persons', () => {
      const person1 = mockPerson({
        id: 'a49ae316-fc6a-4db1-89a1-b7fcc0206986',
        life: 'DEAD'
      });
      const person2 = mockPerson({
        id: '01b7c746-a7a4-4418-abe7-75292c7c5514',
        life: 'ALIVE'
      });
      const person3 = mockPerson({
        id: 'e7539ead-f8ae-472f-afad-19996b7580a6',
        life: 'DEAD'
      });
      const person4 = mockPerson({
        id: 'b59df5e0-7038-44e8-baef-09214d51ecf7',
        life: 'ALIVE'
      });
      const person5 = mockPerson({
        id: 'c33c8fb6-c2bc-4e70-9d2f-f916eae03d5a',
        life: 'DEAD'
      });

      const game = {
        dayPhase: mockDayPhase(),
        phaseDuration: mockPhaseDuration(),
        residents: Persons.ofArray([person1, person2, person3, person4, person5]),
        records: DailyOutcomeRecords.empty(),
        status: 'IN_PROGRESS'
      } satisfies Game;

      const survivors = Game.getSurvivors(game);

      expect(survivors.has(person2.id)).toBe(true);
      expect(survivors.has(person4.id)).toBe(true);
    });

    it('should return empty persons', () => {
      const person1 = mockPerson({
        id: 'a49ae316-fc6a-4db1-89a1-b7fcc0206986',
        life: 'DEAD'
      });
      const person2 = mockPerson({
        id: '01b7c746-a7a4-4418-abe7-75292c7c5514',
        life: 'DEAD'
      });
      const person3 = mockPerson({
        id: 'e7539ead-f8ae-472f-afad-19996b7580a6',
        life: 'DEAD'
      });
      const person4 = mockPerson({
        id: 'b59df5e0-7038-44e8-baef-09214d51ecf7',
        life: 'DEAD'
      });
      const person5 = mockPerson({
        id: 'c33c8fb6-c2bc-4e70-9d2f-f916eae03d5a',
        life: 'DEAD'
      });

      const game = {
        dayPhase: mockDayPhase(),
        phaseDuration: mockPhaseDuration(),
        residents: Persons.ofArray([person1, person2, person3, person4, person5]),
        records: DailyOutcomeRecords.empty(),
        status: 'IN_PROGRESS'
      } satisfies Game;

      const survivors = Game.getSurvivors(game);

      expect(survivors.size).toBe(0);
    });
  });

  describe('getWinners', () => {
    it('should return empty array when villagers are more than werewolves', () => {
      const person1 = mockPerson({
        id: 'a49ae316-fc6a-4db1-89a1-b7fcc0206986',
        life: 'ALIVE',
        identity: Identity.VILLAGER
      });
      const person2 = mockPerson({
        id: '01b7c746-a7a4-4418-abe7-75292c7c5514',
        life: 'ALIVE',
        identity: Identity.VILLAGER
      });
      const person3 = mockPerson({
        id: 'e7539ead-f8ae-472f-afad-19996b7580a6',
        life: 'ALIVE',
        identity: Identity.VILLAGER
      });
      const person4 = mockPerson({
        id: 'b59df5e0-7038-44e8-baef-09214d51ecf7',
        life: 'ALIVE',
        identity: Identity.WEREWOLF
      });
      const person5 = mockPerson({
        id: 'c33c8fb6-c2bc-4e70-9d2f-f916eae03d5a',
        life: 'ALIVE',
        identity: Identity.WEREWOLF
      });
      const person6 = mockPerson({
        id: 'c2846ded-e956-4be4-a6ff-c5520db39c04',
        life: 'DEAD',
        identity: Identity.WEREWOLF
      });

      const game = {
        dayPhase: mockDayPhase(),
        phaseDuration: mockPhaseDuration(),
        residents: Persons.ofArray([person1, person2, person3, person4, person5, person6]),
        records: DailyOutcomeRecords.empty(),
        status: 'IN_PROGRESS'
      } satisfies Game;

      const winners = Game.getWinners(game);

      expect(winners.length).toBe(0);
    });

    it('should return werewolves when werewolves are more than villagers', () => {
      const person1 = mockPerson({
        id: 'a49ae316-fc6a-4db1-89a1-b7fcc0206986',
        life: 'ALIVE',
        identity: Identity.WEREWOLF
      });
      const person2 = mockPerson({
        id: '01b7c746-a7a4-4418-abe7-75292c7c5514',
        life: 'ALIVE',
        identity: Identity.WEREWOLF
      });
      const person3 = mockPerson({
        id: 'e7539ead-f8ae-472f-afad-19996b7580a6',
        life: 'ALIVE',
        identity: Identity.WEREWOLF
      });
      const person4 = mockPerson({
        id: 'b59df5e0-7038-44e8-baef-09214d51ecf7',
        life: 'ALIVE',
        identity: Identity.VILLAGER
      });
      const person5 = mockPerson({
        id: 'c33c8fb6-c2bc-4e70-9d2f-f916eae03d5a',
        life: 'ALIVE',
        identity: Identity.VILLAGER
      });
      const person6 = mockPerson({
        id: 'c2846ded-e956-4be4-a6ff-c5520db39c04',
        life: 'DEAD',
        identity: Identity.VILLAGER
      });

      const game = {
        dayPhase: mockDayPhase(),
        phaseDuration: mockPhaseDuration(),
        residents: Persons.ofArray([person1, person2, person3, person4, person5, person6]),
        records: DailyOutcomeRecords.empty(),
        status: 'IN_PROGRESS'
      } satisfies Game;

      const winners = Game.getWinners(game);

      expect(winners.length).toBe(1);
      expect(winners[0]).toBe(Team.WEREWOLVES);
    });

    it('should return werewolves when werewolves are equal to villagers', () => {
      const person1 = mockPerson({
        id: 'a49ae316-fc6a-4db1-89a1-b7fcc0206986',
        life: 'ALIVE',
        identity: Identity.WEREWOLF
      });
      const person2 = mockPerson({
        id: '01b7c746-a7a4-4418-abe7-75292c7c5514',
        life: 'ALIVE',
        identity: Identity.WEREWOLF
      });
      const person3 = mockPerson({
        id: 'e7539ead-f8ae-472f-afad-19996b7580a6',
        life: 'ALIVE',
        identity: Identity.VILLAGER
      });
      const person4 = mockPerson({
        id: 'b59df5e0-7038-44e8-baef-09214d51ecf7',
        life: 'ALIVE',
        identity: Identity.VILLAGER
      });
      const person5 = mockPerson({
        id: 'c33c8fb6-c2bc-4e70-9d2f-f916eae03d5a',
        life: 'DEAD',
        identity: Identity.VILLAGER
      });

      const game = {
        dayPhase: mockDayPhase(),
        phaseDuration: mockPhaseDuration(),
        residents: Persons.ofArray([person1, person2, person3, person4, person5]),
        records: DailyOutcomeRecords.empty(),
        status: 'IN_PROGRESS'
      } satisfies Game;

      const winners = Game.getWinners(game);

      expect(winners.length).toBe(1);
      expect(winners[0]).toBe(Team.WEREWOLVES);
    });

    it('should return werewolves when werewolves and catalysts are equal to villagers', () => {
      const person1 = mockPerson({
        id: 'a49ae316-fc6a-4db1-89a1-b7fcc0206986',
        life: 'ALIVE',
        identity: Identity.WEREWOLF
      });
      const person2 = mockPerson({
        id: '01b7c746-a7a4-4418-abe7-75292c7c5514',
        life: 'ALIVE',
        identity: Identity.CATALYST
      });
      const person3 = mockPerson({
        id: 'e7539ead-f8ae-472f-afad-19996b7580a6',
        life: 'ALIVE',
        identity: Identity.VILLAGER
      });
      const person4 = mockPerson({
        id: 'b59df5e0-7038-44e8-baef-09214d51ecf7',
        life: 'ALIVE',
        identity: Identity.VILLAGER
      });
      const person5 = mockPerson({
        id: 'c33c8fb6-c2bc-4e70-9d2f-f916eae03d5a',
        life: 'DEAD',
        identity: Identity.VILLAGER
      });

      const game = {
        dayPhase: mockDayPhase(),
        phaseDuration: mockPhaseDuration(),
        residents: Persons.ofArray([person1, person2, person3, person4, person5]),
        records: DailyOutcomeRecords.empty(),
        status: 'IN_PROGRESS'
      } satisfies Game;

      const winners = Game.getWinners(game);

      expect(winners.length).toBe(1);
      expect(winners[0]).toBe(Team.WEREWOLVES);
    });

    it('should return villagers when werewolves and catalysts are equal to villagers', () => {
      const person1 = mockPerson({
        id: 'a49ae316-fc6a-4db1-89a1-b7fcc0206986',
        life: 'ALIVE',
        identity: Identity.CATALYST
      });
      const person2 = mockPerson({
        id: '01b7c746-a7a4-4418-abe7-75292c7c5514',
        life: 'ALIVE',
        identity: Identity.VILLAGER
      });
      const person3 = mockPerson({
        id: 'e7539ead-f8ae-472f-afad-19996b7580a6',
        life: 'DEAD',
        identity: Identity.WEREWOLF
      });

      const game = {
        dayPhase: mockDayPhase(),
        phaseDuration: mockPhaseDuration(),
        residents: Persons.ofArray([person1, person2, person3]),
        records: DailyOutcomeRecords.empty(),
        status: 'IN_PROGRESS'
      } satisfies Game;

      const winners = Game.getWinners(game);

      expect(winners.length).toBe(1);
      expect(winners[0]).toBe(Team.VILLAGERS);
    });

    it('should return villagers when werewolves do not exist', () => {
      const person1 = mockPerson({
        id: 'a49ae316-fc6a-4db1-89a1-b7fcc0206986',
        life: 'ALIVE',
        identity: Identity.VILLAGER
      });
      const person2 = mockPerson({
        id: '01b7c746-a7a4-4418-abe7-75292c7c5514',
        life: 'ALIVE',
        identity: Identity.VILLAGER
      });
      const person3 = mockPerson({
        id: 'e7539ead-f8ae-472f-afad-19996b7580a6',
        life: 'ALIVE',
        identity: Identity.VILLAGER
      });
      const person4 = mockPerson({
        id: 'b59df5e0-7038-44e8-baef-09214d51ecf7',
        life: 'DEAD',
        identity: Identity.WEREWOLF
      });
      const person5 = mockPerson({
        id: 'c33c8fb6-c2bc-4e70-9d2f-f916eae03d5a',
        life: 'DEAD',
        identity: Identity.WEREWOLF
      });
      const person6 = mockPerson({
        id: 'c2846ded-e956-4be4-a6ff-c5520db39c04',
        life: 'DEAD',
        identity: Identity.WEREWOLF
      });

      const game = {
        dayPhase: mockDayPhase(),
        phaseDuration: mockPhaseDuration(),
        residents: Persons.ofArray([person1, person2, person3, person4, person5, person6]),
        records: DailyOutcomeRecords.empty(),
        status: 'IN_PROGRESS'
      } satisfies Game;

      const winners = Game.getWinners(game);

      expect(winners.length).toBe(1);
      expect(winners[0]).toBe(Team.VILLAGERS);
    });

    it('should return kitsunes when werewolves do not exist and kitsunes are alive', () => {
      const person1 = mockPerson({
        id: 'a49ae316-fc6a-4db1-89a1-b7fcc0206986',
        life: 'ALIVE',
        identity: Identity.VILLAGER
      });
      const person2 = mockPerson({
        id: '01b7c746-a7a4-4418-abe7-75292c7c5514',
        life: 'ALIVE',
        identity: Identity.VILLAGER
      });
      const person3 = mockPerson({
        id: 'e7539ead-f8ae-472f-afad-19996b7580a6',
        life: 'ALIVE',
        identity: Identity.VILLAGER
      });
      const person4 = mockPerson({
        id: 'b59df5e0-7038-44e8-baef-09214d51ecf7',
        life: 'DEAD',
        identity: Identity.WEREWOLF
      });
      const person5 = mockPerson({
        id: 'c33c8fb6-c2bc-4e70-9d2f-f916eae03d5a',
        life: 'DEAD',
        identity: Identity.WEREWOLF
      });
      const person6 = mockPerson({
        id: 'c2846ded-e956-4be4-a6ff-c5520db39c04',
        life: 'ALIVE',
        identity: Identity.KITSUNE
      });

      const game = {
        dayPhase: mockDayPhase(),
        phaseDuration: mockPhaseDuration(),
        residents: Persons.ofArray([person1, person2, person3, person4, person5, person6]),
        records: DailyOutcomeRecords.empty(),
        status: 'IN_PROGRESS'
      } satisfies Game;

      const winners = Game.getWinners(game);

      expect(winners.length).toBe(1);
      expect(winners[0]).toBe(Team.KITSUNES);
    });

    // 人狼陣営が勝利した時に妖狐が生存していれば妖狐陣営が勝利する
    it('should return kitsunes when werewolves are more than villagers and kitsunes are alive', () => {
      const person1 = mockPerson({
        id: 'a49ae316-fc6a-4db1-89a1-b7fcc0206986',
        life: 'ALIVE',
        identity: Identity.WEREWOLF
      });
      const person2 = mockPerson({
        id: '01b7c746-a7a4-4418-abe7-75292c7c5514',
        life: 'ALIVE',
        identity: Identity.WEREWOLF
      });
      const person3 = mockPerson({
        id: 'e7539ead-f8ae-472f-afad-19996b7580a6',
        life: 'ALIVE',
        identity: Identity.VILLAGER
      });
      const person4 = mockPerson({
        id: 'b59df5e0-7038-44e8-baef-09214d51ecf7',
        life: 'ALIVE',
        identity: Identity.VILLAGER
      });
      const person5 = mockPerson({
        id: 'c33c8fb6-c2bc-4e70-9d2f-f916eae03d5a',
        life: 'ALIVE',
        identity: Identity.KITSUNE
      });

      const game = {
        dayPhase: mockDayPhase(),
        phaseDuration: mockPhaseDuration(),
        residents: Persons.ofArray([person1, person2, person3, person4, person5]),
        records: DailyOutcomeRecords.empty(),
        status: 'IN_PROGRESS'
      } satisfies Game;

      const winners = Game.getWinners(game);

      expect(winners.length).toBe(1);
      expect(winners[0]).toBe(Team.KITSUNES);
    });

    it('should return empty array when no team has won', () => {
      const person1 = mockPerson({
        id: 'a49ae316-fc6a-4db1-89a1-b7fcc0206986',
        life: 'ALIVE',
        identity: Identity.WEREWOLF
      });
      const person2 = mockPerson({
        id: '01b7c746-a7a4-4418-abe7-75292c7c5514',
        life: 'DEAD',
        identity: Identity.WEREWOLF
      });
      const person3 = mockPerson({
        id: 'e7539ead-f8ae-472f-afad-19996b7580a6',
        life: 'ALIVE',
        identity: Identity.VILLAGER
      });
      const person4 = mockPerson({
        id: 'b59df5e0-7038-44e8-baef-09214d51ecf7',
        life: 'ALIVE',
        identity: Identity.VILLAGER
      });
      const person5 = mockPerson({
        id: 'c33c8fb6-c2bc-4e70-9d2f-f916eae03d5a',
        life: 'ALIVE',
        identity: Identity.KITSUNE
      });

      const game = {
        dayPhase: mockDayPhase(),
        phaseDuration: mockPhaseDuration(),
        residents: Persons.ofArray([person1, person2, person3, person4, person5]),
        records: DailyOutcomeRecords.empty(),
        status: 'IN_PROGRESS'
      } satisfies Game;

      const winners = Game.getWinners(game);

      expect(winners.length).toBe(0);
    });

    it('should return villagers and jesters when no werewolves exist and jesters are dead', () => {
      const person1 = mockPerson({
        id: 'a49ae316-fc6a-4db1-89a1-b7fcc0206986',
        life: 'DEAD',
        identity: Identity.JESTER
      });
      const person2 = mockPerson({
        id: '01b7c746-a7a4-4418-abe7-75292c7c5514',
        life: 'ALIVE',
        identity: Identity.VILLAGER
      });
      const person3 = mockPerson({
        id: 'e7539ead-f8ae-472f-afad-19996b7580a6',
        life: 'ALIVE',
        identity: Identity.VILLAGER
      });
      const person4 = mockPerson({
        id: 'b59df5e0-7038-44e8-baef-09214d51ecf7',
        life: 'ALIVE',
        identity: Identity.VILLAGER
      });
      const person5 = mockPerson({
        id: 'c33c8fb6-c2bc-4e70-9d2f-f916eae03d5a',
        life: 'DEAD',
        identity: Identity.KITSUNE
      });

      const game = {
        dayPhase: mockDayPhase(),
        phaseDuration: mockPhaseDuration(),
        residents: Persons.ofArray([person1, person2, person3, person4, person5]),
        records: DailyOutcomeRecords.empty(),
        status: 'IN_PROGRESS'
      } satisfies Game;

      const winners = Game.getWinners(game);

      expect(winners.length).toBe(2);
      expect(winners[0]).toBe(Team.JESTERS);
      expect(winners[1]).toBe(Team.VILLAGERS);
    });

    it('should return werewolves and jesters when werewolves are more than or equal to villagers and jesters are dead', () => {
      const person1 = mockPerson({
        id: 'a49ae316-fc6a-4db1-89a1-b7fcc0206986',
        life: 'DEAD',
        identity: Identity.JESTER
      });
      const person2 = mockPerson({
        id: '01b7c746-a7a4-4418-abe7-75292c7c5514',
        life: 'ALIVE',
        identity: Identity.WEREWOLF
      });
      const person3 = mockPerson({
        id: 'e7539ead-f8ae-472f-afad-19996b7580a6',
        life: 'ALIVE',
        identity: Identity.WEREWOLF
      });
      const person4 = mockPerson({
        id: 'b59df5e0-7038-44e8-baef-09214d51ecf7',
        life: 'ALIVE',
        identity: Identity.VILLAGER
      });
      const person5 = mockPerson({
        id: 'c33c8fb6-c2bc-4e70-9d2f-f916eae03d5a',
        life: 'ALIVE',
        identity: Identity.VILLAGER
      });
      const person6 = mockPerson({
        id: 'c2846ded-e956-4be4-a6ff-c5520db39c04',
        life: 'DEAD',
        identity: Identity.KITSUNE
      });

      const game = {
        dayPhase: mockDayPhase(),
        phaseDuration: mockPhaseDuration(),
        residents: Persons.ofArray([person1, person2, person3, person4, person5, person6]),
        records: DailyOutcomeRecords.empty(),
        status: 'IN_PROGRESS'
      } satisfies Game;

      const winners = Game.getWinners(game);

      expect(winners.length).toBe(2);
      expect(winners[0]).toBe(Team.JESTERS);
      expect(winners[1]).toBe(Team.WEREWOLVES);
    });

    // 妖狐陣営が勝利したときに道化が死亡していれば道化陣営も勝利する
    it('should return kitsunes and jesters when game is over and kitsunes are alive and jesters are dead', () => {
      const person1 = mockPerson({
        id: 'a49ae316-fc6a-4db1-89a1-b7fcc0206986',
        life: 'DEAD',
        identity: Identity.JESTER
      });
      const person2 = mockPerson({
        id: '01b7c746-a7a4-4418-abe7-75292c7c5514',
        life: 'ALIVE',
        identity: Identity.KITSUNE
      });
      const person3 = mockPerson({
        id: 'e7539ead-f8ae-472f-afad-19996b7580a6',
        life: 'ALIVE',
        identity: Identity.VILLAGER
      });
      const person4 = mockPerson({
        id: 'b59df5e0-7038-44e8-baef-09214d51ecf7',
        life: 'ALIVE',
        identity: Identity.WEREWOLF
      });

      const game = {
        dayPhase: mockDayPhase(),
        phaseDuration: mockPhaseDuration(),
        residents: Persons.ofArray([person1, person2, person3, person4]),
        records: DailyOutcomeRecords.empty(),
        status: 'IN_PROGRESS'
      } satisfies Game;

      const winners = Game.getWinners(game);

      expect(winners.length).toBe(2);
      expect(winners[0]).toBe(Team.JESTERS);
      expect(winners[1]).toBe(Team.KITSUNES);
    });
  });

  describe('randomResident', () => {
    it('should return none when residents is empty', () => {
      const game = {
        dayPhase: mockDayPhase(),
        phaseDuration: mockPhaseDuration(),
        residents: Persons.empty(),
        records: DailyOutcomeRecords.empty(),
        status: 'IN_PROGRESS'
      } satisfies Game;

      const option = Game.randomResident(game);

      expect(isNone(option)).toBe(true);
    });

    it('should return a random resident when ids is not specified', () => {
      const person1 = mockPerson({
        id: 'a49ae316-fc6a-4db1-89a1-b7fcc0206986'
      });
      const person2 = mockPerson({
        id: '01b7c746-a7a4-4418-abe7-75292c7c5514'
      });
      const person3 = mockPerson({
        id: 'e7539ead-f8ae-472f-afad-19996b7580a6'
      });
      const person4 = mockPerson({
        id: 'b59df5e0-7038-44e8-baef-09214d51ecf7'
      });
      const person5 = mockPerson({
        id: 'c33c8fb6-c2bc-4e70-9d2f-f916eae03d5a'
      });

      const game = {
        dayPhase: mockDayPhase(),
        phaseDuration: mockPhaseDuration(),
        residents: Persons.ofArray([person1, person2, person3, person4, person5]),
        records: DailyOutcomeRecords.empty(),
        status: 'IN_PROGRESS'
      } satisfies Game;

      const option = Game.randomResident(game);

      expect(isSome(option)).toBe(true);

      if (isNone(option)) {
        throw new Error('This should not happen');
      }

      expect([person1, person2, person3, person4, person5]).toContain(option.value);
    });

    it('should return none when ids is specified but not found in residents', () => {
      const person1 = mockPerson({
        id: 'a49ae316-fc6a-4db1-89a1-b7fcc0206986'
      });
      const person2 = mockPerson({
        id: '01b7c746-a7a4-4418-abe7-75292c7c5514'
      });
      const person3 = mockPerson({
        id: 'e7539ead-f8ae-472f-afad-19996b7580a6'
      });
      const person4 = mockPerson({
        id: 'b59df5e0-7038-44e8-baef-09214d51ecf7'
      });
      const person5 = mockPerson({
        id: 'c33c8fb6-c2bc-4e70-9d2f-f916eae03d5a'
      });

      const game = {
        dayPhase: mockDayPhase(),
        phaseDuration: mockPhaseDuration(),
        residents: Persons.ofArray([person1, person2, person3, person4, person5]),
        records: DailyOutcomeRecords.empty(),
        status: 'IN_PROGRESS'
      } satisfies Game;

      const option = Game.randomResident(game, [
        Person.ID.from('12960cff-847f-4545-ab22-add24cf7db64'),
        Person.ID.from('f2ebb2b5-4a4a-4ddb-b222-cb1449e56822'),
        Person.ID.from('c6ee7b9c-c68e-4d42-821d-71bd08943b7f')
      ]);

      expect(isNone(option)).toBe(true);
    });
  });
});
