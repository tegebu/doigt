import type { GatewayError } from '@/adapters/gateways/GatewayError.js';
import type { InTransactionRepository } from '@/applications/unitOfWork/InTransactionRepository.js';
import type { GenericError } from '@/lib/Error/Errors.js';
import type { Either } from 'fp-ts/lib/Either.js';
import type { Option } from 'fp-ts/lib/Option.js';
import type { Room, RoomID } from './Room.js';
import type { Rooms } from './Rooms.js';

export interface IRoomRepository extends InTransactionRepository<RoomID, Room> {
  all(): Promise<Either<GenericError | GatewayError, Rooms>>;

  create(room: Room): Promise<Either<GenericError | GatewayError, unknown>>;

  delete(id: RoomID): Promise<Either<GenericError | GatewayError, unknown>>;

  find(id: RoomID): Promise<Either<GenericError | GatewayError, Option<Room>>>;

  update(room: Room): Promise<Either<GenericError | GatewayError, unknown>>;
}
