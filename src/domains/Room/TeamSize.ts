import { ExhaustiveError } from '@jamashita/anden/error';
import { z } from 'zod';
import type { Person } from '../Person/Person.js';
import type { Persons } from '../Person/Persons.js';

const TeamSizeSchema = z.object({
  villagers: z.number().int().nonnegative(),
  werewolves: z.number().int().nonnegative(),
  kitsunes: z.number().int().nonnegative(),
  jesters: z.number().int().nonnegative()
});

export type TeamSize = Readonly<z.infer<typeof TeamSizeSchema>>;

export namespace TeamSize {
  export const SCHEMA = TeamSizeSchema;

  export const calculateTeamSize = (persons: Persons): TeamSize => {
    let villagers = 0;
    let werewolves = 0;
    let kitsunes = 0;
    let jesters = 0;

    for (const [, person] of persons) {
      switch (person.identity.role) {
        // 内通者は村人陣営に加算
        case 'INFORMANT': {
          villagers++;

          continue;
        }
        // 触媒だけ換算が特殊、人狼が含まれていれば人狼陣営に加算
        // 生存しているかどうかをここで判定していないので注意
        case 'CATALYST': {
          if (isWerewolfAlive(persons)) {
            werewolves++;

            continue;
          }

          // 人狼がいない場合は村人陣営としても数えない
          continue;
        }
      }
      switch (person.identity.team.team) {
        case 'VILLAGERS': {
          villagers++;

          continue;
        }
        case 'WEREWOLVES': {
          werewolves++;

          continue;
        }
        case 'KITSUNES': {
          kitsunes++;

          continue;
        }
        case 'JESTERS': {
          jesters++;

          continue;
        }
        default: {
          throw new ExhaustiveError(person.identity.team.team);
        }
      }
    }

    return {
      villagers,
      werewolves,
      kitsunes,
      jesters
    } satisfies TeamSize;
  };

  const isWerewolfAlive = (persons: Persons): boolean => {
    return [...persons.values()].some((person: Person) => {
      return person.identity.role === 'WEREWOLF';
    });
  };
}
