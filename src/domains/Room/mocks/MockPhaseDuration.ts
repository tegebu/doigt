import type { PhaseDuration } from '../PhaseDuration.js';

type PhaseDurationArgs = Partial<
  Readonly<{
    morning: number;
    day: number;
    evening: number;
    night: number;
  }>
>;

export const mockPhaseDuration = ({ morning = 1, day = 2, evening = 3, night = 4 }: PhaseDurationArgs = {}): PhaseDuration => {
  return {
    morning,
    day,
    evening,
    night
  } satisfies PhaseDuration;
};
