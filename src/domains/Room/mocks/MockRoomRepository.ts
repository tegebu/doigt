import type { GatewayError } from '@/adapters/gateways/GatewayError.js';
import type { GenericError } from '@/lib/Error/Errors.js';
import { UnimplementedError } from '@jamashita/anden/error';
import type { Either } from 'fp-ts/lib/Either.js';
import type { Option } from 'fp-ts/lib/Option.js';
import type { IRoomRepository } from '../IRoomRepository.js';
import type { Room } from '../Room.js';
import type { Rooms } from '../Rooms.js';

export class MockRoomRepository implements IRoomRepository {
  public all(): Promise<Either<GenericError | GatewayError, Rooms>> {
    throw new UnimplementedError();
  }

  public create(): Promise<Either<GenericError | GatewayError, unknown>> {
    throw new UnimplementedError();
  }

  public delete(): Promise<Either<GenericError | GatewayError, unknown>> {
    throw new UnimplementedError();
  }

  public find(): Promise<Either<GenericError | GatewayError, Option<Room>>> {
    throw new UnimplementedError();
  }

  public update(): Promise<Either<GenericError | GatewayError, unknown>> {
    throw new UnimplementedError();
  }
}
