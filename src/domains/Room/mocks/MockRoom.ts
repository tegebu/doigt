import type { Client } from '@/domains/Client/Client.js';
import { Clients } from '@/domains/Client/Clients.js';
import { mockClient } from '@/domains/Client/mocks/MockClient.js';
import type { GameStatus } from '@/domains/Room/GameStatus.js';
import type { Nullable } from '@jamashita/anden/type';
import type { Game } from '../Game.js';
import { Room, type RoomList } from '../Room.js';
import { mockGame } from './MockGame.js';

type RoomArgs = Partial<
  Readonly<{
    id: string;
    name: string;
    password: Nullable<string>;
    quota: number;
    moderator: Nullable<Client>;
    participants: ReadonlyArray<Client>;
    game: Game;
  }>
>;
type RoomListArgs = Partial<
  Readonly<{
    id: string;
    name: string;
    requirePassword: boolean;
    quota: number;
    moderator: Nullable<Client>;
    participants: number;
    status: GameStatus;
  }>
>;

export const mockRoom = ({
  id = 'fd75fd2a-5624-4cfd-80e2-debef7cdd720',
  name = 'ROOM NAME',
  password = 'NpSx9jhLigi',
  quota = 10,
  moderator = null,
  participants = [mockClient()],
  game = mockGame()
}: RoomArgs = {}): Room => {
  return {
    id: Room.ID.from(id),
    name,
    password,
    moderator,
    participants: Clients.ofArray(participants),
    quota,
    game
  };
};

export const mockRoomList = ({
  id = 'fd75fd2a-5624-4cfd-80e2-debef7cdd720',
  name = 'ROOM NAME',
  requirePassword = false,
  quota = 10,
  moderator = null,
  participants = 0,
  status = 'OPEN'
}: RoomListArgs = {}): RoomList => {
  return {
    id: Room.ID.from(id),
    name,
    requirePassword,
    moderator,
    participants,
    quota,
    status
  };
};
