import type { DayPhase } from '../DayPhase.js';
import type { Phase } from '../Phase.js';

type DayPhaseArgs = Partial<
  Readonly<{
    days: number;
    phase: Phase;
  }>
>;

export const mockDayPhase = ({ days = 1, phase = 'MORNING' }: DayPhaseArgs = {}): DayPhase => {
  return {
    days,
    phase
  } satisfies DayPhase;
};
