import type { Person } from '@/domains/Person/Person.js';
import { Persons } from '@/domains/Person/Persons.js';
import type { DailyOutcomeRecord } from '../DailyOutcomeRecord.js';
import { DailyOutcomeRecords } from '../DailyOutcomeRecords.js';
import type { DayPhase } from '../DayPhase.js';
import type { Game } from '../Game.js';
import type { GameStatus } from '../GameStatus.js';
import type { PhaseDuration } from '../PhaseDuration.js';
import { mockDailyOutcomeRecord } from './MockDailyOutcomeRecord.js';
import { mockDayPhase } from './MockDayPhase.js';
import { mockPhaseDuration } from './MockPhaseDuration.js';

type GameArgs = Partial<
  Readonly<{
    dayPhase: DayPhase;
    phaseDuration: PhaseDuration;
    residents: ReadonlyArray<Person>;
    records: ReadonlyArray<DailyOutcomeRecord>;
    status: GameStatus;
  }>
>;

export const mockGame = ({
  dayPhase = mockDayPhase(),
  phaseDuration = mockPhaseDuration(),
  residents = [],
  records = [mockDailyOutcomeRecord()],
  status = 'IN_PROGRESS'
}: GameArgs = {}): Game => {
  return {
    dayPhase,
    phaseDuration,
    residents: Persons.ofArray(residents),
    records: DailyOutcomeRecords.ofArray(records),
    status
  } satisfies Game;
};
