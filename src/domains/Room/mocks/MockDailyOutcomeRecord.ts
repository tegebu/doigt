import type { VoteOutcome } from '@/domains/Outcome/VoteOutcome.js';
import { VoteOutcomes } from '@/domains/Outcome/VoteOutcomes.js';
import type { PersonID } from '@/domains/Person/Person.js';
import type { DailyOutcomeRecord } from '@/domains/Room/DailyOutcomeRecord.js';
import type { Nullable } from '@jamashita/anden/type';

type DailyOutcomeRecordArgs = Partial<
  Readonly<{
    days: number;
    attack: ReadonlyArray<VoteOutcome>;
    corruption: ReadonlyArray<VoteOutcome>;
    divination: ReadonlyArray<VoteOutcome>;
    execution: Nullable<PersonID>;
    protection: ReadonlyArray<VoteOutcome>;
    retaliation: ReadonlyArray<VoteOutcome>;
    victim: ReadonlyArray<PersonID>;
    vote: ReadonlyArray<VoteOutcome>;
  }>
>;

export const mockDailyOutcomeRecord = ({
  days = 0,
  attack = [],
  corruption = [],
  divination = [],
  execution = null,
  protection = [],
  retaliation = [],
  victim = [],
  vote = []
}: DailyOutcomeRecordArgs = {}): DailyOutcomeRecord => {
  return {
    days,
    attack: VoteOutcomes.from(attack),
    corruption: VoteOutcomes.from(corruption),
    divination: VoteOutcomes.from(divination),
    execution,
    protection: VoteOutcomes.from(protection),
    retaliation: VoteOutcomes.from(retaliation),
    victim: [...victim],
    vote: VoteOutcomes.from(vote)
  } satisfies DailyOutcomeRecord;
};
