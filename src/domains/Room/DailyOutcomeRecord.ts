import { Arrays } from '@/lib/Collection/Array/Arrays.js';
import type { Nullable } from '@jamashita/anden/type';
import { z } from 'zod';
import { VoteOutcomes } from '../Outcome/VoteOutcomes.js';
import { Person, type PersonID } from '../Person/Person.js';
import type { Persons } from '../Person/Persons.js';

const DailyOutcomeRecordSchema = z.object({
  days: z.number().int().nonnegative(),
  // 人狼の襲撃
  attack: VoteOutcomes.SCHEMA,
  // 憑依者の汚染
  corruption: VoteOutcomes.SCHEMA,
  // 予言者の予言
  divination: VoteOutcomes.SCHEMA,
  // 臆病者の避難
  // evacuation: VoteOutcomes.SCHEMA,
  // 処刑
  execution: Person.ID.SCHEMA.nullable(),
  // 騎士の護衛
  protection: VoteOutcomes.SCHEMA,
  // 猟師の報復
  retaliation: VoteOutcomes.SCHEMA,
  // 犠牲者
  victim: z.array(Person.ID.SCHEMA),
  // 投票 // TODO 再投票に対応する
  vote: VoteOutcomes.SCHEMA
});

export type DailyOutcomeRecord = Readonly<z.infer<typeof DailyOutcomeRecordSchema>>;

// 日々の記録です
export namespace DailyOutcomeRecord {
  export const SCHEMA = DailyOutcomeRecordSchema;

  export const addAttack = (record: DailyOutcomeRecord, source: Person, destination: Person): DailyOutcomeRecord => {
    return {
      ...record,
      attack: VoteOutcomes.add(record.attack, source.id, destination.id)
    } satisfies DailyOutcomeRecord;
  };

  export const addCorruption = (record: DailyOutcomeRecord, source: Person, destination: Person): DailyOutcomeRecord => {
    return {
      ...record,
      corruption: VoteOutcomes.add(record.corruption, source.id, destination.id)
    } satisfies DailyOutcomeRecord;
  };

  export const addDivination = (record: DailyOutcomeRecord, source: Person, destination: Person): DailyOutcomeRecord => {
    return {
      ...record,
      divination: VoteOutcomes.add(record.divination, source.id, destination.id)
    } satisfies DailyOutcomeRecord;
  };

  export const addExecution = (record: DailyOutcomeRecord, target: Person): DailyOutcomeRecord => {
    return {
      ...record,
      execution: target.id
    } satisfies DailyOutcomeRecord;
  };

  export const addProtection = (record: DailyOutcomeRecord, source: Person, destination: Person): DailyOutcomeRecord => {
    return {
      ...record,
      protection: VoteOutcomes.add(record.protection, source.id, destination.id)
    } satisfies DailyOutcomeRecord;
  };

  export const addRetaliation = (record: DailyOutcomeRecord, source: Person, destination: Person): DailyOutcomeRecord => {
    return {
      ...record,
      retaliation: VoteOutcomes.add(record.retaliation, source.id, destination.id)
    } satisfies DailyOutcomeRecord;
  };

  export const addVictim = (record: DailyOutcomeRecord, target: Persons): DailyOutcomeRecord => {
    const ids = [...target.values()].map((person: Person) => {
      return person.id;
    });

    return {
      ...record,
      victim: [...record.victim, ...ids]
    } satisfies DailyOutcomeRecord;
  };

  export const addVote = (record: DailyOutcomeRecord, source: Person, destination: Person): DailyOutcomeRecord => {
    return {
      ...record,
      vote: VoteOutcomes.add(record.vote, source.id, destination.id)
    } satisfies DailyOutcomeRecord;
  };

  export const create = (days: number): DailyOutcomeRecord => {
    return {
      days,
      attack: VoteOutcomes.empty(),
      corruption: VoteOutcomes.empty(),
      divination: VoteOutcomes.empty(),
      execution: null,
      protection: VoteOutcomes.empty(),
      retaliation: VoteOutcomes.empty(),
      victim: [],
      vote: VoteOutcomes.empty()
    } satisfies DailyOutcomeRecord;
  };

  // 選んだ順番がバレないように混ぜてから返す
  export const getAttack = (record: DailyOutcomeRecord): VoteOutcomes => {
    return VoteOutcomes.from(Arrays.shuffle(record.attack));
  };

  // 選んだ順番がバレないように混ぜてから返す
  export const getCorruption = (record: DailyOutcomeRecord): VoteOutcomes => {
    return VoteOutcomes.from(Arrays.shuffle(record.corruption));
  };

  // 選んだ順番がバレないように混ぜてから返す
  export const getDivination = (record: DailyOutcomeRecord): VoteOutcomes => {
    return VoteOutcomes.from(Arrays.shuffle(record.divination));
  };

  export const getExecution = (record: DailyOutcomeRecord): Nullable<PersonID> => {
    return record.execution;
  };

  // 選んだ順番がバレないように混ぜてから返す
  export const getProtection = (record: DailyOutcomeRecord): VoteOutcomes => {
    return VoteOutcomes.from(Arrays.shuffle(record.protection));
  };

  // 選んだ順番がバレないように混ぜてから返す
  export const getRetaliation = (record: DailyOutcomeRecord): VoteOutcomes => {
    return VoteOutcomes.from(Arrays.shuffle(record.retaliation));
  };

  // 選んだ順番がバレないように混ぜてから返す
  export const getVictim = (record: DailyOutcomeRecord): Array<PersonID> => {
    return Arrays.shuffle(record.victim);
  };

  // 選んだ順番がバレないように混ぜてから返す
  export const getVote = (record: DailyOutcomeRecord): VoteOutcomes => {
    return VoteOutcomes.from(Arrays.shuffle(record.vote));
  };
}
