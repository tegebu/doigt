import { ExhaustiveError } from '@jamashita/anden/error';
import { z } from 'zod';

const PhaseSchema = z.union([z.literal('MORNING'), z.literal('DAY'), z.literal('EVENING'), z.literal('NIGHT')]);

export type Phase = z.infer<typeof PhaseSchema>;

export namespace Phase {
  export const SCHEMA = PhaseSchema;

  export const next = (phase: Phase): Phase => {
    switch (phase) {
      case 'MORNING': {
        return 'DAY';
      }
      case 'DAY': {
        return 'EVENING';
      }
      case 'EVENING': {
        return 'NIGHT';
      }
      case 'NIGHT': {
        return 'MORNING';
      }
      default: {
        throw new ExhaustiveError(phase);
      }
    }
  };
}
