import { createRuntimeError, type RuntimeError } from '@/lib/Error/Errors.js';
import { Kind } from '@jamashita/anden/type';
import { bindW, Do, type Either, left, map, right } from 'fp-ts/lib/Either.js';
import { pipe } from 'fp-ts/lib/function.js';
import { fromNullable, type Option } from 'fp-ts/lib/Option.js';
import type { Tagged } from 'type-fest';
import { z } from 'zod';
import type { Person } from '../Person/Person.js';
import type { Persons } from '../Person/Persons.js';
import { DailyOutcomeRecord } from './DailyOutcomeRecord.js';

export type DailyOutcomeRecords = Tagged<ReadonlyMap<number, DailyOutcomeRecord>, 'DailyOutcomeRecords'>;

const DailyOutcomeRecordsSchema = z.custom<DailyOutcomeRecords>((value: unknown) => z.map(z.number(), DailyOutcomeRecord.SCHEMA).safeParse(value));

export namespace DailyOutcomeRecords {
  export const SCHEMA = DailyOutcomeRecordsSchema;

  // TODO ? add?
  const add = (records: DailyOutcomeRecords, days: number): Either<RuntimeError, DailyOutcomeRecord> => {
    const record = records.get(days);

    if (Kind.isUndefined(record)) {
      return left(createRuntimeError(`${days}日目の記録が存在しません。`));
    }

    return right(record);
  };

  export const addAttack = (
    records: DailyOutcomeRecords,
    days: number,
    source: Person,
    destination: Person
  ): Either<RuntimeError, DailyOutcomeRecords> => {
    return pipe(
      Do,
      bindW('record', () => add(records, days)),
      map(({ record }) => {
        const map = new Map(records);

        return DailyOutcomeRecords.from(map.set(days, DailyOutcomeRecord.addAttack(record, source, destination)));
      })
    );
  };

  export const addCorruption = (
    records: DailyOutcomeRecords,
    days: number,
    source: Person,
    destination: Person
  ): Either<RuntimeError, DailyOutcomeRecords> => {
    return pipe(
      Do,
      bindW('record', () => add(records, days)),
      map(({ record }) => {
        const map = new Map(records);

        return DailyOutcomeRecords.from(map.set(days, DailyOutcomeRecord.addCorruption(record, source, destination)));
      })
    );
  };

  export const addDivination = (
    records: DailyOutcomeRecords,
    days: number,
    source: Person,
    destination: Person
  ): Either<RuntimeError, DailyOutcomeRecords> => {
    return pipe(
      Do,
      bindW('record', () => add(records, days)),
      map(({ record }) => {
        const map = new Map(records);

        return DailyOutcomeRecords.from(map.set(days, DailyOutcomeRecord.addDivination(record, source, destination)));
      })
    );
  };

  export const addExecution = (records: DailyOutcomeRecords, days: number, target: Person): Either<RuntimeError, DailyOutcomeRecords> => {
    return pipe(
      Do,
      bindW('record', () => add(records, days)),
      map(({ record }) => {
        const map = new Map(records);

        return DailyOutcomeRecords.from(map.set(days, DailyOutcomeRecord.addExecution(record, target)));
      })
    );
  };

  export const addProtection = (
    records: DailyOutcomeRecords,
    days: number,
    source: Person,
    destination: Person
  ): Either<RuntimeError, DailyOutcomeRecords> => {
    return pipe(
      Do,
      bindW('record', () => add(records, days)),
      map(({ record }) => {
        const map = new Map(records);

        return DailyOutcomeRecords.from(map.set(days, DailyOutcomeRecord.addProtection(record, source, destination)));
      })
    );
  };

  export const addRetaliation = (
    records: DailyOutcomeRecords,
    days: number,
    source: Person,
    destination: Person
  ): Either<RuntimeError, DailyOutcomeRecords> => {
    return pipe(
      Do,
      bindW('record', () => add(records, days)),
      map(({ record }) => {
        const map = new Map(records);

        return DailyOutcomeRecords.from(map.set(days, DailyOutcomeRecord.addRetaliation(record, source, destination)));
      })
    );
  };

  export const addVictim = (records: DailyOutcomeRecords, days: number, target: Persons): Either<RuntimeError, DailyOutcomeRecords> => {
    return pipe(
      Do,
      bindW('record', () => add(records, days)),
      map(({ record }) => {
        const map = new Map(records);

        return DailyOutcomeRecords.from(map.set(days, DailyOutcomeRecord.addVictim(record, target)));
      })
    );
  };

  export const addVote = (
    records: DailyOutcomeRecords,
    days: number,
    source: Person,
    destination: Person
  ): Either<RuntimeError, DailyOutcomeRecords> => {
    return pipe(
      Do,
      bindW('record', () => add(records, days)),
      map(({ record }) => {
        const map = new Map(records);

        return DailyOutcomeRecords.from(map.set(days, DailyOutcomeRecord.addVote(record, source, destination)));
      })
    );
  };

  export const create = (records: DailyOutcomeRecords): DailyOutcomeRecords => {
    const map = new Map(records);

    // 現在の最大日数を取得する
    if (records.size === 0) {
      const record = DailyOutcomeRecord.create(0);

      return DailyOutcomeRecords.from(map.set(0, record));
    }

    const maxDays = DailyOutcomeRecords.getMaxDays(records);
    const record = DailyOutcomeRecord.create(maxDays + 1);

    return DailyOutcomeRecords.from(map.set(maxDays + 1, record));
  };

  export const empty = (): DailyOutcomeRecords => {
    return DailyOutcomeRecords.from(new Map());
  };

  export const from = (records: ReadonlyMap<number, DailyOutcomeRecord>): DailyOutcomeRecords => {
    return records as DailyOutcomeRecords;
  };

  export const getChronologically = (records: DailyOutcomeRecords): Array<DailyOutcomeRecord> => {
    return [...records.values()].sort((r1: DailyOutcomeRecord, r2: DailyOutcomeRecord) => {
      return r1.days - r2.days;
    });
  };

  export const getLatest = (records: DailyOutcomeRecords): Option<DailyOutcomeRecord> => {
    const maxDays = Math.max(...records.keys());
    const record = records.get(maxDays);

    return fromNullable(record);
  };

  export const getMaxDays = (records: DailyOutcomeRecords): number => {
    return Math.max(...records.keys());
  };

  export const ofArray = (records: ReadonlyArray<DailyOutcomeRecord>): DailyOutcomeRecords => {
    if (records.length === 0) {
      return DailyOutcomeRecords.empty();
    }

    const map = new Map<number, DailyOutcomeRecord>();

    for (const record of records) {
      map.set(record.days, record);
    }

    return DailyOutcomeRecords.from(map);
  };
}
