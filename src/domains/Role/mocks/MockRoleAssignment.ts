import type { RoleAssignment } from '../RoleAssignment.js';

type RoleAssignmentArgs = Partial<
  Readonly<{
    atonement: number;
    catalyst: number;
    coward: number;
    hunter: number;
    informant: number;
    jester: number;
    kitsune: number;
    knight: number;
    medium: number;
    monastic: number;
    orator: number;
    possessed: number;
    seer: number;
    villager: number;
    werewolf: number;
  }>
>;

export const mockRoleAssignment = ({
  atonement = 0,
  catalyst = 0,
  coward = 0,
  hunter = 0,
  informant = 0,
  jester = 0,
  kitsune = 0,
  knight = 0,
  medium = 0,
  monastic = 0,
  orator = 0,
  possessed = 0,
  seer = 0,
  villager = 0,
  werewolf = 0
}: RoleAssignmentArgs = {}) => {
  return {
    atonement,
    catalyst,
    coward,
    hunter,
    informant,
    jester,
    kitsune,
    knight,
    medium,
    monastic,
    orator,
    possessed,
    seer,
    villager,
    werewolf
  } satisfies RoleAssignment;
};
