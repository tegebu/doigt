import { Random } from '@jamashita/steckdose/random';
import { isLeft, isRight } from 'fp-ts/lib/Either.js';
import { RoleAssignment } from '../RoleAssignment.js';

describe('RoleAssignment', () => {
  describe('expandRoles', () => {
    it('should return an empty sequence when the assignment is empty', () => {
      const assignment = {
        atonement: 0,
        catalyst: 0,
        coward: 0,
        hunter: 0,
        informant: 0,
        jester: 0,
        kitsune: 0,
        knight: 0,
        medium: 0,
        monastic: 0,
        orator: 0,
        possessed: 0,
        seer: 0,
        villager: 0,
        werewolf: 0
      } satisfies RoleAssignment;

      const result = RoleAssignment.expandRoles(assignment);

      expect(result.length).toBe(0);
    });

    it.each(Array.from({ length: 100 }, (_, i) => i + 1))('should return a sequence of roles when the assignment is not empty: #%i attempt', () => {
      const assignment = {
        atonement: Random.integer(0, 5),
        catalyst: Random.integer(0, 5),
        coward: Random.integer(0, 5),
        hunter: Random.integer(0, 5),
        informant: Random.integer(0, 5),
        jester: Random.integer(0, 5),
        kitsune: Random.integer(0, 5),
        knight: Random.integer(0, 5),
        medium: Random.integer(0, 5),
        monastic: Random.integer(0, 5),
        orator: Random.integer(0, 5),
        possessed: Random.integer(0, 5),
        seer: Random.integer(0, 5),
        villager: Random.integer(0, 5),
        werewolf: Random.integer(0, 5)
      } satisfies RoleAssignment;

      const result = RoleAssignment.expandRoles(assignment);

      expect(result.length).toBe(Object.values(assignment).reduce((acc, cur) => acc + cur, 0));

      for (let i = 0; i < assignment.atonement; i++) {
        expect(result.includes('ATONEMENT')).toBe(true);
      }
      for (let i = 0; i < assignment.catalyst; i++) {
        expect(result.includes('CATALYST')).toBe(true);
      }
      for (let i = 0; i < assignment.coward; i++) {
        expect(result.includes('COWARD')).toBe(true);
      }
      for (let i = 0; i < assignment.hunter; i++) {
        expect(result.includes('HUNTER')).toBe(true);
      }
      for (let i = 0; i < assignment.informant; i++) {
        expect(result.includes('INFORMANT')).toBe(true);
      }
      for (let i = 0; i < assignment.jester; i++) {
        expect(result.includes('JESTER')).toBe(true);
      }
      for (let i = 0; i < assignment.kitsune; i++) {
        expect(result.includes('KITSUNE')).toBe(true);
      }
      for (let i = 0; i < assignment.knight; i++) {
        expect(result.includes('KNIGHT')).toBe(true);
      }
      for (let i = 0; i < assignment.medium; i++) {
        expect(result.includes('MEDIUM')).toBe(true);
      }
      for (let i = 0; i < assignment.monastic; i++) {
        expect(result.includes('MONASTIC')).toBe(true);
      }
      for (let i = 0; i < assignment.orator; i++) {
        expect(result.includes('ORATOR')).toBe(true);
      }
      for (let i = 0; i < assignment.possessed; i++) {
        expect(result.includes('POSSESSED')).toBe(true);
      }
      for (let i = 0; i < assignment.seer; i++) {
        expect(result.includes('SEER')).toBe(true);
      }
      for (let i = 0; i < assignment.villager; i++) {
        expect(result.includes('VILLAGER')).toBe(true);
      }
      for (let i = 0; i < assignment.werewolf; i++) {
        expect(result.includes('WEREWOLF')).toBe(true);
      }
    });
  });

  describe('validate', () => {
    it('should return right when the number of villagers are greater than the number of werewolves', () => {
      const assignment = {
        atonement: 0,
        catalyst: 0,
        coward: 0,
        hunter: 0,
        informant: 0,
        jester: 0,
        kitsune: 0,
        knight: 0,
        medium: 0,
        monastic: 0,
        orator: 0,
        possessed: 0,
        seer: 1,
        villager: 2,
        werewolf: 2
      } satisfies RoleAssignment;

      const either = RoleAssignment.validate(assignment);

      expect(isRight(either)).toBe(true);
    });

    it('should return left when the assignment is empty', () => {
      const assignment = {
        atonement: 0,
        catalyst: 0,
        coward: 0,
        hunter: 0,
        informant: 0,
        jester: 0,
        kitsune: 0,
        knight: 0,
        medium: 0,
        monastic: 0,
        orator: 0,
        possessed: 0,
        seer: 0,
        villager: 0,
        werewolf: 0
      } satisfies RoleAssignment;

      const either = RoleAssignment.validate(assignment);

      expect(isLeft(either)).toBe(true);
    });

    it('should return left when the number of villagers is 0', () => {
      const assignment = {
        atonement: 0,
        catalyst: 0,
        coward: 0,
        hunter: 0,
        informant: 0,
        jester: 0,
        kitsune: 0,
        knight: 0,
        medium: 0,
        monastic: 0,
        orator: 0,
        possessed: 0,
        seer: 0,
        villager: 0,
        werewolf: 1
      } satisfies RoleAssignment;

      const either = RoleAssignment.validate(assignment);

      expect(isLeft(either)).toBe(true);
    });

    it('should return left when the number of werewolves is 0', () => {
      const assignment = {
        atonement: 0,
        catalyst: 0,
        coward: 0,
        hunter: 0,
        informant: 0,
        jester: 0,
        kitsune: 0,
        knight: 0,
        medium: 0,
        monastic: 0,
        orator: 0,
        possessed: 0,
        seer: 1,
        villager: 1,
        werewolf: 0
      } satisfies RoleAssignment;

      const either = RoleAssignment.validate(assignment);

      expect(isLeft(either)).toBe(true);
    });

    it('should return left when the number of villagers are less than the sum of the number of werewolves and the number of catalysts', () => {
      const assignment = {
        atonement: 0,
        catalyst: 1,
        coward: 0,
        hunter: 0,
        informant: 0,
        jester: 0,
        kitsune: 0,
        knight: 0,
        medium: 0,
        monastic: 0,
        orator: 0,
        possessed: 0,
        seer: 1,
        villager: 1,
        werewolf: 1
      } satisfies RoleAssignment;

      const either = RoleAssignment.validate(assignment);

      expect(isLeft(either)).toBe(true);
    });

    it('should return left when the number of villagers are equal to the number of werewolves', () => {
      const assignment = {
        atonement: 0,
        catalyst: 0,
        coward: 0,
        hunter: 0,
        informant: 0,
        jester: 0,
        kitsune: 0,
        knight: 0,
        medium: 0,
        monastic: 0,
        orator: 0,
        possessed: 0,
        seer: 1,
        villager: 1,
        werewolf: 2
      } satisfies RoleAssignment;

      const either = RoleAssignment.validate(assignment);

      expect(isLeft(either)).toBe(true);
    });

    it('should return left when the number of villagers are less than the number of werewolves', () => {
      const assignment = {
        atonement: 0,
        catalyst: 0,
        coward: 0,
        hunter: 0,
        informant: 0,
        jester: 0,
        kitsune: 0,
        knight: 0,
        medium: 0,
        monastic: 0,
        orator: 0,
        possessed: 0,
        seer: 1,
        villager: 1,
        werewolf: 3
      } satisfies RoleAssignment;

      const either = RoleAssignment.validate(assignment);

      expect(isLeft(either)).toBe(true);
    });

    it('should return right when the number of villagers are greater than the sum of the number of werewolves and the number of catalysts', () => {
      const assignment = {
        atonement: 0,
        catalyst: 1,
        coward: 0,
        informant: 0,
        hunter: 0,
        jester: 0,
        kitsune: 0,
        knight: 0,
        medium: 0,
        monastic: 0,
        orator: 0,
        possessed: 0,
        seer: 1,
        villager: 2,
        werewolf: 1
      } satisfies RoleAssignment;

      const either = RoleAssignment.validate(assignment);

      expect(isLeft(either)).toBe(false);
    });
  });
});
