type Detail = 'QuotaExceeded' | 'TooFewPlayers' | 'UnsupportedQuota' | 'NegativeVillagers' | 'NegativeWerewolves' | 'InvalidHeadcount';

export type RoleError = Readonly<{
  error: 'RoleError';
  detail: Detail;
  message: string;
}>;

export const createRoleError = (detail: Detail, message: string): RoleError => {
  return {
    error: 'RoleError',
    detail,
    message
  };
};
