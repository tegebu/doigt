import { z } from 'zod';

const RoleSchema = z.union([
  z.literal('ACCUSER'),
  z.literal('APOSTATE'),
  z.literal('ATONEMENT'),
  z.literal('CATALYST'),
  z.literal('COUNT'),
  z.literal('COWARD'),
  z.literal('CULT_LEADER'),
  z.literal('HUNTER'),
  z.literal('INFORMANT'),
  z.literal('JESTER'),
  z.literal('KITSUNE'),
  z.literal('KNIGHT'),
  z.literal('MEDIUM'),
  z.literal('MONASTIC'),
  z.literal('ORATOR'),
  z.literal('POSSESSED'),
  z.literal('SEER'),
  z.literal('VILLAGER'),
  z.literal('WEREWOLF')
]);

export type Role = z.infer<typeof RoleSchema>;

export namespace Role {
  export const SCHEMA = RoleSchema;
}
