import { ExhaustiveError } from '@jamashita/anden/error';
import type { Either } from 'fp-ts/lib/Either.js';
import { left, right } from 'fp-ts/lib/Either.js';
import { z } from 'zod';
import { Identity } from '../Identity/Identity.js';
import type { Role } from './Role.js';
import { createRoleError, type RoleError } from './RoleError.js';

const RoleAssignmentSchema = z.object({
  atonement: z.number().int().nonnegative(),
  catalyst: z.number().int().nonnegative(),
  coward: z.number().int().nonnegative(),
  hunter: z.number().int().nonnegative(),
  informant: z.number().int().nonnegative(),
  jester: z.number().int().nonnegative(),
  kitsune: z.number().int().nonnegative(),
  knight: z.number().int().nonnegative(),
  medium: z.number().int().nonnegative(),
  monastic: z.number().int().nonnegative(),
  orator: z.number().int().nonnegative(),
  possessed: z.number().int().nonnegative(),
  seer: z.number().int().nonnegative(),
  villager: z.number().int().nonnegative(),
  werewolf: z.number().int().nonnegative()
});

export type RoleAssignment = Readonly<z.infer<typeof RoleAssignmentSchema>>;
export type RoleAssignmentKey = keyof RoleAssignment;

const MIN_QUOTA = 4;

export namespace RoleAssignment {
  export const SCHEMA = RoleAssignmentSchema;

  export const expandRoles = (assignment: RoleAssignment): Array<Role> => {
    const arr: Array<Role> = [];

    for (const [key, count] of Object.entries(assignment)) {
      const role = toRole(toKey(key));

      for (let i = 0; i < count; i++) {
        arr.push(role);
      }
    }

    return arr;
  };

  const forge = (assignment: Partial<RoleAssignment>): RoleAssignment => {
    return {
      atonement: assignment.atonement ?? 0,
      catalyst: assignment.catalyst ?? 0,
      coward: assignment.coward ?? 0,
      hunter: assignment.hunter ?? 0,
      informant: assignment.informant ?? 0,
      jester: assignment.jester ?? 0,
      kitsune: assignment.kitsune ?? 0,
      knight: assignment.knight ?? 0,
      medium: assignment.medium ?? 0,
      monastic: assignment.monastic ?? 0,
      orator: assignment.orator ?? 0,
      possessed: assignment.possessed ?? 0,
      seer: assignment.seer ?? 0,
      villager: assignment.villager ?? 0,
      werewolf: assignment.werewolf ?? 0
    } satisfies RoleAssignment;
  };

  /**
   * 通常の人数別役職割り当てを取得する
   *
   * @param quota
   */
  export const getByQuota = (quota: number): Either<RoleError, RoleAssignment> => {
    // MIN_QUOTA名以下では遊べない
    if (quota <= MIN_QUOTA) {
      return left({
        error: 'RoleError',
        detail: 'TooFewPlayers',
        message: `${MIN_QUOTA}名以下では遊べません`
      } satisfies RoleError);
    }

    switch (quota) {
      case 5: {
        return right(
          forge({
            informant: 1,
            seer: 1,
            villager: 2,
            werewolf: 1
          })
        );
      }
      case 6: {
        return right(
          forge({
            informant: 1,
            seer: 1,
            villager: 3,
            werewolf: 1
          })
        );
      }
      case 7: {
        return right(
          forge({
            informant: 1,
            knight: 1,
            seer: 1,
            villager: 3,
            werewolf: 1
          })
        );
      }
      case 8: {
        return right(
          forge({
            informant: 1,
            knight: 1,
            seer: 1,
            villager: 3,
            werewolf: 2
          })
        );
      }
      case 9: {
        return right(
          forge({
            informant: 1,
            knight: 1,
            medium: 1,
            seer: 1,
            villager: 3,
            werewolf: 2
          })
        );
      }
      case 10: {
        return right(
          forge({
            informant: 1,
            kitsune: 1,
            knight: 1,
            medium: 1,
            seer: 1,
            villager: 3,
            werewolf: 2
          })
        );
      }
      case 11: {
        return right(
          forge({
            informant: 1,
            kitsune: 1,
            knight: 1,
            medium: 1,
            possessed: 1,
            seer: 1,
            villager: 3,
            werewolf: 2
          })
        );
      }
      case 12: {
        return right(
          forge({
            informant: 1,
            hunter: 1,
            kitsune: 1,
            knight: 1,
            medium: 1,
            possessed: 1,
            seer: 1,
            villager: 3,
            werewolf: 2
          })
        );
      }
      default: {
        return left({
          error: 'RoleError',
          detail: 'UnsupportedQuota',
          message: `未対応の人数です: ${quota}`
        } satisfies RoleError);
      }
    }
  };

  const toKey = (key: string): RoleAssignmentKey => {
    return key as RoleAssignmentKey;
  };

  const toRole = (key: RoleAssignmentKey): Role => {
    switch (key) {
      case 'atonement': {
        return 'ATONEMENT';
      }
      case 'catalyst': {
        return 'CATALYST';
      }
      case 'coward': {
        return 'COWARD';
      }
      case 'informant': {
        return 'INFORMANT';
      }
      case 'hunter': {
        return 'HUNTER';
      }
      case 'jester': {
        return 'JESTER';
      }
      case 'kitsune': {
        return 'KITSUNE';
      }
      case 'knight': {
        return 'KNIGHT';
      }
      case 'medium': {
        return 'MEDIUM';
      }
      case 'monastic': {
        return 'MONASTIC';
      }
      case 'orator': {
        return 'ORATOR';
      }
      case 'possessed': {
        return 'POSSESSED';
      }
      case 'seer': {
        return 'SEER';
      }
      case 'villager': {
        return 'VILLAGER';
      }
      case 'werewolf': {
        return 'WEREWOLF';
      }
      default: {
        throw new ExhaustiveError(key);
      }
    }
  };

  /**
   * 村人陣営が1名以上いる
   * 人狼陣営が1名以上いる
   * 村人陣営は人狼陣営よりも多い
   */
  export const validate = (assignment: RoleAssignment): Either<RoleError, unknown> => {
    const villagers = Object.entries(assignment).reduce((prev, [key, count]) => {
      if (count <= 0) {
        return prev;
      }

      const role = toRole(toKey(key));
      const identity = Identity.getByRole(role);

      if (identity.team.team === 'VILLAGERS') {
        return prev + count;
      }
      return prev;
    }, 0);

    const werewolves = Object.entries(assignment).reduce((prev, [key, count]) => {
      if (count <= 0) {
        return prev;
      }

      const role = toRole(toKey(key));
      const identity = Identity.getByRole(role);

      if (identity.role === 'WEREWOLF') {
        return prev + count;
      }

      return prev;
    }, 0);
    const catalysts = Object.entries(assignment).reduce((prev, [key, count]) => {
      if (count <= 0) {
        return prev;
      }

      const role = toRole(toKey(key));
      const identity = Identity.getByRole(role);

      if (identity.role === 'CATALYST') {
        return prev + count;
      }

      return prev;
    }, 0);

    if (villagers <= 0) {
      return left(createRoleError('NegativeVillagers', '村人陣営が存在しません'));
    }
    if (werewolves <= 0) {
      return left(createRoleError('NegativeWerewolves', '人狼が存在しません'));
    }

    if (villagers <= werewolves + catalysts) {
      return left(createRoleError('InvalidHeadcount', '村人陣営が人狼よりも少ないです'));
    }

    return right(null);
  };
}
