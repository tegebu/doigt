import { Person } from '@/domains/Person/Person.js';
import { SeerOutcome } from '../SeerOutcome.js';

describe('SeerOutcome', () => {
  describe('negate', () => {
    it.each`
      identity      | expected
      ${'VILLAGER'} | ${'WEREWOLF'}
      ${'WEREWOLF'} | ${'VILLAGER'}
    `('returns $expected when $identity is passed', ({ identity, expected }) => {
      const personID = Person.ID.from('42b50c28-7be2-46cd-b857-7745af7f1993');
      const outcome = {
        id: personID,
        identity
      } satisfies SeerOutcome;

      const result = SeerOutcome.negate(outcome);

      expect(result).toEqual({
        id: personID,
        identity: expected
      });
    });
  });
});
