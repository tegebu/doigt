import { Person } from '@/domains/Person/Person.js';
import { VoteOutcomes } from '../VoteOutcomes.js';

describe('VoteOutcomes', () => {
  describe('add', () => {
    it('should return new Votes', () => {
      const votes = VoteOutcomes.empty();
      const source = Person.ID.from('96c723c1-a89b-4162-bd1b-0a41f69c246e');
      const destination = Person.ID.from('f2d34656-5e52-4e93-890a-3041f17541aa');

      const newVotes = VoteOutcomes.add(votes, source, destination);

      expect(votes.length).toBe(0);
      expect(newVotes.length).toBe(1);
      expect(newVotes[0]?.source).toBe('96c723c1-a89b-4162-bd1b-0a41f69c246e');
      expect(newVotes[0]?.destination).toBe('f2d34656-5e52-4e93-890a-3041f17541aa');
    });

    it('should overwrite when source is already in Votes', () => {
      const source = Person.ID.from('96c723c1-a89b-4162-bd1b-0a41f69c246e');
      const destination1 = Person.ID.from('f2d34656-5e52-4e93-890a-3041f17541aa');
      const destination2 = Person.ID.from('8aa40de8-f1e8-4bed-bf35-a53529e89b36');
      const votes = VoteOutcomes.from([
        {
          source,
          destination: destination1
        }
      ]);

      expect(votes.length).toBe(1);

      const newVotes = VoteOutcomes.add(votes, source, destination2);

      expect(votes.length).toBe(1);
      expect(newVotes.length).toBe(1);
      expect(newVotes[0]?.source).toBe('96c723c1-a89b-4162-bd1b-0a41f69c246e');
      expect(newVotes[0]?.destination).toBe('8aa40de8-f1e8-4bed-bf35-a53529e89b36');
    });
  });

  describe('countVotes', () => {
    it('should return empty counts when there is no Votes', () => {
      const votes = VoteOutcomes.empty();
      const counts = VoteOutcomes.countVotes(votes);

      expect(counts.size).toBe(0);
    });

    it('should return counts of Votes', () => {
      const source1 = Person.ID.from('96c723c1-a89b-4162-bd1b-0a41f69c246e');
      const source2 = Person.ID.from('fdc8615a-183b-442b-a9ff-2aa3bd049559');
      const source3 = Person.ID.from('6edc0851-7ac3-4cfc-a7ad-7b6d1340190d');
      const destination1 = Person.ID.from('f2d34656-5e52-4e93-890a-3041f17541aa');
      const destination2 = Person.ID.from('8aa40de8-f1e8-4bed-bf35-a53529e89b36');
      const destination3 = Person.ID.from('0d818482-d5cc-4102-96e5-96fb0d377396');
      const votes = VoteOutcomes.from([
        {
          source: source1,
          destination: destination2
        },
        {
          source: source2,
          destination: destination3
        },
        {
          source: source3,
          destination: destination1
        }
      ]);

      expect(votes.length).toBe(3);

      const counts = VoteOutcomes.countVotes(votes);

      expect(counts.size).toBe(3);
      expect(counts.get(destination1)).toBe(1);
      expect(counts.get(destination2)).toBe(1);
      expect(counts.get(destination3)).toBe(1);
    });

    it('should return counts of Votes with multiple votes for the same destination', () => {
      const source1 = Person.ID.from('96c723c1-a89b-4162-bd1b-0a41f69c246e');
      const source2 = Person.ID.from('fdc8615a-183b-442b-a9ff-2aa3bd049559');
      const source3 = Person.ID.from('6edc0851-7ac3-4cfc-a7ad-7b6d1340190d');
      const destination1 = Person.ID.from('f2d34656-5e52-4e93-890a-3041f17541aa');
      const votes = VoteOutcomes.from([
        {
          source: source1,
          destination: destination1
        },
        {
          source: source2,
          destination: destination1
        },
        {
          source: source3,
          destination: destination1
        }
      ]);

      expect(votes.length).toBe(3);

      const counts = VoteOutcomes.countVotes(votes);

      expect(counts.size).toBe(1);
      expect(counts.get(destination1)).toBe(3);
    });
  });

  describe('getMostVoted', () => {
    it('should return empty when there is no Votes', () => {
      const votes = VoteOutcomes.empty();
      const mostVoted = VoteOutcomes.getMostVoted(votes);

      expect(mostVoted.length).toBe(0);
    });

    it('should return most voted destination', () => {
      const source1 = Person.ID.from('96c723c1-a89b-4162-bd1b-0a41f69c246e');
      const source2 = Person.ID.from('fdc8615a-183b-442b-a9ff-2aa3bd049559');
      const source3 = Person.ID.from('6edc0851-7ac3-4cfc-a7ad-7b6d1340190d');
      const destination1 = Person.ID.from('f2d34656-5e52-4e93-890a-3041f17541aa');
      const destination2 = Person.ID.from('8aa40de8-f1e8-4bed-bf35-a53529e89b36');
      const votes = VoteOutcomes.from([
        {
          source: source1,
          destination: destination1
        },
        {
          source: source2,
          destination: destination2
        },
        {
          source: source3,
          destination: destination1
        }
      ]);

      expect(votes.length).toBe(3);

      const mostVoted = VoteOutcomes.getMostVoted(votes);

      expect(mostVoted.length).toBe(1);
      expect(mostVoted[0]).toBe(destination1);
    });

    it('should return multiple most voted destinations', () => {
      const source1 = Person.ID.from('96c723c1-a89b-4162-bd1b-0a41f69c246e');
      const source2 = Person.ID.from('fdc8615a-183b-442b-a9ff-2aa3bd049559');
      const source3 = Person.ID.from('6edc0851-7ac3-4cfc-a7ad-7b6d1340190d');
      const source4 = Person.ID.from('fde58824-9ee8-4201-8608-45edcf534b3e');
      const destination1 = Person.ID.from('f2d34656-5e52-4e93-890a-3041f17541aa');
      const destination2 = Person.ID.from('8aa40de8-f1e8-4bed-bf35-a53529e89b36');
      const votes = VoteOutcomes.from([
        {
          source: source1,
          destination: destination1
        },
        {
          source: source2,
          destination: destination1
        },
        {
          source: source3,
          destination: destination2
        },
        {
          source: source4,
          destination: destination2
        }
      ]);

      expect(votes.length).toBe(4);

      const mostVoted = VoteOutcomes.getMostVoted(votes);

      expect(mostVoted.length).toBe(2);
      expect(mostVoted[0]).toBe(destination1);
      expect(mostVoted[1]).toBe(destination2);
    });
  });

  describe('remove', () => {
    it('should return new Votes', () => {
      const source1 = Person.ID.from('96c723c1-a89b-4162-bd1b-0a41f69c246e');
      const source2 = Person.ID.from('fdc8615a-183b-442b-a9ff-2aa3bd049559');
      const source3 = Person.ID.from('6edc0851-7ac3-4cfc-a7ad-7b6d1340190d');
      const destination1 = Person.ID.from('f2d34656-5e52-4e93-890a-3041f17541aa');
      const destination2 = Person.ID.from('8aa40de8-f1e8-4bed-bf35-a53529e89b36');
      const destination3 = Person.ID.from('0d818482-d5cc-4102-96e5-96fb0d377396');
      const votes = VoteOutcomes.from([
        {
          source: source1,
          destination: destination1
        },
        {
          source: source2,
          destination: destination2
        },
        {
          source: source3,
          destination: destination3
        }
      ]);

      expect(votes.length).toBe(3);

      const newVotes1 = VoteOutcomes.remove(votes, source2);

      expect(votes.length).toBe(3);
      expect(newVotes1.length).toBe(2);
      expect(newVotes1[0]?.source).toBe('96c723c1-a89b-4162-bd1b-0a41f69c246e');
      expect(newVotes1[0]?.destination).toBe('f2d34656-5e52-4e93-890a-3041f17541aa');
      expect(newVotes1[1]?.source).toBe('6edc0851-7ac3-4cfc-a7ad-7b6d1340190d');
      expect(newVotes1[1]?.destination).toBe('0d818482-d5cc-4102-96e5-96fb0d377396');

      const newVotes2 = VoteOutcomes.remove(newVotes1, source3);

      expect(votes.length).toBe(3);
      expect(newVotes1.length).toBe(2);
      expect(newVotes2[0]?.source).toBe('96c723c1-a89b-4162-bd1b-0a41f69c246e');
      expect(newVotes2[0]?.destination).toBe('f2d34656-5e52-4e93-890a-3041f17541aa');

      const newVotes3 = VoteOutcomes.remove(newVotes2, source1);

      expect(votes.length).toBe(3);
      expect(newVotes1.length).toBe(2);
      expect(newVotes2.length).toBe(1);
      expect(newVotes3.length).toBe(0);
    });

    it('should not reduce anything when source is not found', () => {
      const source1 = Person.ID.from('96c723c1-a89b-4162-bd1b-0a41f69c246e');
      const source2 = Person.ID.from('fdc8615a-183b-442b-a9ff-2aa3bd049559');
      const source3 = Person.ID.from('6edc0851-7ac3-4cfc-a7ad-7b6d1340190d');
      const destination1 = Person.ID.from('f2d34656-5e52-4e93-890a-3041f17541aa');
      const destination2 = Person.ID.from('8aa40de8-f1e8-4bed-bf35-a53529e89b36');
      const destination3 = Person.ID.from('0d818482-d5cc-4102-96e5-96fb0d377396');
      const votes = VoteOutcomes.from([
        {
          source: source1,
          destination: destination1
        },
        {
          source: source2,
          destination: destination2
        },
        {
          source: source3,
          destination: destination3
        }
      ]);

      expect(votes.length).toBe(3);

      const newVotes = VoteOutcomes.remove(votes, Person.ID.from('6603adb5-f9e4-4d31-943b-64a83a8e6936'));

      expect(votes.length).toBe(3);
      expect(newVotes.length).toBe(3);
      expect(votes[0]).toBe(newVotes[0]);
      expect(votes[1]).toBe(newVotes[1]);
      expect(votes[2]).toBe(newVotes[2]);
    });
  });

  describe('replace', () => {
    it('should return new Votes', () => {
      const source1 = Person.ID.from('96c723c1-a89b-4162-bd1b-0a41f69c246e');
      const source2 = Person.ID.from('fdc8615a-183b-442b-a9ff-2aa3bd049559');
      const source3 = Person.ID.from('6edc0851-7ac3-4cfc-a7ad-7b6d1340190d');
      const destination1 = Person.ID.from('f2d34656-5e52-4e93-890a-3041f17541aa');
      const destination2 = Person.ID.from('8aa40de8-f1e8-4bed-bf35-a53529e89b36');
      const destination3 = Person.ID.from('0d818482-d5cc-4102-96e5-96fb0d377396');
      const votes = VoteOutcomes.from([
        {
          source: source1,
          destination: destination1
        },
        {
          source: source2,
          destination: destination2
        },
        {
          source: source3,
          destination: destination3
        }
      ]);

      expect(votes.length).toBe(3);

      const newVotes1 = VoteOutcomes.replace(votes, source2, destination1);

      expect(votes.length).toBe(3);
      expect(newVotes1.length).toBe(3);
      expect(newVotes1[0]?.source).toBe('96c723c1-a89b-4162-bd1b-0a41f69c246e');
      expect(newVotes1[0]?.destination).toBe('f2d34656-5e52-4e93-890a-3041f17541aa');
      expect(newVotes1[1]?.source).toBe('fdc8615a-183b-442b-a9ff-2aa3bd049559');
      expect(newVotes1[1]?.destination).toBe('f2d34656-5e52-4e93-890a-3041f17541aa');
      expect(newVotes1[2]?.source).toBe('6edc0851-7ac3-4cfc-a7ad-7b6d1340190d');
      expect(newVotes1[2]?.destination).toBe('0d818482-d5cc-4102-96e5-96fb0d377396');

      const newVotes2 = VoteOutcomes.replace(newVotes1, source1, destination3);

      expect(votes.length).toBe(3);
      expect(newVotes1.length).toBe(3);
      expect(newVotes2.length).toBe(3);
      expect(newVotes2[0]?.source).toBe('96c723c1-a89b-4162-bd1b-0a41f69c246e');
      expect(newVotes2[0]?.destination).toBe('0d818482-d5cc-4102-96e5-96fb0d377396');
      expect(newVotes2[1]?.source).toBe('fdc8615a-183b-442b-a9ff-2aa3bd049559');
      expect(newVotes2[1]?.destination).toBe('f2d34656-5e52-4e93-890a-3041f17541aa');
      expect(newVotes2[2]?.source).toBe('6edc0851-7ac3-4cfc-a7ad-7b6d1340190d');
      expect(newVotes2[2]?.destination).toBe('0d818482-d5cc-4102-96e5-96fb0d377396');
    });

    it('should not replace anything when source is not found', () => {
      const source1 = Person.ID.from('96c723c1-a89b-4162-bd1b-0a41f69c246e');
      const source2 = Person.ID.from('fdc8615a-183b-442b-a9ff-2aa3bd049559');
      const source3 = Person.ID.from('6edc0851-7ac3-4cfc-a7ad-7b6d1340190d');
      const destination1 = Person.ID.from('f2d34656-5e52-4e93-890a-3041f17541aa');
      const destination2 = Person.ID.from('8aa40de8-f1e8-4bed-bf35-a53529e89b36');
      const destination3 = Person.ID.from('0d818482-d5cc-4102-96e5-96fb0d377396');
      const votes = VoteOutcomes.from([
        {
          source: source1,
          destination: destination1
        },
        {
          source: source2,
          destination: destination2
        },
        {
          source: source3,
          destination: destination3
        }
      ]);

      expect(votes.length).toBe(3);

      const newVotes = VoteOutcomes.replace(votes, Person.ID.from('6603adb5-f9e4-4d31-943b-64a83a8e6936'), destination1);

      expect(votes.length).toBe(3);
      expect(newVotes.length).toBe(3);
      expect(votes[0]).toBe(newVotes[0]);
      expect(votes[1]).toBe(newVotes[1]);
      expect(votes[2]).toBe(newVotes[2]);
    });
  });
});
