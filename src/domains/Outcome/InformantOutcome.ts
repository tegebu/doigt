import { z } from 'zod';
import { Person } from '../Person/Person.js';

const InformantOutcomeSchema = z.object({
  ids: z.array(Person.ID.SCHEMA)
});

export type InformantOutcome = Readonly<z.infer<typeof InformantOutcomeSchema>>;

export namespace InformantOutcome {
  export const SCHEMA = InformantOutcomeSchema;
}
