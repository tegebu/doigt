import { ExhaustiveError } from '@jamashita/anden/error';
import { z } from 'zod';
import { Person } from '../Person/Person.js';

const SeerOutcomeTypeSchema = z.union([z.literal('VILLAGER'), z.literal('WEREWOLF')]);
const SeerOutcomeSchema = z.object({
  id: Person.ID.SCHEMA,
  identity: SeerOutcomeTypeSchema
});

export type SeerOutcomeType = z.infer<typeof SeerOutcomeTypeSchema>;
export type SeerOutcome = Readonly<z.infer<typeof SeerOutcomeSchema>>;

export namespace SeerOutcome {
  export const SCHEMA = SeerOutcomeSchema;

  export const negate = (outcome: SeerOutcome): SeerOutcome => {
    switch (outcome.identity) {
      case 'VILLAGER': {
        return {
          id: outcome.id,
          identity: 'WEREWOLF'
        } satisfies SeerOutcome;
      }
      case 'WEREWOLF': {
        return {
          id: outcome.id,
          identity: 'VILLAGER'
        } satisfies SeerOutcome;
      }
      default: {
        throw new ExhaustiveError(outcome.identity);
      }
    }
  };
}
