import { z } from 'zod';
import { Person } from '../Person/Person.js';

const MediumOutcomeTypeSchema = z.union([z.literal('VILLAGER'), z.literal('WEREWOLF')]);
const MediumOutcomeSchema = z.object({
  id: Person.ID.SCHEMA,
  identity: MediumOutcomeTypeSchema
});

export type MediumOutcomeType = z.infer<typeof MediumOutcomeTypeSchema>;
export type MediumOutcome = Readonly<z.infer<typeof MediumOutcomeSchema>>;

export namespace MediumOutcome {
  export const SCHEMA = MediumOutcomeSchema;
}
