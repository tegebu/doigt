import type { Tagged } from 'type-fest';
import { z } from 'zod';
import type { PersonID } from '../Person/Person.js';
import { VoteOutcome } from './VoteOutcome.js';

export type VoteOutcomes = Tagged<ReadonlyArray<VoteOutcome>, 'Votes'>;

export const VoteOutcomesSchema = z.custom<VoteOutcomes>((value) => z.array(VoteOutcome.SCHEMA).safeParse(value));

export namespace VoteOutcomes {
  export const SCHEMA = VoteOutcomesSchema;

  /**
   * 投票を追加します。すでにsourceが同じ投票がある場合は上書きします
   *
   * @param votes
   * @param source
   * @param destination
   */
  export const add = (votes: VoteOutcomes, source: PersonID, destination: PersonID): VoteOutcomes => {
    const [updated, hasSameSource] = replaceInner(votes, source, destination);

    if (hasSameSource) {
      return VoteOutcomes.from(updated);
    }

    const vote = {
      source,
      destination
    } satisfies VoteOutcome;

    return VoteOutcomes.from([...votes, vote]);
  };

  export const countVotes = (votes: VoteOutcomes): ReadonlyMap<PersonID, number> => {
    const counts = new Map<PersonID, number>();

    for (const vote of [...votes.values()]) {
      const current = counts.get(vote.destination) ?? 0;

      counts.set(vote.destination, current + 1);
    }

    return counts;
  };

  export const empty = (): VoteOutcomes => {
    return VoteOutcomes.from([]);
  };

  export const from = (votes: ReadonlyArray<VoteOutcome>): VoteOutcomes => {
    return votes as VoteOutcomes;
  };

  /**
   * 最多得票者を取得します
   * 場合により複数の住人や、存在しない場合もあります
   *
   * @param votes
   */
  export const getMostVoted = (votes: VoteOutcomes): Array<PersonID> => {
    const counts = VoteOutcomes.countVotes(votes);

    if (counts.size === 0) {
      return [];
    }

    const maxVotes = Math.max(...counts.values());

    return [...counts]
      .filter(([_, votes]) => {
        return votes === maxVotes;
      })
      .map(([id]) => id);
  };

  export const remove = (votes: VoteOutcomes, source: PersonID): VoteOutcomes => {
    const filtered = votes.filter((v: VoteOutcome) => {
      return v.source !== source;
    });

    return VoteOutcomes.from(filtered);
  };

  // sourceをもとにdestinationを差し替える
  export const replace = (votes: VoteOutcomes, source: PersonID, destination: PersonID): VoteOutcomes => {
    const [updated] = replaceInner(votes, source, destination);

    return VoteOutcomes.from(updated);
  };

  const replaceInner = (votes: VoteOutcomes, source: PersonID, destination: PersonID): [Array<VoteOutcome>, hasSameSource: boolean] => {
    let hasSameSource = false;
    const updated = votes.map((v: VoteOutcome) => {
      if (v.source === source) {
        hasSameSource = true;

        return {
          source,
          destination
        } satisfies VoteOutcome;
      }

      return v;
    });

    return [updated, hasSameSource];
  };
}
