import { z } from 'zod';
import { Person } from '../Person/Person.js';

const VoteOutcomeSchema = z.object({
  source: Person.ID.SCHEMA,
  destination: Person.ID.SCHEMA
});

export type VoteOutcome = Readonly<z.infer<typeof VoteOutcomeSchema>>;

export namespace VoteOutcome {
  export const SCHEMA = VoteOutcomeSchema;
}
