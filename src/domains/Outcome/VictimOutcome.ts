import { z } from 'zod';
import { Person } from '../Person/Person.js';

const VictimOutcomeSchema = z.object({
  id: z.array(Person.ID.SCHEMA)
});

export type VictimOutcome = Readonly<z.infer<typeof VictimOutcomeSchema>>;

export namespace VictimOutcome {
  export const SCHEMA = VictimOutcomeSchema;
}
